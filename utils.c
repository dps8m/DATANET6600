#include "dn355.h"
#include "utils.h"

#define SIGN18 0400000
#define SIGN36 0400000000000

#define BIT18  01000000
#define BIT19  02000000
#define BIT37  01000000000000
#define BIT38  02000000000000

/*
 * 36-bit arithmetic stuff ...
 */
/* Single word integer routines */

word36 Add36b (word36 op1, word36 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf)
  {

// https://en.wikipedia.org/wiki/Two%27s_complement#Addition
//
// In general, any two N-bit numbers may be added without overflow, by first
// sign-extending both of them to N + 1 bits, and then adding as above. The
// N + 1 bits result is large enough to represent any possible sum (N = 5 two's
// complement can represent values in the range −16 to 15) so overflow will
// never occur. It is then possible, if desired, to 'truncate' the result back
// to N bits while preserving the value if and only if the discarded bit is a
// proper sign extension of the retained result bits. This provides another
// method of detecting overflow—which is equivalent to the method of comparing
// the carry bits—but which may be easier to implement in some situations,
// because it does not require access to the internals of the addition.

    // 37 bit arithmetic for the above N+1 algorithm
    word38 op1e = op1 & BITS36;
    word38 op2e = op2 & BITS36;
    word38 ci = carryin ? 1 : 0;

    // extend sign bits
    if (op1e & SIGN36)
      op1e |= BIT37;
    if (op2e & SIGN36)
      op2e |= BIT37;

    // Do the math
    word38 res = op1e + op2e + ci;

    // Extract the overflow bits
    bool r37 = res & BIT37 ? true : false;
    bool r36 = res & SIGN36 ? true : false;

    // Extract the carry bit
    bool r38 = res & BIT38 ? true : false;

    // Check for overflow 
    * ovf = r37 ^ r36;

    // Check for carry 
    bool cry = r38;

    // Truncate the result
    res &= BITS36;
    if (flagsToSet & I_CARRY)
      {
        if (cry)
          SETF (* flags, I_CARRY);
        else
          CLRF (* flags, I_CARRY);
      }

    if (flagsToSet & I_OVF)
      {
        if (* ovf)
          SETF (* flags, I_OVF);      // overflow
      }

    if (flagsToSet & I_ZERO)
      {
        if (res)
          CLRF (* flags, I_ZERO);
        else
          SETF (* flags, I_ZERO);       // zero result
      }

    if (flagsToSet & I_NEG)
      {
        if (res & SIGN36)
          SETF (* flags, I_NEG);
        else
          CLRF (* flags, I_NEG);
      }

    return res;
  }

word36 Sub36b (word36 op1, word36 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf)
  {

// https://en.wikipedia.org/wiki/Two%27s_complement
//
// As for addition, overflow in subtraction may be avoided (or detected after
// the operation) by first sign-extending both inputs by an extra bit.
//
// AL39:
//
//  If carry indicator ON, then C(A) - C(Y) -> C(A)
//  If carry indicator OFF, then C(A) - C(Y) - 1 -> C(A)

    // 38 bit arithmetic for the above N+1 algorithm
    word38 op1e = op1 & BITS36;
    word38 op2e = op2 & BITS36;
    // Note that carryin has an inverted sense for borrow
    word38 ci = carryin ? 0 : 1;

    // extend sign bits
    if (op1e & SIGN36)
      op1e |= BIT37;
    if (op2e & SIGN36)
      op2e |= BIT37;

    // Do the math
    word38 res = op1e - op2e - ci;

    // Extract the overflow bits
    bool r37 = (res & BIT37) ? true : false;
    bool r36 = (res & SIGN36) ? true : false;

    // Extract the carry bit
    bool r38 = res & BIT38 ? true : false;

    // Truncate the result
    res &= BITS36;

    // Check for overflow 
    * ovf = r37 ^ r36;

    // Check for carry 
    bool cry = r38;

    if (flagsToSet & I_CARRY)
      {
        if (cry) // Note inverted logic for subtraction
          CLRF (* flags, I_CARRY);
        else
          SETF (* flags, I_CARRY);
      }

    if (flagsToSet & I_OVF)
      {
        if (* ovf)
          SETF (* flags, I_OVF);      // overflow
      }

    if (flagsToSet & I_ZERO)
      {
        if (res)
          CLRF (* flags, I_ZERO);
        else
          SETF (* flags, I_ZERO);       // zero result
      }

    if (flagsToSet & I_NEG)
      {
        if (res & SIGN36)
          SETF (* flags, I_NEG);
        else
          CLRF (* flags, I_NEG);
      }

    return res;
  }

word18 Add18b (word18 op1, word18 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf)
  {
// https://en.wikipedia.org/wiki/Two%27s_complement#Addition
//
// In general, any two N-bit numbers may be added without overflow, by first
// sign-extending both of them to N + 1 bits, and then adding as above. The
// N + 1 bits result is large enough to represent any possible sum (N = 5 two's
// complement can represent values in the range −16 to 15) so overflow will
// never occur. It is then possible, if desired, to 'truncate' the result back
// to N bits while preserving the value if and only if the discarded bit is a
// proper sign extension of the retained result bits. This provides another
// method of detecting overflow—which is equivalent to the method of comparing
// the carry bits—but which may be easier to implement in some situations,
// because it does not require access to the internals of the addition.

    // 19 bit arithmetic for the above N+1 algorithm
    word20 op1e = op1 & BITS18;
    word20 op2e = op2 & BITS18;
    word20 ci = carryin ? 1 : 0;

    // extend sign bits
    if (op1e & SIGN18)
      op1e |= BIT18;
    if (op2e & SIGN18)
      op2e |= BIT18;

    // Do the math
    word20 res = op1e + op2e + ci;

    // Extract the overflow bits
    bool r18 = (res & BIT18) ? true : false;
    bool r17 = (res & SIGN18) ? true : false;

    // Extract the carry bit
    bool r19 = res & BIT19 ? true : false;
    // Truncate the result
    res &= BITS18;

    // Check for overflow 
    * ovf = r18 ^ r17;

    // Check for carry 
    bool cry = r19;

    if (flagsToSet & I_CARRY)
      {
        if (cry)
          SETF (* flags, I_CARRY);
        else
          CLRF (* flags, I_CARRY);
      }

    if (flagsToSet & I_OVF)
      {
        if (* ovf)
          SETF (* flags, I_OVF);      // overflow
      }

    if (flagsToSet & I_ZERO)
      {
        if (res)
          CLRF (* flags, I_ZERO);
        else
          SETF (* flags, I_ZERO);       // zero result
      }

    if (flagsToSet & I_NEG)
      {
        if (res & SIGN18)
          SETF (* flags, I_NEG);
        else
          CLRF (* flags, I_NEG);
      }

    return res;
  }

word18 Sub18b (word18 op1, word18 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf)
  {

// https://en.wikipedia.org/wiki/Two%27s_complement
//
// As for addition, overflow in subtraction may be avoided (or detected after
// the operation) by first sign-extending both inputs by an extra bit.
//
// AL39:
//
//  If carry indicator ON, then C(A) - C(Y) -> C(A)
//  If carry indicator OFF, then C(A) - C(Y) - 1 -> C(A)

    // 19 bit arithmetic for the above N+1 algorithm
    word20 op1e = op1 & BITS18;
    word20 op2e = op2 & BITS18;
    // Note that carryin has an inverted sense for borrow
    word20 ci = carryin ? 0 : 1;

    // extend sign bits
    if (op1e & SIGN18)
      op1e |= BIT18;
    if (op2e & SIGN18)
      op2e |= BIT18;

    // Do the math
    word20 res = op1e - op2e - ci;

    // Extract the overflow bits
    bool r18 = res & BIT18 ? true : false;
    bool r17 = res & SIGN18 ? true : false;

    // Extract the carry bit
    bool r19 = res & BIT19 ? true : false;

    // Truncate the result
    res &= BITS18;

    // Check for overflow 
    * ovf = r18 ^ r17;

    // Check for carry 
    bool cry = r19;

    if (flagsToSet & I_CARRY)
      {
        if (cry) // Note inverted logic for subtraction
          CLRF (* flags, I_CARRY);
        else
          SETF (* flags, I_CARRY);
      }

    if (flagsToSet & I_OVF)
      {
        if (* ovf)
          SETF (* flags, I_OVF);      // overflow
      }

    if (flagsToSet & I_ZERO)
      {
        if (res)
          CLRF (* flags, I_ZERO);
        else
          SETF (* flags, I_ZERO);       // zero result
      }

    if (flagsToSet & I_NEG)
      {
        if (res & SIGN18)
          SETF (* flags, I_NEG);
        else
          CLRF (* flags, I_NEG);
      }

    return res;
  }

void cmp18(word18 oP1, word18 oP2, word18 *flags)
  {
    int32_t op1 = SIGNEXT18 (oP1 & BITS18);
    int32_t op2 = SIGNEXT18 (oP2 & BITS18);
    word18 sign1 = op1 & SIGN18;
    word18 sign2 = op2 & SIGN18;

    if ((! sign1) && sign2)  // op1 > 0, op2 < 0 :: op1 > op2
      CLRF (* flags, I_ZERO | I_NEG | I_CARRY);

    else if (sign1 == sign2) // both operands have the same sogn
      {
         if (op1 > op2)
           {
             SETF (* flags, I_CARRY);
             CLRF (* flags, I_ZERO | I_NEG);
           }
         else if (op1 == op2)
           {
             SETF (* flags, I_ZERO | I_CARRY);
             CLRF (* flags, I_NEG);
           }
         else //  op1 < op2
          {
            SETF (* flags, I_NEG);
            CLRF (* flags, I_ZERO | I_CARRY);
          }
      }
    else // op1 < 0, op2 > 0 :: op1 < op2
      {
        SETF (* flags, I_CARRY | I_NEG);
        CLRF (* flags, I_ZERO);
      }
  }

int cfg_parse (const char * tag, const char * cptr, config_list_t * clist, config_state_t * state, int64_t * result)
  {
    if (! cptr)
      return -2;
    char * start = NULL;
    if (! state -> copy)
      {
        state -> copy = strdup (cptr);
        start = state -> copy;
        state ->  statement_save = NULL;
      }

    int ret = -2; // error

    // grab every thing up to the next semicolon
    char * statement;
    statement = strtok_r (start, ";", & state -> statement_save);
    start = NULL;
    if (! statement)
      {
        ret = -1; // done
        goto done;
      }

    // extract name
    char * name_start = statement;
    char * name_save = NULL;
    char * name;
    name = strtok_r (name_start, "=", & name_save);
    if (! name)
      {
        sim_printf ("error: %s: can't parse name\n", tag);
        goto done;
      }

    // lookup name
    config_list_t * p = clist;
    while (p -> name)
      {
        if (strcasecmp (name, p -> name) == 0)
          break;
        p ++;
      }
    if (! p -> name)
      {
        sim_printf ("error: %s: don't know name <%s>\n", tag, name);
        goto done;
      }

    // extract value
    char * value;
    value = strtok_r (NULL, "", & name_save);
    if (! value)
      {
        // Special case; min>max and no value list
        // means that a missing value is ok
        if (p -> min > p -> max && ! p -> value_list)
          {
            return (int) (p - clist);
          }
        sim_printf ("error: %s: can't parse value\n", tag);
        goto done;
      }

    // first look to value in the value list
    config_value_list_t * v = p -> value_list;
    if (v)
      {
        while (v -> value_name)
          {
            if (strcasecmp (value, v -> value_name) == 0)
              break;
            v ++;
          }

        // Hit?
        if (v -> value_name)
          {
            * result = v -> value;
            return (int) (p - clist);
          }
      }

    // Must be a number

    if (p -> min > p -> max)
      {
        sim_printf ("error: %s: can't parse value\n", tag);
        goto done;
      }

    if (strlen (value) == 0)
      {
         sim_printf ("error: %s: missing value\n", tag);
         goto done;
      }
    char * endptr;
    int64_t n = strtoll (value, & endptr, 0);
    if (* endptr)
      {
        sim_printf ("error: %s: can't parse value\n", tag);
        goto done;
      }

// XXX small bug; doesn't check for junk after number...
    if (n < p -> min || n > p -> max)
      {
        sim_printf ("error: %s: value out of range\n", tag);
        goto done;
      }

    * result = n;
    return (int) (p - clist);

done:
    free (state -> copy);
    state -> copy= NULL;
    return ret;
  }

void cfg_parse_done (config_state_t * state)
  {
    if (state -> copy)
      free (state -> copy);
    state -> copy = NULL;
  }

/*  
 * Removes the trailing spaces from a string.
 */
char * rtrim( char * s) {
  if (! s)
    return s;
  for (int index = (int)strlen(s) - 1; index >= 0 && isspace((unsigned char)s[index]); index--)
    s[index] = '\0';
  return s;
}

/*
 * Removes the leading spaces from a string.
 */
char * ltrim (char * s) {
  char *p;
  if (s == NULL)
    return NULL;

  for (p = s; isspace ((unsigned char) * p) && * p != '\0'; p ++)
    ;

  memmove (s, p, strlen (p) + 1);
  return s;
}

char * trim (char * s) {
  return ltrim (rtrim (s));
}


