#include <string.h>
#include <uv.h>


#include "dn355.h"
#include "iom.h"
#include "caf.h"
#include "utils.h"
#include "core.h"
#include "hdbg.h"
#include "cpu.h"
#include "dnpkt.h"
#include "hsla.h"
#include "dialin.h"

#include "libtelnet.h"

#define DEFAULT_BACKLOG 1024
#define USE_REQ_DATA

// telnet listener for hsla lines

void hslauv_associated_brk (uv_tcp_t * client);
void close_connection (uv_stream_t* stream);

static void alloc_buffer (UNUSED uv_handle_t * handle, size_t suggested_size, uv_buf_t * buf) {
  * buf = uv_buf_init ((char *) malloc (suggested_size), (uint) suggested_size);
}


static const telnet_telopt_t my_telopts[] = {
    { TELNET_TELOPT_SGA,       TELNET_WILL, TELNET_DO   },
    { TELNET_TELOPT_ECHO,      TELNET_WILL, TELNET_DONT },
  //{ TELNET_TELOPT_TTYPE,     TELNET_WONT, TELNET_DONT },
    { TELNET_TELOPT_BINARY,    TELNET_WILL, TELNET_DO   },
  //{ TELNET_TELOPT_NAWS,      TELNET_WONT, TELNET_DONT },
    { -1, 0, 0 }
  };

static void evHandler (UNUSED telnet_t *telnet, telnet_event_t *event, void *user_data) {
  uv_tcp_t * client = (uv_tcp_t *) user_data;
  switch (event->type) {
    case TELNET_EV_DATA: {
      if (! client || ! client->data) {
        sim_printf ("evHandler TELNET_EV_DATA bad client data\n");
        return;
      }
      uvClientData * p = (uvClientData *) client->data;
      (* p->read_cb) (client, (ssize_t) event->data.size, (unsigned char *)event->data.buffer);
      break;
    }

    case TELNET_EV_SEND: {
      //sim_printf ("evHandler: send %zu <%s>\n", event->data.size, event->data.buffer);
      //hslauv_start_write_actual (client, (char *) event->data.buffer, (ssize_t) event->data.size);
      if (! client || ! client->data) {
        sim_printf ("evHandler TELNET_EV_SEND bad client data\n");
        return;
      }
      uvClientData * p = client->data;
      (* p->write_actual_cb) (client, (unsigned char *) event->data.buffer, (ssize_t) event->data.size);
      break;
    }

   case TELNET_EV_DO: {
      if (event->neg.telopt == TELNET_TELOPT_BINARY) {
        // DO Binary
      } else if (event->neg.telopt == TELNET_TELOPT_SGA) {
        // DO Suppress Go Ahead
      } else if (event->neg.telopt == TELNET_TELOPT_ECHO) {
        // DO Suppress Echo
      } else if (event->neg.telopt == TELNET_TELOPT_EOR) {
        //sim_printf ("EOR rcvd\n");
        //hslauv_recv_eor (client);
        // DO EOR
      } else {
        sim_printf ("evHandler DO %d\n", event->neg.telopt);
      }
      break;
    }

    case TELNET_EV_DONT: {
      sim_printf ("evHandler DONT %d\n", event->neg.telopt);
      break;
    }

    case TELNET_EV_WILL: {
      if (event->neg.telopt == TELNET_TELOPT_BINARY) {
        // WILL BINARY
      } else if (event->neg.telopt == TELNET_TELOPT_TTYPE) {
        // WILL TTYPE
      } else if (event->neg.telopt == TELNET_TELOPT_EOR) {
        // WILL EOR
      } else {
        if (event->neg.telopt != 3)
          sim_printf ("evHandler WILL %d\n", event->neg.telopt);
      }
      break;
    }

    case TELNET_EV_WONT: {
      sim_printf ("evHandler WONT %d\n", event->neg.telopt);
      break;
    }

    case TELNET_EV_ERROR: {
      sim_printf ("libtelnet evHandler error <%s>\n", event->error.msg);
      break;
    }

    case TELNET_EV_IAC: {
      if (event->iac.cmd == TELNET_BREAK || event->iac.cmd == TELNET_IP) {
        if (! client || ! client->data) {
          sim_printf ("evHandler TELNET_EV_IAC bad client data\n");
          return;
        }
        uvClientData * p = (uvClientData *) client->data;
        if (p -> assoc) {
          hslauv_associated_brk (client);
        } else
        sim_printf ("libtelnet dropping unassociated BRK\n");
      //} else if (event->iac.cmd == TELNET_EOR) {
        //hslauv_recv_eor (client);
      } else
        if ((!sim_quiet) || (event->iac.cmd != 241))
          sim_printf ("libtelnet unhandled IAC event %d\n", event->iac.cmd);
      break;
    }

    case TELNET_EV_TTYPE: {
      if (! client || ! client->data) {
        sim_printf ("evHandler TELNET_EV_IAC bad client data\n");
        return;
      }
      //uvClientData * p = (uvClientData *) client->data;
      break;
    }

    case TELNET_EV_SUBNEGOTIATION: {
        /* no subnegotiation */
      break;
    }

    default:
      sim_printf ("evHandler: unhandled event %d\n", event->type);
      break;
  }
}


void * ltnConnect (uv_tcp_t * client) {
  void * p = (void *) telnet_init (my_telopts, evHandler, 0, client);
  const telnet_telopt_t * q = my_telopts;
  while (q->telopt != -1) {
    telnet_negotiate (p, q->us, (unsigned char) q->telopt);
    q ++;
  }
  return p;
}


//
// fuv_read_cb: libuv read complete callback
//
//   Cleanup on read error.
//   Forward data to appropriate handler.

static void fuv_read_cb (uv_stream_t* stream, ssize_t nread, const uv_buf_t* buf) {
  if (nread < 0) {
    close_connection (stream);
  } else if (nread > 0) {
    if (! stream) {
      sim_printf ("fuv_read_cb bad client data\n");
      return;
    }
    uvClientData * p = (uvClientData *) stream->data;
    if (p) {
      if (p->telnetp) {
        telnet_recv (p->telnetp, buf->base, (size_t) nread);
      } else {
        (* p->read_cb) ((uv_tcp_t *) stream, nread, (unsigned char *) buf->base);

      }
    }
  }

  if (buf->base)
    free (buf->base); /* X-LINTED: FREE */
}

static void fuv_close_cb (uv_handle_t * stream) {
  free (stream);
}


void close_connection (uv_stream_t* stream) {
  if (! stream) {
      sim_printf ("close_connection bad client data\n");
      return;
    }
  uvClientData * p = (uvClientData *) stream->data;

  // If stream->data, the stream is associated with a Multics line.
  // Tear down that association
  if (p) {
#if 0
    struct t_line * linep = & hslaData.hsla[p->hslano].MState.line[p->lineno];
    sim_printf ("[FNP emulation: DISCONNECT %c.d%03d]\n", p->hslano+'a', p->lineno);
    linep -> line_disconnected = true;
    linep -> listen = false;
    if (linep->inBuffer)
      free (linep->inBuffer);
    linep->inBuffer = NULL;
    linep->inSize   = 0;
    linep->inUsed   = 0;
    linep->nPos     = 0;
    if (linep->line_client) {
      linep->line_client = NULL;
    }
#endif
    struct hslaSubChnl_s * subChnlp = & hslaData.hsla[p->hslano].subChnl[p->lineno];
    if (subChnlp->line_client) {
      subChnlp->line_client = NULL;
    }
    // Clean up allocated data
    if (p->telnetp) {
      telnet_free (p->telnetp);
      p->telnetp = NULL;
    }
    free (stream->data);
    stream->data = NULL;
  } // if (p)
  if (! uv_is_closing ((uv_handle_t *) stream))
    uv_close ((uv_handle_t *) stream, fuv_close_cb);
}
 

void hslauv_associated_brk (uv_tcp_t * client) {
  if (! client || uv_is_closing ((uv_handle_t *) client))
    return;
  if (! client->data) {
    sim_printf ("hslauv_associated_brk bad client data\n");
    return;
  }
  uvClientData * p = (uvClientData *) client->data;
  //uint hslano = p -> hslano;
  uint lineno = p -> lineno;
  //struct t_line * linep = & hslaData.hslaUnitData[hslano].MState.line[lineno];
  // The break key forces an input buffer flush.
  // XXX is this a race condition? Is it possible for processFnpMbx to see
  // the line_break before the accept input?
  sim_printf ("brk on %u\n", lineno);
}


//
// fuv_write_cb: libuv write complete callback
//
//   Cleanup on error
//   Free buffers
//

static void fuv_write_cb (uv_write_t * req, int status) {
  if (status < 0) {
    if (status == -ECONNRESET || status == -ECANCELED ||
        status == -EPIPE) {
      // This occurs when the other end disconnects; not an "error"
    } else {
      sim_printf ("fuv_write_cb status %d (%s)\n", -status, strerror (-status));
    }

    // connection reset by peer
    close_connection (req->handle);
  }

#if defined(USE_REQ_DATA)
  free (req->data);
#else
  unsigned int nbufs = req->nbufs;
  uv_buf_t * bufs = req->bufs;
  for (unsigned int i = 0; i < nbufs; i ++) {
    if (bufs && bufs[i].base) {
      free (bufs[i].base);
    }
    if (req->bufsml[i].base) {
      free (req->bufsml[i].base);
    }
  }
#endif /* if defined(USE_REQ_DATA) */

  // the buf structure is copied; do not free.
  free (req);
}

//
// Enable reading on connection
//

void hslauv_read_start (uv_tcp_t * client) {
  if (! client || uv_is_closing ((uv_handle_t *) client))
    return;
  uv_read_start ((uv_stream_t *) client, alloc_buffer, fuv_read_cb);
}

//
// Disable reading on connection
//

void hslauv_read_stop (uv_tcp_t * client) {
  if (! client || uv_is_closing ((uv_handle_t *) client))
    return;
  uv_read_stop ((uv_stream_t *) client);
}


void hslauv_start_write_actual (uv_tcp_t * client, unsigned char * data, ssize_t datalen) {
  if (! client || uv_is_closing ((uv_handle_t *) client))
    return;
  uv_write_t * req = (uv_write_t *) malloc (sizeof (uv_write_t));
  // This makes sure that bufs*.base and bufsml*.base are NULL
  (void) memset (req, 0, sizeof (uv_write_t));
  uv_buf_t buf = uv_buf_init ((char *) malloc ((unsigned long) datalen), (uint) datalen);
#if defined(USE_REQ_DATA)
  req->data = buf.base;
#endif /* if defined(USE_REQ_DATA) */
  memcpy (buf.base, data, (unsigned long) datalen);
  int ret = uv_write (req, (uv_stream_t *) client, & buf, 1, fuv_write_cb);
// There seems to be a race condition when Multics signals a disconnect_line;
// We close the socket, but Multics is still writing its goodbye text trailing
// NULs.
// If the socket has been closed, write will return BADF; just ignore it.
  if (ret < 0 && ret != -EBADF)
    sim_printf ("\n[FNP emulation: uv_write returned %d]\n", ret);
}


//
// Write data to a connection, doing Telnet if needed.
//

void hslauv_start_write (uv_tcp_t * client, unsigned char * data, ssize_t datalen)
  { 
    if (! client || uv_is_closing ((uv_handle_t *) client))
      return;
    uvClientData * p = (uvClientData *) client->data;
    if (! p)
      return;
    if (!p->telnetp)
      {
        sim_printf ("telnetp NULL; dropping hslauv_start_write()\n");
        return;
      }
    telnet_send (p->telnetp, (char *) data, (size_t) datalen);
  }


// C-string wrapper for hslauv_start_write
    
void hslauv_start_writestr (uv_tcp_t * client, unsigned char * data) {
  if (! client || uv_is_closing ((uv_handle_t *) client))
    return;
  if (! client->data) {
    sim_printf ("hslauv_start_writestr bad client data\n");
    return;
  }
  uvClientData * p = client->data;
  (* p->write_cb) (client, data, (ssize_t) strlen ((char *) data));
}       


void processLineInput (uv_tcp_t * client, unsigned char * buf, ssize_t nread) {
  if (! client || ! client->data) {
    sim_printf ("processLineInput bad client data\n");
    return;
  }
  uvClientData * p = (uvClientData *) client->data;
  uint hslano       = p -> hslano;
  uint lineno      = p -> lineno;
  if (hslano >= N_HSLAS || lineno >= N_HSLA_LINES) {
    sim_printf ("bogus client data\n");
    return;
  }
  // XXX 
  sim_printf ("rcvd: ");
  for (uint i = 0; i < nread; i ++) {
    sim_printf (" %03o", buf[i]);
  }
  sim_printf ("\n");
}


void hslauv_associated_readcb (uv_tcp_t * client, ssize_t nread, unsigned char * buf) { 
  processLineInput (client, buf, nread);  
} 


#define PROMPT  "HSLA Port ("
    
void hslaConnectPrompt (uv_tcp_t * client) { 
  hslauv_start_writestr (client, (unsigned char *) PROMPT);
  bool first = true; 
  //uint hsla_unit_idx = 0;
  for (uint lineno = 0; lineno < N_HSLA_LINES; lineno ++) {
    //struct line_s * linep = & hslaData.hslaUnitData[hsla_unit_idx].line[lineno];
    if (! first)
      hslauv_start_writestr (client, (unsigned char *) ",");
    char name [16];
    first = false;
    (void) sprintf (name, "%c.h%03u", /*prefix(hsla_unit_idx)*/ 'h', lineno);
    hslauv_start_writestr (client, (unsigned char *) name);
  }
  hslauv_start_writestr (client, (unsigned char *) ")? ");
}


void processUserInput (uv_tcp_t * client, unsigned char * buf, ssize_t nread) { 
  if (! client || ! client->data) {                    
    sim_printf ("processUserInput bad client data\n");
    return;
  }
  uvClientData * p = (uvClientData *) client->data;
  for (ssize_t nchar = 0; nchar < nread; nchar ++) {
    unsigned char kar = buf [nchar];

    if (kar == 0x1b || kar == 0x03) {             // ESCape ('\e') | ^C
      close_connection ((uv_stream_t *) client);
      return;
    }

    // buffer too full for anything more?
    if (p->nPos >= sizeof(p->buffer) - 1) {
      // yes. Only allow \n, \r, ^H, ^R
      switch (kar) {
        case '\b':  // backspace
        case 127: { // delete
          if (p->nPos) {
            hslauv_start_writestr (client, (unsigned char *) "\b \b");
                                         // removes char from line
            p->buffer[p->nPos]  = 0;     // remove char from buffer
            p->nPos            -= 1;     // back up buffer pointer
          }
          break;
        }

        case '\n':
        case '\r': {
          p->buffer[p->nPos] = 0;
          goto check;
        }

        case 0x12: {  // ^R
          hslauv_start_writestr (client, (unsigned char *) "^R\r\n");       // echo ^R
          hslaConnectPrompt (client);
          hslauv_start_writestr (client, (unsigned char *) p->buffer);
          break;
        }

        default:
          break;
      } // switch kar
      continue; // process next character in buffer
    } // if buffer full

    if (isprint (kar)) {  // printable?
      unsigned char str [2] = { kar, 0 };
      hslauv_start_writestr (client,  str);
      p->buffer[p->nPos++]  = (char) kar;
    } else {
      switch (kar) {
        case '\b':  // backspace
        case 127: { // delete
          if (p->nPos) {
            hslauv_start_writestr (client, (unsigned char *) "\b \b");
                                         // removed char from line
            p->buffer[p->nPos]  = 0;     // remove char from buffer
            p->nPos            -= 1;     // back up buffer pointer
          }
          break;
        }

        case '\n':
        case '\r': {
          p->buffer[p->nPos] = 0;
          goto check;
        }

        case 0x12: {  // ^R
          hslauv_start_writestr (client, (unsigned char *) "^R\r\n");       // echo ^R
          hslaConnectPrompt (client);
          hslauv_start_writestr (client, (unsigned char *) p->buffer);
          break;
        }

        default:
          break;
      } // switch kar
    } // not printable
  } // for nchar
  return;

check:;
  char cpy [p->nPos + 1];
  memcpy (cpy, p->buffer, p->nPos);
  cpy [p->nPos] = 0;
  trim (cpy);
  sim_printf ("<%s>", cpy);
  p->nPos = 0;
  hslauv_start_writestr (client, (unsigned char *) "\r\n");

  uint hsla_unit_idx = 0;
  uint lineno       = 0;

  if (strlen (cpy)) {
// XXX This is incomplete; only handles a single hsla.
    int cnt = sscanf (cpy, "%u", & lineno);
    if (cnt != 1 || lineno >= N_HSLA_LINES) {
      hslauv_start_writestr (client, (unsigned char *) "can't parse\r\n");
sim_printf (">>>>>>>>>>>>>>>>> can't parse '%s' cnt %d lineno %u\n", cpy, cnt, lineno);
      goto reprompt;
    }

    if (hslaData.hsla[hsla_unit_idx].subChnl[lineno].line_client) {
      hslauv_start_writestr (client, (unsigned char *) "not available\r\n");
      goto reprompt;
    }
    goto associate;
  } else {
    for (lineno = 0; lineno < N_HSLA_LINES; lineno ++) {
      if (! hslaData.hsla[hsla_unit_idx].subChnl[lineno].line_client) {
        goto associate;
      }
    }
    hslauv_start_writestr (client, (unsigned char *) "not available\r\n");
    goto reprompt;
  }

reprompt:;
  hslaConnectPrompt (client);
  return;

associate:;

  hslaData.hsla[hsla_unit_idx].subChnl[lineno].line_client = client;
  p->assoc           = true;
  p->hslano           = hsla_unit_idx;
  p->lineno          = lineno;
  p->read_cb         = hslauv_associated_readcb;
  p->write_cb        = hslauv_start_write;
  p->write_actual_cb = hslauv_start_write_actual;

  char buf2 [1024];

  struct sockaddr name;
  int namelen = sizeof (name);
  int ret     = uv_tcp_getpeername (client, & name, & namelen);
  if (ret < 0) {
    sim_printf ("CONNECT (addr err %d) to %03d\n", ret, lineno);
  } else {
    struct sockaddr_in * p = (struct sockaddr_in *) & name;
    sim_printf ("CONNECT %s to %03d\n", inet_ntoa (p -> sin_addr), lineno);
  }

  (void) sprintf (buf2, "Attached to line %03u\r\n", lineno);
  hslauv_start_writestr (client, (unsigned char *) buf2);

  offhookEvent (hsla_unit_idx * N_HSLA_LINES + lineno);
}

//`
// read callback for connections that are associated with an HSLA line;
// forward the data to the FNP input handler
//

void hslauv_unassociated_readcb (uv_tcp_t * client, ssize_t nread, unsigned char * buf) {
  processUserInput (client, buf, nread);
}


//  
// Connection callback handler for dialup connections
//  
    
static void on_new_connection (uv_stream_t * server, int status) {
  if (status < 0) {
    sim_printf ("HSLA emulation: new connection error: %s\n", uv_strerror (status));
    return;
  }
    
  uv_tcp_t * client = (uv_tcp_t *) malloc (sizeof (uv_tcp_t));
  uv_tcp_init (uv_default_loop (), client);
  if (uv_accept (server, (uv_stream_t *) client) != 0) {
    uv_close ((uv_handle_t *) client, fuv_close_cb);
    return;
  }

  struct sockaddr name;
  int namelen = sizeof (name);
  uv_tcp_nodelay (client,1);
  int ret = uv_tcp_getpeername (client, & name, & namelen);
  if (ret < 0) {
      sim_printf ("[FNP simulation: CONNECT; addr err %d]\n", ret);
  } else {
    struct sockaddr_in * p = (struct sockaddr_in *) & name;
    sim_printf ("HSLA simulation: CONNECT %s]\n", inet_ntoa (p -> sin_addr));
  }

  uvClientData * p = (uvClientData *) malloc (sizeof (uvClientData));
  client->data       = p;
  p->assoc = false;
  p->write_actual_cb = hslauv_start_write_actual;
  p->read_cb  = hslauv_unassociated_readcb;
  p->write_cb = hslauv_start_write;
  p->telnetp  = ltnConnect (client);
  if (! p->telnetp) {
    sim_printf ("ltnConnect failed\n");
    return;
  }

  hslauv_read_start (client);
  hslaConnectPrompt (client);
}


//
// Setup the dialup listener
//  
    
int hslaUvStart (void) {
  if (hslaData.started) {
    sim_printf ("HSLA telnet address error; listener already started.\n");
    return 1;
  }

  // Initialize the server socket
  uv_tcp_init (uv_default_loop (), & hslaData.du_server);

  // Bind and listen
  struct sockaddr_in addr;
  uv_ip4_addr (hslaData.telnetAddress, hslaData.telnetPort, & addr);
  uv_tcp_bind (& hslaData.du_server, (const struct sockaddr *) & addr, 0);
  int r = uv_listen ((uv_stream_t *) & hslaData.du_server, DEFAULT_BACKLOG, on_new_connection);
  if (r) {
    sim_printf ("listen error: %s:%ld: %s]\n", hslaData.telnetAddress, (long) hslaData.telnetPort, uv_strerror (r));
    return 1;
  }
  hslaData.started = true;
  sim_printf ("TELNET server listening on %s:%ld]\n", hslaData.telnetAddress, (long) hslaData.telnetPort);
  return 0;
}


