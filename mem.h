word18 fromMemoryPtrInc (word18 * ptr, const char * ctx);
word18 fromMemory (word18 addr, const char * ctx);
word36 fromMemory36 (word15 addr, int charaddr, const char * ctx);
void toMemory1 (word18 data, word18 addr, word18 zoneMask, const char * ctx);
void toMemory (word18 data, word15 addr, word3 charaddr, word18 zoneMask, const char * ctx);
void toMemory36 (word36 data36, word15 addr, word3 charaddr, const char * ctx);

