typedef word18 (* handler_t) (word18 addr, word18 data, bool writep);

void coreInit (void);
void registerHWCM (word18 addr, word18 len, handler_t handler);
word18 coreRead (word18 addr, const char * ctx);
word36 coreReadDouble (word18 addr, const char * ctx);
void coreWrite (word18 addr, word18 data, const char * ctx);
void coreWriteDouble (word18 addr, word36 data, const char * ctx);
void coreOrWriteDouble (word18 addr, word36 data, const char * ctx);

