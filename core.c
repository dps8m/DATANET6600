#include "dn355.h"
#include "core.h"
#include "hdbg.h"

// XXX For the moment, assuming that memory mapped registers are all below 1024
// AN87, pg 6-28 FNP Store Map has control word areas up to 3777
#define regMapSz 04000

static struct {
  //word18 (* handler) (word18 addr, word18 data, bool writep);
  handler_t handler;
} regMap[regMapSz];

void registerHWCM (word18 addr, word18 len, handler_t handler) {
  if (addr + len > regMapSz) {
    printf ("invalid %s call addr %o len %o\n", __func__, addr, len);
    return;
  }
  for (uint i = addr; i < addr + len; i ++) {
    regMap[i].handler = handler;
  }
}

void coreInit (void) {
  memset (regMap, 0, sizeof (regMap));
}

word18 coreRead (word18 addr, const char * ctx) {
  if (addr >= MEM_SIZE) {
    sim_printf ("%s oops addr %o %s\n", __func__, addr, ctx);
    bail ();
  }
  if (addr < regMapSz && regMap[addr].handler)
    return regMap[addr].handler (addr, 0, false);
  word18 data = cpu.M[addr] & BITS18;
  HDBGCoreRead (addr, data, ctx);
  return data;
}

word36 coreReadDouble (word18 addr, const char * ctx) {
    return (((word36) coreRead (addr, ctx)) << 18) | coreRead (addr + 1, ctx);
  }

void coreWrite (word18 addr, word18 data, const char * ctx) {
  if (addr >= MEM_SIZE) {
    sim_printf ("%s oops addr %o %s\n", __func__, addr, ctx);
    bail ();
  }
  if (addr < regMapSz && regMap[addr].handler)
    regMap[addr].handler (addr, data, true);
  else {
    HDBGCoreWrite (addr, data, ctx);
    cpu.M[addr] = data & BITS18;
  }
}

void coreWriteDouble (word18 addr, word36 data, const char * ctx) {
  coreWrite (addr + 0, (data >> 18) & BITS18, ctx);
  coreWrite (addr + 1, (data >>  0) & BITS18, ctx);
}

void coreOrWriteDouble (word18 addr, word36 data, const char * ctx) {
  word36 t;
  t = coreReadDouble (addr, ctx);
  t |= data;
  coreWriteDouble (addr, t, ctx);
  //coreWrite (addr + 0, (data >> 18) & BITS18, ctx);
  //coreWrite (addr + 1, (data >>  0) & BITS18, ctx);
}
