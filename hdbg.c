/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 56a3950b-f62f-11ec-9ec8-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2016 Charles Anthony
 * Copyright (c) 2021-2022 The DPS8M Development Team
 *
 * All rights reserved.
 *
 * This software is made available under the terms of the ICU
 * License, version 1.8.1 or later.  For more details, see the
 * LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

// History Debugging

#ifdef HDBG

# include "dn355.h"
# include "hdbg.h"
# include "cpu.h" // disassemble

// -1: No snapshotting
// 0: Snaphotting off
// >0: Snapshops left

int hdbgSnap = -1;
//int hdbgSnap = 0;
char * hdbgXRegNames[4] = {"IC", "X1", "X2", "X3"};
enum hevtType {
  hevtEmpty = 0,
  hevtTrace,
  hevgtCore,
  hevgtMap,
  // hevtAPU,
  // hevtFault,
  hevtIntrSet,
  hevtIntr,
  hevtReg,
  // hevtPAReg,
  // hevtDSBRReg,
  hevtNote,
};

struct hevt {
  enum hevtType type;
  uint64_t time;
#define CTX_SZ 64
  char ctx[CTX_SZ];
  bool rw; // F: read  T: write
  union {
    struct {
      word15 ic;
      word18 ins;
    } trace;

    struct {
      word18 addr;
      word18 data;
    } coreref;

    struct {
      word15 addr;
      word18 mapped;
    } mapref;

#ifdef NOTYET
    struct {
      _fault faultNumber;
      _fault_subtype subFault;
      char faultMsg [64];
    } fault;
#endif

    struct {
      uint level, sublevel;
    } intrSet;

    struct {
      uint level, sublevel;
      word15 intrAddr;
    } intr;

#define REG_NAME_LEN 16
    struct {
      char name[REG_NAME_LEN];
      word18 data;
    } reg;

    struct {
# define NOTE_SZ 1024
      char noteBody [NOTE_SZ];
    } note;
  }; // union
}; // hevt

static struct hevt * hevents = NULL;
static long hdbgSize = 0;
static long hevtPtr = 0;
static long hevtMark = 0;

static void createBuffer (void) {
  if (hevents) {
    FREE (hevents);
    hevents = NULL;
  }
  if (hdbgSize <= 0)
    return;
  hevents = malloc (sizeof (struct hevt) * (unsigned long) hdbgSize);
  if (! hevents) {
    sim_printf ("hdbg createBuffer failed\n");
    return;
  }
  memset (hevents, 0, sizeof (struct hevt) * (unsigned long) hdbgSize);

  hevtPtr = 0;
}

static long hdbg_inc (void) {
  //hevtPtr = (hevtPtr + 1) % hdbgSize;
  long ret = __sync_fetch_and_add (& hevtPtr, 1l) % hdbgSize;

  if (hevtMark > 0) {
    long ret = __sync_fetch_and_sub (& hevtMark, 1l);
    if (ret <= 1) {
      sim_printf ("hdbg mark\n");
      hdbgPrint ();
      exit (1);
    }
  }
  return ret;
}

// < 04776 elide sceduler
// >= 035470 elide utilites, trace, breakpoint_man, printer_trace, init
// >= 044220 elide           trace, breakpoint_man, printer_trace, init
// >= 044616 elide                  breakpoint_man, printer_trace, init
#define ELIDE 077770
#define SKIP \
if (0) { \
if (cpu.rIC < 04776) goto done; \
if (cpu.rIC >= ELIDE) goto done; \
if (cpu.rIC == 015033) goto done; \
if (cpu.rIC == 0036215) goto done; \
if (cpu.rIC == 0053476) goto done; \
if (cpu.rIC == 0053477) goto done; \
}


# define hev(t, tf, filter) \
  if (! hevents) \
    goto done; \
  SKIP; \
  if (hdbgSnap == 0) \
    goto done; \
  if (hdbgSnap > 0) \
    hdbgSnap --; \
  long p = hdbg_inc (); \
  hevents[p].type = t; \
  hevents[p].time = cpu.cycleCnt; \
  strncpy (hevents[p].ctx, ctx, CTX_SZ-1); \
  hevents[p].ctx[CTX_SZ-1] = 0; \
  hevents[p].rw = tf;

# define hevnc(t, tf, filter) \
  if (! hevents) \
    goto done; \
  SKIP; \
  long p = hdbg_inc (); \
  hevents[p].type = t; \
  hevents[p].time = cpu.cycleCnt; \
  hevents[p].ctx[0] = 0; \
  hevents[p].rw = tf;

# define FILTER true
# define NO_FILTER false

# define WR true
# define RD false

static bool live = false;

static void hlive (struct hevt * evtp);

void hdbgTrace (word15 ic, word18 ins, const char * ctx) {
  hev (hevtTrace, RD, FILTER);
  hevents[p].trace.ic       = ic;
  hevents[p].trace.ins      = ins;
  hlive (hevents + p);
done: ;
}

void hdbgCoreRead (word18 addr, word18 data, const char * ctx) {
  hev (hevgtCore, RD, FILTER);
  hevents[p].coreref.addr = addr;
  hevents[p].coreref.data = data;
  hlive (hevents + p);
done: ;
}

void hdbgCoreWrite (word18 addr, word18 data, const char * ctx) {
  hev (hevgtCore, WR, FILTER);
  hevents[p].coreref.addr = addr;
  hevents[p].coreref.data = data;
  hlive (hevents + p);
done: ;
}

void hdbgMap (word15 addr, int write, word18 mapped, const char * ctx) {
  hev (hevgtMap, write, FILTER);
  hevents[p].mapref.addr = addr;
  hevents[p].mapref.mapped = mapped;
  hlive (hevents + p);
done: ;
}


#ifdef NOTYET

void hdbgFault (_fault faultNumber, _fault_subtype subFault, const char * faultMsg, const char * ctx) {
  hev (hevtFault, RD, FILTER);
  hevents[p].fault.faultNumber  = faultNumber;
  hevents[p].fault.subFault     = subFault;
  strncpy (hevents[p].fault.faultMsg, faultMsg, 63);
  hevents[p].fault.faultMsg[63] = 0;
  hlive (hevents + p);
done: ;
}

#endif

void hdbgIntrSet (uint level, uint sublevel, const char * ctx) {
  hev (hevtIntrSet, RD, FILTER);
  hevents[p].intrSet.level = level;
  hevents[p].intrSet.sublevel = sublevel;
  hlive (hevents + p);
done: ;
}

void hdbgIntr (uint level, uint sublevel, word15 intrAddr, const char * ctx) {
  hev (hevtIntr, RD, FILTER);
  hevents[p].intr.level = level;
  hevents[p].intr.sublevel = sublevel;
  hevents[p].intr.intrAddr = intrAddr;
  hlive (hevents + p);
done: ;
}

void hdbgRegR (const char * name, word18 data) {
  hevnc (hevtReg, RD, FILTER);
  strncpy (hevents[p].reg.name, name, sizeof (hevents[p].reg.name) - 1);
  hevents[p].reg.name[REG_NAME_LEN - 1] = 0;
  hevents[p].reg.data = data;
  hlive (hevents + p);
done: ;
}

void hdbgRegW (const char * name, word18 data) {
  hevnc (hevtReg, WR, FILTER);
  strncpy (hevents[p].reg.name, name, sizeof (hevents[p].reg.name) - 1);
  hevents[p].reg.name[REG_NAME_LEN - 1] = 0;
  hevents[p].reg.data = data;
  hlive (hevents + p);
done: ;
}

void hdbgNote (const char * ctx, const char * fmt, ...) {
  hev (hevtNote, RD, NO_FILTER);
  va_list arglist;
  va_start (arglist, fmt);
  vsnprintf (hevents [p].note.noteBody, NOTE_SZ - 1, fmt, arglist);
  va_end (arglist);
done: ;
}

void hdbgNote0 (const char * ctx, const char * msg) {
  hev (hevtNote, RD, NO_FILTER);
  strncpy (hevents[p].note.noteBody, msg, NOTE_SZ - 1);
  hevents[p].note.noteBody[NOTE_SZ - 1] = 0;
done: ;
}

static FILE * hdbgOut = NULL;

static void printCore (struct hevt * p) {
  fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> CORE: %s %s %06o %06o\n",
           p->time,
           p->ctx,
           p->rw ? "write" : "read ",
           p->coreref.addr,
           p->coreref.data);
}

static void printMap (struct hevt * p) {
  fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> MAP: %s %s %06o %06o\n",
           p->time,
           p->ctx,
           p->rw ? "write" : "read ",
           p->mapref.addr,
           p->mapref.mapped);
}

#ifdef NOTYET
static void printAPU (struct hevt * p) {
  fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> APU: %s %s %05o:%06o %08o %012"PRIo64"\n",
           p->time,
           p->ctx,
           p->rw ? "write" : "read ",
           p->apu.segno,
           p->apu.offset,
           p->apu.final,
           p->apu.data);
}
#endif

static void printTrace (struct hevt * p) {
  //char buf[256];
  fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> TRACE: %s %05o %06o (%s)\n",
             p->time,
             p->ctx,
             p->trace.ic,
             p->trace.ins,
             disassemble (p->trace.ic, p->trace.ins));
}

#ifdef NOTYET
static void printFault (struct hevt * p) {
  fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> FAULT: %s Fault %d(0%o), sub %"PRIu64"(0%"PRIo64"), '%s'\n",
           p->time,
           p->ctx,
           p->fault.faultNumber,
           p->fault.faultNumber,
           p->fault.subFault.bits,
           p->fault.subFault.bits,
           p->fault.faultMsg);
}
#endif

static void printIntrSet (struct hevt * p) {
  fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> INTR_SET: %s level %o sublevel %o\n",
           p->time,
           p->ctx,
           p->intrSet.level,
           p->intrSet.sublevel);
}

static void printIntr (struct hevt * p) {
  fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> INTR: %s level %o sublevel %o intrAddr %05o\n",
           p->time,
           p->ctx,
           p->intr.level,
           p->intr.sublevel,
           p->intr.intrAddr);
}

static void printReg (struct hevt * p) {
  if (strcmp (p->reg.name, "IR") == 0) {
    fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> REG: %s %s %s %06o %c%c%c%c%c%c%c%c Channel %02o\n", p->time, p->ctx, p->rw ? "write" : "read ", p->reg.name, p->reg.data, 
    p->reg.data & I_ZERO  ? 'Z' : 'z',
    p->reg.data & I_NEG   ? 'N' : 'n',
    p->reg.data & I_CARRY ? 'C' : 'c',
    p->reg.data & I_OVF   ? 'O' : 'o',
    p->reg.data & I_II    ? 'I' : 'i',
    p->reg.data & I_PFI   ? 'P' : 'p',
    p->reg.data & I_OFI   ? 'V' : 'v',
    p->reg.data & I_PE    ? 'E' : 'e',
    p->reg.data & BITS6);
  } else {
    fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> REG: %s %s %s %06o\n", p->time, p->ctx, p->rw ? "write" : "read ", p->reg.name, p->reg.data);
  }
}

#ifdef NOTYET
static void printPAReg (struct hevt * p)
{
  if (p->reg.type >= hreg_PR0 && p->reg.type <= hreg_PR7)
    fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> REG: %s %s %s %05o:%06o BIT %2o RNR %o\n",
             p->time,
             p->ctx,
             p->rw ? "write" : "read ",
             regNames[p->reg.type],
             p->par.data.SNR,
             p->par.data.WORDNO,
             p->par.data.PR_BITNO,
             p->par.data.RNR);
  else
    fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> REG: %s write %s %05o:%06o CHAR %o BIT %2o RNR %o\n",
             p->time,
             p->ctx,
             regNames[p->reg.type],
             p->par.data.SNR,
             p->par.data.WORDNO,
             p->par.data.AR_CHAR,
             p->par.data.AR_BITNO,
             p->par.data.RNR);
}

static void printDSBRReg (struct hevt * p) {
  fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> REG: %s %s %s %05o:%06o BIT %2o RNR %o\n",
           p->time,
           p->ctx,
           p->rw ? "write" : "read ",
           regNames[p->reg.type],
           p->par.data.SNR,
           p->par.data.WORDNO,
           p->par.data.PR_BITNO,
           p->par.data.RNR);
}
#endif

static void printNote (struct hevt * p) {
  fprintf (hdbgOut ? hdbgOut : stdout, "DBG(%"PRIu64")> NOTE: %s %s\n",
               p->time,
               p->ctx,
               p->note.noteBody);
}

static void hdbgPrintEv (struct hevt * evtp) {
  switch (evtp -> type) {
    case hevtEmpty:
      break;

    case hevtTrace:
      printTrace (evtp);
      break;

    case hevgtCore:
      printCore (evtp);
      break;

    case hevgtMap:
      printMap (evtp);
      break;

#ifdef NOTYET
    case hevtAPU:
      printAPU (evtp);
      break;

# if 0
    case hevtIWBUpdate:
      printIWBUpdate (evtp);
      break;
# endif

# if 0
    case hevtRegs:
      printRegs (evtp);
      break;
# endif

    case hevtFault:
      printFault (evtp);
      break;
#endif

    case hevtIntrSet:
      printIntrSet (evtp);
      break;

    case hevtIntr:
      printIntr (evtp);
      break;

    case hevtReg:
      printReg (evtp);
      break;

#ifdef NOTYET
    case hevtPAReg:
      printPAReg (evtp);
      break;

    case hevtDSBRReg:
      printDSBRReg (evtp);
      break;
#endif

    case hevtNote:
      printNote (evtp);
      break;

    default:
      fprintf (hdbgOut, "hdbgPrint ? %d\n", evtp -> type);
      break;
  }
}

static void hlive (struct hevt * evtp) {
  if (live)
    hdbgPrintEv (evtp);
}

void hdbgPrint (void) {
  sim_printf ("hdbg print\n");
  if (! hevents)
    goto done;
  struct hevt * t = hevents;
  hevents = NULL;
  hdbgOut = fopen ("hdbg.list", "w");
  if (! hdbgOut) {
    sim_printf ("can't open hdbg.list\n");
    goto done;
  }
  time_t curtime;
  time (& curtime);
  fprintf (hdbgOut, "%s\n", ctime (& curtime));

  for (long p = 0; p < hdbgSize; p ++) {
    long q = (hevtPtr + p) % hdbgSize;
    struct hevt * evtp = t + q;
    hdbgPrintEv (evtp);
  }
  fclose (hdbgOut);
done: ;
}

void hdbg_mark (void) {
  hevtMark = hdbgSize;
  sim_printf ("hdbg mark set to %ld\n", (long) hevtMark);
}

// set buffer size
t_stat hdbg_size (UNUSED int32_t arg, const char * buf) {
  hdbgSize = (long) strtoul (buf, NULL, 0);
  sim_printf ("hdbg size set to %ld\n", (long) hdbgSize);
  createBuffer ();
  return SCPE_OK;
}

t_stat hdbg_print (UNUSED int32_t arg, const char * buf) {
  hdbgPrint ();
  return SCPE_OK;
}

#endif // HDBG
