
typedef void (* uv_read_cb_t)  (uv_tcp_t * client, ssize_t nread, unsigned char * buf);
typedef void (* uv_write_cb_t) (uv_tcp_t * client, unsigned char * data, ssize_t datalen);

struct uvClientData_s {
  uint hslano;
  uint lineno;
  bool assoc;
  void * telnetp;
  uv_read_cb_t  read_cb;
  uv_write_cb_t write_cb;
  uv_write_cb_t write_actual_cb;
  // Work buffer for processLineInput
  char buffer [1024];
  size_t nPos;
};

typedef struct uvClientData_s uvClientData;

int hslaUvStart (void);

