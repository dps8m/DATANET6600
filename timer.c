#include "dn355.h"
#include "cpu.h"
#include "iom.h"
#include "pager.h"


static struct {
#ifdef WALL
  uv_timer_t uv_timer_handle;
#endif
  bool running;
} timerData;

// Timer/Switch channel

// Channel 077
// Interrupt vectors
//   IOM detected fault 360
//   Interval timer runout 361
//   Elapsed timer rollover 362
// MBXs
//   Interval timer 450
//   Elapsed timer  451
// PCW - only mask bit (23) is used:
//   1: Interval timer turned off.
//   0: Interval timer tunrne on.
// Status - none reported
// Other: To read maintenance panel data switches, use STEX to channel 77

// init.map355
//
//           ldaq      sdqdat-*  schedule dummy rtn to kick off timer
//           tsy       a.m026-*,*          (dspqur) since clock doesnt start til it is used
//
// Not clear if it is interval, elapsed or both that don't start; assuming both. XXX

void timerConnect (word36 PCW) {
  word1 mask = (word1) getbits36 (PCW, MASK_BIT, 1);
  iom.masked[TIMER_CHANNEL_NO] = mask;
  timerData.running = true;
}

void updateTimers (void) {
  if (! timerData.running)
    return;
  word18 t = cpu.M[ADDR_IOM_MBX_INTERVAL_TIMER];
  if (t) {  // Is the interval non-zero?
    t --;
    cpu.M[ADDR_IOM_MBX_INTERVAL_TIMER] = t;
    if (t == 0) {
// Verified correct by dn355 trace: seen to vector to
// itl250 ind     *+1             addr of place to go on timer interrupt
//        ind     ** 
      doIntr (1, 15, "interval timer"); // 361 
    }
  }
  t = (cpu.M[ADDR_IOM_MBX_ELAPSED_TIMER] + 1) & 0777777;
  cpu.M[ADDR_IOM_MBX_ELAPSED_TIMER] = t;
  if (t == 0) {
    // dd01, pg 3-9: "The sublevel word bit position is the number specified by the four least 
    // significant bits of the I/O channel number of the channel which caused the interrupt"
    // Timer channel is 77, so 017 (15 decimal) is the sublevel.
    // Interrupt vector is sublevel << 4 | level
    //    017 << 4 => 360
    //    360 | 2 => 362
// Verified correct by dn355 trace; seen to vector to:
// itl215 ind     **              elapsed timer runout comes here
    doIntr (2, 15, "elapsed timer"); // 362 
  }
}

#ifdef WALL
static void timerCB (uv_timer_t * handle) {
  updateTimers ();
}
#endif

void timerInit (void) {
  timerData.running = true;
  iom.masked[TIMER_CHANNEL_NO] = false;
#ifdef WALL
  uv_timer_init (uv_default_loop (), & timerData.uv_timer_handle);
  uv_timer_start (& timerData.uv_timer_handle, timerCB, 1, 1); // 1 ms, repeat 1ms
#endif
}


