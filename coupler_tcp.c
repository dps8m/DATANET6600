#include "dn355.h"
#include "cpu.h"
#include "iom.h"
#include "caf.h"
#include "utils.h"
#include "coupler_tcp.h"
#include "coupler.h"
#include "hdbg.h"

/* From sim_sock.c */

/* sim_parse_addr       host:port

   Presumption is that the input, if it doesn't contain a ':' character is a port specifier.
   If the host field contains one or more colon characters (i.e. it is an IPv6 address),
   the IPv6 address MUST be enclosed in square bracket characters (i.e. Domain Literal format)

   Inputs:
        cptr    =       pointer to input string
        default_host
                =       optional pointer to default host if none specified
        host_len =      length of host buffer
        default_port
                =       optional pointer to default port if none specified
        port_len =      length of port buffer
        validate_addr = optional name/addr which is checked to be equivalent
                        to the host result of parsing the other input.  This
                        address would usually be returned by sim_accept_conn.
   Outputs:
        host    =       pointer to buffer for IP address (may be NULL), 0 = none
        port    =       pointer to buffer for IP port (may be NULL), 0 = none
        result  =       status (0 on complete success or -1 if
                        parsing can't happen due to bad syntax, a value is
                        out of range, a result can't fit into a result buffer,
                        a service name doesn't exist, or a validation name
                        doesn't match the parsed host)
*/

static int sim_parse_addr (const char *cptr, char *host, size_t host_len, const char *default_host, char *port, size_t port_len, const char *default_port, const char *validate_addr) {
  char gbuf[CBUFSIZE], default_pbuf[CBUFSIZE];
  const char *hostp;
  char *portp;
  char *endc;
  unsigned long portval;

  if ((host != NULL) && (host_len != 0))
    memset (host, 0, host_len);
  if ((port != NULL) && (port_len != 0))
    memset (port, 0, port_len);
  if ((cptr == NULL) || (*cptr == 0)) {
    if (((default_host == NULL) || (*default_host == 0)) || ((default_port == NULL) || (*default_port == 0)))
      return -1;
    if ((host == NULL) || (port == NULL))
      return -1;                                  /* no place */
    if ((strlen(default_host) >= host_len) || (strlen(default_port) >= port_len))
      return -1;                                  /* no room */
    strcpy (host, default_host);
    strcpy (port, default_port);
    return 0;
  }
  memset (default_pbuf, 0, sizeof (default_pbuf));
  if (default_port)
    strncpy (default_pbuf, default_port, sizeof (default_pbuf)-1);
  gbuf[sizeof (gbuf)-1] = '\0';
  strncpy (gbuf, cptr, sizeof (gbuf)-1);
  hostp = gbuf;                                           /* default addr */
  portp = NULL;
  if ((portp = strrchr (gbuf, ':')) &&                    /* x:y? split */
      (NULL == strchr (portp, ']'))) {
    *portp++ = 0;
    if (*portp == '\0')
      portp = default_pbuf;
  } else {                                                  /* No colon in input */
    portp = gbuf;                                       /* Input is the port specifier */
    hostp = (const char *)default_host;                 /* host is defaulted if provided */
  }
  if (portp != NULL) {
    portval = strtoul (portp, &endc, 10);
    if ((*endc == '\0') && ((portval == 0) || (portval > 65535)))
      return -1;                                      /* numeric value too big */
    if (*endc != '\0') {
      struct servent *se = getservbyname (portp, "tcp");

      if (se == NULL)
        return -1;                                  /* invalid service name */
    }
  }
  if (port)                                               /* port wanted? */
    if (portp != NULL) {
      if (strlen (portp) >= port_len)
        return -1;                                  /* no room */
      else
        strcpy (port, portp);
    }
  if (hostp != NULL) {
    if (']' == hostp[strlen (hostp)-1]) {
      if ('[' != hostp[0])
        return -1;                                  /* invalid domain literal */
      /* host may be the const default_host so move to temp buffer before modifying */
      strncpy (gbuf, hostp+1, sizeof (gbuf)-1);         /* remove brackets from domain literal host */
      gbuf[strlen (gbuf)-1] = '\0';
      hostp = gbuf;
    }
  }
  if (host) {                                             /* host wanted? */
    if (hostp != NULL) {
      if (strlen (hostp) >= host_len)
        return -1;                                  /* no room */
      else
        if (('\0' != hostp[0]) || (default_host == NULL))
          strcpy (host, hostp);
        else if (strlen (default_host) >= host_len)
          return -1;                          /* no room */
        else
          strcpy (host, default_host);
    } else {
      if (default_host) {
        if (strlen (default_host) >= host_len)
          return -1;                              /* no room */
        else
          strcpy (host, default_host);
      }
    }
  }
  if (validate_addr) {
    struct addrinfo *ai_host, *ai_validate, *ai, *aiv;
    int status;

    if (hostp == NULL)
      return -1;
    if (getaddrinfo (hostp, NULL, NULL, & ai_host))
      return -1;
    if (getaddrinfo (validate_addr, NULL, NULL, & ai_validate))
      return -1;
    status = -1;
    for (ai = ai_host; ai != NULL; ai = ai->ai_next) {
      for (aiv = ai_validate; aiv != NULL; aiv = aiv->ai_next) {
        if ((ai->ai_addrlen == aiv->ai_addrlen) &&
            (ai->ai_family == aiv->ai_family) &&
            (0 == memcmp (ai->ai_addr, aiv->ai_addr, ai->ai_addrlen))) {
          status = 0;
          break;
        }
      }
    }
    if (status != 0) {
      /* be generous and allow successful validations against variations of localhost addresses */
      if (((0 == strcmp ("127.0.0.1", hostp)) &&
           (0 == strcmp ("::1", validate_addr))) ||
          ((0 == strcmp ("127.0.0.1", validate_addr)) &&
           (0 == strcmp ("::1", hostp))))
        status = 0;
    }
    return status;
  }
  return 0;
}

// From sihm udplib

static int tcp_parse_remote (const char * premote, char * rhost, size_t rhostl, int32_t * rport) {
  // This routine will parse a remote address string in any of these forms -
  //
  //            :w.x.y.z:rrrr
  //            :name.domain.com:rrrr
  //            :rrrr
  //            w.x.y.z:rrrr
  //            name.domain.com:rrrr
  //
  // In all examples, "rrrr" is the remote port number that we use for transmitting.  
  // "w.x.y.z" is a dotted IP for the remote machine
  // and "name.domain.com" is its name (which will be looked up to get the IP).
  // If the host name/IP is omitted then it defaults to "localhost".

  char host [64], port [16];
  if (* premote == '\0')
    return -1;
  * rhost = '\0';

  if (sim_parse_addr (premote, host, sizeof (host), "localhost", port, sizeof (port), NULL, NULL))
    return -1;
  strncpy (rhost, host, rhostl);
  * rport = atoi (port);
  return 0;
}

//
// allocBuffer: libuv callback handler to allocate buffers for incomingd data.
//

static void allocBuffer (UNUSED uv_handle_t * handle, size_t suggested_size, uv_buf_t * buf) {
  char * p = (char *) malloc (suggested_size);
  if (! p) {
     sim_printf ("ERR: %s malloc fail\n", __func__);
     HDBGNote0 (__func__, "ERR: malloc fail");
  }
  * buf = uv_buf_init (p, (uint) suggested_size);
}

static void onRead (uv_stream_t * req, ssize_t nread, const uv_buf_t * buf) {
  int is_error = 0;
  if (nread < 0) {
    if (nread == UV_EOF) {
      sim_printf ("INFO: client EOF\n");
      HDBGNote0 (__func__, "INFO: client EOF");
    } else {
      sim_printf ("ERR: Read error!\n");
      HDBGNote0 (__func__, "ERR: Read error!");
    }
    is_error = 1;
  } else if (nread > 0) {
//sim_printf ("DEBUG: recv %ld %c\n", nread, buf->base[0]);
#if defined (PKT_TRACE) || defined (PKT_TRACE2)
printf (">");
for (uint i = 0; i < sizeof (dnPkt); i ++) {
  uint8_t ch = ((uint8_t *) buf->base) [i];
  printf ("%c", isprint (ch) ? ch : '_');
}
printf ("\n");
#endif
    cmdReceived (req, (dnPkt *) buf->base, nread);

    free (buf->base);
    return;
  } else {
// sim_printf ("DEBUG: recv empty\n");
  }
  if (buf->base)
    free (buf->base);
  if (is_error)
    uv_close ((uv_handle_t *) req, NULL);
  return;
}
static void onConnect (uv_stream_t * server, int status) {
  if (status < 0) {
    sim_printf ("ERR: %s connected %d\n", __func__, status);
    HDBGNote (__func__, "ERR: connected %d", status);
    return;
  }
  //sim_printf ("INFO: %s connected %d\n", __func__, status);
  //HDBGNote (__func__, "INFO: connected %d", status);
  uv_tcp_init (uv_default_loop (), & cd.clientHandle);
  int rc = uv_accept ((uv_stream_t *) & cd.tcpHandle, (uv_stream_t *) & cd.clientHandle);
  if (rc < 0) {
    sim_printf ("ERR: %s uv_accept failed %d\n", __func__, rc);
    HDBGNote (__func__, "ERR: uv_accept failed %d", rc);
    return;
  }
  
  //uv_tcp_nodelay ((uv_tcp_t *) & cd.clientHandle, 1);
  //uv_stream_set_blocking ((uv_stream_t *) & cd.clientHandle, 1);
  uv_read_start ((uv_stream_t *) & cd.clientHandle, allocBuffer, onRead);

}

//   simh calls this routine for (what else?) the ATTACH command.  There are
// three distinct formats for ATTACH -
//
//    ATTACH -p MIn COMnn          - attach MIn to a physical COM port
//    ATTACH MIn llll:w.x.y.z:rrrr - connect via UDP to a remote simh host
//

t_stat couplerAttach (UNIT * uptr, const char * cptr) {
  if (! cptr)
    return SCPE_ARG;

  // If we're already attached, then detach ...
  if ((uptr -> flags & UNIT_ATT) != 0)
    detach_unit (uptr);

  // Make a copy of the "file name" argument.  udp_create() actually 
  // modifies the string buffer we give it, so we make a copy now so we'll 
  // have something to display in the "SHOW MIn ..." command.
  strncpy (cd.attachAddress, cptr, ATTACH_ADDRESS_SZ);
  uptr->filename = cd.attachAddress;

  int32_t rport;
  char rhost[64];
  int rc = tcp_parse_remote (cptr, rhost, sizeof (rhost), & rport);
  if (rc) {
    sim_printf ("ERR: %s unable to parse address %d\n", __func__, rc);
    HDBGNote (__func__, "ERR: unable to parse address %d", rc);
    return SCPE_ARG;
  }
// sim_printf ("DEBUG: %d:%s:%d\n", rc, rhost, rport);

// Set up send

  rc = uv_tcp_init (uv_default_loop (), & cd.tcpHandle);
  if (rc) {
    sim_printf ("ERR: %s unable to init tcpHandle %d\n", __func__, rc);
    HDBGNote (__func__, "ERR: unable to init tcpHandle %d", rc);
    return SCPE_ARG;
  }

  struct sockaddr_in raddr;
  uv_ip4_addr (rhost, rport, & raddr);

  rc = uv_tcp_bind (& cd.tcpHandle, (const struct sockaddr *) & raddr, 0);
  if (rc) {
    sim_printf ("ERR: %s unable to cbind tcpHandle %d\n", __func__, rc);
    HDBGNote (__func__, "ERR: unable to cbind tcpHandle %d", rc);
    return SCPE_ARG;
  }

  rc = uv_listen ((uv_stream_t *) & cd.tcpHandle, 0, onConnect);
  if (rc) {
    sim_printf ("ERR: %s unable to connect tcpSendHandle %d\n", __func__, rc);
    HDBGNote (__func__, "ERR: unable to connect tcpSendHandle %d", rc);
    return SCPE_ARG;
  }

  uptr->flags |= UNIT_ATT;
  //cd.connected = true;
sim_printf ("listening\n");
  return SCPE_OK;
}

static void close_cb (uv_handle_t * stream) {
  free (stream);
}

// Detach device ...
t_stat couplerDetach (UNIT * uptr) {
  if ((uptr->flags & UNIT_ATT) == 0)
    return SCPE_OK;
  uv_close ((uv_handle_t *) & cd.tcpHandle, close_cb);
  uptr->flags &= ~ (unsigned int) UNIT_ATT;
  uptr->filename = NULL;
  return SCPE_OK;
}


static void csSendCB (uv_write_t * req, int status) {
  if (status) {
    sim_printf ("WARN: %s status %d\n", __func__, status);
    HDBGNote (__func__, "WARN: status %d", status);
  }
}

void csSend (dnPkt * pkt) {
#if defined (PKT_TRACE) || defined (PKT_TRACE2)
printf ("<");
for (uint i = 0; i < sizeof (dnPkt); i ++) {
  uint8_t ch = ((uint8_t *) pkt) [i];
  printf ("%c", isprint (ch) ? ch : '_');
}
printf ("\n");
#endif
  uv_write_t * req = malloc (sizeof (uv_write_t));
  if (! req) {
    sim_printf ("ERR: %s req malloc fail\n", __func__);
    HDBGNote0 (__func__, "ERR: req malloc fail");
    return;
  }
  // set base to null
  memset (req, 0, sizeof (uv_write_t));

  void * p = malloc (sizeof (dnPkt));
  if (! p) {
    sim_printf ("ERR: %s buf malloc fail\n", __func__);
    HDBGNote0 (__func__, "ERR: buf malloc fail");
    return;
  }
  uv_buf_t buf = uv_buf_init ((char *) p, (uint) sizeof (dnPkt));

  memcpy (buf.base, pkt, sizeof (dnPkt));
  //int rc = uv_write (req, (uv_stream_t *) & cd.tcpHandle, & buf, 1, csSendCB);
  int rc = uv_write (req, (uv_stream_t *) & cd.clientHandle, & buf, 1, csSendCB);
  if (rc < 0) {
    sim_printf ("ERR: %s uv_write failed %d\n", __func__, rc);
    HDBGNote (__func__, "ERR: uv_write failed %d", rc);
  }
}



