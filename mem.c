#include "dn355.h"
#include "cpu.h"
#include "caf.h"
#include "pager.h"
#include "mem.h"
#include "hdbg.h"


static struct  cinfo_t {
  int size;   // size in bits of operand
  word18 mask1;  // mask data presented to processor/memory
  word18 mask2;  // mask for removing uninteresting bits
  int shift;  // L/R shift count to position bits correctly
} cinfo[8] = {
  { 18, 0777777,       0,  0 },   // 000 = single 18-bit word
  { 36,       0,       0,  0 },   // 001 = double (36-bit) word
  {  9,    0777, 0777000,  9 },   // 010 = 9 bit char 0, bits 0-8
  {  9,    0777, 0000777,  0 },   // 011 = 9 bit char 1. bits 9-17
  {  6,     077, 0770000, 12 },   // 100 = 6 bit char 0, bits 0-5
  {  6,     077, 0007700,  6 },   // 101 = 6 bit char 1, bits 6-11
  {  6,     077, 0000077,  0 },   // 110 = 6 bit char 2, bits 12-17
  {  0,       0,       0,  0 }    // 111 = illegal
};


word18 fromMemoryPtrInc (word18 * ptr, const char * ctx) {
  word15 addr = (* ptr) & BITS15;
  word3 CY =  ((* ptr) >> 15) & BITS3;
  word18 res = fromMemory (* ptr, ctx);
  switch (CY) {
    case 0: // whole word ptr
    case 7:
      * ptr = (((word18) CY << 15)) | (word18) ((addr + 1) & BITS15);
      break;
    case 1:
      sim_printf ("ERROR: %s case 1\n", __func__);
      break;
    case 2: // 9 bit char 0
      //res = (res >> 9) & BITS9;
      * ptr = (03 << 15) | (addr & BITS15);
      break;
    case 3: // 9 bit char 1
      //res = res & BITS9;
      * ptr = (02 << 15) | ((addr + 1) & BITS15); 
      break;
    case 4: // 6 bit char 0
      //res = (res >> 12) & BITS6;
      * ptr = (05 << 15) | (addr & BITS15);
      break;
    case 5: // 6 bit char 1
      //res = (res >> 6) & BITS6;
      * ptr = (06 << 15) | (addr & BITS15);
      break;
    case 6: // 6 bit char 2
      //res = res & BITS6;
      * ptr = (04 << 15) | ((addr + 1) & BITS15);
      break;
  }
  return res;
}


word18 fromMemory (word18 addr, const char * ctx) {
  word15 W = _W (addr);
  word3  C = _C (addr);
    
  switch (C) {
      
    case 0:
      return memoryRead (W, ctx);
      
    case 1: 
      doFault (faultIllegalStore, "fromMemory(): illegal charaddr");
      return 0;
      
    case 2: // 0/2
       return (memoryRead (W, ctx) >> 9) & BITS9;

    case 3: // 1/2
       return (memoryRead (W, ctx) >> 0) & BITS9;
    
    case 4: // 0/3
       return (memoryRead (W, ctx) >> 12) & BITS6;
      
    case 5: // 1/3
       return (memoryRead (W, ctx) >>  6) & BITS6;
      
    case 6: // 2/3
       return (memoryRead (W, ctx) >>  0) & BITS6;

    case 7: // 0/3
      return memoryRead (W, ctx);
  }
  return memoryRead (W, ctx);
}


word36 fromMemory36 (word15 addr, int charaddr, const char * ctx) {
  // we're reading from memory, so, we need look at charAddr to see what kind of data to present to the processor or IOM
    
  addr     &= BITS15; // keep word address to 15-bits
  charaddr &= BITS3;  // keep char addr to 0..7
    
  switch (charaddr) {
    case 0: { // word addressing
      word18 data = memoryRead (addr, ctx);
      sim_debug(DBG_FINAL, &cpuDev, "Read Addr%o: %05o Data: %06o\n", charaddr, addr, data);
      return data;
    }

    case 1: { // double-word addressing
      // an odd address will use the pair (Y-1, Y)
      // an odd word is the least significant part of a double-precision number
      // an even address will use the pair (Y, Y+1)
      // an even word is the most significant part of a double-precision number
      // the memory location with the lower (even) address contains the most significant part of a double-word address
      word36 even = memoryRead (addr & 077776, ctx); // this will force an odd even (Y-1) and leave even alone (Y)
      word36 odd  = memoryRead (addr | 000001, ctx); // this will force an even odd (Y+1) and leave an odd alone (Y)
      return even << 18LL | odd;
    }

    case 7: {
      doFault (faultIllegalStore, "fromMemory(): illegal charaddr");
      return 0;
    }

    case 2:
    case 3:
    case 4:
    case 5:
    case 6: {
      break;
    }
  } // switch charaddr

  struct cinfo_t *ci = cinfo + charaddr;      // get mask & shifting info
  word18 data = memoryRead (addr, ctx);            // get 18-bits of data from memory
  data &= ci->mask2;                          // mask off unneeded bits
  data >>= ci->shift;                         // right justify
  return data & ci->mask1;                    // and present it to the CPU/IOM
}


void toMemory1 (word18 data, word18 addr, word18 zoneMask, const char * ctx) {
  toMemory (data, addr & BITS15, (addr >> 15) & BITS3, zoneMask, ctx);
}



void toMemory (word18 data, word15 addr, word3 charaddr, word18 zoneMask, const char * ctx) {
  // we're writing to memory, so, we need look at charaddr to see what kind of data to put where
  charaddr &= BITS3;

  switch (charaddr) {

    case 0: {
      if (zoneMask) {
        word18 v = memoryRead (addr & BITS15, ctx);
        v = (v & ~zoneMask) | (data & zoneMask);
        memoryWrite (addr & BITS15, v, ctx);
      } else {
        memoryWrite (addr & BITS15, data & BITS18, ctx);
      }
      return;
    }

    case 1: // double-word addressing
    case 7: {
      doFault (faultIllegalStore, "to Memory(): illegal charaddr");
      return;
    }

    case 2:
    case 3:
    case 4:
    case 5:
    case 6: {
      break;
    }

  } // switch charaddr
    
  //
  // Every character store is a RMW access ...
  //

  struct cinfo_t *ci = cinfo + charaddr;
    
  word18 oldM = memoryRead (addr, ctx);          // get 18-bits of data from memory
  data &= ci->mask1;                        // mask everything from incomming data but bits of interest
  data <<= ci->shift;                       // shift over by required amount
  word18 newM = (oldM & ~ci->mask2) | data; // mask out previous bits, 'or' in new data
  memoryWrite (addr, newM & BITS18, ctx);         // write out modified data back to memory
}


void toMemory36 (word36 data36, word15 addr, word3 charaddr, const char * ctx) {
  // we're writing to memory, so, we need look at charaddr to see what kind of data to put where
  charaddr &= BITS3;
    
  switch (charaddr) {

    case 0:
    case 7: {
      memoryWrite (addr & BITS15, data36 & BITS18, ctx);
      return;
    }

    case 1: { // double-word addressing
      // an odd address will use the pair (Y-1, Y)
      // an odd word is the least significant part of a double-precision number
      // an even address will use the pair (Y, Y+1)
      // an even word is the most significant part of a double-precision number
      // the memory location with the lower (even) address contains the most significant part of a double-word address
      word36 even = (data36 >> 18LL) & BITS18;
      memoryWrite (addr & 077776, (word18) even, ctx); // this will force an odd even (Y-1) and leave even alone (Y)
      word36 odd  =  data36 & BITS18;
      memoryWrite (addr | 000001, (word18) odd, ctx);  // this will force an even odd (Y+1) and leave an odd alone (Y)
      return;
    }

    case 2:
    case 3:
    case 4:
    case 5:
    case 6: {
      break;
    }

  } // switch chaddr
    
  //
  // Every character store is a RMW access ...
  //

  struct cinfo_t *ci = cinfo + charaddr;
    
  word18 oldM = memoryRead (addr, ctx);         // get 18-bits of data from memory
  word18 data18 = data36 &= ci->mask1;     // mask everything from incomming data but bits of interest
  data18 <<= ci->shift;                    // shift over by required amount
  word18 newM = (oldM & ~ci->mask2) | data18; // mask out previous bits, 'or' in new data
  memoryWrite (addr, newM & BITS18, ctx);            // write out modified data back to memory
}


