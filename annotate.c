#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <stdbool.h>
#include <string.h>

typedef unsigned int uint;
typedef uint32_t word18;

#define N_SYMS 4096
static struct symtab_s {
  char symnam[7];
  unsigned int symval;
} symtab[N_SYMS];
static int nsyms = 0;

static int symcmp (const void * a, const void * b) {
  struct symtab_s * aa = (struct symtab_s *) a;
  struct symtab_s * bb = (struct symtab_s *) b;
  if (aa->symval < bb->symval)
    return -1;
  if (aa->symval == bb->symval)
    return 0;
  return 1;
}


static char * lookupSym (uint offset) {
  int use = -1;
  for (int i = 0; i < nsyms; i ++) {
    if (symtab[i].symval > offset)
      break;
    use = i;
  }
  if (use == -1)
    return "";
  static char buf[129];
  sprintf (buf, "%s+%05o", symtab[use].symnam, offset - symtab[use].symval);
  return buf;
}

// LOAD_SYSTEM_BOOK <filename>
#define BK "site_mcs.list"
#define PATH "./"
#define bookModuleNameLen 25
#define bookModulesMax 20
static struct bookModule {
  char * moduleName;
  int start;
  int length;
} bookModules[bookModulesMax];
static int nBookModules = 0;

static int addBookModule (char * name, int start, int length) {
  if (nBookModules >= bookModulesMax)
    return -1;
  bookModules[nBookModules].moduleName = strdup (name);
  bookModules[nBookModules].start = start;
  bookModules[nBookModules].length = length;
  int n = nBookModules;
  nBookModules ++;
  return n;
}

// Warning: returns ptr to static buffer
static char * lookupSystemBookAddress (int offset, char * * moduleName, int * moduleOffset) {
  static char buf [129];
  int i;
  for (i = 0; i < nBookModules; i ++) {
    if (bookModules[i].start <= offset &&
        bookModules[i].start + bookModules[i].length > offset) {
      sprintf (buf, "%s:%05o", bookModules[i].moduleName, offset - bookModules[i].start);
      if (moduleName)
        * moduleName = bookModules[i].moduleName;
      if (moduleOffset)
        * moduleOffset = offset - bookModules[i].start;
      return buf;
    }
  }
  if (moduleName)
    * moduleName = "????";
  if (moduleOffset)
    * moduleOffset = offset;
  sprintf (buf, "????:%05o", offset);
  return buf;
}

static char * lookupAddress (int offset, char * * moduleName, int * moduleOffset) {
  if (moduleName)
    * moduleName = NULL;
  if (moduleOffset)
    * moduleOffset = 0;
  char * ret = lookupSystemBookAddress (offset, moduleName, moduleOffset);
  return ret;
}


static char * sourceSearchPath = NULL;

// search path is path:path:path....

static void setSearchPath (const char * buf)
  {
    if (sourceSearchPath)
      free (sourceSearchPath);
    sourceSearchPath = strdup (buf);
  }

static char sourceLine [1025];
void listSource (char * moduleName, int offset, int dflag) {
  const int offset_str_len = 12;
  char offset_str [17];
  sprintf (offset_str, "       %05o", offset);
  const int offset_str_len2 = 12+12;
  char offset_str2 [17+12];
  sprintf (offset_str2, "                   %05o", offset);

  char path [(sourceSearchPath ? strlen (sourceSearchPath) : 1) + 
    1 + // "/"
    (moduleName ? strlen (moduleName) : 1) +
    1 + strlen (".list") + 1];
  char * searchp = sourceSearchPath ? sourceSearchPath : ".";
  // find <search path>/<compname>.list
  while (* searchp) {
    size_t pathlen = strcspn (searchp, ":");
    strncpy (path, searchp, pathlen);
    path [pathlen] = '\0';
    if (searchp [pathlen] == ':')
      searchp += pathlen + 1;
    else
      searchp += pathlen;

    if (moduleName) {
      strcat (path, "/");
      strcat (path, moduleName);
    }
    strcat (path, ".list");
    FILE * listing = fopen (path, "r");
    if (listing) {
      // map355 listing files look like:
      //        01550  0 01660    1          363 a.a004 ind     print           line printer
      // or:
      //                    00420            448 pwrbak return  pwrite


      while (! feof (listing)) {
        fgets (sourceLine, 1024, listing);
        if (strncmp (sourceLine, offset_str, (size_t) offset_str_len) == 0 ||
            strncmp (sourceLine, offset_str2, (size_t) offset_str_len2) == 0)  {
          printf ("%s", sourceLine);
          //break;
        }
      }
      fclose (listing);
    } // if (listing)
  }
}

static void loadSystemBook (const char * buf) {
#define bufSz 257
  char filebuf [bufSz];

  FILE * fp = fopen (buf, "r");
  if (! fp) {
    fprintf (stderr, "error opening file %s\n", buf);
    exit (2);
  }

  char * bufp;

// Fill symtab

  // Search for "Symbol  Offset"
  for (;;) {
    bufp = fgets (filebuf, bufSz, fp);
    if (! bufp)
      break;
    if (strncmp (bufp, "Symbol  Offset ", strlen ("Symbol  Offset ")) == 0)
      break;
  }

  // If found
  if (bufp) {
    for (;;) {
      bufp = fgets (filebuf, bufSz, fp);
rescan:
      if (! bufp)
        break;
      if (strncmp (bufp, "Bindmap", strlen ("Bindmap")) == 0)
        break;
// 01234567890123456
// adbyte   10765   badint   35523   begin    12000   bfcksw   43034   bkpt     44616   brkhit   45034
      char * p = bufp;
      for (int i = 0; i < 6; i ++) {
        if (strlen (p) < 14)
          break;
        if (nsyms >= N_SYMS)
          break;
        int n = sscanf (p, "%6s %o", symtab[nsyms].symnam, & symtab[nsyms].symval);
        if (n != 2)
          break;
        nsyms ++;
        p += 14;
        // Either EOL, 3 space column spacing, or ^L
        if (! * p)
          break;
        if (*p == '\n')
          break;
        if (strncmp (p, "   ", 3) == 0) {
          p += 3;
          continue;
        }
        if (* p == '\f') {
          bufp = p + 1;
          goto rescan;
        }  
        fprintf (stderr, "confused at %lu %o '%s'\n", strlen (p), * p, p);
        break;
      } // for i
    } // forever
    qsort (symtab, nsyms, sizeof (struct symtab_s), symcmp);
  } // if bufp

// Fill system book

  // Search for "Component"
  for (;;) {
    bufp = fgets (filebuf, bufSz, fp);
    if (! bufp)
      break;
    if (strncmp (bufp, "Component ", strlen ("Component ")) == 0)
      break;
  }


  // If found
  if (bufp) {
    for (;;) {
      bufp = fgets (filebuf, bufSz, fp);
      if (! bufp)
        break;
      if (strlen (bufp) < 4)
        continue;
// Component                Modnum  Start  Length  Date Compiled   Directory
// scheduler                     1   4540    2436  09/23/82 1211  >user_dir_dir>SysAdmin>a>mcs.7.6c
      char name[bookModuleNameLen];
      int start, length;
      int cnt = sscanf (filebuf, "%24s", name);
      if (cnt != 1)
        continue;
      cnt = sscanf (filebuf + 33, "%o %o", & start, & length);
      if (cnt != 2)
        continue;
      int rc = addBookModule (name, start, length);
      if (rc < 0) {
        fprintf (stderr, "error adding segment name\n");
        fclose (fp);
        exit (2);
      }
    }
  }
  fclose (fp);
  fprintf (stderr, "system book: %d modules.\n", nBookModules);
}

int main (int argc, char * argv []) {
  if (argc == 2) {  // ./annotate address
    setSearchPath (PATH);
    loadSystemBook (BK);
    long offset = strtol (argv[1], NULL, 8);
    char * moduleName = "";
    int moduleOffset;
    char * where = lookupAddress ((uint) offset, & moduleName, & moduleOffset);
    if (where)
      printf ("%05o:  %s\n", (uint) offset, where);
    else
      printf ("can't find %05o\n", (uint) offset);
    return 0;
  }

  if (argc == 3) {
    setSearchPath (argv[1]);
    loadSystemBook (argv[2]);
  } else if (argc == 1) {
    setSearchPath (PATH);
    loadSystemBook (BK);
  } else {
    fprintf (stderr, "annotate [search_path book] < raw > annotated\n");
    exit (1);
  }
// DBG(3299)> CPU TRACE: 01006:673004 ILA 004^M
// DBG(6823349)> TRACE:  02533 673000 (ILA 000)

  char * buf = NULL;
  size_t size;
  ssize_t ret;

  char * moduleName = "";
  int moduleOffset;
  int lastOffset = -1;

  while ((ret = getline (& buf, & size, stdin)) != -1) {
    static long last_cycles = 0;
    unsigned long cycles;
    int cnt = sscanf (buf, "DBG(%lu)", & cycles);
    if (cnt && cycles != last_cycles)
      printf ("\n");
    last_cycles = cycles;
    printf ("%s", buf);
    if (! cnt)
      continue;

    uint offset;
    cnt = sscanf (buf, "%*s TRACE: %o", & offset);
    if (cnt == 1) {
      if (offset != lastOffset) {
        char * where = lookupAddress (offset, & moduleName, & moduleOffset);
        if (where) {
          char * symnam = lookupSym (offset);
          printf ("%05o %s %s\n", offset, where, symnam);
          listSource (moduleName, moduleOffset, 0);
          lastOffset = offset;
        } else {
          lastOffset = 0;
        }
      } else {
        printf ("%s", sourceLine);
      }
    }
  }
}
