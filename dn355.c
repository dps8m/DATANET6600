#include "dn355.h"
#include "cpu.h"
#include "core.h"
#include "iom.h"
#include "caf.h"
#include "dnpkt.h"
#include "coupler.h"
#include "hsla.h"
#include "console.h"
#include "timer.h"
#include "utils.h"
#include "pager.h"
#include "hdbg.h"

char sim_name [] = "dn355";
int32_t sim_emax = 1;
// DEVICE *sim_devices[] table of pointers to simulated devices, NULL terminated
// t_stat sim_load (…) binary loader subroutine
// t_stat parse_sym (…) symbolic instruction parse subroutine
// t_stat fprint_sym (…) symbolic instruction print subroutine 
// void (*sim_vm_init) (void) pointer to once-only initialization routine for VM
// t_addr (*sim_vm_parse_addr) (…) pointer to address parsing routine
// void (*sim_vm_fprint_addr) (…) pointer to address output routine
// char (*sim_vm_read) (…) pointer to command input routine
// void (*sim_vm_post) (…) pointer to command post-processing routine
// CTAB *sim_vm_cmd pointer to simulator-specific command table

//uint64_t sim_deb_start = 0xffffffffffffffff;
uint64_t sim_deb_start = 0;
uint64_t sim_deb_stop = 0;

static t_stat dn_debug_stop (UNUSED int arg, const char * buf) {
  sim_deb_stop = strtoull (buf, NULL, 0);
  sim_printf ("Debug set to stop at cycle: %lu\n", sim_deb_stop);
  return SCPE_OK;
}

static t_stat dn_debug_start (UNUSED int arg, const char * buf) {
  sim_deb_start = strtoull (buf, NULL, 0);
  sim_printf ("Debug set to start at cycle: %lu\n", sim_deb_start);
  return SCPE_OK;
}

static CTAB dn355_cmds[] = {
  {"DBGSTART",            dn_debug_start,          0, "dbgstart: Limit debugging to N > Cycle count\n", NULL, NULL},
  {"DBGSTOP",            dn_debug_stop,          0, "dbgstart: Limit debugging to N < Cycle count\n", NULL, NULL},
#ifdef HDBG
  {"HDBG",               hdbg_size,             0, "Set history debugger buffer size\n",                       NULL, NULL},
  {"PHDBG",              hdbg_print,            0, "Display history size\n",                                   NULL, NULL},
#endif

}; // dn355_cmds


void sim_vm_init (void) {
  coreInit (); // Call first; resets regMap which is set by other inits....
  iomInit ();
  consoleInit ();
  couplerInit ();
  timerInit ();
  cpuInit ();
  hslaInit ();
  sim_vm_cmd = dn355_cmds;
}

// Memory

// 18 bit words, 18 bit Addresses

// INSTRUCTION FORMAT (dd01, pg 304).
//
// Memory Reference Instructions
//
//                                           1   1   1   1   1   1   1   1
//   0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// | I |   T   |    OPCODE             |      D                            |
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
//
// I      Indirect bit: when on, the effective address is computed from the 
//        inidrect word.
// T      Tag Field: specifies address modifcation using an index register (X1,
//        X2, X3, or the instruction counter (IC).
// Opcode Operation code.
// D      Displacement.
//

// The basic method of forming effective addresses consistes of adding the the
// 9-but displacement field (D) to the complete address obtained from one of
// the three index registers of the instruction counter. The displacement (D) 
// is treated as a 9-bit number in 2's complement form. When indirect 
// addressing is used as the address of an indirect word with the following
// format:
//
//                                           1   1   1   1   1   1   1   1
//   0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// | I |   T   |    Y                                                      |
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
//
// This differs from the instruction word, in that Y is an address field
// rather the a displacement field, and no base address is needed to form a
// a full 15-bit address. The I specifies further indirect addressing. [CAC: 
// what about T?]
//
//
// Nonmemory reference instructions
//
// Group 1
//
//                                           1   1   1   1   1   1   1   1
//   0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// |     S1    |    OPCODE             |      D                            |
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
//
//  S1   Suboperation code
//
// Group 2
//                                           1   1   1   1   1   1   1   1
//   0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// |     S1    |    OPCODE             |     S2    |     K                 |
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
//
// S1, S2   Suboperation codes; These two code form a prefix and a suffix
//          to the operation codes and are used to determine the instruction
//          within the group.
// K        Operation value: This field is used for such functions as shift
//          counts.

DEVICE * sim_devices [] =
  {
    & cpuDev,
    & couplerDev,
    & hslaDev,
    & consoleDevice,
    NULL
  };

const char * sim_stop_messages [] = {
  "Unknown error",           // SCPE_OK
  "Simulation stop",         // STOP_STOP
};

t_stat sim_load (FILE * fileref, const char * cptr, const char * fnam, int flag)
  {
    while (! feof (fileref)) {
      char buf [128];
      fgets (buf, sizeof (buf), fileref);
      //sim_printf ("<%s>\n", buf);
      char addrStr[6], dataStr[19];
      if (sscanf (buf, "%5s : %18s", addrStr, dataStr) == 2) {
        //sim_printf ("%s:%s\n", addrStr, dataStr);
        unsigned int addr, data;
        addr = (unsigned int) strtoul (addrStr, NULL, 8);
        data = (unsigned int) strtoul (dataStr, NULL, 2);
        //sim_printf ("%o:%o\n", addr, data);
        cpu.M[addr] = data;
      }
    }
    cpu.booted = true;
    return SCPE_OK;
  }

//Based on the switch variable, parse character string cptr for a symbolic 
// value val at the specified addr in unit uptr.

t_stat parse_sym (UNUSED const char * cptr, UNUSED t_addr addr, UNUSED UNIT * uptr,
                  UNUSED t_value * val, UNUSED int32_t sswitch)
{
// XXX
    return SCPE_ARG;
}

// simh "fprint_sym" – Based on the switch variable, symbolically output to 
// stream ofile the data in array val at the specified addr in unit uptr.

t_stat fprint_sym (FILE * ofile, UNUSED t_addr  addr, t_value *val,
                   UNIT *uptr, int32_t sw)
{
// XXX
    return SCPE_ARG;
}



void bail (void) {
  HDBGPrint ();
  exit (2);
}

