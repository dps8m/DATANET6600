#include "dn355.h"
#include "iom.h"
#include "caf.h"
#include "dnpkt.h"
#include "coupler.h"
#include "console.h"
#include "hsla.h"
#include "timer.h"
#include "core.h"
#include "prt.h"

iom_t iom;

void iomCIOC (void) {
  if (iom.iomState != iomIdle) {
    sim_printf ("CIOC when IOM not idle\n");
    // fault?
  }
  iom.ciocW = cpu.W;
  iom.chan = cpu.rIR & 077;
  iom.iomState = iomConnect;
  // debug
  iom.IC = cpu.rIC;
  DBG_DIA (if (iom.chan == COUPLER_CHANNEL_NO) sim_printf ("CIOC W %05o chan %02o IC %05o cycle %ld\n", iom.ciocW, iom.chan, iom.IC, cpu.cycleCnt);)
}

void iomRun (void) {
  switch (iom.iomState) {

    case iomIdle:
      break;

    case iomConnect: {
      word36 PCW = fromMemory36 (iom.ciocW, 1, __func__);
      //sim_printf ("DEBUG: PCW %06o:%012lo chan %o IC %05o\n", iom.ciocW, PCW, iom.chan, cpu.rIC);
      //iom.status = 0;
      word1 mask = (word1) getbits36 (PCW, MASK_BIT, 1);
      iom.masked[iom.chan] = !! mask;
      if (iom.masked[iom.chan]) {
        //sim_printf ("DEBUG: channel %02o masked\n", iom.chan);
        goto done;
      }

      switch (iom.chan) {
        case CONSOLE_CHANNEL_NO:
          consoleConnect (iom.chan, iom.ciocW, PCW);
          break;

        case PRINTER_CHANNEL_NO:
          printerConnect (iom.chan, iom.ciocW, PCW);
          break;

        case COUPLER_CHANNEL_NO:
          couplerConnect (PCW);
          break;

        case TIMER_CHANNEL_NO:
          timerConnect (PCW);
          break;

        case HSLA1_CHANNEL_NO:
          hslaConnect (iom.chan, 0, PCW);
          break;

        case HSLA2_CHANNEL_NO:
          /*action =*/ hslaConnect (iom.chan, 1, PCW);
          break;

        case HSLA3_CHANNEL_NO:
          /*action =*/ hslaConnect (iom.chan, 2, PCW);
          break;

        default: {
          sim_printf ("ERR: iom don't grok channel %d(%o)\n", iom.chan, iom.chan);
          //word4 cmd = (word4) getbits36 (PCW, 2, 4);
          //if (cmd == 0) {
            word1 mask = (word1) getbits36 (PCW, MASK_BIT, 1);
            iom.masked[iom.chan] = !! mask;
            //break;
          //}
          break;
        }
      } // switch channel
done:
      iom.iomState = iomIdle;
    } // case connect
  } // switch iomState
} // iomRun

void setStatusChar (word15 mbox, word9 status) {
  word18 statusICW = coreRead (mbox + 0, __func__);
  word18 tallyWord = coreRead (mbox + 1, __func__);
  word15 charPtr = (word15) getbits18 (statusICW, 0, 3);
  word15 addr = (word15) getbits18 (statusICW, 3, 15);
  word1 exhaust = (word1) getbits18 (tallyWord, 5, 1);
  word12 tally = (word12) getbits18 (tallyWord, 6, 12);

  if (charPtr != 2 && charPtr != 3) {
    sim_printf ("setStatusChar charPtr (%o) not byte; skipping\n", charPtr);
    return;
  }

  if (tally != 1) {
    sim_printf ("setStatusChar tally (%o) not 1; skipping\n", tally);
    return;
  }
  coreWrite (addr, (word18) status, __func__);

  if (! exhaust) {
    if (tally <= 1) {
      tally = 0;
      exhaust = 1;
    } else {
      tally --;
    }
    putbits18 (& tallyWord, 5, 1, (word18) exhaust);
    putbits18 (& tallyWord, 6, 12, (word18) tally);
    coreWrite (mbox + 1, tallyWord, __func__);
  }
}

void setStatus (word15 mbox, word18 statusEven, word18 statusOdd) {
  // ICW:
  //  bits 0-2:  Character address
  //       3-17: Word address
  //       23 (5): Exhaust
  //       24-35 (6-18): Tally

  word18 statusICW = fromMemory (mbox + 0, __func__);
  word18 tallyWord = fromMemory (mbox + 1, __func__);

  word15 charPtr = (word15) getbits18 (statusICW, 0, 3);
  word15 addr = (word15) getbits18 (statusICW, 3, 15);
  word1 exhaust = (word1) getbits18 (tallyWord, 5, 1);
  word12 tally = (word12) getbits18 (tallyWord, 6, 12);

  sim_printf ("DEBUG: status %06o %06o icw %05o:%06o tally %05o exhaust %o IC %05o\n", statusEven, statusOdd, mbox, statusICW, tally, exhaust, iom.IC);

  if (charPtr == 1 && tally != 1) {
    sim_printf ("WARN: status ICW is 'double word'; status tally is %o, should be 1\n", tally);
    sim_printf ("skipping iom status\n");
    return;
  }

  if ((charPtr == 2 || charPtr == 3) && tally != 2) {
    sim_printf ("WARN: status ICW is 'byte'; status tally is %o, should be 2\n", tally);
    sim_printf ("skipping iom status\n");
    return;
  }

  if (charPtr == 1 && tally == 1) { // double word
    word36 status = (((word36) statusEven) << 18) | statusOdd;
sim_printf ("status36 %012lo @ %05o\n", status, addr);
    toMemory36 (status, addr, 1, __func__);
    putbits18 (& statusICW, 3, 15, (addr + 2) & BITS15); // Bump pointer
    // We know character address is 1, so no fractional update
sim_printf ("statusICW even %06o @ %05o\n", statusICW, mbox);
    toMemory (statusICW, mbox, 0, 0, __func__); // Bump pointer
    // We know tally is one, so exhaust it
sim_printf ("statusICW odd %06o @ %05o\n", 0100, mbox + 1);
    toMemory (0100, mbox + 1, 0, 0, __func__); // Exhaust tally
    return;
  }
  sim_printf ("Error: unhandled setStatus\n");
}

void iomInit (void) {
  memset (& iom, 0, sizeof (iom));
// MCS init() checks to see if devices exist by checking if STEX returns non-zero. The only ones we care about are
// the HSLAs 
  iom.channelStatus[HSLA1_CHANNEL] = 0777777;
  iom.channelStatus[HSLA2_CHANNEL] = 0777777;
  iom.channelStatus[HSLA3_CHANNEL] = 0777777;
  iom.iomState = iomIdle;
}

