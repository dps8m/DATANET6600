// Pages are 256 (0400) words.
// Page table is 128 words.  128*256 -> 32768, so covers all of addressable memory space.
// Maximum memory is 256K words. 262144 / 256 -> 1024 (02000) pages
/// Page table entry:
//
//    pte.r  bool    200             page table entry read only bit
//    pte.s  bool    100             page table entry security bit
//    pte.a  bool    40              page table entry active bit

//    0   1   2 | 3   4   5 | 6   7   8 | 9  10  11 |12  13  14 |15  16  17
//    p   p   p   p   p   p   p   p   p   p   r   s   a           

#define ACTIVE   0040
#define SECURITY 0100
#define READONLY 0200

//  Memory addresses:
//    000475  contains address of page table

#define CRPTE 000475

// map (a, write)
//  page table address <= M[000475]
//  page number = a >> 8
//  page table entry = M[page table address + page number]
//  if not page table entry . active
//    return a
//  if page table entry . security
//    fault
//  if page table entry . read only and write
//     fault
//  real page number = page table entry & 0777400
//  offset a & 0377
//  return real page number | offset

#include "dn355.h"
#include "cpu.h"
#include "core.h"
#include "hdbg.h"

static word18 map (word15 address, bool write) {
  word18 ptp = cpu.M[CRPTE];
  if (ptp == 0)
    return address;
  unsigned int pageNumber = (address >> 8) & 0177;
  word18 pte = cpu.M[ptp + pageNumber];
  if (! (pte & ACTIVE))
    return address;
  if (pte & SECURITY)
    doFault (293 /* XXX Guess emflt */, "map");
  if (pte & READONLY && write)
    doFault (293 /* XXX Guess emflt */, "map");
  word18 realPageAddr = pte & 0777400;
  word18 offset = address & 0377;
  word18 mapped = realPageAddr | offset;
  HDBGMap (address, write, mapped, __func__);
  return mapped;
}

word18 memoryRead (word15 address, const char * ctx) {
  return coreRead (map (address, false), ctx) & BITS18;
}

void memoryWrite (word15 address, word18 data, const char * ctx) {
  coreWrite (map (address, true), data & BITS18, ctx);
}


