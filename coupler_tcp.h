#include "dnpkt.h"

#define CS_SEND_READ(address) { \
  dnPkt pkt; \
  memset (& pkt, 0, sizeof (pkt)); \
  pkt.cmd = DN_CMD_READ; \
  sprintf (pkt.cmdRead.addr, "%08o", address); \
  csSend (& pkt); \
}

#define CS_SEND_READ_CLEAR(address) { \
  dnPkt pkt; \
  memset (& pkt, 0, sizeof (pkt)); \
  pkt.cmd = DN_CMD_READ_CLEAR; \
  sprintf (pkt.cmdRead.addr, "%08o", address); \
  csSend (& pkt); \
}

#define CS_SEND_WRITE(a, d) { \
  dnPkt pkt; \
  memset (& pkt, 0, sizeof (pkt)); \
  pkt.cmd = DN_CMD_WRITE; \
  sprintf (pkt.cmdWrite.data, "%08o:%012lo", a, d); \
  csSend (& pkt); \
}

#define CS_SEND_SXC(intrLvl) { \
  dnPkt pkt; \
  memset (& pkt, 0, sizeof (pkt)); \
  pkt.cmd = DN_CMD_SET_EXEC_CELL; \
  sprintf (pkt.cmdSet.cell, "%02o", intrLvl); \
  csSend (& pkt); \
}

#define CS_SEND_DISC() { \
  dnPkt pkt; \
  memset (& pkt, 0, sizeof (pkt)); \
  pkt.cmd = DN_CMD_DISCONNECT; \
  csSend (& pkt); \
}

t_stat couplerAttach (UNIT * uptr, const char * cptr);
t_stat couplerDetach (UNIT * uptr);
void csSend (dnPkt * pkt);

