#include "dn355.h"
#include "cpu.h"
#include "iom.h"
#include "caf.h"
#include "utils.h"
#include "console.h"
#include "uvutil.h"

// dd01 pg 5-70 (243)
//   I/O channel 0
// Interrupt vectors
//   IOM detected fault 00
//   Special            01
//   Terminate          02
//     Terminate may be caused by:
//       ICW PTRO
//       TRO
//       CR (015)
//       ^X (030)
//       XOFF (023)
//       30 second timer.
//     Always preceded by normal status store
//
//  PCW    23 mask
//      30-35 Opcode
//
//  opcode  Command         MAP nmemonic
//    00    Request status  REQS
//    44    Write mode      WTTY
//    50    Read mode       RTTY
//    54    Wraparound mode
//    
//  Status  460, 461
//  Data    463, 463
//
//  Status return character
//     Bit   Meaning
//      0    Ready
//      1    Timer runout
//      2    Tally runout
//      3    Pre-tally runout
//      4    Transfer timing error
//      5    Control character
//      6    Connect while busy
//      7    Illegal PCW
//      8    Parity on read

// ************************************************************************
// *
// * console "special interrupt" processing routine
// *
// * the "special interrupt" is caused by pressing the "break" key on the
// * console tty.  the routine "spcon" is called in response to this
// * interrupt.  this is the method to be used by the operator to request
// * the performance of the several special operations provided.
// *
// * the routine responds to the special interrupt by printing "???" to
// * which the operator is expected to type in one of the commands
// * listed below --
// *
// *      command variable(s)     function
// *
// *      abort                   abort 355.  cause an immediate dump.
// *      alter   aaaaa,bbbbbb    store bbbbbb in location aaaaa, absolute.
// *      peek    aaaaa           write contents of loc aaaaa, absolute
// *      peek    aaaaa,n         write n words starting at aaaaa.
// *      test                    call the on-line t&d system.
// *
// * n.b. -- future developers
// *      command words must be at least four (4) characters in length.  to
// *      increase the number of variable fields, see the comments in the
// *      "idx" subroutine.


static struct {

  unsigned char *tailp;
  unsigned char *readp;

  // DCW list processing
  word14 W;
  word36 LICW;
  word15 licwY;
  int licwZ;

  enum console_mode { opc_no_mode, opc_read_mode, opc_write_mode } io_mode;

  // stuff saved from the Read command
  uint tally;
  uint daddr;
  int chan;
  time_t startTime;

  // telnet console
  bool telentAttached;
  uv_access console_access;

  //bool brkPressed; 

#define bufsize 257
  unsigned char keyboardLineBuffer[bufsize];


} consoleData;

static t_stat consoleReset (DEVICE * dev) {
  memset (& consoleData, 0, sizeof (consoleData));
  consoleData.io_mode = opc_no_mode;
  consoleData.tailp = consoleData.keyboardLineBuffer;
  consoleData.readp = consoleData.keyboardLineBuffer;
  return SCPE_OK;
}

#ifdef REMOTE_CONSOLE
static void startRemoteConsole (void) {
  consoleData.console_access.connected     = NULL;
  consoleData.console_access.useTelnet     = true;
  uv_open_access (& consoleData.console_access);
  sim_printf ("console on port %d\n", consoleData.console_access.port);
}
#endif

void consoleInit (void) {
  consoleReset (& consoleDevice);
}

static DEBTAB consoleDT [] = {
  { "DEBUG", DBG_DEBUG },
  { NULL, 0 }
};

static UNIT consoleUnit = {
  UDATA (NULL, 0, 0), 0, 0, 0, 0, 0, NULL, NULL
};

static t_stat opc_set_console_port (UNIT * uptr, UNUSED int32_t value, const char * cptr, UNUSED void * desc) {
  if (cptr) {
    int port = atoi (cptr);
    if (port < 0 || port > 65535) // 0 is 'disable'
      return SCPE_ARG;
    consoleData.console_access.port = port;
    if (port != 0)
      sim_printf ("[OPC emulation: TELNET server port set to %d]\n", (int)port);
#ifdef REMOTE_CONSOLE
    startRemoteConsole ();
#endif
  } else {
    consoleData.console_access.port = 0;
  }
  return SCPE_OK;
}

static t_stat opc_show_console_port (UNUSED FILE * st, UNIT * uptr, UNUSED int val, UNUSED const void * desc) {
  if (consoleData.console_access.port)
    sim_printf("port     : %d", consoleData.console_access.port);
  else
    sim_printf("port     : disabled");
  return SCPE_OK;
}

static t_stat opc_set_console_address (UNIT * uptr, UNUSED int32_t value, const char * cptr, UNUSED void * desc) { 
  if (consoleData.console_access.address) {
    FREE (consoleData.console_access.address);
    consoleData.console_access.address = NULL;
  }

  if (cptr) {
    consoleData.console_access.address = strdup (cptr);
    sim_printf ("Console address set to %s\n", consoleData.console_access.address);
  }

  return SCPE_OK;
}

static t_stat opc_show_console_address (UNUSED FILE * st, UNIT * uptr, UNUSED int val, UNUSED const void * desc) {
  if (consoleData.console_access.address)
    sim_printf("address  : %s", consoleData.console_access.address);
  else
    sim_printf("address  : any");
  return SCPE_OK;
}


static MTAB consoleMod [] = {
  {
    MTAB_dev_valr_nouc,             /* Mask               */
    0,                              /* Match              */
    "PORT",                         /* Print string       */
    "PORT",                         /* Match string       */
    opc_set_console_port,           /* validation routine */
    opc_show_console_port,          /* Display routine    */
    "Set the console port number",  /* Value descriptor   */
    NULL                            /* Help               */
  },

  {
    MTAB_dev_valr_nouc,             /* Mask               */
    0,                              /* Match              */
    "ADDRESS",                      /* Print string       */
    "ADDRESS",                      /* Match string       */
    opc_set_console_address,        /* Validation routine */
    opc_show_console_address,       /* Display routine    */ 
    "Set the console IP Address",   /* Value descriptor   */
    NULL                            /* Help               */
  },

  { 0, 0, NULL, NULL, NULL, NULL, NULL, NULL }
};

DEVICE consoleDevice = {
  "OPC",        /* name */
  & consoleUnit,    /* units */
  NULL,             /* registers */
  consoleMod,       /* modifiers */
  1,                /* #units */
  8,                /* address radix */
  15,               /* address width */
  1,                /* address increment */
  8,                /* data radix */
  18,               /* data width */
  NULL,             /* examine routine */
  NULL,             /* deposit routine */
  consoleReset,     /* reset routine */
  NULL,             /* boot routine */
  NULL,             /* attach routine */
  NULL,             /* detach routine */
  NULL,             /* context */
  DEV_DEBUG,        /* flags */
  0,                /* debug control flags */
  consoleDT,        /* debug flag names */
  NULL,             /* memory size change */
  NULL,             /* logical name */
  NULL,             // attach help
  NULL,             // help
  NULL,             // help context
  NULL,             // device description
};

// Pending: startDCW will be re-called by a completion routine
// Next: process the next DCW

#define DCW_PENDING 0
#define DCW_NEXT 1

static int consoleCmd (word6 cmd, word18 W, word6 X, word15 Y, word12 Z) {
  sim_printf ("DEBUG: console cmd %02o W %06o X %02o Y %05o Z %04o IC %05o\n", cmd, W, X, Y, Z, cpu.rIC);
  return DCW_NEXT;
}

static void startDCW (void) {
reentry: ;
sim_printf ("DEBUG: %s\n", __func__);
  if (consoleData.licwZ <= 0) { // XXX DCW list exhausted
sim_printf ("DEBUG: %s DCW list exhaused, interrupting...\n", __func__);
    //setIOMStatus ();
    setStatusChar (ADDR_IOM_MBX_CONSOLE_STATUS_ICW, 400);

    doIntr (2, CONSOLE_CHANNEL_NO, "console startDCW");
    return;
  }

  // Get the next DCW
  word36 DCW1 = fromMemory36 (consoleData.licwY, 1, __func__);
  word36 DCW2 = fromMemory36 ((consoleData.licwY + 2) & BITS15, 1, __func__);

  // Update LICW
  consoleData.licwY += 4;
  putbits36 (& consoleData.LICW, 3, 15, consoleData.licwY); // Set E=0, Z;
  if (consoleData.licwZ >= 2)
    consoleData.licwZ -= 2;
  else
    consoleData.licwZ = 0;
  if (consoleData.licwZ)
    putbits36 (& consoleData.LICW, 23, 13, (word36) consoleData.licwZ); // Set E=0, Z;
  else
    putbits36 (& consoleData.LICW, 23, 13, 010000); // Set E=1, Z=0;
  putbits36 (& consoleData.LICW, 3, 15, consoleData.licwY); // 
  
  word6  dcwC = (word6) getbits36 (DCW1, 30, 6);
  word18 dcwW = (word18) getbits36 (DCW1, 0, 18);
  word6  dcwX = (word6) getbits36 (DCW1, 24, 6);
  word15 dcwY = (word15) getbits36 (DCW2, 3, 15);
  word12 dcwZ = (word12) getbits36 (DCW2, 24, 12);
//sim_printf ("DEBUG: dia DCW1 %012lo DCW2 %012lo\n", DCW1, DCW2);
//sim_printf ("DEBUG: dia DCW C %02o W %06o X %02o Y %05o Z %06o\n", dcwC, dcwW, dcwX, dcwY, dcwZ);
  int rc = consoleCmd (dcwC, dcwW, dcwX, dcwY, dcwZ);
  if (rc == DCW_NEXT)
    goto reentry;
}


void consoleConnect (uint channelNumber, word14 W, word36 PCW) {
  consoleData.W = W;
  //sim_printf ("DEBUG: console PCW %012lo\n", PCW);
  word6 pcwCmd = (word1) getbits36 (PCW, 30, 6);
  // Ready
  word9 status = 0400;

#if 0
sim_printf ("DEBUG: console pcwCmd %02o\n", pcwCmd);
  if (pcwCmd == 072) { // CCA command?
    // Get the LICW
    word15 pcwY = (word15) getbits36 (PCW, 3, 15);
    word36 LICW = fromMemory36 (pcwY, 1, __func__);

    word15 Y = (word15) getbits36 (LICW, 3, 15);
    word18 Z = (word18) getbits36 (LICW, 24, 12);
sim_printf ("DEBUG: LICW %012lo Y %05o Z %012o\n", LICW, Y, Z);

    consoleData.LICW = LICW;
    consoleData.licwY = Y;
    consoleData.licwZ = Z;

    startDCW ();
    return;
  } else if (pcwCmd == 000) {
    word1 mask = (word1) getbits36 (PCW, MASK_BIT, 1);
    iom.masked[channelNumber] = !! mask;
  } else {
    sim_printf ("ERR: %s PCW CMD %o\n", __func__, pcwCmd);
  }
#endif
  switch (pcwCmd) {
    case 000: { // request status
      //sim_printf ("DEBUG: console request status\n");
      word1 mask = (word1) getbits36 (PCW, MASK_BIT, 1);
      iom.masked[channelNumber] = !! mask;
      // Don't set status or do interrupt if masked.
      if (mask)
        return;
      break;
    } // request status

    case 044: { // write
      //sim_printf ("DEBUG: console write IC %05o cycles %ld\n", cpu.rIC, cpu.cycleCnt);
      //word18 ICW = fromMemory (ADDR_IOM_MBX_CONSOLE_DATA_ICW, 0, __func__);
      word18 ICW = fromMemory (ADDR_IOM_MBX_CONSOLE_DATA_ICW, __func__);
      //sim_printf ("ICW %06o\n", ICW);
      //word18 statusICW = fromMemory (ADDR_IOM_MBX_CONSOLE_STATUS_ICW, 0, __func__);
      //word18 statusICW = fromMemory (ADDR_IOM_MBX_CONSOLE_STATUS_ICW, __func__);
// The ICW appers to be the address of a 2 word structure
//
//        05270  2 05320    1         2554 iwcrlf icw     l.v004,b.0,2
//        05271  000002     0         
//
//        05320  015012     0         2582 l.v004 vfd     9/cr,9/lf
//
// A character pointer and a byte count
//
      word18 strptr = ICW;
      //word18 strcnt = fromMemory (ADDR_IOM_MBX_CONSOLE_DATA_ICW + 1, 0, __func__);
      word18 strcnt = fromMemory (ADDR_IOM_MBX_CONSOLE_DATA_ICW + 1, __func__);

      word18 p = strptr;
      //for (uint i = 0; i < strcnt; i ++) {
        //word18 ch = fromMemoryPtrInc (& p, __func__);
        //sim_printf ("   %3d %06o\n", i, ch);
      //}
      //p = strptr;
      sim_printf ("CONS: '");
      for (uint i = 0; i < strcnt; i ++) {
        word18 ch = fromMemoryPtrInc (& p, __func__);
        if (isprint (ch))
          sim_printf ("%c", ch);
        else
          sim_printf ("\\%03o", ch);
      }
      sim_printf ("'\n");
      break;
    } // write

    case 050: { // read
      sim_printf ("DEBUG: console read\n");
      break;
    } // read

    case 054: { // wraparound
      sim_printf ("DEBUG: console wraparound\n");
      break;
    } // write

    default: {
      sim_printf ("unhandled console pcwCmd %06o\n", pcwCmd);
    }
  } // cmd
// console_man.map355
//   console status word
//     line 384. 400000
//     line 308  PTRO, Control char
//          000050
//     line 442 "process write termination status"  Ready 400000
//     line 469 "process write termination status"  PTRO 000040
//     line 495 "process read termination status"  timer runout 000200
//     line 501 "process read termination status"  TRO 000100
//     line 513 "process read termination status"  Ready 400000

//  400000 Ready
//  200000 Timer runout
//  100000 Tally runout
//  040000 Pre-tally runout
//  020000 Transfer timing error
//  010000 Control char
//  040000 Connect while busy
//  020000 Illegal PCW
//  010000 Read parity

  setStatusChar (ADDR_IOM_MBX_CONSOLE_STATUS_ICW, status);
// Verfied; dn355 trace shows vector to 
//        ttls    contip -- terminate interrupt ptocessor for wcon
// contip ind     **

  doIntr (2, 0, "consoleConnect");
}

#ifndef TA_BUFFER_SIZE
# define TA_BUFFER_SIZE 256
#endif

static int ta_buffer[TA_BUFFER_SIZE];
static uint ta_cnt  = 0;
static uint ta_next = 0;
static bool ta_ovf  = false;

static void ta_flush (void) {
  ta_cnt = ta_next = 0;
  ta_ovf = false;
}

static void ta_push (int c) {
  // discard overflow
  if (ta_cnt >= TA_BUFFER_SIZE) {
    if (! ta_ovf)
      sim_printf ("typeahead buffer overflow");
    ta_ovf = true;
    return;
  }
  ta_buffer [ta_cnt ++] = c;
}

static int ta_peek (void) {
  if (ta_next >= ta_cnt)
    return SCPE_OK;
  int c = ta_buffer[ta_next];
  return c;
}

static int ta_get (void) {
  if (ta_next >= ta_cnt)
    return SCPE_OK;
  int c = ta_buffer[ta_next ++];
  if (ta_next >= ta_cnt)
    ta_flush ();
  return c;
}


static void console_putchar (char ch) {
  //sim_putchar (ch);  
  accessStartWrite (consoleData.console_access.client, & ch, 1);
}

static void console_putstr (char * str) {
  size_t l = strlen (str);
  for (size_t i = 0; i < l; i ++)
    sim_putchar (str[i]);
  accessStartWrite (consoleData.console_access.client, str, (ssize_t) l);
}

// Send entered text to the IOM.
static void sendConsole (word12 stati) { 
  sim_printf ("UNIMP sendConsole %06o\n", stati);
}

#ifdef REMOTE_CONSOLE
void consoleProcess (void) {
  int c;
  // Move data from keyboard buffers into type-ahead buffer
  for (;;) {
    c = accessGetChar (& consoleData.console_access);

    // Check for stop signaled by scp

    if (stop_cpu) {
      console_putstr (" Got <sim stop> \n");
      return;
    }

    // Check for ^C

    if (c == SCPE_STOP) {
      console_putstr (" Got <sim stop> \n");
      stop_cpu = 1;
      return; // User typed ^C to stop simulation
    }

    // End of available input

    if (c == SCPE_OK)
      break;

    // sanity test

    if (c < SCPE_KFLAG) {
      sim_printf ("impossible %d %o\n", c, c);
      continue; // Should be impossible
    }

    // translate to ascii

    int ch = c - SCPE_KFLAG;

    // ESC or ^B is pressed, signal "break key pressed"
    if (ch == '\033' || ch == '\002') { // escape or ^A
      //if (consoleData.attn_flush)
        ta_flush ();
      //consoleData.brkPressed = true;
      doIntr (1, CONSOLE_CHANNEL_NO, "consoleProcess brk");
      continue;
    }

    if (ch == 024) { // ^T
      char buf[256];
      //char cms[3] = "?RW";
      // XXX Assumes console 0
      //sprintf (buf, "^T attn %c %c cnt %d next %d\n", console_state[0].brkPressed+'0', cms[console_state[0].io_mode], ta_cnt, ta_next);
      sprintf (buf, "^T rIC %06o DIS %o\n", cpu.rIC, cpu.disState);
      console_putstr (buf);
      continue;
    }

    // Save the character

    ta_push (c);
  }

//// Check for stop signaled by scp

  if (stop_cpu) {
    console_putstr (" Got <sim stop> \n");
    return;
  }

//// Read mode 
////   Check for timeout

  if (consoleData.io_mode == opc_read_mode &&
      consoleData.tailp == consoleData.keyboardLineBuffer) {
    if (consoleData.startTime + 30 < time (NULL)) {
      console_putstr ("CONSOLE: TIMEOUT\n");
      consoleData.readp = consoleData.keyboardLineBuffer;
      consoleData.tailp = consoleData.keyboardLineBuffer;
      sendConsole (04310); // Null line, status operator distracted
    }
  }

//// Peek at the character in the typeahead buffer

  c = ta_peek ();

    // No data
  if (c == SCPE_OK)
    return;

  // Convert from scp encoding to ASCII
  if (c < SCPE_KFLAG) {
    sim_printf ("impossible %d %o\n", c, c);
    return; // Should be impossible
  }

  // translate to ascii

  int ch = c - SCPE_KFLAG;

// XXX This is subject to race conditions
  if (consoleData.io_mode != opc_read_mode) {
    if (ch == '\033' || ch == '\002') { // escape or ^B
      ta_get ();
      //consoleData.brkPressed = true;
      doIntr (1, CONSOLE_CHANNEL_NO), "consoleProcess brk2";
    }
    return;
  }

  if (ch == '\177' || ch == '\010') { // backspace/del
    ta_get ();
    if (consoleData.tailp > consoleData.keyboardLineBuffer) {
      * consoleData.tailp = 0;
      -- consoleData.tailp;
      console_putstr ("\b \b");
    }
    return;
  }

  if (ch == '\022') { // ^R
    ta_get ();
    console_putstr ("^R\n");
    for (unsigned char * p = consoleData.keyboardLineBuffer; p < consoleData.tailp; p ++)
      console_putchar ((char) (*p));
    return;
  }

  if (ch == '\025') { // ^U
    ta_get ();
    console_putstr ("^U\n");
    consoleData.tailp = consoleData.keyboardLineBuffer;
    return;
  }

  if (ch == '\030') { // ^X
    ta_get ();
    console_putstr ("^X\n");
    consoleData.tailp = consoleData.keyboardLineBuffer;
    return;
  }

  if (ch == '\012' || ch == '\015') { // CR/LF
    ta_get ();
    console_putstr ("\n");
    sendConsole (04000); // Normal status
    return;
  }

  if (ch == '\033' || ch == '\004' || ch == '\032') { // ESC/^D/^Z
    ta_get ();
    console_putstr ("\n");
    // Empty input buffer
    consoleData.readp = consoleData.keyboardLineBuffer;
    consoleData.tailp = consoleData.keyboardLineBuffer;
    sendConsole (04310); // Null line, status operator  distracted
    console_putstr ("CONSOLE: RELEASED\n");
    return;
  }

  if (isprint (ch)) {
    // silently drop buffer overrun
    ta_get ();
    if (consoleData.tailp >= consoleData.keyboardLineBuffer + sizeof (consoleData.keyboardLineBuffer))
      return;

    * consoleData.tailp ++ = (unsigned char) ch;
    console_putchar ((char) ch);
    return;
  }
  // ignore other chars...
  ta_get ();
}

#else
void consoleProcess (void) {
  int c;
  // Move data from keyboard buffers into type-ahead buffer
  for (;;) {
    c = sim_poll_kbd ();
#if 0
    if (c == SCPE_OK)
      c = accessGetChar (& consoleData.console_access);
#endif

    // Check for stop signaled by scp

    if (stop_cpu) {
      console_putstr (" Got <sim stop> \n");
      return;
    }

    // Check for ^C

    if (c == SCPE_STOP) {
      console_putstr (" Got <sim stop> \n");
      stop_cpu = 1;
      return; // User typed ^C to stop simulation
    }

    // End of available input

    if (c == SCPE_OK)
      break;

    // sanity test

    if (c < SCPE_KFLAG) {
      sim_printf ("impossible %d %o\n", c, c);
      continue; // Should be impossible
    }

    // translate to ascii

    int ch = c - SCPE_KFLAG;
//if (ch) sim_printf ("got %o\n", ch);

    // ESC or ^B is pressed, signal "break key pressed"
    if (ch == '\033' || ch == '\002') { // escape or ^B
      //if (consoleData.attn_flush)
        ta_flush ();
//sim_set_debon (0, "-n dn355.debug");
      //consoleData.brkPressed = true;
      setStatusChar (ADDR_IOM_MBX_CONSOLE_STATUS_ICW, 0400 /* ready */);
      doIntr (1, CONSOLE_CHANNEL_NO, "consoleProcess brk3");
      //continue;
      return;
    }

    if (ch == 023) { // ^S
      iom.channelStatus[077] = 0777777;
      continue;
    }

    if (ch == 024) { // ^T
      char buf[256];
      //char cms[3] = "?RW";
      // XXX Assumes console 0
      //sprintf (buf, "^T attn %c %c cnt %d next %d\n", console_state[0].brkPressed+'0', cms[console_state[0].io_mode], ta_cnt, ta_next);
      sprintf (buf, "^T rIC %06o DIS %o\n", cpu.rIC, cpu.disState);
      console_putstr (buf);
      continue;
    }

    // Save the character

    ta_push (c);
  } // for (;;) move data into typeahead buffer

//// Check for stop signaled by scp

  if (stop_cpu) {
    console_putstr (" Got <sim stop> \n");
    return;
  }

//// Read mode and nothing in console buffer
////   Check for timeout

  if (consoleData.io_mode == opc_read_mode &&
      consoleData.tailp == consoleData.keyboardLineBuffer) {
    if (consoleData.startTime + 30 < time (NULL)) {
      console_putstr ("CONSOLE: TIMEOUT\n");
      consoleData.readp = consoleData.keyboardLineBuffer;
      consoleData.tailp = consoleData.keyboardLineBuffer;
      sendConsole (04310); // Null line, status operator distracted
    }
  }

//// Peek at the character in the typeahead buffer

  c = ta_peek ();

    // No data
  if (c == SCPE_OK)
    return;

  // Convert from scp encoding to ASCII
  if (c < SCPE_KFLAG) {
    sim_printf ("impossible %d %o\n", c, c);
    return; // Should be impossible
  }

  // translate to ascii

  int ch = c - SCPE_KFLAG;

// XXX This is subject to race conditions
  if (consoleData.io_mode != opc_read_mode) {
    if (ch == '\033' || ch == '\002') { // escape or ^B
      ta_get ();
      //consoleData.brkPressed = true;
      doIntr (1, CONSOLE_CHANNEL_NO, "consoleProcess brk4");
    }
    return;
  }

  if (ch == '\177' || ch == '\010') { // backspace/del
    ta_get ();
    if (consoleData.tailp > consoleData.keyboardLineBuffer) {
      * consoleData.tailp = 0;
      -- consoleData.tailp;
      console_putstr ("\b \b");
    }
    return;
  }

  if (ch == '\022') { // ^R
    ta_get ();
    console_putstr ("^R\n");
    for (unsigned char * p = consoleData.keyboardLineBuffer; p < consoleData.tailp; p ++)
      console_putchar ((char) (*p));
    return;
  }

  if (ch == '\025') { // ^U
    ta_get ();
    console_putstr ("^U\n");
    consoleData.tailp = consoleData.keyboardLineBuffer;
    return;
  }

  if (ch == '\030') { // ^X
    ta_get ();
    console_putstr ("^X\n");
    consoleData.tailp = consoleData.keyboardLineBuffer;
    return;
  }

  if (ch == '\012' || ch == '\015') { // CR/LF
    ta_get ();
    console_putstr ("\n");
    sendConsole (04000); // Normal status
    return;
  }

  if (ch == '\033' || ch == '\004' || ch == '\032') { // ESC/^D/^Z
    ta_get ();
    console_putstr ("\n");
    // Empty input buffer
    consoleData.readp = consoleData.keyboardLineBuffer;
    consoleData.tailp = consoleData.keyboardLineBuffer;
    sendConsole (04310); // Null line, status operator  distracted
    console_putstr ("CONSOLE: RELEASED\n");
    return;
  }

  if (isprint (ch)) {
    // silently drop buffer overrun
    ta_get ();
    if (consoleData.tailp >= consoleData.keyboardLineBuffer + sizeof (consoleData.keyboardLineBuffer))
      return;

    * consoleData.tailp ++ = (unsigned char) ch;
    console_putchar ((char) ch);
    return;
  }
  // ignore other chars...
  ta_get ();
}
#endif
