// ICW are data pointers
//
//  012 3          16 17 18    22 23  24       35
// | C |      Y      | 0|  MBZ   | E |  TALLY    |
//
//  C
//   W.1  0   16 bit data
//   w.2  1   36 bit data
//   B.0  2    9 bit data, char 0
//   B.1  3    9 bit data, char 1
//   C.0  4    6 bit data, char 0
//   C.1  5    6 bit data, char 1
//   C.2  6    6 bit data, char 2
//   I    7   idle, no data
//
// Y Address
// E Exhaust bit; if set, do not increment address and tally fields
// TALLY

struct hslaSubChnl_s {
  word36 ric0;  // ric0  Primary recieve ICW
  word36 ric1;  // ric1  Alternate recieve ICW
  word36 sic0;  // sic0  Primary send ICW
  word36 sic1;  // sic1  Alternate send ICW
  word18 baw;    // baw   Base address word
  word18 sfcm;  // sfcm  S/W comm. region address
  word36 mask;  // mask  Mask register  (set by CIOC)
  word36 aicw;  // aicw  Active status ICW  (writable)
  word36 cnfg;  // cnfg  Configuration status ICW  (readable)

  // TCP, TELNET
  uv_tcp_t * line_client;

  word1 sync;     // 0 - async, 1 - sync
  word4 char_len; // 1100 5 bit chars
                  // 1101 6 bit chars
                  // 1110 7 bit chars
                  // 1111 8 bit chars
  word1 rcv_parity; // 1 - check parity
  word1 xmt_parity; // 1 - generate parity
  word1 parity_odd; // 1 - odd parity
  word1 two_icws;   // 1 - alt. cont. word
  word1 cct_enable; // 1 - use baw
  word1 two_stop;   // 1 - two stop bits
  // speed bits
  //word1 b110;
  //word1 b134_5;
  //word1 b150;
  //word1 b300;
  //word1 b1050;
  //word1 b1200;
  //word1 b1800;
  //word1 b75_600;
  word8 speed_bits;
};


struct hsla_s {
  bool running;
  //bool subchannelMask[32];
  bool mask;
  struct hslaSubChnl_s subChnl[N_HSLA_LINES];
};


struct hslaData_s {
  struct hsla_s hsla[N_HSLAS];

  // TCP, TELNET
  bool started; 
  int telnetPort;
  char * telnetAddress;
  uv_tcp_t du_server;
};


extern struct hslaData_s hslaData;

extern DEVICE hslaDev;
void hslaConnect (uint channelNumber, int unitNumver /* 1-3*/, word36 PCW);
//int hslaPoll (uint8_t * * pktp);
void hslaInit (void);
void offhookCmdReceived (uv_stream_t * req, dnPkt * pkt);
void offhookEvent (uint lineNo);


