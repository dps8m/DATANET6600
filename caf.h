//
//  caf.h
//  dn355
//
//  Created by Harry Reed on 12/31/15.
//
// header file to help support dn355 enulator
//

#ifndef caf_h
#define caf_h

#include <stdbool.h>


word18 fromMemoryPtrInc (word18 * ptr, const char * ctx);
//word18 fromMemory  (word15 addr, int charAddr);
word18 fromMemory  (word18 addr, const char * ctx);
word36 fromMemory36(word15 addr, int charaddr, const char * ctx);

void toMemory1  (word18 data, word18 addr, word18 zoneMask, const char * ctx);
void toMemory  (word18 data, word15 addr, word3 charaddr, word18 zoneMask, const char * ctx);
void toMemory36(word36 data, word15 addr, word3 charaddr, const char * ctx);

//bool doCAF(bool i, word2 t, word9 d, word15 *w, word3 *c);
word18 doCAF (void);

word18 readITD  (bool i, word2 t, word9 d);
word36 readITD36(bool i, word2 t, word9 d);

void writeITD  (word18 data, bool i, word2 t, word9 d);
void writeITD36(word36 data, bool i, word2 t, word9 d);

bool addAddr32(int wx, word3 cx, int wy, word3 cy, word15 *wz, word3 *cz);

word9 getICWChar (word15 addr);

#endif /* caf_h */
