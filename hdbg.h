/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: 5e1e2bcf-f62f-11ec-8cca-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2016 Charles Anthony
 * Copyright (c) 2021-2022 The DPS8M Development Team
 *
 * All rights reserved.
 *
 * This software is made available under the terms of the ICU
 * License, version 1.8.1 or later.  For more details, see the
 * LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

#ifndef HDBG_H
# define HDBG_H
# ifdef HDBG

extern int hdbgSnap;

extern char * hdbgXRegNames[4];

void hdbg_mark (void);
t_stat hdbg_size (int32_t arg, UNUSED const char * buf);
t_stat hdbg_print (int32_t arg, UNUSED const char * buf);

void hdbgTrace (word15 ic, word18 ins, const char * ctx);
void hdbgPrint (void);
void hdbgCoreRead (word18 addr, word18 data, const char * ctx);
void hdbgCoreWrite (word18 addr, word18 data, const char * ctx);
void hdbgMap (word15 address, int write, word18 mapped, const char * ctx);
#ifdef NOTYET
void hdbgFault (_fault faultNumber, _fault_subtype subFault, const char * faultMsg, const char * ctx);
#endif
void hdbgIntrSet (uint level, uint sublevel, const char * ctx);
void hdbgIntr (uint level, uint sublevel, word15 intrAddr, const char * ctx);
void hdbgNote (const char * ctx, const char * fmt, ...)
#  ifdef __GNUC__
  __attribute__ ((format (printf, 2, 3)))
#  endif
;
void hdbgNote0 (const char * ctx, const char * msg);

void hdbgRegR (const char * name, word18 data);
void hdbgRegW (const char * name, word18 data);

#  define HDBGCoreRead(a, d, c) hdbgCoreRead (a, d, c)
#  define HDBGCoreWrite(a, d, c) hdbgCoreWrite (a, d, c)
#  define HDBGMap(a, w, m, c) hdbgMap (a, w, m, c)
#  define HDBGRegAR() hdbgRegR ("A", cpu.rA)
#  define HDBGRegAW() hdbgRegW ("A", cpu.rA)
#  define HDBGRegQR() hdbgRegR ("Q", cpu.rQ)
#  define HDBGRegQW() hdbgRegW ("Q", cpu.rQ)
#  define HDBGRegXR(i) /* hdbgRegR (hdbgXRegNames[i], (word18) (i == 1 ? cpu.rX1 : i == 2 ? cpu.rX2 : i == 3 ? cpu.rX3 : 0777777)) */
#  define HDBGRegXW(i) /* hdbgRegW (hdbgXRegNames[i], (word18) (i == 1 ? cpu.rX1 : i == 2 ? cpu.rX2 : i == 3 ? cpu.rX3 : 0777777)) */
#  define HDBGRegYR() hdbgRegR (hreg_Y, (word18) cpu.rY)
#  define HDBGRegYW() hdbgRegW (hreg_Y, (word18) cpu.rY)
#  define HDBGRegIR() hdbgRegR ("IR", cpu.rIR)
#  define HDBGRegIW() hdbgRegW ("IR", cpu.rIR)
#  define HDBGRegICR() /* hdbgRegR ("IC", cpu.rIC) */
#  define HDBGRegICW() /* hdbgRegW ("IC", cpu.rIC) */
#  define HDBGRegNXR() /* hdbgRegR ("NEXT_IC", cpu.NEXT_IC) */
#  define HDBGRegNXW() /* hdbgRegW ("NEXT_IC", cpu.NEXT_IC)*/
#  define HDBGTrace(a, i, c) hdbgTrace(a, i, c)
#  define HDBGNote(c, f, ...) hdbgNote(c, f, __VA_ARGS__)
#  define HDBGNote0(c, m) hdbgNote0(c, m)
#  define HDBGIntrSet(l, s, c) hdbgIntrSet(l, s, c)
#  define HDBGIntr(l, s, i, c) hdbgIntr(l, s, i, c)
#  define HDBGFault(n, s, m, c) hdbgFault(n, s, m, c)
#  define HDBGPrint() hdbgPrint()
# else
#  define HDBGCoreRead(a, d, c)
#  define HDBGCoreWrite(a, d, c)
#  define HDBGMap(a, w, m, c)
#  define HDBGRegAR()
#  define HDBGRegAW()
#  define HDBGRegQR()
#  define HDBGRegQW()
#  define HDBGRegXR(i)
#  define HDBGRegXW(i)
#  define HDBGRegYR()
#  define HDBGRegYW()
#  define HDBGRegIR()
#  define HDBGRegIW()
#  define HDBGRegICR()
#  define HDBGRegICW()
#  define HDBGRegNXR()
#  define HDBGRegNXW()
#  define HDBGTrace(a, i, c)
#  define HDBGNote(c, f, ...)
#  define HDBGNote0(c, m)
#  define HDBGIntrSet(o, l, s, c)
#  define HDBGIntr(l, s, i, c)
#  define HDBGFault(n, s, m, c)
#  define HDBGPrint()
# endif
#endif // HDBG_H
