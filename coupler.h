enum couplerState_e {
  cstateIdle = 0, // memset

  // L6 states
  cstateL6Connect,
  cstateL6GetDCW,
  cstateL6GetDCW2,
  cstateL6StartXferFromCS,
  cstateL6WaitXferFromCS,
  cstateL6StartXferToCS,

  // L66 states
  cstateL66Connect,
  cstateL66WaitPCW,
  cstateL66WaitLICW,
  cstateL66WaitTICW,
  cstateL66StartXferFromCS,
  cstateL66WaitXferFromCS,
  cstateL66StartXferToCS,
};

struct couplerData_s {

  enum couplerState_e state;

  // Incoming DMA write packet buffer
  bool DMADataRecvd;
  word24 DMADataAddr;
  word36 DMAData;

  // L6 data
  struct {
    bool connect;
    word36 PCW;
    word6 pcwCmd;
    word36 LICW;
    word15 licwY;  // address
    word18 licwZ;  // tally
    word36 DCW1;
    word36 DCW2;
    // If set, the xfer from CS uses a read & clear
    bool readClear;

    // DMA 
    word24 dmaL66Addr;
    word18 dmaL6Addr;
    int dmaTally;

  } l6;

  // L66 data
  struct {
    bool connect;   // Connect command received
    word36 PCW;     // Connect PCW
    word36 LICW;

    // DMA 
    word24 dmaL66Addr;
    word18 dmaL6Addr;
    int dmaTally;

  } l66;

  struct {
    word1 BT_INH; // Bootload Inhibit  60132445 pg 33
    word1 STORED_BOOT; // 60132445 pg 33
  } task_register;

  uv_tcp_t tcpHandle;
  uv_tcp_t clientHandle;

#define ATTACH_ADDRESS_SZ 4096
  char attachAddress [ATTACH_ADDRESS_SZ];

   bool booting;
   bool booted;
};

extern struct couplerData_s cd;

extern DEVICE couplerDev;
void /* iomAction */ couplerConnect (word36 PCW);
int poll_coupler (uint8_t * * pktp);
void wait_for_boot (void);
void couplerInit (void);
void couplerRun (void);
void cmdReceived (uv_stream_t * req, dnPkt * pkt, ssize_t nread);
