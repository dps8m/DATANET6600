#include "dn355.h"
#include "iom.h"
#include "caf.h"
#include "utils.h"
#include "core.h"
#include "hdbg.h"
#include "cpu.h"
#include "dnpkt.h"
#include "hsla.h"
#include "dialin.h"


word18 hslaHandler (word18 addr, word18 data, bool writep);



// MCS initializes HSLA
//
// hsla0: connect PCW 300000000000 cmd grp 1 cmd 8 subChnNo 0 mask 0
// Initialize subchannel 0.
// hsla0: connect PCW 301000000000 cmd grp 1 cmd 8 subChnNo 8 mask 0
// Initialize subchannel 8.
// hsla0: connect PCW 302000000000 cmd grp 1 cmd 8 subChnNo 16 mask 0
// Initialize subchannel 16.
// hsla0: connect PCW 303000000000 cmd grp 1 cmd 8 subChnNo 24 mask 0
// Initialize subchannel 24.
//
// DBG(1555355)> TRACE:  51621 304016 (LDAQ 16,3   "51637-*)
//
// 51621 init:02147
//        02147  3 04 016   0          674        ldaq    h.cnfg,3        get config pcw, if any (stored
// DBG(1554626)> NOTE: hslaHandler hslaHandler read  001016 000000
// DBG(1554626)> NOTE: hslaHandler hsla 0. line 0. cnfg even read  000000
// DBG(1554626)> NOTE: hslaHandler hslaHandler read  001017 000000
// DBG(1554626)> NOTE: hslaHandler hsla 0. line 0. cnfg odd  read  000000
// DBG(1554631)> TRACE:  52146 314014 (STAQ 14,3   "52162-*)
//
// 52146 init:02474
//        02474  3 14 014   0          999        staq    h.aicw,3
// DBG(1554631)> NOTE: hslaHandler hslaHandler write 001014 001010
// DBG(1554631)> NOTE: hslaHandler hsla 0. line 0. aicw even write 001010
// DBG(1554631)> NOTE: hslaHandler hslaHandler write 001015 010001
// DBG(1554631)> NOTE: hslaHandler hsla 0. line 0. aicw odd  write 010001

//
// *********************************************************
// *
// *      perform the following initialization procedures for    
// * each subchannel that was configured in the cdt.
// * if a subchannel does not exist, or does not conform
// * to the configuration desired, set the "exhaust" bit in 
// * its active status icw; otherwise, allocate a tib
// * (terminal information block) and a software comm.
// * region for the subchannel.
//*
//*********************************************************
//
//       02147  3 04 016   0          674        ldaq    h.cnfg,3        get config pcw, if any (stored
//                                    675                                here by load_fnp_ routine)
//       02150  0 74 321     2471     676        tze     hsl400-*        this channel isnt configured
//                                    677
//
//                    02471            993 hsl400 null                    we will set up active status icw with
//                                    994                                exhaust bit on
//       02471  3333 00    0          995        cx3a                    get hwcm address 
//       02472  773  010   0          996        iaa     h.baw           we will point status icw at base address word
//                                    997                                pointer to avoid getting status stored randomly
//       02473  0 47 004     2477     998        ldq     hbdicw+1-*      get rest of icw with exhaust bit
//       02474  3 14 014   0          999        staq    h.aicw,3
//
// 001010 010001
// 0     W.1   18 bit data
// 01010 Y     address (hsla 0, sub channel 0, baw)
// 01    E     exhaust bit set
// 00001 TALLY tally 1
//
//
// DBG(1554645)> TRACE:  51621 304016 (LDAQ 16,3   "51637-*)
// 51621 init:02147
//        02147  3 04 016   0          674        ldaq    h.cnfg,3        get config pcw, if any (stored
// 
// DBG(1554645)> NOTE: hslaHandler hslaHandler read  001036 000000
// DBG(1554645)> NOTE: hslaHandler hsla 0. line 1. cnfg even read  000000
// DBG(1554645)> NOTE: hslaHandler hslaHandler read  001037 000000
// DBG(1554645)> NOTE: hslaHandler hsla 0. line 1. cnfg odd  read  000000
//
// ...
// DBG(1555633)> NOTE: hslaHandler hslaHandler write 001774 001770
// DBG(1555633)> NOTE: hslaHandler hsla 0. line 31. aicw even write 001770
// DBG(1555633)> NOTE: hslaHandler hslaHandler write 001775 010001
// DBG(1555633)> NOTE: hslaHandler hsla 0. line 31. aicw odd  write 010001
//
// DBG(1555643)> TRACE:  52110 060056 (CIOC 56     "52166-*)
// 52110 init:02436
//        02436  0 60 056     2514     955        cioc    hsmska-*        store the mask register
//        02436  0 60 056     2514     955        cioc    hsmska-*        store the mask register
//        02437  0 60 057     2516     956        cioc    hsmskb-*        in case this is a dn6670, do it for each mlc
//        02440  0 60 060     2520     957        cioc    hsmskc-*
//        02441  0 60 061     2522     958        cioc    hsmskd-*
//
//        02514  110000     0         1020 hsmska vfd     2/0,4/stomrg,12/0,18/0
//        02515  000000     0 
//        02516  111000     0         1021 hsmskb vfd     2/0,4/stomrg,6/8,6/0,18/0
//        02517  000000     0 
//        02520  112000     0         1022 hsmskc vfd     2/0,4/stomrg,6/16,6/0,18/0
//        02521  000000     0 
//        02522  113000     0         1023 hsmskd vfd     2/0,4/stomrg,6/24,6/0,18/0
//
//                  000010            137 initop bool    10
//                  000003            138 reqcst bool    3
//                  000005            139 rstmsk bool    5
//                  000004            140 setmsk bool    4
//                  000011            141 stomrg bool    11
//
// BG(1555672)> TRACE:  51323 060005 (CIOC 5      "51330-*)
// 51323 init:01651
//        01651  0 60 005     1656     391        cioc    nmpcw-*         send mask pcw
// 
// DBG(1555673)> NOTE: hslaConnect hsla1: connect PCW 000000010000 cmd grp 0 cmd 0 subChnNo 0 mask 1
// 
// * non-implemented channel:
// *
// *  --set appropriate iv's to point to an "extraneous interrupt"
// *    reporting routine.
// *
// *  --send a mask pcw (bit 23 =1) to insure channel is off.









// init.list:
//
//                                   2409                                * initialize this hsla
//                                   2410                                * (we need to hit it 4 times in case it's a 6670
//                                   2411                                *   with 4 mlcs pretending to be 1 hsla)
//       05101  0 60 061     5162    2412        cioc    ipcwa-*         pcw1, cmd 10, subchannel 0
//       05102  0 60 062     5164    2413        cioc    ipcwb-*          "      "          "     8
//       05103  0 60 063     5166    2414        cioc    ipcwc-*          "      "          "     16
//       05104  0 60 064     5170    2415        cioc    ipcwd-*          "      "          "     24
//
//       05162  300000     0         2474 ipcwa  vfd     2/1,4/initop,12/0,18/0  initialize pcw
//       05163  000000     0  
//       05164  301000     0         2475 ipcwb  vfd     2/1,4/initop,6/8,6/0,18/0   same for second mlc
//       05165  000000     0
//       05166  302000     0         2476 ipcwc  vfd     2/1,4/initop,6/16,6/0,18/0  third
//       05167  000000     0  
//       05170  303000     0         2477 ipcwd  vfd     2/1,4/initop,6/24,6/0,18/0  fourth
//
//                  000010            138 initop bool    10
//                  000003            139 reqcst bool    3
//                  000005            140 rstmsk bool    5
//                  000004            141 setmsk bool    4
//                  000011            142 stomrg bool    11


struct hslaData_s hslaData;


// 1000, 2000, 3000
static word18 commRegBase[N_HSLAS] = { ADDR_IOM_MBX_HSLA1_BASE, ADDR_IOM_MBX_HSLA2_BASE, ADDR_IOM_MBX_HSLA3_BASE };

// Each subchannel gets 020 words in the comm. region

// config word offset 016, 017
// h.ric0    equ       0         primary receive icw
// h.ric1    equ       2         alternate receive icw
// h.sic0    equ       4         primary send icw
// h.sic1    equ       6         alternate send icw
// h.baw     equ       8         base address word
// h.sfcm    equ       9         software comm. region address
// h.mask    equ       10        mask register
// h.aicw    equ       12        active status icw
// h.cnfg    equ       14        configuration status

#define h_ric0     0
#define h_ric1     2
#define h_sic0     4
#define h_sic1     6
#define h_baw      8
#define h_sfcm     9
#define h_mask    10
#define h_aicw    12
#define h_cnfg    14

// h_cnfg
// load_fnp_.pl1
//
// dcl  1 async_pcw aligned based (pcwp),
//       2 f18b unal like pcw_f18b,
//       2 pad3 bit (6) unal,
//       2 two_stop bit (1) unal,
//       2 pad4 bit (3) unal,
//       2 speed bit (8) unal;
//
//dcl  1 pcw_f18b based,
//       2 type bit (2) unal,
//       2 char_len bit (4) unal,
//       2 pad1 bit (1) unal,
//       2 subch bit (5) unal,
//       2 rcv_parity bit (1) unal,
//       2 xmt_parity bit (1) unal,
//       2 parity_odd bit (1) unal,
//       2 two_icws bit (1) unal,
//       2 cct_enable bit (1) unal,
//       2 pad2 bit (1) unal;
//
// dcl  async_pcw_model bit (18) internal static options (constant) init ("100000000000010110"b);
//
// MCS image values
//
// hsla 0. line 0. cnfg even write 570026
// hsla 0. line 0. cnfg odd  write 000341
// hsla 0. line 1. cnfg even write 570126
// hsla 0. line 1. cnfg odd  write 000341
// ...
// hsla 0. line 30. cnfg even write 573626
// hsla 0. line 30. cnfg odd  write 000341
// hsla 0. line 31. cnfg even write 573726
// hsla 0. line 31. cnfg odd  write 000341
//
// 570026000341
// 101 111 000 000 010 110 000 000 000 011 100 001
// 101111000000010110000000000011100001
// type  len   pad  subch  rcv_parity  xmt_parity  pairty_odd
// 10    1111  0    00000  0           1           0         
// two_icws  cct_enable pad2
// 1         1          0
// pad3    twostop pad4  speed
// 000000  0        000  11100001
//
//   type 10 async configution command
//   len 8 bit char, 7 data, 1 parity
//   subch  0
//   rcv_parity  0 ignore
//   xmt_parity  1 generate
//   parity_odd  0 even parity
//   two_icws    1 ???
//   cct_enable  1 ???
//   two_stop    0 generate one stop bit
//   speed       11100001  341
//
// speed maybe maybe baud_rates.incl.pl1
//  hsla.baud_rate   baud    speed_bit
//    1               110       1
//    2               133       2
//    3               150       3
//    4               300       4
//    5               600       8
//    6              1200       6
//    7              1800       7
//    8              2400       8
//    9              4800       8
//   10              7200       8
//   11              9600       8
//   12             19200       8
//   13             40800
//   14             50000
//   15             72000
//
// async_pcw.speed
//
//  2 speed bit (8) unal
//
//  /* indicate speed in pcw bits for async channels */
//  substr (async_pcw.speed, speed_bit (hsla_array (ano, i).baud_rate), 1) = "1"b;
// dcl  speed_bit (12) internal static options (constant) fixed bin init (1, 2, 3, 4, 8, 6, 7, 8, 8, 8, 8, 8);
//


// bits
//   0 -  1     type   10 configure async channel
//   2 -  5     char len  
//                     1110  7 bit char, 6 data 1 parity
//                     1111  8 bit char, 7 data, 1 parity
//   6 -  6     pad
//   7 - 11     subch  subchannel # (0-31) [redundant; no load_fnp_ is stashing a pcw in here]
//  12 - 12     rcv_parity  1 = check parity
//  13 - 13     xmt_parity  1 = generate parity
//  14 - 14     parity_odd  1 = odd parity
//  15 - 15     two_icws    ????
//  16 - 16     cct_enable  ????
//  17 - 17     pad2
//  18 - 23     pad3
//  24 - 24     two_stop    1 = generate two stop bits
//  25 - 27     pad4
//  28 - 35     speed
//
//  (see dd01, 5-30)
//                       
//  
//           ttls      hsla status bits
//           rem       first status word
// hs.rcs    bool      400000    send/rcv status indicator(1=rcv)
// hs.nms    bool      200000    normal marker status
// hs.dms    bool      100000    delayed marker status 
// hs.trm    bool      040000    terminate character
// hs.aiw    bool      020000    alternate icw active
// hs.siw    bool      010000    switching icw's after status store
// hs.tro    bool      004000    tally runout
// hs.ptr    bool      002000    pre-tally runout
// hs.per    bool      001000    parity error, rcv
// hs.crj    bool      000400    command reject
// hs.dss    bool      000200    data set status change
// hs.isd    bool      000100    idle state detect (hdlc)
// hs.xte    bool      000040    transfer timing error
// hs.fce    bool      000020    frame check sequence error (hdlc)
// hs.sqo    bool      000010    status queue overflow (set by software)
// hs.nsb    bool      000004    no stop bit rcvd
// hs.rab    bool      000004    receive abort (hdlc)
// hs.dlo    bool      000002    data line occupied(acu)
// hs.pwi    bool      000001    power indicator(acu)
//
//           rem       second status word
// hs.dsr    bool      400000    data set ready
// hs.cts    bool      200000    clear to send
// hs.cd     bool      100000    carrier detect
// hs.src    bool      040000    supervisory receive
// hs.acr    bool      020000    abandon call and retry(acu)
// hs.ads    bool      010000    data set status(acu)
// hs.ri     bool      004000    ring indicator
// hs.brk    bool      002000    line break received
// hs.byt    bool      002000    partial byte (hdlc)
// hs.rbt    bool      001000    receive block terminate (bsc & hdlc)
// hs.rcv    bool      000400    receive mode
// hs.xmt    bool      000200    transmit mode
// hs.wam    bool      000100    wrap around mode
// hs.dtr    bool      000040    data terminal ready
// hs.rts    bool      000020    request to send
// hs.mby    bool      000010    make busy
// hs.sxt    bool      000004    supervisory transmit
// hs.crc    bool      000004    crc error (bsc)
// hs.tre    bool      000004    tally runout enable (hdlc)
// hs.crq    bool      000002    call request(acu)
// hs.rto    bool      000002    receive time out (bsc)
//           bool      000001

#define hs_rcs    0400000
#define hs_nms    0200000
#define hs_dms    0100000
#define hs_trm    0040000
#define hs_aiw    0020000
#define hs_siw    0010000
#define hs_tro    0004000
#define hs_ptr    0002000
#define hs_per    0001000
#define hs_crj    0000400
#define hs_dss    0000200
#define hs_isd    0000100
#define hs_xte    0000040
#define hs_fce    0000020
#define hs_sqo    0000010
#define hs_nsb    0000004
#define hs_rab    0000004
#define hs_dlo    0000002
#define hs_pwi    0000001

#define hs_dsr    0400000
#define hs_cts    0200000
#define hs_cd     0100000
#define hs_src    0040000
#define hs_acr    0020000
#define hs_ads    0010000
#define hs_ri     0004000
#define hs_brk    0002000
#define hs_byt    0002000
#define hs_rbt    0001000
#define hs_rcv    0000400
#define hs_xmt    0000200
#define hs_wam    0000100
#define hs_dtr    0000040
#define hs_rts    0000020
#define hs_mby    0000010
#define hs_sxt    0000004
#define hs_crc    0000004
#define hs_tre    0000004
#define hs_crq    0000002
#define hs_rto    0000002
//  
//  
//  h1ft    set     96      hsla 1 fault
//  h1a0    set     4       hsla 1 subch  0 active terminate vector
//  h1a16   set     5       hsla 1 subch 16 active terminate vector
//  h1c0    set     6       hsla 1 subch  0 config terminate vector
//  h1c16   set     7       hsla 1 subch 16 config terminate vector
//  h1a1    set     20      hsla 1 subch  1 active terminate vector
//  h1a17   set     21      hsla 1 subch 17 active terminate vector
//  h1c1    set     22      hsla 1 subch  1 config terminate vector
//  h1c17   set     23      hsla 1 subch 17 config terminate vector
//  h1a2    set     36      hsla 1 subch  2 active terminate vector
//  h1a18   set     37      hsla 1 subch 18 active terminate vector
//  h1c2    set     38      hsla 1 subch  2 config terminate vector
//  h1c18   set     39      hsla 1 subch 18 config terminate vector
//  h1a3    set     52      hsla 1 subch  3 active terminate vector
//  h1a19   set     53      hsla 1 subch 19 active terminate vector
//  h1c3    set     54      hsla 1 subch  3 config terminate vector
//  h1c19   set     55      hsla 1 subch 19 config terminate vector
//  h1a4    set     68      hsla 1 subch  4 active terminate vector
//  h1a20   set     69      hsla 1 subch 20 active terminate vector
//  h1c4    set     70      hsla 1 subch  4 config terminate vector
//  h1c20   set     71      hsla 1 subch 20 config terminate vector
//  h1a5    set     84      hsla 1 subch  5 active terminate vector
//  h1a21   set     85      hsla 1 subch 21 active terminate vector
//  h1c5    set     86      hsla 1 subch  5 config terminate vector
//  h1c21   set     87      hsla 1 subch 21 config terminate vector
//  h1a6    set     100     hsla 1 subch  6 active terminate vector
//  h1a22   set     101     hsla 1 subch 22 active terminate vector
//  h1c6    set     102     hsla 1 subch  6 config terminate vector
//  h1c22   set     103     hsla 1 subch 22 config terminate vector
//  h1a7    set     116     hsla 1 subch  7 active terminate vector
//  h1a23   set     117     hsla 1 subch 23 active terminate vector
//  h1c7    set     118     hsla 1 subch  7 config terminate vector
//  h1c23   set     119     hsla 1 subch 23 config terminate vector
//  h1a8    set     132     hsla 1 subch  8 active terminate vector
//  h1a24   set     133     hsla 1 subch 24 active terminate vector
//  h1c8    set     134     hsla 1 subch  8 config terminate vector
//  h1c24   set     135     hsla 1 subch 24 config terminate vector
//  h1a9    set     148     hsla 1 subch  9 active terminate vector
//  h1a25   set     149     hsla 1 subch 25 active terminate vector
//  h1c9    set     150     hsla 1 subch  9 config terminate vector
//  h1c25   set     151     hsla 1 subch 25 config terminate vector
//  h1a10   set     164     hsla 1 subch 10 active terminate vector
//  h1a26   set     165     hsla 1 subch 26 active terminate vector
//  h1c10   set     166     hsla 1 subch 10 config terminate vector
//  h1c26   set     167     hsla 1 subch 26 config terminate vector
//  h1a11   set     180     hsla 1 subch 11 active terminate vector
//  h1a27   set     181     hsla 1 subch 27 active terminate vector
//  h1c11   set     182     hsla 1 subch 11 config terminate vector
//  h1c27   set     183     hsla 1 subch 27 config terminate vector
//  h1a12   set     196     hsla 1 subch 12 active terminate vector
//  h1a28   set     197     hsla 1 subch 28 active terminate vector
//  h1c12   set     198     hsla 1 subch 12 config terminate vector
//  h1c28   set     199     hsla 1 subch 28 config terminate vector
//  h1a13   set     212     hsla 1 subch 13 active terminate vector
//  h1a29   set     213     hsla 1 subch 29 active terminate vector
//  h1c13   set     214     hsla 1 subch 13 config terminate vector
//  h1c29   set     215     hsla 1 subch 29 config terminate vector
//  h1a14   set     228     hsla 1 subch 14 active terminate vector
//  h1a30   set     229     hsla 1 subch 30 active terminate vector
//  h1c14   set     230     hsla 1 subch 14 config terminate vector
//  h1c30   set     231     hsla 1 subch 30 config terminate vector
//  h1a15   set     244     hsla 1 subch 15 active terminate vector
//  h1a31   set     245     hsla 1 subch 31 active terminate vector
//  h1c15   set     246     hsla 1 subch 15 config terminate vector
//  h1c31   set     247     hsla 1 subch 31 config terminate vector
//  
//          rem             ***************************
//          rem             * program interrupt cells *
//          rem             ***************************
//          rem
//  ilev0   set     256     level 0  (common peripheral fault)      0400
//  ilev1   set     257     level 1  (common peripheral req/attn)
//  ilev2   set     258     level 2  (common peripheral terminate)
//  ilev3   set     259     level 3  (dia special)
//  ilev4   set     260     level 4  (hsla 1 subch  0-15 active)
//  ilev5   set     261     level 5  (hsla 1 subch 16-31 active)
//  ilev6   set     262     level 6  (hsla 1 subch  0-15 config)
//  ilev7   set     263     level 7  (hsla 1 subch 16-31 config)
//  ilev8   set     264     level 8  (hsla 2 subch  0-15 active)
//  ilev9   set     265     level 9  (hsla 2 subch 16-31 active)
//  ilev10  set     266     level 10 (hsla 2 subch  0-15 config)
//  ilev11  set     267     level 11 (hsla 2 subch 16-31 config)
//  ilev12  set     268     level 12 (hsla 3 subch  0-15 active)
//  ilev13  set     269     level 13 (hsla 3 subch 16-31 active)
//  ilev14  set     270     level 14 (hsla 3 subch  0-15 config)
//  ilev15  set     271     level 15 (hsla 3 subch 16-31 config)
//  

// config: false active, true config

static void hslaIntr (uint unitNumber, uint subChnl, bool config) {

  uint level = unitNumber * 4 + 4 /* ilev4 */;
  uint sublevel = subChnl;

  if (subChnl >= 16) {
    level += 1;
    sublevel = subChnl - 16;
  }
  if (config) {
    level += 2;
  }
  doIntr (level, sublevel, "hslaIntr");
}


//          rem             ********************
//          rem             * iom fault status *
//          rem             ********************
//  h1fts   set     278     hsla 1 fault status word
//  h2fts   set     279     hsla 2 fault status word
//  h3fts   set     280     hsla 3 fault status word
//  h1mb    set     512     hsla 1 mailbox base address
//  h2mb    set     1024    hsla 2 mailbox base address
//  h3mb    set     1536    hsla 3 mailbox base address
//  
//  
//  
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//  *
//  *       hintr   
//  *       
//  *            this routine handles interrupts for all of
//  *       the hsla subchannels, and takes status from the
//  *       hardware status buffer and queues it for process-
//  *       ing by the scheduled status processor, hstprc. hstprc
//  *       will be scheduled to process the status queue if it is
//  *       not already scheduled.
//  *
//  *       upon entry:
//  *            a call to g3jwt will return the third word of
//  *            the jump table, as follows:
//  *       
//  *               bits 0-3    iom channel number
//  *               bits 4-5    hsla number(1-3)
//  *               bits 6-10   subchannel number(0-31)        
//  *               bits 11-17  module number of hsla_man
//  *
//  *       returns:
//  *            entries queued for hstprc
//  * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
//          rem
//          tsy     a.c004-*,*      (=g3jwt) get third word of jump table
//          stq     intjtw-*        save the result
//          rem
//          trace   tt.int,ts.int,(intjtw)
//          ldq     intjtw-*
//          cqa             put copy into the a
//          ars     12      shift down to get hsla number
//          iana    3       leave only the hsla number
//          iaa     -1      subtract one to get 0-2
//          ora     l.d001-*        (=000010) turn on is_hsla bit
//          als     6       shift back into proper position
//          sta     intlno-*        save as part of line number
//          rem
//          cqa             get another copy
//          ars     7       shift down subchannel number
//          iana    31      leave only subchan number
//          orsa    intlno-*        put into line number
//          rem
//          lda     intlno-*        pick it up for call
//          tsy     a.d005-*,*      (=gettib) get the tib addr
//          iaa     0       set the indicators
//          tze     intret-*        no tib, ignore interrupt
//          rem
//          sta     intrtb-*        save real tib address for dspqur
//          tsy     a.d010-*,*      (setptw) virtualize real tib address
//          sta     intvtb-*        save for easy reference
//          cax1            put virtual tib address into x1
//          ldx2    t.sfcm,1        get the virtual sfcm address
//          tnz     2       should be non-zero
//          die     10      nope, die
//          rem
//          lda     sf.ssl,2        figure out current number of pending status
//          sba     sf.tly,2        to meter it
//          sta     intcnt-*
//          cmeter  mupdat,m.nst,intcnt-*
//          rem
//          stz     inthqf-*        clear status exhaust indicator
//          stz     intsqo-*        clear status queue overflow switch
//          ila     hpri    get basic priority
//          sta     intskd-*        set in sked words
//          rem
//          ldx3    sf.hcm,2        get hardware comm region address
//          lda     h.aicw+1,3 get second word of status icw
//          cana    l.d002-*        is it exhausted?
//          tze     int002-*        no
//          aos     inthqf-*        indicate that is so for later
//          rem
//  int002  lda     t.sfcm,1        get address of sfcm
//          iaa     sf.sta  add in the queue offset
//          sta     intcrp-*        first status if primary
//          rem
//          lda     sf.flg,2        get sfcm flag word
//          cana    l.d019-*        (=sffcai) is hardware alt buffer being used
//          tnz     int004-*        yes
//          rem
//          lda     l.d019-*        (=sffcai) or in alt hardware status
//          orsa    sf.flg,2
//          lda     intcrp-*        get back address of status queu
//          iaa     sf.shq-sf.sta add in the difference
//          tra     int006-*
//          rem
//  int004  lda     l.d020-*        (=^sffcai)
//          ansa    sf.flg,2        turn off alt indicator
//          ldq     intcrp-*        get current position
//          ila     sf.shq-sf.sta delta of hardware queue
//          asa     intcrp-*        adjust first status pointer
//          cqa             get back orginal address for status
//          rem
//  int006  sta     intvir-*        temporarily save virtual address
//          tsy     a.d007-*,*      (cvabs) get absolute address for icw
//          ldq     intsai-*        second word of status icw
//  int007  staq    h.aicw,3        store icw now
//          nop             buy some time
//          cmpa    h.aicw,3        see if we change it
//          tze     int008-*        yes all done
//          ldx3    intvir-*        set to point to first status of this queue
//          szn     0,3     set indicators
//          tnz     int008-*        new status all done
//          ldx3    sf.hcm,2        get hcm address
//          tra     int007-*        try again
//          rem
//  int008  lda     intcrp-*        get first status to process
//          iaa     sfhsiz*2+2 calc end of queue
//          sta     intend-*        for later processing
//          ldx3    intcrp-*        set to point to first status
//          rem
//          lda     sf.flg,2        get sfcm flags
//          icana   sffisc  inactive subchannel?
//          tnz     int050-*        yes, get out of here
//          rem
//  int010  lda     0,3     is this status word zero?
//          tze     int050-*        yes, all done
//          cana    l.d010-*        (=hs.rcs) rcv status ?
//          tze     int014-*        no
//          rem
//          cana    l.d004-*        (=hs.tro) tally runout ?
//          tze     int011-*        no
//          rem
//          rem             tally runout means we just dropped out of
//          rem             rcv mode.  make sure we stay out.
//          rem
//          lda     l.d011-*        (=/pb.rcv)
//          ansa    sf.pcw,2        turn off pcw rcv bit
//          tra     int020-*
//          rem
//  int011  cana    l.d007-*        (=hs.siw) switching icw ?
//          tze     int020-*        no, continue
//          rem
//          stx3    intcrp-*        save status queue addr
//          caq             put status word in q
//          rem
//          ldx3    sf.hcm,2        get hwcm addr
//          iacx3   h.ric0  get primary rcv icw addr
//          rem
//          lda     1,3     get icw tally
//          ana     l.d009-*        (=007777) leave only tally
//          sta     inttly-*        save it
//          rem
//          lda     l.d018-*        (=410000) get exhausted tally
//          sta     1,3     put it in icw
//          rem
//          lda     sf.flg,2        synchronous?
//          cana    l.d005-*        =sffsyn
//          tnz     int012-*        yes
//          cqa             no, get first word of status again
//          tra     int013-*        skip tally manipulation
//  int012  cqa             get status back in a
//          ldx3    sf.ib0,2        get primary buffer addr
//          cana    l.d008-*        (=hs.aiw) alt. icw active ?
//          tze     2       no
//          ldx3    sf.ib1,2        get alt buffer addr
//          cx3a            virtualize it
//          tsy     a.d001-*,*      setbpt
//          cax3
//          rem
//          ldq     bf.tly,3        get max buffer tally
//          sbq     inttly-*        subtract icw tally
//          stq     bf.tly,3        put actual tally in buffer
//          rem
//  int013  ldx3    intcrp-*        restore status queue addr
//          rem
//  int014  cana    l.d007-*        (=hs.siw) switching icw ?
//          tze     int020-*        no, process status at normal priority
//          rem
//  *       lda     sf.flg,2        get sfcm flags
//  *       cana    l.d005-*        synchronous line ?
//  *       tze     int015-*        no, use priority 3
//  *       rem
//  *       ldx2    sf.hsl,2        get address of hsla table
//  *       lda     ht.flg,2        pick up word with speed
//  *       ldx2    t.sfcm,1        restore x2
//  *       iana    htfspd  leave only the channel speed
//  *       icmpa   8       is it more than 9600 baud?
//  *       tmi     int015-*        no, use priority 3
//  *       rem
//  *       ila     hprip2  use real high priority
//  *       tra     2
//          rem
//  int015  ila     hprip3  get priority 3 for sked
//          sta     intskd-*        reset scheduler priority
//          rem
//          lda     l.d014-*        =sffmsp
//          iera    -1      if switching icws, turn it off
//          ansa    sf.flg,2
//          rem
//  int020  szn     sf.tly,2        any room in status queue?
//          tnz     int025-*        yes, continue
//          rem
//          szn     intsqo-*        status overflow occurred already ?
//          tnz     int024-*        yes, skip it
//          rem
//          tsy     a.d015-*,*      handle status queue overflow
//          aos     intsqo-*        remember it
//          rem
//  int024  stz     0,3     zero the ignored status
//          tra     int030-*        leave here
//          rem
//  int025  lda     sf.flg,2        synchronous?
//          cana    l.d005-*        =sffsyn
//          tnz     int028-*        yes, skip the marker test
//          lda     0,3     get first word of status
//          cana    l.d013-*        (=hs.nms) marker status?
//          tze     int028-*        nope
//          lda     sf.flg,2        yes, is one already pending
//          cana    l.d014-*        (=sffmsp)
//          tze     int027-*        no, we'll have to store this one
//          lda     0,3     (get first word of status back)
//          cana    l.d015-*        yes, but are there any other interesting ones?
//          tnz     int027-*        yep
//          stz     0,3     ignore this status
//          tra     int040-*
//          rem
//  int027  lda     0,3     get first word of status again
//          cana    l.d007-*        (=hs.siw) switching icws?
//          tnz     int028-*        then don't set flag
//          lda     l.d014-*        =sffmsp
//          orsa    sf.flg,2        turn it on
//  int028  null
//          ldaq    0,3     get the current status words
//          staq    sf.nxa,2*       put into the software queue
//          stz     0,3     zero the current status
//          rem
//          cana    l.d007-*        *is it hs.siw for switching
//          tze     int28a-*        *no continue
//          cana    l.d010-*        *is it hs.rcv recieve only
//          tze     int28a-*        *no again
//          lda     sf.flg,2
//          cana    l.d017-*        *(sffhdl+sffbsc) is it bsc or HDLC?
//          tze     int28a-*        no
//          tsy     a.d014-*,*      *(swphic) switch buffer now
//          rem
//  int28a  ila     -1      decrement the tally
//          asa     sf.tly,2
//          ila     4       increment the next available
//          asa     sf.nxa,2        pointer
//          rem
//          lda     sf.ssl,2        get length of status queue
//          als     2       in words
//          sta     intsql-*
//          cx2a            put sfcm ptr into a
//          iaa     sf.waq
//          ada     intsql-*        get ptr to end of status queue
//          cmpa    sf.nxa,2        are we at end of queue?
//          tnz     int030-*        nope, continue
//          rem
//          cx2a            copy sfcm ptr to a
//          iaa     sf.waq  get ptr to beginning of queue
//          sta     sf.nxa,2        put into q ptr, wrapping q
//          rem
//  int030  lda     sf.flg,2        get the sfcm flags
//          icana   sffskd  is the status processor scheduled?
//          tnz     int040-*        yes, continue
//          rem
//          ila     sffskd  get flag bit
//          orsa    sf.flg,2        turn it on now
//          rem
//          ldx1    intrtb-*        get real tib address for dspqur
//          ldaq    intskd-*        (queue element)
//          tsy     a.d002-*,*      (=dspqur) queue hstprc to process status
//          rem             note: x1 contains real tib address
//          ldx1    intvtb-*        restore virtual tib address to x1
//          rem
//  int040  iacx3   2       bump to next status
//          cmpx3   intend-*        are we at end of buffer?
//          tnz     int010-*        no, continue
//          rem
//  int050  szn     intsqo-*        did we overflow?
//          tnz     int060-*        yes and we process it
//          szn     inthqf-*        hardware overflow?
//          tze     int060-*        no
//          rem
//          cmeter  mincs,m.hsqo,l.d016-*
//          rem
//          tsy     sqovfl-*        handle status queue overflow
//          aos     intsqo-*        now process hardware the same as software
//          rem
//  int060  null
//  int070  szn     intsqo-*        did we get another status queue overflow?
//          tze     intret-*        no, all is well
//          rem
//          aos     sf.rct,2        bump the repeat count
//          lda     sf.rct,2        get the new value
//          icmpa   20      compare to some random number
//          tmi     intret-*        no there yet, let channel run a bit more
//          rem
//          ldx2    t.sfcm,1        reset sfcm ptr
//          ila     pb.msk  set software "mask" bit
//          sta     sf.pcw,2        in pcw, and zero other bits (like dtr!!)
//          lda     l.d003-*        (=p.msk) get mask op
//          tsy     a.d009-*,*      (=cioc) connect to channel
//          rem
//  intret  tra     a.d004-*,*      (=mdisp) return to master dispatcher
//          rem
//          rem
//  intcnt  bss     1       used for count of pending status
//  intjtw  bss     1       (altrd) 3rd word of jump table
//  inthqf  bss     1       hardware queue overflow
//  intend  bss     1       (altrd) end of hardware status buffer
//  intcrp  bss     1       (altrd) current hardware status ptr
//  intrtb  bss     1       save for real tib address
//  intvtb  bss     1       save for virtual tib address
//  intsql  bss     1       length of software status queue
//  intvir  bss     1       virtual address of head of hardware status queue
//          rem
//          even
//  intskd  zero    hpri    priority of hstprc
//          ind     hstprc  routine to be run
//  intsai  amicwo  w.2,sfhsiz,0
//          rem
//          rem     following two words must be together for error message
//  interr  dec     4       error code
//  intlno  bss     1       (altrd) line number - tib type
//  intlno  bss     1       (altrd) line number - tib type
//          rem
//  intsqo  bss     1
//  inttly  bss     1
//          rem
//          rem
//  l.d001  vfd     o18/000010      is_hsla bit
//  l.d002  vfd     o18/010000
//  l.d003  vfd     18/p.msk
//  l.d004  vfd     18/hs.tro
//  l.d005  vfd     18/sffsyn
//  l.d006  oct     000130,000110
//  l.d007  vfd     18/hs.siw
//  l.d008  vfd     18/hs.aiw
//  l.d009  oct     007777
//  l.d010  vfd     18/hs.rcs
//  l.d011  vfd     o18//pb.rcv
//  l.d012  vfd     18/sffsqo
//  l.d013  vfd     18/hs.nms
//  l.d014  vfd     18/sffmsp
//  l.d015  vfd     18/hs.siw+hs.ptr+hs.tro+hs.dss
//  l.d016  dec     1
//  l.d017  vfd     18/sffhdl+sffbsc
//  l.d018  oct     410000  exhausted icw with 18-bit addressing on
//  l.d019  vfd     18/sffcai       altenate status buffer
//  l.d020  vfd     o18//sffcai and mask for turning off alt status
//          rem
//  a.d001  ind     setbpt
//  a.d002  ind     dspqur
//  a.d003  ind     0,w.2
//  a.d004  ind     mdisp
//  a.d005  ind     gettib
//  a.d006  ind     .crpte  pointer to variable cpu page table word
//  a.d007  ind     cvabs
//  *a.d008 unused
//  a.d009  ind     cioc
//  a.d010  ind     setptw  set up page table word
//  a.d011  ind     stpcnt
//  a.d012  ind     stpret
//  a.d013  ind     stpswd
//  a.d014  ind     swphic
//  a.d015  ind     sqovfl
//  
// dd01 pg 5-29
//  400000  1 set for config word
//  200000  0: async 1: sync
//  100000  spare
//  077000  subchannel type
//            00 illegal
//            01 general purpose S/C
//            02 general purpose SC (with ACU)
//            03 Dual sync. ASCII S/C
//            04 Dual sync. ASCII S/C (with ACU)
//            05 Dual symc. S/C (EIA)
//            06 Not to be used; reserved for SLA 355)
//            07 Dual sunc. S/C (VCA)
//            10 General purpose S/C (MIL188)
//            11 Wideband S/C (TELPAK)
//            12 High level data link control S/C (ADCCP)
//            13 Dual sync. S/C (MIL188)
//            14 Bi-sync. S/C (BSC)
//            15 Voice answerback S/C (generator)
//            16 Voice answerback S/C (receiver)
//            17 Spare
//            20 Dual sync. S/C (MIL188)
//            21 High level data link control S/C (ADCCP) (wide band)
//            22-31 spare

static UNIT hslaUnit[N_HSLAS] = {
  { UDATA (NULL, 0, 0), 0, 0, 0, 0, 0, NULL, NULL },
  { UDATA (NULL, 0, 0), 0, 0, 0, 0, 0, NULL, NULL },
  { UDATA (NULL, 0, 0), 0, 0, 0, 0, 0, NULL, NULL },
};

#if 0
static config_list_t hslaConfigList [] = {
  { NULL,                    0,  0,               NULL                   }
};
#endif
 
#define HSLA_UNIT_IDX(uptr) ((uptr) - hslaUnit)


static t_stat hslaSetAddress (UNIT * uptr, UNUSED int value, const char * cptr, UNUSED void * desc) {
  if (hslaData.started) {
    sim_printf ("HSLA telnet address error; listener already started.\n");
    return SCPE_INCOMP;
  }
  if ( (!cptr) || (cptr[0] == 0) )
      return SCPE_ARG;
  if (hslaData.telnetAddress) 
    free (hslaData.telnetAddress);
  hslaData.telnetAddress = strdup (cptr);
  return SCPE_OK;
}


static t_stat hslaShowAddress (FILE *st, UNIT *uptr, int val, const void *desc) {
  sim_printf ("%s\n", hslaData.telnetAddress);
  return SCPE_OK;
}


static t_stat hslaSetPort (UNIT * uptr, UNUSED int value, const char * cptr, UNUSED void * desc) {
  if (hslaData.started) {
    sim_printf ("HSLA telnet address error; listener already started.\n");
    return SCPE_INCOMP;
  }
  if ( (!cptr) || (cptr[0] == 0) )
      return SCPE_ARG;
  int n = atoi (cptr);
  if (n < 1 || n > 65535)
    return SCPE_ARG;
  hslaData.telnetPort = n;
  return SCPE_OK;
}


static t_stat hslaShowPort (FILE *st, UNIT *uptr, int val, const void *desc) {
  sim_printf ("%d\n", hslaData.telnetPort);
  return SCPE_OK;
}


static MTAB hslaMod [] = {
#if 0
  {
    MTAB_XTD | MTAB_VUN | MTAB_VDV | MTAB_NMO | MTAB_VALR,           // mask
    0,                         // match
    "CONFIG",                  // print string
    "CONFIG",                  // match string
    hslaSetConfig,           // validation routine
    hslaShowConfig,          // display routine
    NULL,                      // value descriptor
    NULL                       // help
  },
#endif
  {
    MTAB_XTD | MTAB_VDV | MTAB_VALR,
    0,                         // match
    "ADDRESS",                  // print string
    "ADDRESS",                  // match string
    hslaSetAddress,           // validation routine
    hslaShowAddress,          // display routine
    NULL,                      // value descriptor
    NULL                       // help
  },
  {
    MTAB_XTD | MTAB_VDV | MTAB_VALR,
    0,                         // match
    "PORT",                  // print string
    "PORT",                  // match string
    hslaSetPort,           // validation routine
    hslaShowPort,          // display routine
    NULL,                      // value descriptor
    NULL                       // help
  },
  { 0, 0, NULL, NULL, NULL, NULL, NULL, NULL }
};

static DEBTAB hslaDT [] = {
  { "DEBUG", DBG_DEBUG },
  { NULL, 0 }
};

static t_stat hslaReset (DEVICE *dptr) {
  return SCPE_OK;
}

static t_stat hslaAttach (UNIT * uptr, const char * cptr) {
  return SCPE_OK;
}

static t_stat hslaDetach (UNIT * uptr) {
  return SCPE_OK;
}

DEVICE hslaDev = {
  "HSLA",           /* name */
  hslaUnit,         /* units */
  NULL,             /* registers */
  hslaMod,          /* modifiers */
  N_HSLAS,          /* #units */
  8,                /* address radix */
  15,               /* address width */
  1,                /* address increment */
  8,                /* data radix */
  18,               /* data width */
  NULL,             /* examine routine */
  NULL,             /* deposit routine */
  hslaReset,        /* reset routine */
  NULL,             /* boot routine */
  hslaAttach,       /* attach routine */
  hslaDetach,       /* detach routine */
  NULL,             /* context */
  DEV_DEBUG,        /* flags */
  0,                /* debug control flags */
  hslaDT,           /* debug flag names */
  NULL,             /* memory size change */
  NULL,             /* logical name */
  NULL,             // attach help
  NULL,             // help
  NULL,             // help context
  NULL,             // device description
};

//         ttls    hsla pcw op-codes and broadside commands
//         rem
//         rem     pcw command type codes
//         rem
// pcw.0   bool    000000  command type 0 pcw
// pcw.1   bool    200000  cmd type 1 w/broadside
// pcw.2   bool    400000  config type 2 async
// pcw.3   bool    600000  config type 3 sync
//         rem
//         rem     op-codes
//         rem
// p.nop   bool    000000  no operation
// p.ris   bool    010000  request input status
// p.ros   bool    020000  request output status
// p.rcs   bool    030000  request config status
// p.msk   bool    040000  set subchannel mask bit
// p.rmsk  bool    050000  reset subchannel mask bit
// p.sriw  bool    060000  switch receive icw
// p.ssiw  bool    070000  switch send icw
// p.init  bool    100000  initialize
// p.smsk  bool    110000  store subchannel mask register
//         bool    120000
//         bool    130000
// p.rsyn  bool    140000  re-sync the subchannel
// p.tlbk  bool    150000  transmit line break
//         bool    160000
//         bool    170000
//         rem
//         rem     broadside bits
//         rem
// pb.rcv  bool    000400  set receive mode
// pb.xmt  bool    000200  set xmit mode
// pb.wam  bool    000100  set wraparound mode
// pb.dtr  bool    000040  set data terminal ready
// pb.rts  bool    000020  set request to send
// pb.mby  bool    000010  make busy
// pb.sxt  bool    000004  set supervisory transmit
// pb.tre  bool    000004  set tally runout enable (hdlc)
// pb.crq  bool    000002  set call request(acu)
// pb.msk  bool    000001
//         rem
//         rem     pcw type 2 (asynch confiuration) mode bits
//         rem
// p2.5bt  bool    140000  5-bit characters
// p2.6bt  bool    150000  6-bit characters
// p2.7bt  bool    160000  7-bit characters
// p2.8bt  bool    170000  8-bit characters
// p2.mbt  bool    170000  mask for char size field
// p2.lpr  bool    000040  lateral parity receive
// p2.lps  bool    000020  lateral parity send
// p2.lpo  bool    000010  lateral parity odd
// p2.icw  bool    000004  two send icw's
// p2.cct  bool    000002  cct enable
// p2.spr  bool    000001
//         rem
//         rem
//         rem     pcw type 3 (sync config)
//         rem
// p3.itf  bool    000400  hdlc interframe time fill
// p3.beb  bool    000400  bsc ebcdic mode
// p3.btr  bool    000200  bsc transparent
// 


void hslaConnect (uint channelNumber /* 1-3*/, int unitNumber, word36 PCW) {
  // Parse PCW
  word2 cmdGrp = (word2) getbits36 (PCW, 0, 2);
  word4 cmd = (word4) getbits36 (PCW, 2, 4);
  word5 subChnNo = (word5) getbits36 (PCW, 7, 5);
  //word1 mask = (word1) getbits36 (PCW, MASK_BIT, 1);
  HDBGNote (__func__, "hsla%d: connect PCW %012lo cmd grp %o cmd %o subChnNo %u", unitNumber, PCW, cmdGrp, cmd, subChnNo);
  DBG_HSLA (sim_printf ("hsla%d: connect PCW %012lo cmd grp %o cmd %o subChnNo %u IC %05o\n", unitNumber, PCW, cmdGrp, cmd, subChnNo, cpu.rIC);)

  word18 subChanCommReg = commRegBase[unitNumber] + subChnNo * LEN_IOM_MBX_HSLA_SUBCHNL;
  struct hsla_s * hslap = & hslaData.hsla[unitNumber];
  struct hslaSubChnl_s * subChnlp = & hslaData.hsla[unitNumber].subChnl[subChnNo];

  hslap->running = true;

//static int cnt = 0;
//#define when 10
//if (cnt == when) { HDBGPrint (); exit (1); }
//cnt ++;

  switch (cmdGrp) {
    case 0: { // cmdGrp 0
      switch (cmd) {
        case 005: { // Reset subchannel mask bit
          HDBGNote0 (__func__, "Reset subchannel mask bit");
          DBG_HSLA (sim_printf ("Reset subchannel mask bit\n");)
          subChnlp->mask = 0;
          break;
        }

        case 011: { // Store mask 
          HDBGNote0 (__func__, "Store mask");
          DBG_HSLA (sim_printf ("Store mask\n");)
          word36 msk = 0;
          for (int i = 0; i < N_HSLA_LINES; i ++)
            msk |=  ((word36) hslaData.hsla[unitNumber].subChnl[i].mask ? 1 : 0) << (18 - i);
          // leave priority scan indicator at 0
          msk |= 1; // MBO
          hslaData.hsla[unitNumber].subChnl[0].mask = msk;
          break;
        }

        default:
          sim_printf (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> HSLA command group 0 cmd %o ignored\n", cmd);
          break;
      } // switch cmdGrp 0 cmd
      break;
    } // switch cmdGrp 0
      
    case 1: {
      switch (cmd) {
        case 000: { // Noop
          break;
        }

// init.map355 -- setting up aicw for h/w status
//   note that the channel can store 6 though
//   tally is set to 5. hardware stores status
//   in sixth word after tally runout.


// Active status
//     Bit        send/output                              receive/input
//      0         0                                        1
//      1         --                                       normal marker character received
//      2         --                                       delayed marker character received
//      3         --                                       terminate character received
//      4                    1: alternate buffer is active            
//      5                    1: switch buffers after status store
//      6                    1: TY0
//      7                    1: TY1
//      8         --                                       1: lateral parity error
//      9         --                                       1: cmd sent to unimplemented subchannel
//     10         --                                       1: change in date set status occured
//     11                    MBZ
//     12                    1: Transfer timing error
//     13                    MBZ
//     14                    MBZ
//     15                    No stop bit received (async only)
//     16                    DLO (data line occupied ACU)
//     17                    PWI (power indicator ACU)
//     18                    Data set ready
//     19                    Clear to send
//     20                    Carrier detect
//     21                    Supervisorary receive
//     22                    ACR (abandon call and retry - ACU)
//     23                    Data set status lead up (ACU)
//     24                    Ring indicator
//     25                    Line break
//     26                    (spare)
//     27                    Receive mode
//     28                    Send mode
//     29                    Wraparound mode  [cac: loopback?]
//     30                    DTR
//     31                    RTS
//     32                    Make busy
//     33                    Supervisory transmit
//     34                    Call request (ACU)
//     35                    (spare - reserved for subchannel)

        case 001: { // Subchannel input status request
          HDBGNote0 (__func__, "Subchannel input status request");
          DBG_HSLA (sim_printf ("Subchannel input status request\n");)

         
          word36 status = 0;
          putbits36 (& status, 0, 1, 1);  // bit 0 is set for input status
// XXX  Using line_client to decide if we are off-hook
          if (subChnlp->line_client) {
            // off-hook
            putbits36 (& status, 18, 1, 1); // Data set ready
            putbits36 (& status, 19, 1, 1); // Clear to send
            putbits36 (& status, 20, 1, 1); // Carrier detect
            putbits36 (& status, 27, 1, 1); // Receive mode
            putbits36 (& status, 28, 1, 1); // Send mode
            putbits36 (& status, 30, 1, 1); // DTR
            putbits36 (& status, 31, 1, 1); // RTS
          }
          //coreWriteDouble (subChanCommReg + h_cnfg, status, "hsla input status request");
          hslaIntr (unitNumber, subChnNo, true);
          break;
        }

//    ldaq    hcnfig-*        test upper half for matching bits
//   DBG(1545945)> REG:  write A 570026
//   DBG(1545945)> REG:  write Q 000341
//      ana     hmask1-*        (=o600076) (see hmask1 for expl)
//   DBG(1545946)> REG:  write A 400026

// ldaq    hcnfig-*        test upper half for matching bits
// ana     hmask1-*        (=o600076) (see hmask1 for expl)
// sta     htemp-*         this is what it should be
// lda     h.cnfg,3        now, lets see what it is
// ana     hmask1-*        (=o600076) (see hmask1 for expl)
// cmpa    htemp-*         see if what is is what should be
// tnz     hsl390-*        nope, go mask off channel
// cana    l.f002-*        (=o200000) is it synchronous?
// tnz     hsl050-*        if so, don't bother with second word
// lda     h.cnfg+1,3      pick up lower half of status to test char length
// ana     l.f003-*        (=o170000) mask out all but character lengths
// cmpa    l.f004-*        (=o040000) see if 8 bit byte size
// tze     hsl050-*        yes, this is ok
// cmpa    l.f005-*        (=o020000) see if 7 bit byte size
// tze     hsl050-*        yes, this is ok
// cmpa    l.f006-*        (=o010000) see if 6 bit byte size
//                                 yes, this is ok, fall through
// tnz     hsl390-*        not ok, mask out this channel
// lda     ht.flg,2        get the baud rate into
// iana    htfspd          hbaud for maktib
// sta     hbaud-*
// lda     ht.flg,2        get flags again
// ana     l.f008-*        (hftasy) is it sync or async?
// arl     7               just put bit into hbaud
// orsa    hbaud-*         in the right place
//
//
// hmask1 oct     600076         mask to leave only (mbo's, async/sync
//                               parity stuff, icw alternation, and ccc enabled

// bits
//  0 -  0   = 1 (configuration)
//  1 -  1   0: async; 1: sync
//  2 -  2   spare
//  3 -  8   subchannel type
//              0 illegal
//              1 General purpose S/C
//              2 General purpose S/C (with ACU)
//              3 Dual synchronous ASCII S/C
//              4 Dual synchronous ASCII S/C (with ACU)
//              5 Dual synchronous S/C (EIA)
//              6 not to be used (reserved for SLA 355)
//              7 Dual synchronous S/C (VCA)
//              8 General purpose S/C (MIL188)
//              9 Widebacd S/C (TELPAC)
//             10 High level data link control (ADCCP)
//             11 Dual synchronous S/C (MIL188)
//             12 Bi-synchronous S/C (BSC)
//             13 Voice answerback S/C (generator)
//             14 Voice answerback S/C (receiver)
//             15 Spare
//             16 Dual asynchronous S/C (MIL188)
//             17 High level data link control S.C (ADCCP) (wide band)
//          18-31 Spare
//  9 - 11 spare
// 12 - 12 rcv_parity
// 13 - 13 xmt_parity
// 14 - 14 parity_odd
// 15 - 15 two_icws
// 16 - 16 cct_enable
// 17 - 19 spare
// 20 - 23 char_len
// 24 - 26 spare
// 27 - 35 speed_bits
        case 003: { // Subchannel configuration status request
          HDBGNote0 (__func__, "Subchannel configuration status request");
          DBG_HSLA (sim_printf ("Subchannel configuration status request\n");)
          // init masks with 600076 mask to leave only (mbo's, async/sync parity stuff, icw alternation, and ccc enabled
          //  hcnfig 570226 000341
          word36 cfg = 0;
          putbits36 (& cfg, 0, 1, 1); // must be 1
          putbits36 (& cfg, 1, 1, subChnlp->sync);
          putbits36 (& cfg, 12, 1, subChnlp->rcv_parity);
          putbits36 (& cfg, 13, 1, subChnlp->xmt_parity);
          putbits36 (& cfg, 14, 1, subChnlp->parity_odd);
          putbits36 (& cfg, 15, 1, subChnlp->two_icws);
          putbits36 (& cfg, 16, 1, subChnlp->cct_enable);
          putbits36 (& cfg, 20, 4, subChnlp->char_len);
          putbits36 (& cfg, 27, 8, subChnlp->speed_bits);
          subChnlp->cnfg = cfg;
          break;
        }

        case 004: { // Set subchannel mask bit
          HDBGNote0 (__func__, "Set subchannel mask bit");
          DBG_HSLA (sim_printf ("Set subchannel mask bit\n");)
          subChnlp->mask = 1;
          break;
        }

        case 010: { // Initialize
          HDBGNote (__func__, "Initialize subchannel %u.", subChnNo);
          DBG_HSLA (sim_printf ("Initialize subchannel %u.\n", subChnNo);)

          // Among other things, this turns on memory mapped registers.

          // sub-channel number    Initialize channels
          //         0                  0-7
          //         8                  8-15
          //        16                 16-23
          //        24                 24-31

          switch (subChnNo) {
            case  0:
            case  8:
            case 16:
            case 24: {
              //registerHWCM (subChanCommReg, LEN_IOM_MBX_HSLA_SUBCHNL * 8, hslaHandler);
              break;
            }
            default: {
              sim_printf ("%s unexpected sub-channel number sub-channel initialize %o\n", __func__, subChnNo);
              break;
            }
          }
          break;
        }

        default:
sim_printf ("HSLA connect chan %o unit %d PCW %012lo cmdGrp %o cmd %02o subchan %02o mask %o IC %05o\n", channelNumber, unitNumber, PCW, cmdGrp, cmd, subChnNo, iom.masked[channelNumber], cpu.rIC);
sim_printf ("     hsla%d: Unhandled cmd grp %o %u.   XXX \n", cmdGrp, unitNumber, subChnNo);
//bail();
          sim_debug (DBG_DEBUG, & hslaDev, "hsla%d: Command group %u command %u not yet implemented.\n", unitNumber, cmdGrp, cmd);
          break;
      }
      break;
    } // case cmdGrp 1

    case 2:  {
      switch (cmd) {
        case 017: {  // Configure asych subchannel

// bits
//  0 -  1  commad pcw #, 10 config async
//  2 -  5  command
//                  1100 5 bit char
//                  1101 6 bit char
//                  1110 7 bit char
//                  1111 8 bit char
//       6  not used
//  7 - 11  subchannel number
// 12 - 12  rcv_parity  1 = check parity
// 13 - 13  xmt_parity  1 = generate parity
// 14 - 14  parity_odd  1 = odd parity
// 15 - 15  alt. cont. word (MCS says "two_icws")  Enables alternate channel?
// 16 - 16  cct_enable
// 17 - 17  spare
// 18 - 23  unused
// 24 - 24  two_stop  1 = two stop bits
// 25 - 27  not used
// 28 - 28  110
// 29 - 29  134.5
// 30 - 30  150
// 31 - 31  300
// 32 - 32  1050
// 33 - 33  1200
// 34 - 34  1800
// 35 - 35  75/600


          HDBGNote0 (__func__, "Configure async subchannel");
          DBG_HSLA (sim_printf ("Configure async subchannel\n");)

          subChnlp->sync = (word4) getbits36 (PCW, 1, 1);
// init is verifying the HSLA sub-channel configurations.  load_fnp_ provides 
// Set Configuration PCWs; each is built up from the CMF (baud rate, line type,
// etc) for each sub-channel; init CIOC's each PCW, and then does a Get
// Configuration and checks the result for reasonableness.
// load_fnp_ is creating PCWs with the character length set to '1111' (8 bits).
// But init thinks '8 bits' is '1000'.
#if 0
          subChnlp->char_len = (word4) getbits36 (PCW, 2, 4);
#else
          word4 char_len = (word4) getbits36 (PCW, 2, 4);
          if (char_len == 017) { // 8 bits
            subChnlp->char_len = 004;
          } else if (char_len == 016) { // 7 bits
            subChnlp->char_len = 002;
          } else if (char_len == 015) { // 6 bits
            subChnlp->char_len = 001;
          } else {
            sim_printf ("WARN: HSLA Set Async Configuration char_len %o not handled; using 8 bits;\n", char_len);
            subChnlp->char_len = 004;
          }
#endif
          subChnlp->rcv_parity = (word1) getbits36 (PCW, 12, 1);
          subChnlp->xmt_parity = (word1) getbits36 (PCW, 13, 1);
          subChnlp->parity_odd = (word1) getbits36 (PCW, 14, 1);
          subChnlp->two_icws = (word1) getbits36 (PCW, 15, 1);
          subChnlp->cct_enable = (word1) getbits36 (PCW, 16, 1);
          subChnlp->two_stop = (word1) getbits36 (PCW, 24, 1);
          //subChnlp->b110 = (word1) getbits36 (PCW, 28, 1);
          //subChnlp->b134_5 = (word1) getbits36 (PCW, 29, 1);
          //subChnlp->b150 = (word1) getbits36 (PCW, 30, 1);
          //subChnlp->b300 = (word1) getbits36 (PCW, 31, 1);
          //subChnlp->b1050 = (word1) getbits36 (PCW, 32, 1);
          //subChnlp->b1200 = (word1) getbits36 (PCW, 33, 1);
          //subChnlp->b1800 = (word1) getbits36 (PCW, 34, 1);
          //subChnlp->b75_600 = (word1) getbits36 (PCW, 35, 1);
          subChnlp->speed_bits = (word8) getbits36 (PCW, 28, 8);
          
          
          break;
        }

        default:
          sim_debug (DBG_DEBUG, & hslaDev, "hsla%d: Command group %u not yet implemented.\n", unitNumber, cmdGrp);
sim_printf ("HSLA connect chan %o unit %d PCW %012lo cmdGrp %o cmd %02o subchan %02o mask %o IC %05o\n", channelNumber, unitNumber, PCW, cmdGrp, cmd, subChnNo, iom.masked[channelNumber], cpu.rIC);
sim_printf ("     hsla%d: Unhandled cmd grp %o %u.   XXX \n", cmdGrp, unitNumber, subChnNo);
//bail();
          break;
      }
      break;
    } // case cmdGrp 2

    case 3: {  // Configure sync channel
      sim_printf (">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> HSLA configure sync channel unhandled\n");
      break;
    } // case cmdGrp 3


    default:
sim_printf ("HSLA connect chan %o unit %d PCW %012lo cmdGrp %o cmd %02o subchan %02o mask %o IC %05o\n", channelNumber, unitNumber, PCW, cmdGrp, cmd, subChnNo, iom.masked[channelNumber], cpu.rIC);
sim_printf ("     hsla%d: Unhandled cmd grp %o %u.   XXX \n", cmdGrp, unitNumber, subChnNo);
//bail();
      break;
  } // switch cmdGrp
}

void offhookEvent (uint lineNo) {

  DBG_HSLA (sim_printf ("got offhook %d\n", lineNo);)
  HDBGNote (__func__, "got offhook %d", lineNo);


  word36 status = 0;
  
  putbits36 (& status,  0, 1, 1); // receive
  putbits36 (& status, 10, 1, 1); // change in data set status
  putbits36 (& status, 18, 1, 1); // data set ready
  putbits36 (& status, 19, 1, 1); // clear to send
  putbits36 (& status, 20, 1, 1); // carrier detect

  uint unitNumber = lineNo / 32;
  uint subChnNo = lineNo % 32;
  word18 subChanCommReg = commRegBase[unitNumber] + subChnNo * LEN_IOM_MBX_HSLA_SUBCHNL;
  struct hsla_s * hslap = & hslaData.hsla[unitNumber];
  struct hslaSubChnl_s * subChnlp = & hslaData.hsla[unitNumber].subChnl[subChnNo];

// dd01 says that the ICW first word is C/Y, but MCS uses it as an 18 bit address.

  word36 statusICW = coreReadDouble (subChanCommReg + h_aicw, __func__);
  word18 address = (word18) getbits36 (statusICW, 0, 18);
  coreWriteDouble (address, status, "hsla offhook status");
  
  // Update status ICW exhaust and tally
  word1 E = (word1) getbits36 (statusICW, 23, 1);
  word12 T = (word12) getbits36 (statusICW, 24, 12);
  if (E == 0) { // not exhausted
    if (T < 2) { // will be exhausted
      E = 1;
      T = 0;
    } else {
      T -= 2;
    }
    putbits36 (& statusICW, 0, 18, (address + 2) & BITS36);
    putbits36 (& statusICW, 23, 1, E);
    putbits36 (& statusICW, 24, 12, T);
  }
  coreWriteDouble (subChanCommReg + h_aicw, statusICW, __func__);

// active subchannel intr addresses
//   addr  level  sublevel  hsla   subchannel
//   004     4.     0.        1       0.
//   005     5.     0.        1      16.
//   024     4.     1.        1       1.
//   025     5.     1.        1      17
  uint level = (subChnNo > 16) ? 5 : 4;
  uint sublevel = subChnNo % 16;
//hdbgSnap = 1000;
  doIntr (level, sublevel, "offhook");

  //hdbg_mark ();

} //offhookEvent


void offhookCmdReceived (uv_stream_t * req, dnPkt * pkt) {
  uint lineNo;
  int n = sscanf (pkt->cmdHSLA.lineNo, "%02u", & lineNo);
  if (n != 1) {
    sim_printf ("ERR: %s unable to extract lineNo\n", __func__);
    HDBGNote0 (__func__, "ERR: unable to extract lineNo");
    return;
  }

  DBG_HSLA (sim_printf ("got offhook %d\n", lineNo);)
  HDBGNote (__func__, "got offhook %d", lineNo);

  offhookEvent (lineNo);
} // offhookCmdReceived


static void updateCnfg (uint hslano, uint subchnlno) {
  if (! hslaData.hsla[hslano].running)
    return;
  struct hslaSubChnl_s * subChnlp = & hslaData.hsla[hslano].subChnl[subchnlno];
  word18 cnfg = subChnlp->cnfg;

}


word18 hslaHandler (word18 addr, word18 data, bool writep) {
  uint hslano, subchnlno, offset;

  HDBGNote (__func__, "%s %s %06o %06o", __func__, writep ? "write" : "read ", addr, data); 
  //sim_printf ("%s %s %06o %06o IC %05o\n", __func__, writep ? "write" : "read ", addr, data, cpu.rIC); // DEBUG

  if (addr >= ADDR_IOM_MBX_HSLA1_BASE && addr < ADDR_IOM_MBX_HSLA1_BASE + LEN_IOM_MBX_HSLA) {
    hslano = 0;
  } else if (addr >= ADDR_IOM_MBX_HSLA2_BASE && addr < ADDR_IOM_MBX_HSLA2_BASE + LEN_IOM_MBX_HSLA) {
    hslano = 1;
  } else if (addr >= ADDR_IOM_MBX_HSLA3_BASE && addr < ADDR_IOM_MBX_HSLA3_BASE + LEN_IOM_MBX_HSLA) {
    hslano = 2;
  } else {
    printf ("%s invalid address %06o; ignoring\n", __func__, addr);
    return 0;
  }

  subchnlno = (addr - commRegBase[hslano]) / LEN_IOM_MBX_HSLA_SUBCHNL;
  offset = (addr - commRegBase[hslano]) % LEN_IOM_MBX_HSLA_SUBCHNL;
  struct hslaSubChnl_s * subChnlp = & hslaData.hsla[hslano].subChnl[subchnlno];

#define puteven(dw, val) putbits36 (& dw, 0, 18, val)
#define putodd(dw, val) putbits36 (& dw, 18, 18, val)
#define geteven(dw) getbits36 (dw, 0, 18)
#define getodd(dw) getbits36 (dw, 18, 18)
  char * rn[020] = {
     "ric0 even", "ric0 odd ",
     "ric1 even", "ric1 odd ",
     "sic0 even", "sic0 odd ",
     "sic1 even", "sic1 odd ",
     "baw      ", "sfcm     ",
     "mask even", "mask odd ",
     "aicw even", "aicw odd ",
     "cnfg even", "cnfg odd "
  };

  switch (offset) {

    case 000: {                 // ric0 primary recieve icw even
      if (writep) {
        puteven (subChnlp->ric0, data);
      } else {
        data =  geteven (subChnlp->ric0);
      }
      break;
    }

    case 001: {                 // ric0 primary recieve icw odd
      if (writep) {
        putodd (subChnlp->ric0, data);
      } else {
        data =  getodd (subChnlp->ric0);
      }
      break;
    }


    case 002: {                 // ric1 primary recieve icw even
      if (writep) {
        puteven (subChnlp->ric1, data);
      } else {
        data =  geteven (subChnlp->ric1);
      }
      break;
    }

    case 003: {                 // ric1 primary recieve icw odd
      if (writep) {
        putodd (subChnlp->ric1, data);
      } else {
        data =  getodd (subChnlp->ric1);
      }
      break;
    }


    case 004: {                 // sic0 primary recieve icw even
      if (writep) {
        puteven (subChnlp->sic0, data);
      } else {
        data =  geteven (subChnlp->sic0);
      }
      break;
    }

    case 005: {                 // sic0 primary recieve icw odd
      if (writep) {
        putodd (subChnlp->sic0, data);
      } else {
        data =  getodd (subChnlp->sic0);
      }
      break;
    }


    case 006: {                 // sic1 primary recieve icw even
      if (writep) {
        puteven (subChnlp->sic1, data);
      } else {
        data =  geteven (subChnlp->sic1);
      }
      break;
    }

    case 007: {                 // sic1 primary recieve icw odd
      if (writep) {
        putodd (subChnlp->sic1, data);
      } else {
        data =  getodd (subChnlp->sic1);
      }
      break;
    }


    case 010: {                 // baw 
      if (writep) {
        subChnlp->baw = data;
      } else {
        data =  subChnlp->baw;
      }
      break;
    }


    case 011: {                 // sfcm 
      if (writep) {
        subChnlp->sfcm = data;
      } else {
        data =  subChnlp->sfcm;
      }
      break;
    }


    case 012: {                 // mask even
      if (writep) {
        puteven (subChnlp->mask, data);
      } else {
        data =  geteven (subChnlp->mask);
      }
      break;
    }

    case 013: {                 // mask odd
      if (writep) {
        putodd (subChnlp->mask, data);
      } else {
        data =  getodd (subChnlp->mask);
      }
      break;
    }


    case 014: {                 // aicw even
      if (writep) {
        puteven (subChnlp->aicw, data);
      } else {
        data =  geteven (subChnlp->aicw);
      }
      break;
    }

    case 015: {                 // aicw odd
      if (writep) {
        putodd (subChnlp->aicw, data);
      } else {
        data =  getodd (subChnlp->aicw);
      }
      break;
    }


    case 016: {                 // cnfg even
      if (writep) {
        puteven (subChnlp->cnfg, data);
if (data == 0400000) { HDBGPrint(); exit (1); };
      } else {
        data =  geteven (subChnlp->cnfg);
      }
      break;
    }

    case 017: {                 // cnfg odd
      if (writep) {
        putodd (subChnlp->cnfg, data);
        // assuming that MCS always uses STAQ, the odd write should be the second,
        // and the write is complete.
          updateCnfg (hslano, subchnlno);
      } else {
        data =  getodd (subChnlp->cnfg);
      }
      break;
    }


    default : {
      sim_printf (">>>>>>>>>>>>>>>>> offset %o\n", offset);
      break;
    }

  } // switch offset

  HDBGNote (__func__, "hsla %d. line %d. %s %s %06o", hslano, subchnlno, rn[offset], writep ? "write" : "read ", data); 
  //sim_printf ("hsla %d. line %d. %s %s %06o\n", hslano, subchnlno, rn[offset], writep ? "write" : "read ", data); 
  return data;
}

void hslaInit (void) {
  memset (& hslaData, 0, sizeof (hslaData));
  for (int i = 0; i < N_HSLAS_CONFIGURED; i ++) {
    registerHWCM (commRegBase[i], LEN_IOM_MBX_HSLA, hslaHandler);
  }
  //registerHWCM (ADDR_IOM_MBX_HSLA1_BASE, LEN_IOM_MBX_HSLA, hslaHandler);
  //registerHWCM (ADDR_IOM_MBX_HSLA2_BASE, LEN_IOM_MBX_HSLA, hslaHandler);
  //registerHWCM (ADDR_IOM_MBX_HSLA3_BASE, LEN_IOM_MBX_HSLA, hslaHandler);
}

