//
// Examing the pattern of CIOC calls to the HSLA and comparing the notes
// in AN87 establishes that this MCS code is targeted to the DN6670.
//

#include "dn355.h"
#include "cpu.h"
#include "core.h"
#include "mem.h"
#include "iom.h"
#include "caf.h"
#include "dnpkt.h"
#include "coupler.h"
#include "hsla.h"
#include "console.h"
#include "timer.h"
#include "utils.h"
#include "pager.h"
#include "hdbg.h"
#include "dialin.h"

cpu_t cpu;

void cpuInit (void) {
  cpu.rIE = 0xffff;
  cpu.rII = false;
  cpu.rIR = 0;
  cpu.inhIntrChk = false;
  cpu.cycleCnt = 0;
}

// Indicator register (as defined by the LDI/STI instructions (DD01, pg 3-12)

//   bit 
// position    indicator
//
//    0        Zero
//    1        Negative
//    2        Carry
//    3        Overflow
//    4        Interrupt inhibit
//    5        Parity fault inhibit
//    6        Overflow fault inhibit
//    7        Parity error
//

static UNIT cpu_unit =
  {
    UDATA (NULL, UNIT_FIX|UNIT_BINK, MEM_SIZE), 0, 0, 0, 0, 0, NULL, NULL
  };

/* scp Debug flags */

static DEBTAB cpu_dt[] = 
  {
    { "DEBUG",      DBG_DEBUG       },
    { "TRACE",      DBG_TRACE       },
    { "REG",        DBG_REG         },
    { "FINAL",      DBG_FINAL       },
    { "CAF",        DBG_CAF         },
    { "SRC",        DBG_SRC         },
    { "IND",        DBG_IND         },
    { NULL,         0               }
  };

static t_stat cpu_boot (UNUSED int32_t unit_num, UNUSED DEVICE * dptr);

static config_value_list_t cfg_on_off [] =
  {
    { "off", 0 },
    { "on", 1 },
    { "disable", 0 },
    { "enable", 1 },
    { NULL, 0 }
  };

static config_list_t cpu_config_list [] = {
  { "portALogicalPort", 0, 07, NULL },
  { "portAInterlace", 0, 01, cfg_on_off },
  { "portAEnable", 0, 01, cfg_on_off },
  { "portAInitEnable", 0, 01, cfg_on_off },
  { "portAMemSize", 0, 07, NULL },
  { "portBLogicalPort", 0, 07, NULL },
  { "portBInterlace", 0, 01, cfg_on_off },
  { "portBEnable", 0, 01, cfg_on_off },
  { "portBInitEnable", 0, 01, cfg_on_off },
  { "portBMemSize", 0, 07, NULL },
  { "portCLogicalPort", 0, 07, NULL },
  { "portCInterlace", 0, 01, cfg_on_off },
  { "portCEnable", 0, 01, cfg_on_off },
  { "portCInitEnable", 0, 01, cfg_on_off },
  { "portCMemSize", 0, 07, NULL },
  { "portDLogicalPort", 0, 07, NULL },
  { "portDInterlace", 0, 01, cfg_on_off },
  { "portDEnable", 0, 01, cfg_on_off },
  { "portDInitEnable", 0, 01, cfg_on_off },
  { "portDMemSize", 0, 07, NULL },
  { "mbxAddr", 0, 077777777, NULL },
  { "intrCellSwitches", 0, 07, NULL },
  { "emerCellSwitches", 0, 07, NULL },
  { "lowerBound", 0, 0777, NULL },
  { "upperBound", 0, 0777, NULL },
  { "enableMemoryTimer", 0, 01, cfg_on_off },
  { "enableAddressBounds", 0, 01, cfg_on_off },
  { "CSWriteInhibit", 0, 01, cfg_on_off },
  { "fast", 0, 01, cfg_on_off },
  { "ovfEnable", 0, 01, cfg_on_off },
  { "booted", 0, 01, cfg_on_off },
  { "diag", 0, 1, cfg_on_off },
  { "switches", 0, 0777777, NULL },
  { NULL, 0, 0, NULL }
};

static t_stat cpu_set_config (UNIT * uptr, UNUSED int32_t value, const char * cptr, UNUSED void * desc) {
  config_state_t cfg_state = { NULL, NULL };
  for (;;) {
    int64_t v;
    int rc = cfg_parse (__func__, cptr, cpu_config_list, & cfg_state, & v);
    if (rc == -1) { // done
      break;
    }
    if (rc == -2) { // error
      cfg_parse_done (& cfg_state);
      return SCPE_ARG;
    }

    const char * p = cpu_config_list[rc].name;

#define KW(kw) (strcmp (p, #kw) == 0) cpu.kw = ((__typeof__ (cpu.kw)) v)
//#define KW(kw) (strcmp (p, #kw) == 0) cpu.kw = v
    if KW (portALogicalPort);
    else if KW (portAInterlace);
    else if KW (portAEnable);
    else if KW (portAInitEnable);
    else if KW (portAMemSize);
    else if KW (portBLogicalPort);
    else if KW (portBInterlace);
    else if KW (portBEnable);
    else if KW (portBInitEnable);
    else if KW (portBMemSize);
    else if KW (portCLogicalPort);
    else if KW (portCInterlace);
    else if KW (portCEnable);
    else if KW (portCInitEnable);
    else if KW (portCMemSize);
    else if KW (portDLogicalPort);
    else if KW (portDInterlace);
    else if KW (portDEnable);
    else if KW (portDInitEnable);
    else if KW (portDMemSize);
    else if KW (mbxAddr);
    else if KW (intrCellSwitches);
    else if KW (emerCellSwitches);
    else if KW (lowerBound);
    else if KW (upperBound);
    else if KW (enableMemoryTimer);
    else if KW (enableAddressBounds);
    else if KW (CSWriteInhibit);
    else if KW (fast);
    else if KW (ovfEnable);
    else if KW (booted);
    else if KW (diag);
    else if (strcmp (p, "switches") == 0)
      iom.channelStatus[077] = v;
    else {
      sim_printf ("error: cpu_set_config: invalid cfg_parse rc <%d>\n", rc);
      cfg_parse_done (& cfg_state);
      return SCPE_ARG;
    }
  } // process statements
  cfg_parse_done (& cfg_state);

  return SCPE_OK;
}

static t_stat cpu_show_config (UNUSED FILE * st, UNIT * uptr, UNUSED int32_t val, UNUSED const void * desc) {
  sim_printf ("Mailbox address %08o\n", cpu.mbxAddr);
  return SCPE_OK;
}

static REG cpu_reg [] =
  {
    {ORDATA (IC, cpu.rIC,    ASZ), 0, 0}, // Must be first, per simh
    {ORDATA (X1, cpu.rX1,    WSZ), 0, 0},
    {ORDATA (X2, cpu.rX2,    WSZ), 0, 0},
    {ORDATA (X3, cpu.rX3,    WSZ), 0, 0},
    {ORDATA (A,  cpu.rA,     WSZ), 0, 0},
    {ORDATA (Q,  cpu.rQ,     WSZ), 0, 0},
    {ORDATA (IR, cpu.rIR,    8),   0, 0}, // Indicator register
    //{ORDATA (S,  cpu.rS,     6),   0, 0}, // I/O channel select register
    {ORDATA (II, cpu.rII,    1),   0, 0}, // interrupt inhibit
    { NULL },
  };

REG * sim_PC = & cpu_reg [0];


static MTAB cpu_mod[] = {
  {
    MTAB_XTD | MTAB_VUN | MTAB_VDV | MTAB_NMO | MTAB_VALR,           // mask
    0,                         // match
    "CONFIG",                  // print string
    "CONFIG",                  // match string
    cpu_set_config,            // validation routine
    cpu_show_config,           // display routine
    NULL,                      // value descriptor
    NULL                       // help
  },
  { 0, 0, NULL, NULL, NULL, NULL, NULL, NULL }
};

DEVICE cpuDev =
  {
    "CPU",          /* name */
    & cpu_unit,     /* units */
    cpu_reg,        /* registers */
    cpu_mod,        /* modifiers */
    1,              /* #units */
    8,              /* address radix */
    ASZ,            /* address width */
    1,              /* addr increment */
    8,              /* data radix */
    WSZ,            /* data width */
    NULL /*&cpu_ex*/,        /* examine routine */
    NULL /*&cpu_dep*/,       /* deposit routine */
    NULL /*&cpu_reset*/,     /* reset routine */
    & cpu_boot,     /* boot routine */
    NULL,           /* attach routine */
    NULL,           /* detach routine */
    NULL,           /* context */
    DEV_DEBUG,      /* device flags */
    0,              /* debug control flags */
    cpu_dt,         /* debug flag names */
    NULL,           /* memory size change */
    NULL,           /* logical name */
    NULL,           // help
    NULL,           // attach help
    NULL,           // help context
    NULL            // description
  };

// ADDRESS FORMATION

//                                           1   1   1   1   1   1   1   1
//   0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// |     CY    |       WY                                                  |
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
//
//  CY   character address
//  WY   word address
//
//    Fractional       CY
//  Interpretation   Value    Character addressed
//  --------------   -----    -------------------
//     0/3            100     6-bit character number 0 (bits 0-5)
//     1/3            101     6-bit character number 1 (bits 6-11)
//     2/3            110     6-bit character number 2 (bits 12-17)
//     0/2            010     9-bit character number 0 (bits 0-8)
//     1/2            011     9-bit character number 1 (bits 9-17)
//   Full word        000     Full word
//
// When characters are address, they are transfered to and from memory 
// right justified. Unused bit are zeros for operations from memory and are
// ignored for operations to memory.
//
// An example of load and store upon 6-bit character No. 0 (CY = 100):
//
// Memory to processor or IOM:
//                                           1   1   1   1   1   1   1   1
//   0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// |   CHARACTER           |  ignored...                                   |
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// \                      /
//  -----------v----------
//             |
//             +-----------------------------------------------+
//                                                             |
//                                                             ^
//                                                  ----------------------
//                                                 /                      \\
//                                           1   1   1   1   1   1   1   1
//   0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// |   Zeros                                       |   CHARACTER           |
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
//
// Processor or IOM to memory:
//
//                                           1   1   1   1   1   1   1   1
//   0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// |   Ignored                                     |   CHARACTER           |
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
//                                                  \                      /
//                                                   -----------v----------
//                                                              |
//             +-----------------------------------------------+
//             |
//             ^
//   ----------------------
//  /                      \\
//                                           1   1   1   1   1   1   1   1
//   0   1   2   3   4   5   6   7   8   9   0   1   2   3   4   5   6   7
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+
// |  CHARACTER            |   Unchanged                                   |
// +---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+---+



// BASIC EFFECTIVE ADDRESS FORMATION
//
//  -------------------------------------------------------------------
//  |       | Instruction word            | Indirect word             |
//  |       -----------------------------------------------------------
//  |       |  I=0         |  I=1         |  I=9       |  I=1         |
//  -------------------------------------------------------------------
//  | T=00  |  Y**=IC+D    |  Y*=C(IC+D)  |  Y**=Y     |  Y*=C(Y)     |
//  | T=01  |  Y**=X1+D    |  Y*=C(X1+D)  |  Y**=X1+Y  |  Y*=C(X1+Y)  |
//  | T=02  |  Y**=X2+D    |  Y*=C(X2+D)  |  Y**=X2+Y  |  Y*=C(X2+Y)  |
//  | T=03  |  Y**=X3+D    |  Y*=C(X3+D)  |  Y**=X3+Y  |  Y*=C(X3+Y)  |
//  -------------------------------------------------------------------

// Mask table to assist ALS instruction in determining if the carry flag should be set/
// The ALS carry flag indicates if there was a change in BIT 0 of the word being shifted.
// If the value being shifted, with the mask and'ed for the shift count, equals zero or the mask,
// then all the bits are the same and no change will occur in BIT 0. If the values are not equal,
// then the bit will change and the carry flag will be set.
static word18 als_mask [] =
{
    0000000,
    0600000,
    0700000,
    0740000,
    0760000,
    0770000,
    0774000,
    0776000,
    0777000,
    0777400,
    0777600,
    0777700,
    0777740,
    0777760,
    0777770,
    0777774,
    0777776,
    0777777
};

static void dvf (void) {
  // C(AQ) / (Y)
  //  fractional quotient → C(A)
  //  fractional remainder → C(Q)

  // A 35-bit fractional dividend (including sign) is divided by a 18-bit
  // fractional divisor yielding a 18-bit fractional quotient (including
  // sign) and a 18-bit fractional remainder (including sign). C(AQ)71 is
  // ignored; bit position 17 of the remainder corresponds to bit position
  // 34 of the dividend. The remainder sign is equal to the dividend sign
  // unless the remainder is zero.
  //
  // If | dividend | >= | divisor | or if the divisor = 0, division does
  // not take place. Instead, a divide check fault occurs, C(AQ) contains
  // the dividend magnitude in absolute, and the negative indicator
  // reflects the dividend sign.

  // dividend format
  // 0  1     34 35
  // s  dividend x
  //  C(AQ)

  HDBGRegAR ();
  HDBGRegQR ();
  int sign = 1;
  bool dividendNegative = (getbits18 (cpu.rA, 0, 1) != 0);
  bool divisorNegative = (getbits18 (cpu.Y, 0, 1) != 0);

  // Get the 34 bits of the dividend (36 bits less the sign bit and the
  // ignored bit 35.

  // dividend format:   . d(0) ...  d(33)

  uint64_t zFrac = ((((uint64_t) cpu.rA) & BITS17) << 17) |
                   ((cpu.rQ >> 1) & BITS17);

  if (dividendNegative) {
    zFrac = (uint64_t) (((int64_t) (~zFrac)) + 1);
    sign = - sign;
  }
  zFrac &= BITS34;

  // Get the 17 bits of the divisor (18 bits less the sign bit)

  // divisor format: . d(0) .... d(16) 0 0 0 .... 0

  // divisor goes in the low half
  uint64_t dFrac = cpu.Y & BITS17;
  if (divisorNegative) {
    dFrac = (uint64_t) (((int64_t) (~dFrac)) + 1);
    sign = - sign;
  }
  dFrac &= BITS17;

  if (dFrac == 0) {
    // ISOLTS 730 expects the right to be zero and the sign
    // bit to be untouched.
    cpu.rQ = cpu.rQ & (BITS17 << 1);

    SCF (cpu.Y == 0, cpu.rIR, I_ZERO);
    SCF (cpu.rA & SIGN18, cpu.rIR, I_NEG);
    HDBGRegIW ();
    doFault (faultDivideCheck, "DVF: divide check fault");
    return;
  }

// Predict overflow
  bool ovf = zFrac >= (dFrac << 18);


// Do the division

  uint64_t quot = zFrac / dFrac;
  uint64_t remainder = zFrac % dFrac;




  // I am surmising that the "If | dividend | >= | divisor |" is an
  // overflow prediction; implement it by checking that the calculated
  // quotient will fit in 35 bits.

  if (ovf) {
    HDBGRegAR ();
    HDBGRegQR ();
    bool Aneg = (cpu.rA & SIGN18) != 0; // blood type
    bool AQzero = cpu.rA == 0 && cpu.rQ == 0;
    if (cpu.rA & SIGN18) {
      cpu.rA = (~cpu.rA) & BITS18;
      HDBGRegAW ();
      cpu.rQ = (~cpu.rQ) & BITS18;
      cpu.rQ += 1;
      HDBGRegQW ();
      // XXX This assumes rQ can hold 19 bits.
      if (cpu.rQ & 01000000) { // overflow?
        cpu.rQ &= BITS18;
        HDBGRegQW ();
        cpu.rA = (cpu.rA + 1) & BITS18;
        HDBGRegAW ();
      }
    }
    // ISOLTS 730 expects the right to be zero and the sign
    // bit to be untouched.
    cpu.rQ = cpu.rQ & (BITS17 << 1);
    HDBGRegQW ();

    SCF (AQzero, cpu.rIR, I_ZERO);
    SCF (Aneg, cpu.rIR, I_NEG);
    HDBGRegIW ();

    doFault(faultDivideCheck, "DVF: divide check fault");
    return;
  }

  if (sign == -1)
    quot = ~quot + 1;
  if (dividendNegative)
    remainder = ~remainder + 1;
  cpu.rA = quot & BITS18;
  HDBGRegAW ();
  cpu.rQ = remainder & BITS18;
  HDBGRegQW ();

  SCF (cpu.rA == 0 && cpu.rQ == 0, cpu.rIR, I_ZERO);
  SCF (cpu.rA & SIGN18, cpu.rIR, I_NEG);
  HDBGRegIW ();
}












struct opc_t
  {
    char * name;
    // opcG1a: 73
    // opcG1b: 22
    // opcG1c: 52
    // opcG1d: 12
    // opcG2:  33
    enum { opcILL, opcMR, opcG1a, opcG1b, opcG1c, opcG1d, opcG2 } grp; // opcode group (memory, group1, group2)
// prepare address is implied by grp == opcMR
// opcRD operand read
// oprWR operand write
    bool opRD, opWR;
    enum { opW, opDW } opSize;

#define OP_CA    false, false, opW
#define OP_RD    true,  false, opW
#define OP_WR    false, true,  opW
#define OP_RMW   true,  true,  opW
#define OP_DCA   false, false, opDW
#define OP_DRD   true,  false, opDW
#define OP_DWR   false, true,  opDW
#define OP_DRMW  true,  true,  opDW
#define OP_NULL  false, false, 0
  };

static struct opc_t opcTable [64] =
  {
// 00 - 07
    { "ill",     opcILL, OP_NULL   }, // 00
    { "MPF",     opcMR,  OP_RD     }, // 01 Multiply fraction
    { "ADCX2",   opcMR,  OP_RD     }, // 02
    { "LDX2",    opcMR,  OP_RD     }, // 03
    { "LDAQ",    opcMR,  OP_DRD    }, // 04
    { "ill",     opcILL, OP_NULL   }, // 05
    { "ADA",     opcMR,  OP_RD     }, // 06
    { "LDA",     opcMR,  OP_RD     }, // 07

// 10 - 17
    { "TSY",     opcMR,  OP_WR     }, // 10
    { "ill",     opcILL, OP_NULL   }, // 11
    { "grp1d",   opcG1d, OP_NULL   }, // 12
    { "STX2",    opcMR,  OP_WR     }, // 13
    { "STAQ",    opcMR,  OP_DWR    }, // 14
    { "ADAQ",    opcMR,  OP_DRD    }, // 15
    { "ASA",     opcMR,  OP_RMW    }, // 16
    { "STA",     opcMR,  OP_WR     }, // 17

// 20 - 27
    { "SZN",     opcMR,  OP_RD     }, // 20
    { "DVF",     opcMR,  OP_RD     }, // 21
    { "grp1b",   opcG1b, OP_NULL   }, // 22
    { "CMPX2",   opcMR,  OP_RD     }, // 23
    { "SBAQ",    opcMR,  OP_DRD    }, // 24
    { "ill",     opcILL, OP_NULL   }, // 25
    { "SBA",     opcMR,  OP_RD     }, // 26
    { "CMPA",    opcMR,  OP_RD     }, // 27

// 30 - 37
    { "LDEX",    opcMR,  OP_NULL   }, // 30
    { "CANA",    opcMR,  OP_RD     }, // 31
    { "ANSA",    opcMR,  OP_RMW    }, // 32
    { "grp2",    opcG2             }, // 33
    { "ANA",     opcMR,  OP_RD     }, // 34
    { "ERA",     opcMR,  OP_RD     }, // 35
    { "SSA",     opcMR,  OP_RMW    }, // 36
    { "ORA",     opcMR,  OP_RD     }, // 37

// 40 - 47
    { "ADCX3",   opcMR,  OP_RD     }, // 40
    { "LDX3",    opcMR,  OP_RD     }, // 41
    { "ADCX1",   opcMR,  OP_RD     }, // 42
    { "LDX1",    opcMR,  OP_RD     }, // 43
    { "LDI",     opcMR,  OP_RD     }, // 44
    { "TNC",     opcMR,  OP_NULL   }, // 45
    { "ADQ",     opcMR,  OP_RD     }, // 46
    { "LDQ",     opcMR,  OP_RD     }, // 47

// 50 - 57
    { "STX3",    opcMR,  OP_WR     }, // 50
    { "ill",     opcILL, OP_NULL   }, // 51
    { "grp1c",   opcG1c, OP_NULL   }, // 52
    { "STX1",    opcMR,  OP_WR     }, // 53
    { "STI",     opcMR,  OP_WR     }, // 54
    { "TOV",     opcMR,  OP_NULL   }, // 55
    { "STZ",     opcMR,  OP_WR     }, // 56
    { "STQ",     opcMR,  OP_WR     }, // 57

// 60 - 67
    { "CIOC",    opcMR,  OP_NULL   }, // 60
    { "CMPX3",   opcMR,  OP_RD     }, // 61
    { "ERSA",    opcMR,  OP_RMW    }, // 62
    { "CMPX1",   opcMR,  OP_RD     }, // 63
    { "TNZ",     opcMR,  OP_NULL   }, // 64
    { "TPL",     opcMR,  OP_NULL   }, // 65
    { "SBQ",     opcMR,  OP_RD     }, // 66
    { "CMPQ",    opcMR,  OP_RD     }, // 67

// 70 - 77
    { "STEX",    opcMR,  OP_WR     }, // 70
    { "TRA",     opcMR,  OP_NULL   }, // 71
    { "ORSA",    opcMR,  OP_RMW    }, // 72
    { "grp1a",   opcG1a, OP_NULL   }, // 73
    { "TZE",     opcMR,  OP_NULL   }, // 74
    { "TMI",     opcMR,  OP_NULL   }, // 75
    { "AOS",     opcMR,  OP_RMW    }, // 76
    { "ill",     opcILL, OP_NULL   } // 77

  };

char * disassemble (word15 addr, word18 ins) {
  static char result[132] = "???";
  char * rp = NULL;
  char kstr[5] = "";
  word6 OPCODE = (word6) getbits18 (ins, 3, 6);
  word3 S1 = (word3) getbits18 (ins, 0, 3);
  word3 S2 = (word3) getbits18 (ins, 9, 3);
  word3 D = (word3) getbits18 (ins, 9, 9);
  word6 K = (word6) getbits18 (ins, 12, 6);

  switch (opcTable[OPCODE].grp) {
    case opcILL:
    case opcMR: {
      word1 I = (word1) getbits18 (ins, 0, 1);
      word2 T = (word2) getbits18 (ins, 1, 2);
      sprintf (result, "%s %o", opcTable[OPCODE].name, D);
      if (I)
        strcat (result, ",I");
      if (T)
        sprintf (result + strlen (result), ",%o", T);
      sprintf (result + strlen (result), "\t\"%05o-*", (SIGNEXT9_18 (D & BITS9) + addr) & BITS15);
      break;
    }

    case opcG1a: {
      switch (S1) {
        case 0: // SEL
          rp = "SEL";
          break;
        case 1: // IACX1
          rp = "IACX1";
          break;
        case 2: // IACX2
          rp = "IACX2";
          break;
        case 3: // IACX3
          rp = "IACX3";
          break;
        case 4:  // ILQ
          rp = "ILQ";
          break;
        case 5: // IAQ
          rp = "IAQ";
          break;
        case 6:  // ILA
          rp = "ILA";
           break;
         case 7: // IAA
           rp = "IAA";
           break;
      } // switch (S1
      sprintf (kstr, " %03o", D);
      break;
    } // opcG1a

    case opcG1b: {
      switch (S1) {
        case 0:  // IANA
          rp = "IANA";
          break;
        case 1:  // IORA
          rp = "IORA";
          break;
        case 2: // ICANA
          rp = "ICANA";
          break;
        case 3:  // IERA
          rp = "IERA";
          break;
        case 4:  // ICMPA
          rp = "ICMPA";
          break;
        default:
          break;
      } // S1;
      sprintf (kstr, " %03o", D);
      break;
    } // grpib;

    case opcG1c: {
      switch (S1) {
        case 0:  // SIER
          rp = "SIER";
          break;
        case 4:  // SIC
          rp = "SIC";
          break;
        default:
          break;
      } // switch (S1)
      sprintf (kstr, " %03o", D);
      break;
    } // grp1c

    case opcG1d: {
      switch (S1) {
        case 0:  // RIER
          rp = "RIER";
          break;
        case 4:  // RIA
          rp = "RIA";
        default:
          break;
      } // S1;
      sprintf (kstr, " %03o", D);
      break;
    } // grpid;

    case opcG2: {
      switch (S1) {
        case 0: {
          switch (S2) {
            case 2: // CAX2
              rp = "CAX2";
              break;
            case 4: // LLS
              rp = "LLS";
              break;
            case 5: // LRS
              rp = "LRS";
              break;
            case 6: // ALS
              rp = "ALS";
              break;
            case 7: // ARS
              rp = "ARS";
              break;
            default:
              break;
          } // switch (S2)
          break;
        } // S1 0
        case 1: {
          switch (S2) {
            case 2: // CAR
              rp = "CAR";
              break;
            case 4: // NRML
              rp = "NRML";
              break;
            case 6: // NRM
              rp = "NRM";
              break;
            default:
              break;
          } // switch (S2)
          break;
        } // S1 1
        case 2: {
          switch (S2) {
            case 1: // NOP
              rp = "NOP";
              break;
            case 2: // CX1A
              rp = "CX1A";
              break;
            case 4: // LLR
              rp = "LLR";
              break;
            case 5: // LRL
              rp = "LRL";
              break;
            case 6: // ALR
              rp = "ALR";
              break;
            case 7: // ARL
              rp = "ARL";
              break;
            default:
              break;
          } // switch (S2)
          break;
        } // S1 2
        case 3: {
          switch (S2) {
            case 1: // INH
              rp = "INH";
              break;
            case 2: // CX2A
              rp = "CX2A";
              break;
            case 3: // CX3A 
              rp = "CX3A";
              break;
            case 6: // ALP
              rp = "ALP";
              break;
            default:
              break;
          } // switch (S2)
          break;
        } // S1 3
        case 4: {
          switch (S2) {
            case 1: // DIS
              rp = "DIS";
              break;
            case 2: // CAX1
              rp = "CAX1";
              break;
            case 3: // CAX3
              rp = "CAX3";
              break;
            case 6: // QLS
              rp = "QLS";
              break;
            case 7: // QRS
              rp = "QRS";
              break;
            default:
              break;
          } // switch (S2)
          break;
        } // S1 4
        case 5: {
          switch (S2) {
            case 1:
              rp = "CAQC";
              break;
            case 3:
              rp = "CCQ";
              break;
            default:
              break;
          } // S2
        } // S1 5
        case 6: {
          switch (S2) {
            case 3: // CAQ
              rp = "CAQ";
              break;
            case 6: // QLR
              rp = "QLR";
              break;
            case 7: // QRL
              rp = "QRL";
              break;
            default:
              break;
          } // switch (S2)
          break;
        } // S1 6
        case 7: {
          switch (S2) {
            case 1: // ENI
              rp = "ENI";
              break;
            case 2: // CRA
              rp = "CRA";
              break;
            case 3: // CQA
              rp = "CQA";
              break;
            case 6: // QLP
              rp = "QLP";
              break;
            default:
              break;
          } // switch (S2)
          break;
        } // S1 7
        default:
          break;
      } // switch (S1)
      sprintf (kstr, " %02o", K);
      break;
    } // opcG2
    break;
  } // grp
  if (rp) {
    strcpy (result, rp);
    strcat (result, kstr);
  }
  return result;
} // disassemble


static void dlyDoFault (word15 faultAddr, const char * msg) {
  sim_debug (DBG_TRACE, & cpuDev, "Delay fault %05o %s\n", faultAddr, msg);
  cpu.dlyFault = true;
  cpu.dlyFaultAddr = faultAddr;
  cpu.dlyCtx = msg;
}

void doFault (word15 faultAddr, const char * msg) {
  sim_debug (DBG_TRACE, & cpuDev, "Fault %05o %s\n", faultAddr, msg);
#ifdef HDBG
  if (faultAddr == faultIllegalOpcode) { sim_printf ("ill op\n"); hdbg_print (0, NULL); exit (1);}
#endif
  // Fault vector
  word15 ind = (word15) (fromMemory (faultAddr, __func__) & BITS15);
  // Simulate a TSY
  // Store the IC in the handler entry word
  HDBGRegICR ();
  toMemory (cpu.rIC, ind, 0, 0, __func__);
  cpu.rIC = (ind + 1) & BITS15;
  HDBGRegICW ();
  longjmp (jmpMain, JMP_REENTRY);
}

// pg 3-9
// "The interrupt sequence begines whn a channel specifires one of six Set
// Interrupt call operation codes, one of 16 interrupt levels and one of
// 16 interrupt sublevels.

static void intrScan (void) {
  cpu.intrPresent = false;
  for (uint level = 0; level < 16; level ++) {
// debug
    {
      word15 addr = (word15) (ADDR_PROG_INT_CELL_BASE + level);
      word18 sublvlwd = fromMemory (addr, __func__);
      if ((sublvlwd & 0777774) == 0) { // no sublevel bits set
        if (cpu.cells[level]) {
          sim_printf ("XXX intrScan cell %u set; no sublevels set\n", level);
        }
      } else {
        if (! cpu.cells[level]) {
          sim_printf ("XXX intrScan cell %u not set; sublevels %06o set\n", level, sublvlwd);
        }
      }
    }
    if (cpu.cells [level]) {
      cpu.intrPresent = true;
      return;
    }
  }
}

// dd01, pg 54
void doIntr (uint level, uint sublevel, const char * ctx) {
  if (! cpu.booted)
    return;
  // "When the service request is answered, the IOM caues the memory controller
  // to set the flip/flop interrupt cell corresponding to the priority level of
  // the interrupt.
  HDBGIntrSet (level, sublevel, ctx);
  sublevel &= 017;
  cpu.cells [level] = true;
  cpu.intrPresent = true;

  word15 addr = (((word18) ADDR_PROG_INT_CELL_BASE) + ((word18) level)) & BITS15;
  word18 bits = (0400000 >> sublevel) | 3;
  toMemory (bits, addr, 0, bits, __func__);
  DBG_INTR (sim_printf ("interrupt set level %o sublevel %o addr %06o bits %o\n", level, sublevel, addr, bits));
DBG_PRTR(if (level == PRINTER_CHANNEL_NO)
  sim_printf ("interrupt set level %o sublevel %o addr %06o bits %o\n", level, sublevel, addr, bits);)
}


static void doUnimp (word6 opc) NO_RETURN;

static void doUnimp (word6 opc) {
  sim_printf ("unimplemented %02o at %05o\n", opc, cpu.rIC);
  // XXX more later
  longjmp (jmpMain, JMP_STOP);
}

static inline word36 SIGNEXT18_36 (word18 w) {
  if (w & SIGN18) {
    return (w | ((word36) BITS18) << 18) & BITS36;
  }
  return w & BITS18;
}


static word18 addAddr3 (word15 W, word3 C, word18 chAddr) {
  HDBGNote (__func__,  "W %05o C %o chAddr %06o", W, C, chAddr);
  // Extract C and ADDR from chAddr
  word3 c = (chAddr >> 15) & BITS3;
  word15 w = chAddr & BITS15;

  switch (C) {
    case 0: // Word
      if (c == 7) { // Illegal
        HDBGNote (__func__,  "W %05o + 7 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
        //doFault (faultIllegalStore, "addAddr3 word + !word");
        return ((w + W) & BITS15) | 0700000;
      }
      if (c == 0) { // Word + word
        HDBGNote (__func__,  "W %05o + W %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
        return ((word18) W + (word18) w) & BITS15; // implied C == 0.
      }
      // W + charptr
      HDBGNote (__func__,  "W %05o + ptr %05o -> %06o", W, w, ((w + W) & BITS15) | (c << 15));
      return ((w + W) & BITS15) | (((word18) c) << 15);

    case 1: // Word + Double word
      //doFault (faultIllegalStore, __func__,  double word");
      HDBGNote (__func__,  "DW %05o + W %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
      return ((w + W) & BITS15) | 0700000;

    case 2: { // 0/2
      switch (c) {
        case 0: // 0/2 + word
          HDBGNote (__func__,  "0/2 %05o + W %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 0/2 + word");
          return ((w + W) & BITS15) | 0700000;

        case 1: // 0/2 + double word
          HDBGNote (__func__,  "0/2 %05o + DW %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 0/2 + double word");
          return ((w + W) & BITS15) | 0700000;

        case 2: // 0/2 + 0/2
          HDBGNote (__func__,  "0/2 %05o + 0/2 %05o -> %06o", W, w, ((w + W) & BITS15) | 0200000);
          return ((w + W) & BITS15) | 0200000;

        case 3: // 0/2 + 1/2
          HDBGNote (__func__,  "0/2 %05o + 1/2 %05o -> %06o", W, w, ((w + W) & BITS15) | 0300000);
          return ((w + W) & BITS15) | 0300000;

        case 4: // 0/2 + 0/3
          HDBGNote (__func__,  "0/2 %05o + 0/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 0/2 + 0/3");
          return ((w + W) & BITS15) | 0700000;

        case 5: // 0/2 + 1/3
          HDBGNote (__func__,  "0/2 %05o + 1/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 0/2 + 1/3");
          return ((w + W) & BITS15) | 0700000;

        case 6: // 0/2 + 2/3
          HDBGNote (__func__,  "0/2 %05o + 2/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 0/2 + 2/3");
          return ((w + W) & BITS15) | 0700000;

        case 7:
          HDBGNote (__func__,  "0/2 %05o +  7 %05o", W, w);
          doFault (faultIllegalStore, "addAddr3 0/2 + 7");
      } // switch (c)
    } // 0/2

    case 3: { // 1/2
      switch (c) {
        case 0: // 1/2 + word
          HDBGNote (__func__,  "1/2 %05o + W %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 1/2 + word");
          return ((w + W) & BITS15) | 0700000;

        case 1: // 1/2 + double word
          HDBGNote (__func__,  "1/2 %05o + DW %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 1/2 + double word");
          return ((w + W) & BITS15) | 0700000;

        case 2: // 1/2 + 0/2
          HDBGNote (__func__,  "1/2 %05o + 0/2 %05o -> %06o", W, w, ((w + W) & BITS15) | 0300000);
          return ((w + W) & BITS15) | 0300000;

        case 3: // 1/2 + 1/2
          HDBGNote (__func__,  "1/2 %05o + 1/2 %05o -> %06o", W, w, ((w + W + 1) & BITS15) | 0200000);
          return ((w + W + 1) & BITS15) | 0200000;

        case 4: // 1/2 + 0/3
          HDBGNote (__func__,  "1/2 %05o + 0/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 1/2 + 0/3");
          return ((w + W) & BITS15) | 0700000;

        case 5: // 1/2 + 1/3
          HDBGNote (__func__,  "1/2 %05o + 1/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 1/2 + 1/3");
          return ((w + W) & BITS15) | 0700000;

        case 6: // 1/2 + 2/3
          HDBGNote (__func__,  "1/2 %05o + 2/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 1/2 + 2/3");
          return ((w + W) & BITS15) | 0700000;

        case 7:
          HDBGNote (__func__,  "1/2 %05o +  7 %05o", W, w);
          doFault (faultIllegalStore, "addAddr3 1/2 + 7");
      } // switch (c)
    } // 1/2


    case 4: { // 0/3
      switch (c) {
        case 0: // 0/3 + word
          HDBGNote (__func__,  "0/3 %05o + W %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 0/3 + word");
          return ((w + W) & BITS15) | 0700000;

        case 1: // 0/3 + double word
          HDBGNote (__func__,  "0/3 %05o + DW %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 0/3 + double word");
          return ((w + W) & BITS15) | 0700000;

        case 2: // 0/3 + 0/2
          HDBGNote (__func__,  "0/3 %05o + 0/2 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 0/3 + 0/2");
          return ((w + W) & BITS15) | 0700000;

        case 3: // 0/3 + 1/2
          HDBGNote (__func__,  "0/3 %05o + 1/2 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 0/3 + 1/2");
          return ((w + W) & BITS15) | 0700000;

        case 4: // 0/3 + 0/3
          HDBGNote (__func__,  "0/3 %05o + 0/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0400000);
          return ((w + W) & BITS15) | 0400000;

        case 5: // 0/3 + 1/3
          HDBGNote (__func__,  "0/3 %05o + 1/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0500000);
          return ((w + W) & BITS15) | 0500000;

        case 6: // 0/3 + 2/3
          HDBGNote (__func__,  "0/3 %05o + 2/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0600000);
          return ((w + W) & BITS15) | 0600000;

        case 7:
          HDBGNote (__func__,  "1/2 %05o +  7 %05o", W, w);
          doFault (faultIllegalStore, "addAddr3 0/3 + 7");
      } // switch (c)
    } // 1/2


    case 5: { // 1/3
      switch (c) {
        case 0: // 1/3 + word
          HDBGNote (__func__,  "1/3 %05o + W %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 1/3 + word");
          return ((w + W) & BITS15) | 0700000;

        case 1: // 1/3 + double word
          HDBGNote (__func__,  "1/3 %05o + DW %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 1/3 + double word");
          return ((w + W) & BITS15) | 0700000;

        case 2: // 1/3 + 0/2
          HDBGNote (__func__,  "1/3 %05o + 0/2 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 1/3 + 0/2");
          return ((w + W) & BITS15) | 0700000;

        case 3: // 1/3 + 1/2
          HDBGNote (__func__,  "1/3 %05o + 1/2 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 1/3 + 1/2");
          return ((w + W) & BITS15) | 0700000;

        case 4: // 1/3 + 0/3
          HDBGNote (__func__,  "1/3 %05o + 0/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0500000);
          return ((w + W) & BITS15) | 0500000;

        case 5: // 1/3 + 1/3
          HDBGNote (__func__,  "1/3 %05o + 1/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0600000);
          return ((w + W) & BITS15) | 0600000;

        case 6: // 1/3 + 2/3
          HDBGNote (__func__,  "1/3 %05o + 2/3 %05o -> %06o", W, w, ((w + W + 1) & BITS15) | 0400000);
          return ((w + W + 1) & BITS15) | 0400000;

        case 7:
          HDBGNote (__func__,  "1/3 %05o +  7 %05o", W, w);
          doFault (faultIllegalStore, "addAddr3 1/3 + 7");
      } // switch (c)
    } // 1/3

    case 6: { // 2/3
      switch (c) {
        case 0: // 2/3 + word
          HDBGNote (__func__,  "2/3 %05o + W %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 2/3 + word");
          return ((w + W) & BITS15) | 0700000;

        case 1: // 2/3 + double word
          HDBGNote (__func__,  "2/3 %05o + DW %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 2/3 + double word");
          return ((w + W) & BITS15) | 0700000;

        case 2: // 2/3 + 0/2
          HDBGNote (__func__,  "2/3 %05o + 0/2 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 2/3 + 0/2");
          return ((w + W) & BITS15) | 0700000;

        case 3: // 2/3 + 1/2
          HDBGNote (__func__,  "2/3 %05o + 1/2 %05o -> %06o", W, w, ((w + W) & BITS15) | 0700000);
          //doFault (faultIllegalStore, "addAddr3 2/3 + 1/2");
          return ((w + W) & BITS15) | 0700000;

        case 4: // 2/3 + 0/3
          HDBGNote (__func__,  "2/3 %05o + 0/3 %05o -> %06o", W, w, ((w + W) & BITS15) | 0600000);
          return ((w + W) & BITS15) | 0600000;

        case 5: // 2/3 + 1/3
          HDBGNote (__func__,  "2/3 %05o + 1/3 %05o -> %06o", W, w, ((w + W + 1) & BITS15) | 0400000);
          return ((w + W + 1) & BITS15) | 0400000;

        case 6: // 2/3 + 2/3
          HDBGNote (__func__,  "2/3 %05o + 2/3 %05o -> %06o", W, w, ((w + W + 1) & BITS15) | 0500000);
          return ((w + W + 1) & BITS15) | 0500000;

        case 7:
          HDBGNote (__func__,  "2/3 %05o +  7 %05o", W, w);
          doFault (faultIllegalStore, "addAddr3 2/3 + 7");
      } // switch (c)
    } // 2/3

    case 7:
      HDBGNote (__func__,  "7 %05o + %05o", W, w);
      doFault (faultIllegalStore, "addAddr3 7");
  } // switch (C)
  // Can't get here
  return ((w + W) & BITS15) | 0700000;
}


static word18 addAddr2 (word18 chaddr1, word18 chaddr2) {
  return addAddr3 (chaddr1 & BITS15, (chaddr1 >> 15) & BITS3, chaddr2);
}

t_stat simh_hooks (void) {
  int reason = 0;

  if (stop_cpu)
    return STOP_STOP;

  if (sim_interval <= 0) {
    if ((reason = sim_process_event ()))
      return reason;
  }
  if (! cpu.disState)
    sim_interval --;
  return reason;
}

//#define DUMP_MEMORY
#ifdef DUMP_MEMORY
static void dumpMemory (void) {

// Dump after istart
// GICB has finished uploading MCS; copy memory out for the FPGA hardware pre-IOM build
//        00701  1 71 777              430        tra     -1,1            enter mcs

//       04427  4 50 134     4563    1971        stx3    a.t017-*,*      (intcel) set interrupt cell
//DBG(201828)> CPU TRACE: 47507:450134 STX3 134,I   "47643-*^M
//DBG(201828)> CPU REG: rX3 RD 036100

//                                   1981                **************************************************************
//                                   1982                * clear all unused configured memory including extended memory
//                                   1983                **************************************************************
//                                   1984
//       04435  4 07 121     4556    1985        lda     a.t008-*,*      (.crmem) get memory size
// DBG(201834)> CPU TRACE: 47515:407121 LDA 121,I    "47636-*^M




// Dump after mcs/fmp read
//                                    246                check dia status from mcs/fnp read
//                                    247
//  end of binary card gicb0007
//       00464  0 47 771      455     248        ldq     diasts+1-*      check second status word
//DBG(2630)> CPU TRACE: 00464:047771 LDQ 371        "01055-*^M


  bool late = cpu.rIC == 047715 && ins == 0407121;
  bool early = cpu.rIC == 000464 && ins == 0047771;

  if (late || early) {
    uint highWater = 0;
    for (uint addr = 0; addr < MEM_SIZE; addr ++)
      if (cpu.M[addr])
        highWater = addr;
    //sim_printf ("highWater %o\n", highWater);
    //sim_printf ("transfer address %o\n", cpu.W);
    //sim_printf ("transfer address %o\n", cpu.rIC);
    FILE * fp;
    if (late)
      fp = fopen ("dn355.mif", "w");
    else
      fp = fopen ("dn355_1.mif", "w");
    fprintf (fp, "WIDTH=18;\n");
    fprintf (fp, "DEPTH=32768;\n");
    fprintf (fp, "ADDRESS_RADIX=OCT;\n");
    //fprintf (fp, "DATA_RADIX=OCT;\n");
    fprintf (fp, "DATA_RADIX=BIN;\n");
    fprintf (fp, "-- Entry point address %05o\n", cpu.rIC);
    fprintf (fp, "CONTENT BEGIN\n");
    for (uint addr = 0; addr <= highWater; addr ++) {
      word18 w = cpu.M[addr];
      if (w) {
        fprintf (fp, "   %05o : ", addr);
#define bit(x) fprintf (fp, "%d", (w & (x)) ? 1 : 0);
        bit (0400000);
        bit (0200000);
        bit (0100000);
        bit (0040000);
        bit (0020000);
        bit (0010000);
        bit (0004000);
        bit (0002000);
        bit (0001000);
        bit (0000400);
        bit (0000200);
        bit (0000100);
        bit (0000040);
        bit (0000020);
        bit (0000010);
        bit (0000004);
        bit (0000002);
        bit (0000001);
#undef bit
        fprintf (fp, ";\n");
#if 0
        fprintf (fp, "   %05o : %06o;\n", addr, w);
#endif
#if 0
        fprintf (fp, "@%x\n", addr);
#define bit(x) fprintf (fp, "%d", (w & (x)) ? 1 : 0);
        bit (0400000);
        bit (0200000);
        bit (0100000);
        bit (0040000);
        bit (0020000);
        bit (0010000);
        bit (0004000);
        bit (0002000);
        bit (0001000);
        bit (0000400);
        bit (0000200);
        bit (0000100);
        bit (0000040);
        bit (0000020);
        bit (0000010);
        bit (0000004);
        bit (0000002);
        bit (0000001);
#undef bit
        fprintf (fp, "\n");
#endif
      }
    }
    fprintf (fp, "END;\n");
    fclose (fp);
  }
}
#endif

void brkbrk (void) {}

jmp_buf jmpMain;

static uint levelMask[16] = {
  0100000, 0040000, 0020000, 0010000,
  0004000, 0002000, 0001000, 0000400,
  0000200, 0000100, 0000040, 0000020,
  0000010, 0000004, 0000002, 0000001
};

t_stat sim_instr (void) {

  if (!hslaData.started)
    hslaUvStart ();

  static int reason = 0;

  int val = setjmp (jmpMain);
  switch (val) {
    case JMP_ENTRY:
    case JMP_REENTRY:
      reason = 0;
      break;
    case JMP_STOP:
      goto leave;
    default:
      sim_printf ("longjmp value of %d unhandled\n", val);
      goto leave;
  }

  cpu.inhIntrChk = false;
  do {
    if (! cpu.disState)
      cpu.cycleCnt ++;

    reason = simh_hooks ();
    if (reason) {
      break;
    }

    uv_run (uv_default_loop (), UV_RUN_NOWAIT);

    iomRun ();
    couplerRun ();
    consoleProcess ();
#ifndef WALL
    {
      static int cnt = 0;
      cnt = (cnt + 1) & 1023;
      if (! cnt)
       updateTimers ();
    }
#endif
    // Check for outstanding interrupts and process if required. 

#if 0
    // II is ignored in DIS. (DD01, pg 3-65 (110)
    HDBGNote ("cpu intr check", "rII %o disState %o inhIntrChk %o rIE %o cells %o%o%o%o%o%o%o%o%o%o%o%o%o%o%o%o",
              cpu.rII, cpu.disState, cpu.inhIntrChk, cpu.rIE,
              cpu.cells[0], cpu.cells[1], cpu.cells[2], cpu.cells[3],
              cpu.cells[4], cpu.cells[5], cpu.cells[6], cpu.cells[7],
              cpu.cells[8], cpu.cells[9], cpu.cells[10], cpu.cells[11],
              cpu.cells[12], cpu.cells[13], cpu.cells[14], cpu.cells[15]);
#endif

DBG_PRTR(if (cpu.cells[PRINTER_CHANNEL_NO]) sim_printf ("XXXX ptr set rII %u disState %u inhIntrChk %u cpu.rIE & levelMask[level] %06o\n",
cpu.rII, cpu.disState, cpu.inhIntrChk, cpu.rIE & levelMask[PRINTER_CHANNEL_NO]);)
    if (((! cpu.rII) || cpu.disState) && (! cpu.inhIntrChk)) {
      if (cpu.intrPresent) {
        // dd01 pg 54
        uint level;
        for (level = 0; level < N_INTR_CELLS; level ++) {
          if ((cpu.rIE & levelMask[level]) && cpu.cells[level])
            break;
        }
DBG_PRTR(if (level == PRINTER_CHANNEL_NO) sim_printf ("XXXX ptr seen\n");)
        if (level < N_INTR_CELLS) {
          word15 addr = (word15) (ADDR_PROG_INT_CELL_BASE + level);
          word18 sublvlwd = fromMemory (addr, __func__);
          HDBGNote ("cpu intr", "level %o sublevel word %06o", level, sublvlwd);
          uint sublevel;
          for (sublevel = 0; sublevel < N_INTR_SUBLEVELS; sublevel ++) {
            if (getbits18 (sublvlwd, sublevel, 1))
              break;
          }
DBG_PRTR(if (level == PRINTER_CHANNEL_NO) sim_printf ("XXXX ptr sublevel %o\n", sublevel);)
          if (sublevel < N_INTR_SUBLEVELS) {
            putbits18 (& sublvlwd, sublevel, 1, 0);
            toMemory (sublvlwd, addr, 0, 0, __func__);
            if ((sublvlwd & 0777774) == 0)
              cpu.cells[level] = 0; // see dd01 pg 54. Should this test include bits 16,17? XXX
            intrScan ();

DBG_PRTR(if (level == PRINTER_CHANNEL_NO) sim_printf ("XXXX ptr taken %o\n", sublevel);)
            // Interrupt level, sublevel
            word15 intrAddr = ((word15) (sublevel << 4)) | (word15) level;
            HDBGIntr (level, sublevel, intrAddr, "");
            HDBGNote ("INTR", "level %o sublvl %o intrAddr %o", level, sublevel, intrAddr);

            // Fetch the indirect word
            word15 ind = (word15) (fromMemory (intrAddr, __func__) & BITS15);

            // Simulate a TSY
            word18 vector = ((ind + 1) & BITS15);
            HDBGRegICR ();
            //sim_printf ("INTR level %o sublvl %o intrAddr %o IC %06o\r\n", level, sublevel, intrAddr, cpu.rIC);
            if (cpu.disState) {
              cpu.Y = (cpu.rIC + 1) & BITS15;
            } else {
              cpu.Y = cpu.rIC;
            }
            toMemory (cpu.Y, ind, 0, 0, __func__);
            cpu.rIC = (vector) & BITS15;
            HDBGRegICW ();
            cpu.inhIntrChk = true;
            HDBGNote ("interrupt", "TSY to %05o", cpu.rIC);
            if (cpu.disState) {
              HDBGNote ("interrupt", "Leaving DIS state (1) intrPresent %u rII %u inhIntrChk %u", cpu.intrPresent, cpu.rII, cpu.inhIntrChk);
              cpu.disState = false;
            }
          } else {
            // no cwsublevel interrupts found?
            HDBGNote ("interrupt", "Warning: no interupts sublevel cells were set%s", "");
            sim_printf("Warning: no interupts sublevel cells were set\n");
            // XXX dd01 pg 55 sez Illegal Program Interrupt fault
            cpu.inhIntrChk = false;  // XXX review intr inh check logic
          }
        } else {
          // no interrupts found?
          // This can happen if IE is masking out interrupts.
        }
      }
    } else {
      cpu.inhIntrChk = false;  // XXX review intr inh check logic
    }

    if (cpu.disState) {
      if (! cpu.intrPresent) {
        if (cpu.fast)
          /* usleep (1)*/ ;
        else 
          usleep (1000); // 1 ms
        continue;
      }
      HDBGNote ("", "Leaving DIS state (2) intrPresent %u rII %u inhIntrChk %u", cpu.intrPresent, cpu.rII, cpu.inhIntrChk);
      cpu.disState = false;
    }

    HDBGRegICR ();
    word18 ins = fromMemory (cpu.rIC, "ins fetch");
//if (ins == 0171777) { printf ("gidb trap\n"); HDBGPrint (); exit (1); }
    sim_debug (DBG_TRACE, & cpuDev, "%05o:%06o %s\n", cpu.rIC, ins, disassemble (cpu.rIC, ins));
    HDBGTrace (cpu.rIC, ins, "");
// catch error handler early
if (cpu.rIC == 036215) { sim_printf ("error handler catch\n"); HDBGPrint (); exit (1); } // DEBUG
// trap entry to printer_trace
if (cpu.rIC == 045171) { sim_printf ("printer_trace entry\n"); hdbg_mark (); } // DEBUG
// at end of initialization
//if (cpu.rIC == 047452+002426) { printf ("catch 1\n"); HDBGPrint (); exit (1); } // DEBUG
    cpu.OPCODE = (word6) getbits18 (ins, 3, 6);
    cpu.I = 0;
    cpu.T = 0;
    cpu.D = 0;
    cpu.S1 = 0;
    cpu.S2 = 0;
    cpu.K = 0;
    cpu.W = 0;
    cpu.C = 0;
    cpu.Y = 0;
    cpu.YY = 0;
    cpu.NEXT_IC = (cpu.rIC + 1) & BITS15;
    HDBGRegNXW ();
    cpu.dlyFault = false;
    switch (opcTable [cpu.OPCODE].grp) {
      case opcILL: {
        // Fault invoked below
        break;
      }

      case opcMR: {
        cpu.I = (word1) getbits18 (ins, 0, 1);
        cpu.T = (word2) getbits18 (ins, 1, 2);
        cpu.D = (word9) getbits18 (ins, 9, 9);
        word18 ea = doCAF ();
        cpu.W = _W (ea);
        cpu.C = _C (ea);
        if (opcTable [cpu.OPCODE].opRD) {
          if (opcTable [cpu.OPCODE].opSize == opW) {
            cpu.Y = fromMemory (((word18) cpu.W) | (((word18) cpu.C) << 15), "operand read");
          } else if (opcTable [cpu.OPCODE].opSize == opDW) {
            cpu.YY = fromMemory36 (cpu.W, 1, "operand read");
          }
        }
        break;
      }

      case opcG1a:
      case opcG1b:
      case opcG1c:
      case opcG1d: {
        cpu.S1 = (word3) getbits18 (ins, 0, 3);
        cpu.D = (word9) getbits18 (ins, 9, 9);
        word3 cd = (word3) getbits18 (ins, 9, 3);
        if (cd == 7) // C7 fix
          cd = 0;
        word15 wd = (word15) getbits18 (ins, 12, 6);
        if (wd & 040) // 6 bit sign bit
          wd |= 0077700;  // extend sign bit to 15 bits
        cpu.Dchaddr = ((word18) wd) | (((word18) cd) << 15);
        sim_debug (DBG_DEBUG, & cpuDev, "grp1 S1 %o D %03o Dchaddr %06o\n", cpu.S1, cpu.D, cpu.Dchaddr);
        break;
      }

      case opcG2: {
        cpu.S1 = (word3) getbits18 (ins, 0, 3);
        cpu.S2 = (word3) getbits18 (ins, 9, 3);
        cpu.K = (word6) getbits18 (ins, 12, 6);
        sim_debug (DBG_DEBUG, & cpuDev, "grp2 S1 %o S2 %o K %o\n", cpu.S1, cpu.S2, cpu.K);
        break;
      }
    } // switch (opcTable [cpu.OPCODE].grp)

#define ILL doFault (faultIllegalOpcode, "illegal opcode")
#define UNIMP doUnimp (cpu.OPCODE);
#define OVF(ctx) if (TSTF (cpu.rIR, I_OVF) && ! TSTF (cpu.rIR, I_OFI) && cpu.ovfEnable) dlyDoFault (ADDR_FAULT_OVERFLOW, ctx)

#define SET_ZN(R) \
  SCF (R == 0, cpu.rIR, I_ZERO); \
  SCF (getbits18 (R, 0, 1) == 1, cpu.rIR, I_NEG) \
  HDBGRegIW ();

#ifdef DUMP_MEMORY
    dumpMemory ();
#endif

    switch (cpu.OPCODE) {

// 00 - 07

      case 000: // illegal
        if (cpu.diag) {
          sim_printf ("DIAG %03o%03o cycle %ld\n", cpu.diagh, getbits18 (ins, 10, 8), cpu.cycleCnt);
          reason = STOP_STOP;
          goto leave;
        }
        ILL;

      // C(A) * C(Y) -> C(AQ), left adjusted
      //
      // is stored left-adjusted in the AQ register. AQ35 contains a
      // zero. Overflow can occur only in the case of A and Y
      // containing negative 1; the result exceeding the range of
      // the AQ register.

      case 001: { // MPF
        HDBGRegAR ();
        HDBGRegQR ();
        word36 tmp36 = SIGNEXT18_36 (cpu.rA) * SIGNEXT18_36 (cpu.Y);
        tmp36 &= BITS36;
        tmp36 <<= 1;    // left adjust so AQ71 contains 0

        // Overflow can occur only in the case of A and Y containing
        // negative 1
        if (cpu.rA == 0400000 && cpu.Y == 0400000) {
          SETF (cpu.rIR, I_NEG);
          CLRF (cpu.rIR, I_ZERO);
          SETF (cpu.rIR, I_OVF);
          HDBGRegIW ();
          OVF ("MPF");
        } else {
          cpu.rA = (word18) (tmp36 >> 18) & BITS18;
          cpu.rQ = (word18) (tmp36 >>  0) & BITS18;
          SCF (cpu.rA == 0 && cpu.rQ == 0, cpu.rIR, I_ZERO);
          SCF (getbits18 (cpu.rA, 0, 1) == 1, cpu.rIR, I_NEG);
          HDBGRegIW ();
        }
        break;
      }

      case 002: {
        // ADCX2
        // Add Character Address to X2
        // FA (C(X2), C(Y)] -> X2
        HDBGRegXR (2);
        cpu.rX2 = addAddr2 (cpu.Y, cpu.rX2);
        HDBGRegXW (2);
        SCF (cpu.rX2 == 0, cpu.rIR, I_ZERO);
        HDBGRegIW ();
      }
      break;

      case 003: // LDX2
        // Load X2
        cpu.rX2 = cpu.Y;
        HDBGRegXW (2);
        SCF (cpu.rX2 == 0, cpu.rIR, I_ZERO);
        HDBGRegIW ();
        break;

      case 004: // LDAQ
        // Load AQ
        cpu.rA = (cpu.YY >> 18) & BITS18;
        HDBGRegAW ();
        cpu.rQ = (cpu.YY >>  0) & BITS18;
        HDBGRegQW ();
        SCF (cpu.rA == 0 && cpu.rQ == 0, cpu.rIR, I_ZERO);
        SCF (getbits18 (cpu.rA, 0, 1) == 1, cpu.rIR, I_NEG);
        HDBGRegIW ();
        break;

      case 005: // ill
        if (cpu.diag) {
          //sim_printf ("DIAG1 %o\n", getbits18 (ins, 10, 8) | 01000);
          cpu.diagh = getbits18 (ins, 10, 8);
          break;
        }
        ILL;

      case 006: { // ADA
        // Add to A
        bool ovf;
        HDBGRegAR ();
        cpu.rA = Add18b (cpu.rA, cpu.Y, 0, I_ZERO | I_NEG | I_OVF | I_CARRY, & cpu.rIR, & ovf);
        HDBGRegAW ();
        HDBGRegIW ();
        if (ovf)
          OVF ("ADA");
        break;
      }

      case 007: // LDA
        // Load A
        cpu.rA = cpu.Y;
        HDBGRegAW ();
        SET_ZN (cpu.rA);
        break;

// 10 - 17

      case 010: // TSY
        // Transfer amd Store IC in Y

#ifdef FAKE_TRACE
#define TRACE_ENTRY 044232

// *   calling sequence:
// *
// *       tsy     addr-*,*
// *       vfd     6/count,12/type count = number of data words
// *       vfd     6/modnum,12/switch
// * addr  ind     trace
// *       bss     count   data words
// *       ---             return point


// type of tracing
// dia_man
// memory trace types
//
// mt.trm  equ     1
// mt.mbx  equ     2
// mt.rmb  equ     3
// mt.inq  equ     4
// mt.wcd  equ     5
// mt.ouq  equ     6
// mt.inc  equ     7
// mt.wmb  equ     8
// mt.fre  equ     9
// mt.wtx  equ     10
// mt.rtx  equ     11
// mt.alt  equ     12
// mt.acu  equ     13
//
// printer trace switches
// rem
// tr.que  bool    002
// tr.mbx  bool    004
// tr.int  bool    010

// interpreter.map355
//        rem     memory trace types
//        rem
// mt.tst  equ     1 
// mt.wrt  equ     2
// mt.sta  equ     3
// mt.tim  equ     4
// mt.blk  equ     5
//        rem     
//        rem     tracing switches
//        rem
// tr.ent  bool    040
// tr.blk  bool    100

// utilities.map355
//        rem     trace types
//        rem
// mt.get  equ     1       allocating single buffer
// mt.fre  equ     2       freeing single buffer
// mt.gtc  equ     3       allocating buffer chain 
// mt.frc  equ     4       freeing buffer chain

// hsla_man.map355
//         ttls    trace types and switches
//         rem
// tt.dcw  equ     1       trace hdcw calls
// tt.pcw  equ     2       trace pcw connects 
// tt.int  equ     3       trace interrupts
// tt.sta  equ     4       trace status
// tt.ira  equ     5       trace icw recovery attempt
//         rem
//         rem
// ts.dcw  bool    000002
// ts.pcw  bool    000004
// ts.int  bool    000010
// ts.sta  bool    000020

// scheduler.map355
//         ttls    trace types and switches
//         rem
// tt.int  equ     1       trace interrupts
// tt.idl  equ     2       trace idle time
// tt.run  equ     3       trace running of interrupt rtn
// tt.rst  equ     4       trace restart of interrupted routine
// tt.rnq  equ     5       trace running of queued routine
// tt.set  equ     6       trace set timer calls
// tt.itr  equ     7       trace interval timer runouts
// tt.que  equ     8       trace dspqur entries
//         rem
//         rem
// ts.int  bool    000002 
// ts.skd  bool    000004
// ts.tim  bool    000010

//         ttls    printer tracing tables and messages
// modtab  null    
//         ind     skdtrc  
//         ind     diatrc
//         ind     inttrc  
//         ind     utltrc
//         ind     lsltrc
//         ind     hsltrc


        if (cpu.W == TRACE_ENTRY) {

          static char * skdtrc[] = {
            "?",
            "interrupt at *****, 3wjt ******",
            "idle, indicators ******, ier ******",
            "run interrupt routine ******",
            "restart interrupted routine at ******",
            "run queued routine ******",
            "set timer ****** for tib ******",
            "interval timer runout, current time ****** ******",
            "queue routine, pri ******, rtn ******, x1 ******"
          };

          static char * diatrc [] = {
            "?",
            "dia terminate, tcword = **",
            "dia interrupt for mailbox **",
            "dia reading mailbox **", 
            "new entry in dia i/o queue: opcode ***, line ****",
            "wcd in mailbox **: opcode ***, line ****",
            "using dia i/o queue entry: opcode ***, line ****",
            "dia sending input count of ****** for line ****",
            "dia writing mailbox **",    
            "dia freeing mailbox **",    
            "wtx in mailbox ** for line ****, *** buffers",
            "rtx in mailbox ** for line ****",
            "alter parameters: ******",
            "dial: *** digits - ****** ****** ****** ******"
          };

          static char * inttrc [] = {
            "?",
            "itest: tib at ******, t.cur = ******",
            "iwrite: tib at ******, t.cur = ******",
            "istat: tib at ******, t.cur = ******, status ******",
            "itime: tib at ******, t.cur = ******",
            "op block at ******, type = ***"
          };

          static char * utltrc [] = {
            "?",
            "buffer allocated at ******, size = ***",
            "buffer freed at ******, size = ***",
            "request for ** buffers of size ***",
            "freeing buffer list at ******"
          };

          static char * lsltrc [] = {
            "?",
            "lsla interrupt, 3wjt = ******",
            "lsla output frame at ******, sfcm at ******",
            "lsla output buffer at ******",
            "lsla input frame at ******, sfcm at ******",
            "lsla input buffer at ******",
            "sending *** to lsla slot ** for line ****",
            "escape in lsla slot ** for line ****"
          };

          static char * hsltrc [] = {
            "?",
            "hsla dcw processor, tib ******, list ******, len **",
            "hsla pcw, tib ******, pcw ****** ******",
            "hsla interrupt, 3wjt = ******",
            "hsla status, tib ******, status ****** ******",
            "hsla, tib ******, attempting icw indicator recovery"
          };

          static char * * modtab [7] = {
            NULL,
            skdtrc,
            diatrc,
            inttrc,
            utltrc,
            lsltrc,
            hsltrc
          };

          static char * modname[11] = {
            "0?", // 0
            "scheduler", // 1
            "dia_man", // 2
            "interpreter", // 3
            "utilities", // 4
            "5?", // 5
            "hsla_man", // 6
            "console" // 7
            "trace", // 8
            "init", // 9
            "printer_trace" // 10
          };

          word18 a1 = fromMemory (cpu.rIC + 1, __func__);
          word18 a2 = fromMemory (cpu.rIC + 2, __func__);
          word6 count = (a1 >> 12) & BITS6;  // number of data args
          word12 type = a1 & BITS12; // 1st argument "type   specifies type of tracing"
          word6 modnum = (a2 >> 12) & BITS6; // module number
          word12 switches = a2 & BITS12;  // "switches        specify which processor switches must be on for printer tracing to occur"

          char * mname = modnum < 11 ? modname[modnum] : "?";
          char msg[256];
          strcpy (msg, "?");
          if (modnum <= 7)
            strcpy (msg, modtab[modnum][type]);
 
          char * p = msg;
          int argn = 0;
          while (1) {
            char * star = strchr (p, '*');
            if (! star)
              break;
            size_t nstars = strspn (star, "*");

            word18 arg = fromMemory (cpu.rIC + 4 + argn, __func__);
            char buf[7];
            sprintf (buf, "%06o", arg);
            int drop = 6 - nstars;
            while (drop < 0) {
              * star ++ ='0';
              drop ++;
            }
            for (int i = drop; i < 6; i ++) {
               * star ++ = buf[i];
            }
              
            argn ++;
            p = star;
          }

          //sim_printf ("trace %06o %06o %s %s\n", a1, a2, mname, msg);
          sim_printf ("trace %s %s\n", mname, msg);

          cpu.NEXT_IC = cpu.rIC + 1
                        + 1 // vfd     6/count,12/#1
                        + 1 // vfd     6/modnum,12/#2
                        + 1 // ind     trace
                        + count; // bss     count
          break;
        }
#endif // FAKE_TRACE

        HDBGRegNXR ();
        cpu.Y = cpu.NEXT_IC;
        cpu.NEXT_IC = (cpu.W + 1) & BITS15;
        HDBGRegNXW ();
        sim_debug (DBG_TRACE, & cpuDev, "TSY %05o\n", cpu.NEXT_IC);
        cpu.inhIntrChk = true;
        break;

      case 011: // ill
        ILL;

      case 012: { // grp1d
        switch (cpu.S1) {
          case 0:  // RIER
            // Read Interrupt Level Enable Register
            cpu.rA = (word18) ((cpu.rIE & BITS16) << 2);
            HDBGRegAW ();
            SET_ZN (cpu.rA);
            break;

          case 4:  {// RIA
            uint level;
            for (level = 0; level < N_INTR_CELLS; level ++) {
              if ((cpu.rIE & levelMask[level]) && cpu.cells[level])
                break;
            }
            if (level < N_INTR_CELLS) {
              HDBGNote ("RIA", "Cells say level %o", level);
              word15 addr = (word15) (ADDR_PROG_INT_CELL_BASE + level);
              word18 sublvlwd = fromMemory (addr, __func__);
              HDBGNote ("RIA", "level %o sublevel word %06o", level, sublvlwd);
              uint sublevel;
              for (sublevel = 0; sublevel < N_INTR_SUBLEVELS; sublevel ++) {
                if (getbits18 (sublvlwd, sublevel, 1))
                  break;
              }
              if (sublevel < N_INTR_SUBLEVELS) {
                putbits18 (& sublvlwd, sublevel, 1, 0);
                toMemory (sublvlwd, addr, 0, 0, __func__);
                if ((sublvlwd & 0777774) == 0)
                  cpu.cells[level] = 0; // see dd01 pg 54. Should this test include bits 16,17? XXX
                intrScan ();

                // Interrupt level, sublevel
                word15 intrAddr = (word15) ((sublevel << 4) | level);
                HDBGNote ("RIA", "level %o sublvl %o intrAddr %o", level, sublevel, intrAddr);
                cpu.rA = intrAddr | SIGN18;
                HDBGRegAW ();
                SET_ZN (cpu.rA);
                break;
              }
            }
            cpu.rA = 0400447; // Illegal Program Interrupt Fault vector
            SET_ZN (cpu.rA);
            break;
          }  

          default:
            ILL;
        } // switch (S1)
        break;
      } // grp1d

      case 013: // STX2
        // Store X2
        HDBGRegXR (2);
        cpu.Y = cpu.rX2;
        break;

      case 014: // STAQ
        // Store AQ
        HDBGRegAR ();
        HDBGRegQR ();
        cpu.YY = (((word36) cpu.rA) << 18) | cpu.rQ;
        break;

      case 015: { // ADAQ
        // Add to AQ
        bool ovf;
        HDBGRegAR ();
        HDBGRegQR ();
        word36 tmp = ((word36) (cpu.rA) << 18) | cpu.rQ;
        sim_debug (DBG_TRACE, & cpuDev, "ADAQ     %012lo\n", tmp);
        sim_debug (DBG_TRACE, & cpuDev, "ADAQ +   %012lo\n", cpu.YY);
        word36 res = Add36b (tmp, cpu.YY, 0, I_ZERO | I_NEG | I_OVF | I_CARRY, & cpu.rIR, & ovf);
        sim_debug (DBG_TRACE, & cpuDev, "ADAQ =  %d%012lo\n", TSTF (cpu.rIR, I_CARRY) ? 1 : 0, res);
        if (ovf)
          OVF ("ADAQ");

        cpu.rA = (res >> 18) & BITS18;
        HDBGRegAW ();
        cpu.rQ = res & BITS18;
        HDBGRegQW ();
        HDBGRegIW ();
        break;
      }

      case 016: { // ASA
        // Add A to storage
        bool ovf;
        HDBGRegAR ();
        cpu.Y = Add18b (cpu.rA, cpu.Y, 0, I_ZERO | I_NEG | I_OVF | I_CARRY, & cpu.rIR, & ovf);
        if (ovf)
          OVF ("ASA");
        HDBGRegIW ();
        break;
      }

      case 017: // STA
        // Store A
        HDBGRegAR ();
        cpu.Y = cpu.rA;
        break;

// 20 - 27

      case 020: // SZN
        // Set Zero and Negative Indicators from Storage
        SET_ZN (cpu.Y);
        break;

      case 021: // DVF
        dvf ();

      case 022: { // grp1b
        switch (cpu.S1) {
          case 0:  // IANA
            // Immediate AND to A
            cpu.rA &= SIGNEXT9_18 (cpu.D & BITS9);
            cpu.rA &= BITS18;
            HDBGRegAW ();
            SET_ZN (cpu.rA);
            break;

          case 1:  // IORA
            // Immediate OR to A
            cpu.rA |= SIGNEXT9_18 (cpu.D & BITS9);
            cpu.rA &= BITS18;
            HDBGRegAW ();
            SET_ZN (cpu.rA);
            break;

          case 2: { // ICANA
            // Immediate Comparative AND to A
            HDBGRegAR ();
            word18 Z = cpu.rA & SIGNEXT9_18 (cpu.D & BITS9);
            Z &= BITS18;
            SET_ZN (Z);
            break;
          }

          case 3:  // IERA
            // Immediate XOR to A
            cpu.rA ^= SIGNEXT9_18 (cpu.D & BITS9);
            cpu.rA &= BITS18;
            HDBGRegAW ();
            SET_ZN (cpu.rA);
            break;

          case 4:  // ICMPA
            HDBGRegAR ();
            cmp18 (cpu.rA, SIGNEXT9_18 (cpu.D & BITS9), & cpu.rIR);
            HDBGRegIW ();
            break;

          default:
            ILL;
        } // switch (S1)
        break;
      } // grp1b

      case 023: // CMPX2
        HDBGRegXR (2);
        SCF (cpu.rX2 == cpu.Y, cpu.rIR, I_ZERO);
        HDBGRegIW ();
        break;

      case 024: { // SBAQ
        // Subtract from AQ
        bool ovf;
        HDBGRegAR ();
        HDBGRegQR ();
        word36 tmp = ((word36) (cpu.rA) << 18) | cpu.rQ;
        word36 res = Sub36b (tmp, cpu.YY, 1, I_ZERO | I_NEG | I_OVF | I_CARRY, & cpu.rIR, & ovf);
        HDBGRegIW ();
        if (ovf)
          OVF ("SBAQ");

        cpu.rA = (res >> 18) & BITS18;
        HDBGRegAW ();
        cpu.rQ = res & BITS18;
        HDBGRegQW ();
        break;
      }

      case 025: // ill
        ILL;

      case 026: { // SBA
        // Subtract from A
        bool ovf;
        HDBGRegAR ();
        cpu.rA = Sub18b (cpu.rA, cpu.Y, 1, I_ZERO | I_NEG | I_OVF | I_CARRY, & cpu.rIR, & ovf);
        HDBGRegAW ();
        HDBGRegIW ();
        if (ovf)
          OVF ("SBA");
        break;
      }

      case 027: // CMPA
        HDBGRegAR ();
        cmp18 (cpu.rA, cpu.Y, & cpu.rIR);
        HDBGRegIW ();
        break;

// 30 - 37

      case 030: // LDEX
        UNIMP;

      case 031: { // CANA
        // Comparative AND to A
        HDBGRegAR ();
        word18 Z = cpu.rA & cpu.Y;
        SET_ZN (Z);
        break;
      }

      case 032: // ANSA
        // AND to Storage A
        HDBGRegAR ();
        cpu.Y &= cpu.rA;
        SET_ZN (cpu.Y);
        break;

      case 033: { // grp2
        switch (cpu.S1) {
          case 0: {
            switch (cpu.S2) {
              case 2: // CAX2
                // Copy A into X2
                HDBGRegAR ();
                cpu.rX2 = cpu.rA;
                HDBGRegXW (2);
                SCF (cpu.rX2 == 0, cpu.rIR, I_ZERO);
                HDBGRegIW ();
                break;

              case 4: { // LLS
                // Long Left Shift
                // XXX should a shift of 0 clear the carry?
                // XXX Debug
                if (cpu.K == 0) {
                  sim_printf ("XXX Debug: LLS k == 0; flags updated\n");
                }
#define UPDATE_FLAGS 1
                if (UPDATE_FLAGS || cpu.K) {
                  CLRF (cpu.rIR, I_CARRY);
                  HDBGRegIW ();
                  for (uint i = 0; i < cpu.K; i ++) {
                    HDBGRegAR ();
                    HDBGRegQR ();
                    cpu.rA = (cpu.rA << 1) & BITS18;
                    HDBGRegAW ();
                    if (cpu.rQ & BITS0)
                      cpu.rA |= 1;
                    cpu.rQ = (cpu.rQ << 1) & BITS18;
                    HDBGRegQW ();
                    if (cpu.rA & BITS0)
                      SETF (cpu.rIR, I_CARRY);
                    }
                  SCF (cpu.rA == 0 && cpu.rQ == 0, cpu.rIR, I_ZERO);
                  SCF (getbits18 (cpu.rA, 0, 1) == 1, cpu.rIR, I_NEG);
                  HDBGRegIW ();
                }
                break;
              }

              case 5: { // LRS
                // Long Right Shift
                HDBGRegAR ();
                HDBGRegQR ();
                word1 aq0 = (word1) getbits18 (cpu.rA, 0, 1);
                for (uint i = 0; i < cpu.K; i ++) {
                  // shift lower half
                  cpu.rQ = (cpu.rQ >> 1) & BITS17;
                  // copy low bit of upper half to lower
                  putbits18 (& cpu.rQ, 0, 1, getbits18 (cpu.rA, 17, 1));
                  // shift upper half
                  cpu.rA = (cpu.rA >> 1) & BITS17;
                  // fill with orig AQ0
                  putbits18 (& cpu.rA, 0, 1, aq0);
                }
                HDBGRegAW ();
                HDBGRegQW ();
                SCF (cpu.rA == 0 && cpu.rQ == 0, cpu.rIR, I_ZERO);
                SCF (getbits18 (cpu.rA, 0, 1) == 1, cpu.rIR, I_NEG);
                HDBGRegIW ();
                break;
              }

              case 6: { // ALS
                // A Left Shift
                HDBGRegAR ();
                CLRF (cpu.rIR, I_CARRY);
                word6 count = (cpu.K > 17) ? 17 : cpu.K;
                // Special case: since we insert zeros for at the far end, when we shift 18 or more bits,
                //               we'll shift one or more zeros into BIT 0. That means if there are any
                //               one bits in the starting word, we'll have a change in BIT 0 and need to
                //               set the carry flag.
                if (((cpu.K > 17) && (cpu.rA != 0)) ||
                    ! (((cpu.rA & als_mask[count]) == 0) || ((cpu.rA & als_mask[count]) == als_mask[count])))
                  SETF (cpu.rIR, I_CARRY);
                cpu.rA = (cpu.rA << cpu.K) & BITS18;
                HDBGRegAW ();
                SET_ZN (cpu.rA);
                //HDBGRegIW (); SET_ZN does this
                break;
              }

              case 7: { // ARS
                // A Right Shift
                HDBGRegAR ();
                word1 a0 = (word1) getbits18 (cpu.rA, 0, 1);
                for (uint i = 0; i < cpu.K; i ++) {
                  // shift
                  cpu.rA = (cpu.rA >> 1) & BITS17;
                  // fill with orig A0
                  putbits18 (& cpu.rA, 0, 1, a0);
                }
                HDBGRegAW ();
                SET_ZN (cpu.rA);
                break;
              }

              default:
                ILL;
            } // switch (S2)
            break;
          } // S1 0

          case 1: {
            switch (cpu.S2) {
              case 2:
                UNIMP; // CAR  AY34 pg C-1

              case 4: { // NRML
                HDBGRegIR ();
                if (TSTF (cpu.rIR, I_OVF)) {
                  // If the overflow flag is ON, C(A) is shifted rigth one position,
                  // And the the sign bit is inverted to reconstitute the original sign.
                  // The overflow indicator is set OFF and X1 incremented.
                  HDBGRegAR ();
                  HDBGRegQR ();
                  word18 sign = cpu.rA & SIGN18;
                  word1 rA17 = cpu.rA & 1;
                  cpu.rA = (cpu.rA >> 1) & BITS17;
                  cpu.rQ = (cpu.rQ >> 1) & BITS17;
                  if (rA17)
                    cpu.rQ |= SIGN18;
                  if (sign == 0)
                    cpu.rA |= SIGN18;
                  HDBGRegAW ();
                  HDBGRegQW ();
                  CLRF (cpu.rIR, I_OVF);
                  cpu.rX1 = (cpu.rX1 + 1) & BITS18;
                  HDBGRegXW (1);
                } else {
                  HDBGRegAR ();
                  while (cpu.rA) {
                  HDBGRegQR ();
                    word1 cA0 = (cpu.rA >> 17) & 1;
                    word1 cA1 = (cpu.rA >> 16) & 1;
                    if (cA0 == cA1)
                      break;
                    cpu.rA = (cpu.rA << 1) & BITS18;
                    if (cpu.rQ & SIGN18)
                      cpu.rA |= 1;
                    HDBGRegAW ();
                    cpu.rQ = (cpu.rQ << 1) & BITS18;
                    HDBGRegQW ();
                    cpu.rX1 = (cpu.rX1 - 1) & BITS18;
                    HDBGRegXW (1);
                  }
                }
                SCF (cpu.rA == 0 && cpu.rQ == 0, cpu.rIR, I_ZERO);
                SCF (getbits18 (cpu.rA, 0, 1) == 1, cpu.rIR, I_NEG);
              }

              case 6: { // NRM
                if (TSTF (cpu.rIR, I_OVF)) {
                  // If the overflow flag is ON, C(A) is shifted rigth one position,
                  // And the the sign bit is inverted to reconstitute the original sign.
                  // The overflow indicator is set OFF and X1 incremented.
                  HDBGRegAR ();
                  word18 sign = cpu.rA & SIGN18;
                  cpu.rA = (cpu.rA >> 1) & BITS17;;
                  if (sign == 0)
                    cpu.rA |= SIGN18;
                  HDBGRegAW ();
                  CLRF (cpu.rIR, I_OVF);
                  cpu.rX1 = (cpu.rX1 + 1) & BITS18;
                  HDBGRegXW (1);
                } else {
                  HDBGRegAR ();
                  while (cpu.rA) {
                    word1 cA0 = (cpu.rA >> 17) & 1;
                    word1 cA1 = (cpu.rA >> 16) & 1;
                    if (cA0 == cA1)
                      break;
                    cpu.rA = (cpu.rA << 1) & BITS18;
                    HDBGRegAW ();
                    cpu.rX1 = (cpu.rX1 - 1) & BITS18;
                    HDBGRegXW (1);
                  }
                }
                SET_ZN (cpu.rA);
                break;
              }

              default:
                ILL;
            } // switch (S2)
            break;
          } // S1 1

          case 2: {
            switch (cpu.S2) {
              case 1: // NOP
                // No Operation
                break;

              case 2: // CX1A
                // Copy X1 into A
                HDBGRegXR (1);
                cpu.rA = cpu.rX1;
                HDBGRegAW ();
                SCF (cpu.rA == 0, cpu.rIR, I_ZERO);
                HDBGRegIW ();
                break;

              case 4: { // LLR
                // Long Left Rotate
                HDBGRegAR ();
                HDBGRegQR ();
                for (uint i = 0; i < cpu.K; i ++) {
                  word1 a0 = (word1) getbits18 (cpu.rA, 0, 1);
                  // shift upper half
                  cpu.rA = (cpu.rA << 1) & BITS18;
                                    
                  word1 q0 = (word1) getbits18 (cpu.rQ, 0, 1);
                  cpu.rA |= q0;

                  // shift lower half
                  cpu.rQ = (cpu.rQ << 1) & BITS18;
                  cpu.rQ |= a0;
                }
                HDBGRegAW ();
                HDBGRegQW ();
                SCF (cpu.rA == 0 && cpu.rQ == 0, cpu.rIR, I_ZERO);
                SCF (getbits18 (cpu.rA, 0, 1) == 1, cpu.rIR, I_NEG);
                HDBGRegIW ();
                break;
              }

              case 5: { // LRL
                // Long Right Logic
                HDBGRegAR ();
                HDBGRegQR ();
                for (uint i = 0; i < cpu.K; i ++) {
                  // shift lower half
                  cpu.rQ = (cpu.rQ >> 1) & BITS17;
                  // copy low bit of upper half to lower
                  putbits18 (& cpu.rQ, 0, 1, getbits18 (cpu.rA, 17, 1));
                  // shift upper half
                  cpu.rA = (cpu.rA >> 1) & BITS17;
                  // fill with orig 0
                  putbits18 (& cpu.rA, 0, 1, 0);
                }
                HDBGRegAW ();
                HDBGRegQW ();
                SCF (cpu.rA == 0 && cpu.rQ == 0, cpu.rIR, I_ZERO);
                SCF (getbits18 (cpu.rA, 0, 1) == 1, cpu.rIR, I_NEG);
                HDBGRegIW ();
                break;
              }

              case 6: { // ALR
                // A Left Rotate
                HDBGRegAR ();
                for (uint i = 0; i < cpu.K; i ++) {
                  word1 a0 = (word1) getbits18 (cpu.rA, 0, 1);
                  cpu.rA = (cpu.rA << 1) & BITS18;
                  putbits18 (& cpu.rA, 17, 1, a0);
                }
                SET_ZN (cpu.rA);
                break;
              }

              case 7: { // ARL
                // A Right Logic
                HDBGRegAR ();
                for (uint i = 0; i < cpu.K; i ++) {
                  // shift
                  cpu.rA = (cpu.rA >> 1) & BITS17;
                }
                HDBGRegAW ();
                SET_ZN (cpu.rA);
                break;
              }

              default:
                ILL;
            } // switch (S2)
            break;
          } // S1 2

          case 3: {
            switch (cpu.S2) {
              case 1: // INH
                // Interrupt inhibit
                // Interrupt inhibit indicator is turned ON
                cpu.rII = 1;
                cpu.inhIntrChk = true;
                break;

              case 2: // CX2A
                // Copy X2 into A
                HDBGRegXR (1);
                cpu.rA = cpu.rX2;
                HDBGRegAW ();
                SCF (cpu.rA == 0, cpu.rIR, I_ZERO);
                HDBGRegIW ();
                break;

              case 3: // CX3A 
                // this may be a dd01 typo 2,33,3 fits the pattern -- no, looked
                // at interpreter.list; cx3a is 3,33,3
                // Copy X3 into A
                HDBGRegXR (3);
                cpu.rA = cpu.rX3;
                HDBGRegAW ();
                SCF (cpu.rA == 0, cpu.rIR, I_ZERO);
	      HDBGRegIW ();
                break;

              case 6: { // ALP
                // A Left Parity Rotate
                // Rotate C(A) by Y (12-17) positions, enter each
                // bit leaving position zero into position 17.
                // Zero: If the number of 1's leaving position 0
                // is even, then ON; otherwise OFF
                // Negative: If (C(A)0 = 1, then ON; otherwise OFF
                             
                int ones = 0;
                HDBGRegAR ();
                for (uint n = 0; n < cpu.K; n ++) {
                  word1 out = (word1) getbits18 (cpu.rA, 0, 1);
                  cpu.rA <<= 1;
                  cpu.rA |= out;
                  if (out)
                    ones ++;
                }
                SCF (ones % 2 == 0, cpu.rIR, I_ZERO);
                SCF (getbits18 (cpu.rA, 0, 1) == 1, cpu.rIR, I_NEG);
	      HDBGRegIW ();
                break;
              }

              default:
                ILL;
            } // switch (S2)
            break;
          } // S1 3

          case 4: {
            switch (cpu.S2) {
              case 1: // DIS
                cpu.disState = true;
                break;

              case 2: // CAX1
                // Copy A into X1
                HDBGRegAR ();
                cpu.rX1 = cpu.rA;
                HDBGRegXW (1);
                SCF (cpu.rX1 == 0, cpu.rIR, I_ZERO);
                HDBGRegIW ();
                break;

              case 3: // CAX3
                // Copy A into X3
                HDBGRegAR ();
                cpu.rX3 = cpu.rA;
                HDBGRegXW (3);
                SCF (cpu.rX3 == 0, cpu.rIR, I_ZERO);
                HDBGRegIW ();
                break;

              case 6: { // QLS
                // Q Left Shift
                HDBGRegQR ();
                CLRF (cpu.rIR, I_CARRY);
                word6 count = (cpu.K > 17) ? 17 : cpu.K;
                // Special case: since we insert zeros for at the far end, when we shift 18 or more bits,
                //               we'll shift one or more zeros into BIT 0. That means if there are any
                //               one bits in the starting word, we'll have a change in BIT 0 and need to
                //               set the carry flag.
                if (((cpu.K > 17) && (cpu.rQ != 0)) ||
                    ! (((cpu.rQ & als_mask[count]) == 0) || ((cpu.rQ & als_mask[count]) == als_mask[count])))
                  SETF (cpu.rIR, I_CARRY);
                cpu.rQ = (cpu.rQ << cpu.K) & BITS18;
                HDBGRegQW ();
                SET_ZN (cpu.rQ);
                //HDBGRegIW (); SET_ZN does this
                break;
              }

              case 7: { // QRS
                // Q Right Shift
                HDBGRegQR ();
                word1 q0 = (word1) getbits18 (cpu.rQ, 0, 1);
                for (uint i = 0; i < cpu.K; i ++) {
                  // shift
                  cpu.rQ = (cpu.rQ >> 1) & BITS17;
                  // fill with orig Q0
                  putbits18 (& cpu.rQ, 0, 1, q0);
                }
                HDBGRegQW ();
                SET_ZN (cpu.rQ);
                break;
              }

              default:
                ILL;
            } // switch (S2)
            break;
          } // S1 4

          case 5: {
            switch (cpu.S2) {
              case 2:
                UNIMP; // CAQC  AY34 pg C-1
              case 3:
                UNIMP; // CCQ  AY34 pg C-1
              default:
                ILL;;
            } // S2
            break;
          } // S1 5


          case 6: {
            switch (cpu.S2) {
              case 3: // CAQ
                // Copy A into Q
                HDBGRegAR ();
                cpu.rQ = cpu.rA;
                HDBGRegQW ();
                SET_ZN (cpu.rA);
                break;

              case 6: { // QLR
                // Q Left Rotate
                HDBGRegQR ();
                for (uint i = 0; i < cpu.K; i ++) {
                  word1 q0 = (word1) getbits18 (cpu.rQ, 0, 1);
                  cpu.rQ = (cpu.rQ << 1) & BITS18;
                  putbits18 (& cpu.rQ, 17, 1, q0);
                }
                HDBGRegQW ();
                SET_ZN (cpu.rQ);
                break;
              }

              case 7: { // QRL
                // Q Right Logic
                HDBGRegQR ();
                for (uint i = 0; i < cpu.K; i ++) {
                  // shift
                  cpu.rQ = (cpu.rQ >> 1) & BITS17;
                  // fill with 0
                  putbits18 (& cpu.rQ, 0, 1, 0);
                }
                HDBGRegQW ();
                SET_ZN (cpu.rQ);
                break;
              }

              default:
                ILL;
            } // switch (S2)
            break;
          } // S1 6

          case 7: {
            switch (cpu.S2) {
              case 1: // ENI
                // Enable interrupt
                // Interrupt inhibit indicator is turned OFF
                cpu.rII = 0;
                cpu.inhIntrChk = true;
                break;

              case 2:
                UNIMP; // CAQC  AY34 pg C-1

              case 3: // CQA
                // Copy Q into A
                HDBGRegQR ();
                cpu.rA = cpu.rQ;
                HDBGRegAW ();
                SET_ZN (cpu.rA);
                break;

              case 6: { // QLP
                // Q Left Parity Rotate
                // Rotate C(Q) by Y (12-17) positions, enter each
                // bit leaving position zero into position 17.
                // Zero: If the number of 1's leaving position 0
                // is even, then ON; otherwise OFF
                // Negative: If (C(Q)0 = 1, then ON; otherwise OFF

                int ones = 0;
                HDBGRegQR ();
                for (uint n = 0; n < cpu.K; n ++) {
                  word1 out = (word1) getbits18 (cpu.rQ, 0, 1);
                  cpu.rQ <<= 1;
                  cpu.rQ |= out;
                  if (out)
                    ones ++;
                }
                HDBGRegQW ();
                SCF (ones % 2 == 0, cpu.rIR, I_ZERO);
                SCF (getbits18 (cpu.rQ, 0, 1) == 1, cpu.rIR, I_NEG);
                HDBGRegIW ();
                break;
              }

              default:
                ILL;
            } // switch (S2)
            break;
          } // S1 7

          default:
            ILL;
        } // switch (S1)
        break;
      } // grp2

      case 034: // ANA
        // AND to A
        HDBGRegAR ();
        cpu.rA &= cpu.Y;
        HDBGRegAW ();
        SET_ZN (cpu.rA);
        break;

      case 035: // ERA
        // EXCLUSIVE OR to A
        HDBGRegAR ();
        cpu.rA ^= cpu.Y;
        HDBGRegAW ();
        SET_ZN (cpu.rA);
        break;

      case 036: { // SSA
        // Subtract Stored from A
        bool ovf;
        HDBGRegAR ();
        cpu.Y = Sub18b (cpu.rA, cpu.Y, 1, I_ZERO | I_NEG | I_OVF | I_CARRY, & cpu.rIR, & ovf);
        HDBGRegIW ();
        if (ovf)
          OVF ("SSA");
        break;
      }

      case 037: // ORA
        // OR to A
        HDBGRegAR ();
        cpu.rA |= cpu.Y;
        HDBGRegAW ();
        SET_ZN (cpu.rA);
        break;

// 40 - 47

      case 040: { // ADCX3
        // Add Character Address to X3
        // FA (C(X3), C(Y)] -> X3
        HDBGRegXR (3);
        cpu.rX3 = addAddr2 (cpu.Y, cpu.rX3);
        HDBGRegXW (3);
        SCF (cpu.rX3 == 0, cpu.rIR, I_ZERO);
        HDBGRegIW ();
        break;
      }

      case 041: // LDX3
        // Load X3
        cpu.rX3 = cpu.Y;
        HDBGRegXW (3);
        SCF (cpu.rX3 == 0, cpu.rIR, I_ZERO);
        HDBGRegIW ();
        break;

      case 042: { // ADCX1
        // Add Character Address to X1
        // FA (C(X1), C(Y)] -> X1
        HDBGRegXR (1);
        cpu.rX1 = addAddr2 (cpu.Y, cpu.rX1);
        HDBGRegXW (1);
        SCF (cpu.rX1 == 0, cpu.rIR, I_ZERO);
        HDBGRegIW ();
        break;
      }

      case 043: // LDX1
        // Load X1
        cpu.rX1 = cpu.Y;
        HDBGRegXW (1);
        SCF (cpu.rX1 == 0, cpu.rIR, I_ZERO);
        HDBGRegIW ();
        break;

      case 044: // LDI
        // Load I
        // C(Y) (Bits 0-7, 12-17) -> C(I)
        // CAC: My reading of dd-01, pg 3-12 (57) is that the Interrupt inhibit bit reflefts
        // the state if II, and setting or clearing the bit in the IR does not affect II. 
        cpu.rIR = cpu.Y & 0776077;
// But I could be wrong
// utilities.list 
//        00310  030000     0          526 fp.inh oct     030000
//        00043  0 44 245      310     291        ldi     fp.inh-*        set "inh" and parity inh
        cpu.rII = (cpu.rIR & I_II) ? 1 : 0;
        HDBGRegIW ();
        cpu.inhIntrChk = true;
        break;

      case 045: // TNC
        // Transfer on No Carry
        HDBGRegIR ();
        if (! TSTF (cpu.rIR, I_CARRY)) {
          cpu.NEXT_IC = cpu.W;
          HDBGRegNXW ();
          sim_debug (DBG_TRACE, & cpuDev, "TNC %05o\n", cpu.NEXT_IC);
        }
        cpu.inhIntrChk = true;
        break;

      case 046: { // ADQ
        // Add to Q
        bool ovf;
        HDBGRegQR ();
        cpu.rQ = Add18b (cpu.rQ, cpu.Y, 0, I_ZERO | I_NEG | I_OVF | I_CARRY, & cpu.rIR, & ovf);
        HDBGRegQW ();
        HDBGRegIW ();
        if (ovf)
          OVF ("ADQ");
        break;
      }

      case 047: // LDQ
        // Load Q
        cpu.rQ = cpu.Y;
        HDBGRegQW ();
        SET_ZN (cpu.rQ);
        break;

// 50 - 57

      case 050: // STX3
        // Store X3
        HDBGRegXR (3);
        cpu.Y = cpu.rX3;
        break;

      case 051: // ill
        ILL;

      case 052: { // grp1c
        switch (cpu.S1) {
          case 0:  // SIER
            // Set Interrupt Level Enable Register
            HDBGRegAR ();
            cpu.rIE = (cpu.rA >> 2)  & BITS16;
            break;

          case 4: { // SIC
            // Set Interrupt Cells
            // DD01 makes no sense
            cpu.cells [cpu.D] = true;
            word15 addr = ADDR_PROG_INT_CELL_BASE + cpu.D;
            word18 bits = cpu.rA | 3; 
            toMemory (bits, addr, 0, bits, "SIC");
            break;
          }

          default:
            ILL;
        } // switch (S1)
        break;
      } // grp1c

      case 053: // STX1
        // Store X1
        HDBGRegXR (1);
        cpu.Y = cpu.rX1;
        break;

      case 054: // STI
        // Store I
        // C(I) (Bits 0-7, 12-17) -> C(Y)
        // CAC: My reading of dd-01, pg 3-12 (57) is that the Interrupt inhibit bit reflefts
        // the state of II, and setting or clearing the bit in the IR does not affect II. 
        HDBGRegIR ();
        cpu.Y = cpu.rIR & 0776077;
        // Clear the II bit
        cpu.Y &= ~I_II;
        if (cpu.rII)
          cpu.Y |= I_II;
        cpu.inhIntrChk = true;
        break;

      case 055: // TOV
        // Transfer on Overflow
        HDBGRegIR ();
        if (TSTF (cpu.rIR, I_OVF)) {
          cpu.NEXT_IC = cpu.W;
          HDBGRegNXW ();
          sim_debug (DBG_TRACE, & cpuDev, "TOV %05o\n", cpu.NEXT_IC);
          CLRF (cpu.rIR, I_OVF);
          HDBGRegIW ();
        }
        cpu.inhIntrChk = true;
        break;

      case 056: // STZ
        // Store Zero
        cpu.Y = 0;
        break;

      case 057: // STQ
        // Store Q
        HDBGRegQR ();
        cpu.Y = cpu.rQ;
        break;

// 60 - 67

      case 060: // CIOC
        // Connect Input/Output Channel

        // "The CIOC instruction always accesses a double-precision
        // (36-bit) Peripheral Control Word (PCW) and sends it, or
        // portions thereof, to the channel indicated by the I/O channel
        // Select Register. If the channel has a 6-, 9-, or 19-bit
        // interface, it uses only part of the word."

        iomCIOC ();
        break;

      case 061: // CMPX3
        HDBGRegXR (3);
        SCF (cpu.rX3 == cpu.Y, cpu.rIR, I_ZERO);
        HDBGRegIW ();
        break;

      case 062: // ERSA
        // EXCLUSIVE OR to Storage A
        HDBGRegAR ();
        cpu.Y ^= cpu.rA;
        SET_ZN (cpu.Y);
        break;

      case 063: // CMPX1
        HDBGRegXR (1);
        SCF (cpu.rX1 == cpu.Y, cpu.rIR, I_ZERO);
        HDBGRegIW ();
        break;

      case 064: // TNZ
        // Transfer on Not Zero
        HDBGRegIR ();
        if (! TSTF (cpu.rIR, I_ZERO)) {
          cpu.NEXT_IC = cpu.W;
          HDBGRegNXW ();
          sim_debug (DBG_TRACE, & cpuDev, "TNZ %05o\n", cpu.NEXT_IC);
        }
        cpu.inhIntrChk = true;
        break;

      case 065: // TPL
        // Transfer on Plus
        HDBGRegIR ();
        if (! TSTF (cpu.rIR, I_NEG)) {
          cpu.NEXT_IC = cpu.W;
          HDBGRegNXW ();
          sim_debug (DBG_TRACE, & cpuDev, "TPL %05o\n", cpu.NEXT_IC);
        }
        cpu.inhIntrChk = true;
        break;

      case 066: { // SBQ
        // Subtract from Q
        bool ovf;
        HDBGRegQR ();
        cpu.rQ = Sub18b (cpu.rQ, cpu.Y, 1, I_ZERO | I_NEG | I_OVF | I_CARRY, & cpu.rIR, & ovf);
        HDBGRegQW ();
        HDBGRegIW ();
        if (ovf)
          OVF ("SBQ");
        break;
      }

      case 067: // CMPQ
        HDBGRegQR ();
        cmp18 (cpu.rQ, cpu.Y, & cpu.rIR);
        HDBGRegIW ();
        break;

// 70 - 77

      case 070: // STEX
        cpu.Y = iom.channelStatus[cpu.rIR & BITS6];
        break;

      case 071: // TRA
        // Transfer unconditionally
        cpu.NEXT_IC = cpu.W;
        HDBGRegNXW ();
        cpu.inhIntrChk = true;
        sim_debug (DBG_TRACE, & cpuDev, "TRA %05o\n", cpu.NEXT_IC);
        break;

      case 072: // ORSA
        // OR to storage A
        HDBGRegAR ();
        cpu.Y |= cpu.rA;
        SET_ZN (cpu.Y);
        break;

      case 073: { // grp1a
        switch (cpu.S1) {
          case 0:  // SEL
            // Select I/O Channel
            putbits18 (& cpu.rIR, 12, 6, cpu.D & BITS6);
            HDBGRegIW ();
            break;

          case 1:  { // IACX1
            // Immediate Add Character Address to X1
            // FA (C(X1), D] -> X1
            HDBGRegXR (1);
            cpu.rX1 = addAddr2 (cpu.rX1, cpu.Dchaddr);
            HDBGRegXW (1);
            SCF (cpu.rX1 == 0, cpu.rIR, I_ZERO);
            HDBGRegIW ();
            break;
          }

          case 2: { // IACX2
            // Immediate Add Character Address to X2
            // FA (C(X2), D] -> X2
            HDBGRegXR (2);
            cpu.rX2 = addAddr2 (cpu.rX2, cpu.Dchaddr);
            HDBGRegXW (2);
            SCF (cpu.rX2 == 0, cpu.rIR, I_ZERO);
            HDBGRegIW ();
            break;
          }

          case 3: { // IACX3
            // Immediate Add Character Address to X3
            // FA (C(X3), D] -> X3
            HDBGRegXR (3);
            cpu.rX3 = addAddr2 (cpu.rX3, cpu.Dchaddr);
            HDBGRegXW (3);
            SCF (cpu.rX3 == 0, cpu.rIR, I_ZERO);
            HDBGRegIW ();
            break;
          }

          case 4:  // ILQ
            // Immediate Load Q
            cpu.rQ = SIGNEXT9_18 (cpu.D & BITS9);
            cpu.rQ &= BITS18;
            HDBGRegQW ();
            SET_ZN (cpu.rQ);
            break;

          case 5: {  // IAQ
            // Immediate Add Q
            word18 tmp = SIGNEXT9_18 (cpu.D & BITS9);
            tmp &= BITS18;
            bool ovf;
            HDBGRegQR ();
            cpu.rQ = Add18b (cpu.rQ, tmp, 0, I_ZERO | I_NEG | I_OVF | I_CARRY, & cpu.rIR, & ovf);
            HDBGRegQW ();
            HDBGRegIW ();
            if (ovf)
              OVF ("IAQ");
            break;
          }

          case 6:  // ILA
            // Immediate Load A
            cpu.rA = SIGNEXT9_18 (cpu.D & BITS9);
            cpu.rA &= BITS18;
            HDBGRegAW ();
            SET_ZN (cpu.rA);
            break;

          case 7: { // IAA
            // Immediate Add A
            word18 tmp = SIGNEXT9_18 (cpu.D & BITS9);
            tmp &= BITS18;
            bool ovf;
            HDBGRegAR ();
            cpu.rA = Add18b (cpu.rA, tmp, 0, I_ZERO | I_NEG | I_OVF | I_CARRY, & cpu.rIR, & ovf);
            HDBGRegAW ();
            HDBGRegIW ();
            if (ovf)
              OVF ("IAA");
            break;
          }

        } // switch (S1)
        break;
      } // grp1a

      case 074: // TZE
        // Transfer on Zero
        HDBGRegIR ();
        if (TSTF (cpu.rIR, I_ZERO)) {
          cpu.NEXT_IC = cpu.W;
          HDBGRegNXW ();
          sim_debug (DBG_TRACE, & cpuDev, "TZE %05o\n", cpu.NEXT_IC);
        }
        cpu.inhIntrChk = true;
        break;

      case 075: // TMI
        // Transfer on Minus
        HDBGRegIR ();
        if (TSTF (cpu.rIR, I_NEG)) {
          cpu.NEXT_IC = cpu.W;
          HDBGRegNXW ();
          sim_debug (DBG_TRACE, & cpuDev, "TMI %05o\n", cpu.NEXT_IC);
        }
        cpu.inhIntrChk = true;
        break;

      case 076: { // AOS
        // Add One to Storage
        bool ovf;
        cpu.Y = Add18b (cpu.Y, 1, 0, I_ZERO | I_NEG | I_OVF | I_CARRY, & cpu.rIR, & ovf);
        if (ovf)
          OVF ("AOS");
        HDBGRegIW ();
        break;
      }

      case 077: // ill
        ILL;

      } // OPCODE

    if (opcTable [cpu.OPCODE].grp == opcMR) {
      if (opcTable [cpu.OPCODE].opWR) {
        if (opcTable [cpu.OPCODE].opSize == opW) {
          toMemory (cpu.Y, cpu.W, cpu.C, 0, "operand write");
        } else if (opcTable [cpu.OPCODE].opSize == opDW) {
          toMemory36 (cpu.YY, cpu.W, 1, "operand write");
        }
      }
    }

#if 0
    sim_debug (DBG_REG, & cpuDev, "A: %06o Q: %06o X: %06o %06o %06o IR: %06o %s %s %s %s\n",
               cpu.rA, cpu.rQ, cpu.rX1, cpu.rX2, cpu.rX3, cpu.rIR,
               TSTF (cpu.rIR, I_ZERO) ?  " Z" : "!Z",
               TSTF (cpu.rIR, I_NEG) ?   " N" : "!N",
               TSTF (cpu.rIR, I_CARRY) ? " C" : "!C",
               TSTF (cpu.rIR, I_OVF) ?   " O" : "!O");
#endif

    if (! cpu.disState) {
      HDBGRegNXR ();
      cpu.rIC = cpu.NEXT_IC;
      HDBGRegICW ();
    }

    if (cpu.dlyFault) {
      doFault (cpu.dlyFaultAddr, cpu.dlyCtx);
    }

  // Instruction times vary from 1 us up.
    if (! cpu.fast)
      usleep (1);

  } while (reason == 0);

leave:
 sim_printf("\nsimCycles = %ld\n", cpu.cycleCnt);

 return reason;
}

// Temp boot code for testing
static t_stat cpu_boot (UNUSED int32_t unit_num, UNUSED DEVICE * dptr) {
  cpu.disState = true;
  cpu.inhIntrChk = false;
  cpu.booted = false;
  return SCPE_OK;
}
