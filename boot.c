#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>

#include <uv.h>

#include "dnpkt.h"

#define N 00060000
//#define N 1044480
//#define N 517
#define L66ADDR 000010000

typedef uint64_t word36;
typedef uint32_t word24;

static word36 segment[N];

word24 l66Addr = L66ADDR;
//uv_udp_t udpSendHandle;
//uv_udp_t udpRecvHandle;
uv_tcp_t tcpHandle;
uv_connect_t reqConnect;

static void dnSend (dnPkt * pkt);

static void allocBuffer (uv_handle_t * handle, size_t suggested_size, uv_buf_t * buf) {
  char * p = (char *) malloc (suggested_size);
  if (! p) {
     printf ("%s malloc fail\n", __func__);
     exit (1);
  }
  * buf = uv_buf_init (p, (uint) suggested_size);
}



void processCmdDis (uv_stream_t * req, dnPkt * pkt) {
  printf ("disccnnect\n");
}

void processCmdR (uv_stream_t * req, dnPkt * pkt) {
  word24 addr;
  int n = sscanf (pkt->cmdRead.addr, "%o", & addr);
  if (n != 1) {
    printf ("%s unable to extract address\n", __func__);
  } else {
    //printf ("R %08o\n", addr);
//printf ("dia @ %u %u\n", cdp->iomUnitIdx, cdp->chan);
    word36 data = segment[addr /*- L66ADDR */];
//printf ("%08o:%012lo\n", addr, data);

  dnPkt pkt;
  memset (& pkt, 0, sizeof (pkt));
  pkt.cmd = DN_CMD_DATA;
  sprintf (pkt.cmdData.data, "%08o:%012lo", addr, data);
  dnSend (& pkt);

  }
}


void processRecv (uv_stream_t * req, dnPkt * pkt, ssize_t nread) {
  if (nread != sizeof (dnPkt)) {
    printf ("%s incoming packet size %ld, expected %ld\n", __func__, nread, sizeof (dnPkt));
  }
  if (pkt->cmd == DN_CMD_READ) {
    processCmdR (req, pkt);
  } else if (pkt->cmd == DN_CMD_DISCONNECT) {
    processCmdDis (req, pkt);
  } else {
    printf ("Ignoring cmd %c %o\n", isprint (pkt->cmd) ? pkt->cmd : '.', pkt->cmd);
  }
}



static void onRead (uv_stream_t* req, ssize_t nread, const uv_buf_t* buf) {
  if (nread < 0) {
    fprintf (stderr, "Read error!\n");
    if (! uv_is_closing ((uv_handle_t *) req))
      uv_close ((uv_handle_t *) req, NULL);
    free (buf->base);
    return;
  }
  if (nread > 0) { 
    if (! req) {
      printf ("bad req\n");
      return;
    } else {
      //printf ("recv %ld %c\n", nread, buf->base[0]);
      processRecv (req, (dnPkt *) buf->base, nread);
    }
  } else {
    //printf ("recv empty\n");
  }
  if (buf->base)
    free (buf->base);
  return;
}


static void dnSendCB (uv_write_t * req, int status) {
  if (status)
    printf ("send cb %d\n", status);
}


static void dnSend (dnPkt * pkt) {
  uv_write_t * req = malloc (sizeof (uv_write_t));
  if (! req) {
    printf ("%s req malloc fail\n", __func__);
    return;
  }
  // set base to null
  memset (req, 0, sizeof (uv_write_t));

  void * p = malloc (sizeof (dnPkt));
  if (! p) {
    printf ("%s buf malloc fail\n", __func__);
    return;
  }
  uv_buf_t buf = uv_buf_init ((char *) p, (uint) sizeof (dnPkt));

  memcpy (buf.base, pkt, sizeof (dnPkt));
  int rc = uv_write (req, (uv_stream_t *) & tcpHandle, & buf, 1, dnSendCB);
  if (rc < 0) {
    fprintf (stderr, "%s uv_udp_send failed %d\n", __func__, rc);
  }
}

static void onConnect (uv_connect_t * server, int status) {
  if (status < 0) {
    printf ("connected %d\n", status);
    return;
  }
  uv_read_start ((uv_stream_t *) & tcpHandle, allocBuffer, onRead);

}


int main (int argc, char * argv []) {
  int fd = open ("boot_segment.dat", O_RDONLY, 0);
  if (fd < 0) {
    perror ("open boot_segment.dat");
    exit (1);
  }
  ssize_t nr = read (fd, segment, sizeof (segment));
  if (nr != sizeof (segment)) {
    printf ("nr %ld, expected %ld\n", nr, sizeof (segment));
    exit (1);
  }

  
#if 0
#define base 010001
#define tallywords (0517 - 1)
for (uint32_t i = 0; i < tallywords; i ++) {
  uint32_t j = base + i;
  word36 w = segment[j/* - L66ADDR*/];
  printf ("%05o:%06lo\n", i * 2 + 0, (w >> 18) & 0777777);
  printf ("%05o:%06lo\n", i * 2 + 1, (w >>  0) & 0777777);
}
#endif


  int32_t rport = 35500;
  char rhost[64] = "127.0.0.1";

// Set up send

  int rc = uv_tcp_init (uv_default_loop (), & tcpHandle);
  if (rc) {
    printf ("%s unable to init tcpHandle %d\n", __func__, rc);
    exit (1);
  }

  struct sockaddr_in raddr;
  uv_ip4_addr (rhost, rport, & raddr);

  rc = uv_tcp_init (uv_default_loop (), & tcpHandle);
  if (rc) {
    printf ("%s unable to connect tcpHandle %d\n", __func__, rc);
    exit (1);
  }

  rc = uv_tcp_connect (& reqConnect, & tcpHandle, (struct sockaddr *) & raddr, onConnect);
  if (rc) {
    printf ("%s unable to connect udpSendHandle %d\n", __func__, rc);
    exit (1);
  }

  dnPkt pkt;
  memset (& pkt, 0, sizeof (pkt));
  pkt.cmd = DN_CMD_BOOTLOAD;
  dnSend (& pkt);

  uv_run (uv_default_loop (), UV_RUN_DEFAULT);

}
