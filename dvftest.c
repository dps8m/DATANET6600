#include <stdint.h>
#include <stdio.h>
#include <stdbool.h>

typedef uint32_t uint;

typedef uint16_t word15;
typedef uint32_t word18;
typedef uint32_t word17;
typedef uint64_t word36;

#define SIGN18       0400000        // represents sign bit of a 18-bit 2-comp number
#define BITS17  ((word17)0377777)
#define BITS18  ((word18)0777777)
#define BITS34  ((word36)0177777777777l)
#define BITS35  ((word36)0377777777777l)

#define I_ZERO  (1u << 17)  // 400000
#define I_NEG   (1u << 16)  // 200000
#define I_CARRY (1u << 15)  // 100000
#define I_OVF   (1u << 14)  // 040000
#define I_II    (1u << 13)  // 020000
#define I_PFI   (1u << 12)  // 010000
#define I_OFI   (1u << 11)  // 004000
#define I_PE    (1u << 10)  // 002000

#define SETF(flags, x)         flags = ((flags) |  (x))
#define CLRF(flags, x)         flags = ((flags) & ~(x))
#define TSTF(flags, x)         ((flags) & (x))
#define SCF(cond, flags, x)    { if (cond) SETF((flags), x); else CLRF((flags), x); }

typedef struct {
    word18  rIR;            // indicator register
    word18  rA;             // accumulator A
    word18  rQ;             // accumulator Q
    word18 Y;
    bool faulted;
} cpu_t;
static cpu_t cpu;

static inline word18 getbits18 (word18 x, uint i, uint n) {
  // bit 17 is right end, bit zero is 18th from the right
  int shift = 17 - (int) i - (int) n + 1;
  if (shift < 0 || shift > 17) {
    printf ("getbits18: bad args (%06o,i=%d,n=%d)\n", x, i, n);
    return 0;
  } else
    return (x >> (unsigned) shift) & ~ (~0U << n);
}

static void HDBGRegAR (void) {
  //printf ("read  A %06o\n", cpu.rA);
}

static void HDBGRegAW (void) {
  //printf ("write A %06o\n", cpu.rA);
}

static void HDBGRegQR (void) {
  //printf ("read  Q %06o\n", cpu.rQ);
}

static void HDBGRegYR (void) {
  //printf ("read  Y %06o\n", cpu.Y);
}

static void HDBGRegQW (void) {
  //printf ("write Q %06o\n", cpu.rQ);
}

static void HDBGRegIW (void) {
  //printf ("write IR  %06o %c%c%c%c%c%c%c%c\n", cpu.rIR, 
    //cpu.rIR & I_ZERO  ? 'Z' : 'z',
    //cpu.rIR & I_NEG   ? 'N' : 'n',
    //cpu.rIR & I_CARRY ? 'C' : 'c',
    //cpu.rIR & I_OVF   ? 'O' : 'o',
    //cpu.rIR & I_II    ? 'I' : 'i',
    //cpu.rIR & I_PFI   ? 'P' : 'p',
    //cpu.rIR & I_OFI   ? 'V' : 'v', 
    //cpu.rIR & I_PE    ? 'E' : 'e');
}

#define ADDR_FAULT_POWER_SHUTDOWN_BEGINNING 000440
#define ADDR_FAULT_RESTART                  000441
#define ADDR_FAULT_PARITY                   000442
#define ADDR_FAULT_ILLEGAL_OPCODE           000443
#define ADDR_FAULT_OVERFLOW                 000444
#define ADDR_FAULT_ILLEGAL_STORE            000445
#define ADDR_FAULT_DIVIDE_CHECK             000446
#define ADDR_FAULT_ILLEGAL_PROGRAM_INT      000447
enum 
  {
    faultPowerShutdownBeginning = ADDR_FAULT_POWER_SHUTDOWN_BEGINNING,
    faultRestart =                ADDR_FAULT_RESTART,
    faultParity =                 ADDR_FAULT_PARITY,
    faultIllegalOpcode =          ADDR_FAULT_ILLEGAL_OPCODE,
    faultOverflow =               ADDR_FAULT_OVERFLOW,
    faultIllegalStore =           ADDR_FAULT_ILLEGAL_STORE,
    faultDivideCheck =            ADDR_FAULT_DIVIDE_CHECK,
    faultIllegalProgramInt =      ADDR_FAULT_ILLEGAL_PROGRAM_INT
  };

static void doFault (word15 faultAddr, const char * msg) {
  //printf ("fault %o %s\n", faultAddr, msg);
  cpu.faulted = true;
}

static void dvf (void) {
  // C(AQ) / (Y)
  //  fractional quotient → C(A)
  //  fractional remainder → C(Q)

  // A 35-bit fractional dividend (including sign) is divided by a 18-bit
  // fractional divisor yielding a 18-bit fractional quotient (including
  // sign) and a 18-bit fractional remainder (including sign). C(AQ)71 is
  // ignored; bit position 17 of the remainder corresponds to bit position
  // 34 of the dividend. The remainder sign is equal to the dividend sign
  // unless the remainder is zero.
  //
  // If | dividend | >= | divisor | or if the divisor = 0, division does
  // not take place. Instead, a divide check fault occurs, C(AQ) contains
  // the dividend magnitude in absolute, and the negative indicator
  // reflects the dividend sign.

  // dividend format
  // 0  1     34 35
  // s  dividend x
  //  C(AQ)

  HDBGRegAR ();
  HDBGRegQR ();
  HDBGRegYR ();
  int sign = 1;
  bool dividendNegative = (getbits18 (cpu.rA, 0, 1) != 0);
  bool divisorNegative = (getbits18 (cpu.Y, 0, 1) != 0);

  // Get the 34 bits of the dividend (36 bits less the sign bit and the
  // ignored bit 35.

  // dividend format:   . d(0) ...  d(33)

  uint64_t zFrac = ((uint64_t) (cpu.rA & BITS17) << 17) | ((cpu.rQ >> 1) & BITS17);

  if (dividendNegative) {
    zFrac = (uint64_t) (((int64_t) (~zFrac)) + 1);
    sign = - sign;
  }
  zFrac &= BITS34;

  // Get the 17 bits of the divisor (18 bits less the sign bit)

  // divisor format: . d(0) .... d(16) 0 0 0 .... 0

  // divisor goes in the low half
  uint64_t dFrac = cpu.Y & BITS17;
  if (divisorNegative) {
    dFrac = (uint64_t) (((int64_t) (~dFrac)) + 1);
    sign = - sign;
  }
  dFrac &= BITS17;

  if (dFrac == 0) {
    // ISOLTS 730 expects the right to be zero and the sign
    // bit to be untouched.
    cpu.rQ = cpu.rQ & (BITS17 << 1);

    SCF (cpu.Y == 0, cpu.rIR, I_ZERO);
    SCF (cpu.rA & SIGN18, cpu.rIR, I_NEG);
    HDBGRegIW ();
    doFault (faultDivideCheck, "DVF: divide check fault");
    return;
  }

  // I am surmising that the "If | dividend | >= | divisor |" is an
  // overflow prediction.
  bool ovf = zFrac >= (dFrac << 18);

// Do the division

  uint64_t quot = zFrac / dFrac;
  uint64_t remainder = zFrac % dFrac;

  if (ovf) {
    HDBGRegAR ();
    HDBGRegQR ();
    bool Aneg = (cpu.rA & SIGN18) != 0; // blood type
    bool AQzero = cpu.rA == 0 && cpu.rQ == 0;
    if (cpu.rA & SIGN18) {
      cpu.rA = (~cpu.rA) & BITS18;
      HDBGRegAW ();
      cpu.rQ = (~cpu.rQ) & BITS18;
      cpu.rQ += 1;
      HDBGRegQW ();
      if (cpu.rQ & 01000000) { // overflow?
        cpu.rQ &= BITS18;
        HDBGRegQW ();
        cpu.rA = (cpu.rA + 1) & BITS18;
        HDBGRegAW ();
      }
    }
    // ISOLTS 730 expects the right to be zero and the sign
    // bit to be untouched.
    cpu.rQ = cpu.rQ & (BITS17 << 1);
    HDBGRegQW ();

    SCF (AQzero, cpu.rIR, I_ZERO);
    SCF (Aneg, cpu.rIR, I_NEG);
    HDBGRegIW ();

    doFault(faultDivideCheck, "DVF: divide check fault");
    return;
  }

  if (sign == -1)
    quot = ~quot + 1;
  if (dividendNegative)
    remainder = ~remainder + 1;
  cpu.rA = quot & BITS18;
  HDBGRegAW ();
  cpu.rQ = remainder & BITS18;
  HDBGRegQW ();

  SCF (cpu.rA == 0 && cpu.rQ == 0, cpu.rIR, I_ZERO);
  SCF (cpu.rA & SIGN18, cpu.rIR, I_NEG);
  HDBGRegIW ();
}

static void testpp (int n, char * signs, word36 AQ, word18 Y, word18 quot, word18 rem, bool faulted) {
  //printf ("test %d\n", n);
  cpu.rA = (AQ >> 18) & BITS18;
  cpu.rQ = AQ & BITS18;
  cpu.Y = Y;
  cpu.faulted = false;
  dvf ();
  bool ok = true;
  if (cpu.rA != quot) {
    printf ("quot expected %06o, got %06o\n", quot, cpu.rA);
    ok = false;
  }
  if (cpu.rQ != rem) {
    printf ("rem expected %06o, got %06o\n", rem, cpu.rQ);
    ok = false;
  }
  if (faulted != cpu.faulted) {
    printf ("faulted expected %o, got %o\n", faulted, cpu.faulted);
    ok = false;
  }
  printf ("test %d%s: %s\n", n, signs, ok ? "PASS" : "FAIL");
}

static word36 negate35 (word36 AQ) {
  //return ((~ AQ) + 1) & 0777777777776;
  word36 n = AQ;
  n = n >> 1;
  n = n & BITS35;
  n = ~ n;
  n = n + 1;
  n = n & BITS35;
  n = n << 1;
  return n;
}

static word18 negate18 (word18 n) {
  return ((~ n) + 1) & BITS18;
}

static void test (int n, word36 AQ, word18 Y, word18 quot, word18 rem, bool faulted) {
  testpp (n, "++", AQ, Y, quot, rem, faulted);
  testpp (n, "-+", negate35 (AQ), Y, negate18 (quot), negate18 (rem), faulted);
  testpp (n, "+-", AQ, negate18 (Y), negate18 (quot), rem, faulted);
  testpp (n, "--", negate35 (AQ), negate18 (Y), quot, negate18 (rem), faulted);
}

int main (int argc, char * aargv[]) {
  // .25 / .5 -> .5, 0, no overflow
  // .01 / .1 -> .1, 0, no overflow
  test (1, 0100000000000, 0200000, 0200000, 0000000, false);
  // From ISOLTS

// DVF dividend 400000000000000000000000 divisor 000000000000
// DVF divide check fault quotient 400000000000 remainder 000000000000
  test (2, 0400000000000, 0000000, 0400000, 0000000, true);

// DVF dividend 400000000000000000000000 divisor 400000000000
// DVF divide check fault quotient 400000000000 remainder 000000000000
  test (3, 0400000000000, 0400000, 0400000, 0000000, true);

// DVF dividend 222222222222222222222222 divisor 252525252525
// DVF quotient 333333333333 remainder 222222222222
  //test (4, 0222222222222, 0252525, 0333333, 0222222, false);

// DVF dividend 333333333333333333333333 divisor 352525252525
// DVF quotient 357300651436 remainder 075403246167
  //test (5, 0333333333333, 0352525, 0357300, 0075403, false);

// DVF dividend 555555555555555555555555 divisor 377777777777
// DVF quotient 555555555556 remainder 444444444444
  //test (6, 0555555555555, 0377777, 0555555, 0444444, false);

// DVF dividend 212121212121212121212121 divisor 252525000000
// DVF quotient 317172236364 remainder 044444505050
  //test (7, 0212121212121, 0252525, 0317172, 0444445, false);

// DVF dividend 304040404040404040404040 divisor 325252000000
// DVF quotient 353216112075 remainder 210616202020
  //test (8, 0304040404040, 0325252, 0353216, 0210616, false);

// DVF dividend 000000121212121212121212 divisor 000000252525
// DVF quotient 171717363636 remainder 000000171717

// DVF dividend 000000404040404040404040 divisor 000000525252
// DVF quotient 303030606061 remainder 000000060606

// DVF dividend 121212121212121212121212 divisor 000000252525
// DVF divide check fault2 quotient 121212121212 remainder 121212121212

// DVF dividend 404040404040404040404040 divisor 000000525252
// DVF divide check fault2 quotient 373737373737 remainder 373737373740





// DVF dividend 000000000000000000777776 divisor 000000777777
// DVF quotient 000000000000 remainder 000000377777

// DVF dividend 000000000000000000777776 divisor 000000777777
// DVF quotient 000000000000 remainder 000000377777

// DVF dividend 000000000000000000000077 divisor 000000000101
// DVF quotient 000000000000 remainder 000000000037

// DVF dividend 000000000000000000000077 divisor 000000000101
// DVF quotient 000000000000 remainder 000000000037

// DVF dividend 000000000000177777777777 divisor 000000000000
// DVF divide check fault quotient 000000000000 remainder 177777777776

  test (18, 0000000000002, 0200000, 0000000, 0000001, false);
  test (19, 0000000000002, 0000001, 0000001, 0000000, false);
  test (19, 0000000000002, 0000001, 0000001, 0000000, false);
  test (20, 0000000000020, 0000001, 0000010, 0000000, false);
  test (21, 0000000000020, 0000010, 0000001, 0000000, false);

}
