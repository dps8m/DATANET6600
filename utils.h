#include <stdbool.h>

// Interface for cfg_parse

typedef struct config_value_list_s
  {
    const char * value_name;
    int64_t value;
  } config_value_list_t;

typedef struct config_list_s
  {
    const char * name;  // opt name
    int64_t min, max;   // value limits
    config_value_list_t * value_list;
  } config_list_t;

typedef struct config_state_s
  {
    char * copy;
    char * statement_save;
  } config_state_t;

int cfg_parse (const char * tag, const char * cptr, config_list_t * clist, config_state_t * state, int64_t * result);
void cfg_parse_done (config_state_t * state);
word36 Add36b (word36 op1, word36 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf);
word36 Sub36b (word36 op1, word36 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf);
word18 Add18b (word18 op1, word18 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf);
word18 Sub18b (word18 op1, word18 op2, word1 carryin, word18 flagsToSet, word18 * flags, bool * ovf);
void cmp18 (word18 oP1, word18 oP2, word18 * flags);
char * rtrim( char * s);
char * ltrim (char * s);
char * trim (char * s);
