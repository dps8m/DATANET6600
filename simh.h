/*
 * vim: filetype=c:tabstop=4:ai:expandtab
 * SPDX-License-Identifier: ICU
 * scspell-id: e959445f-f62e-11ec-b5eb-80ee73e9b8e7
 *
 * ---------------------------------------------------------------------------
 *
 * Copyright (c) 2007-2013 Michael Mondy
 * Copyright (c) 2012-2016 Harry Reed
 * Copyright (c) 2013-2016 Charles Anthony
 * Copyright (c) 2021-2022 The DPS8M Development Team
 *
 * All rights reserved.
 *
 * This software is made available under the terms of the ICU
 * License, version 1.8.1 or later.  For more details, see the
 * LICENSE.md file at the top-level directory of this distribution.
 *
 * ---------------------------------------------------------------------------
 */

extern DEVICE scu_dev;

/* scp Debug flags */

// Abort codes, used to sort out longjmp's back to the main loop.
// Codes > 0 are simulator stop codes
// Codes < 0 are internal aborts
// Code  = 0 stops execution for an interrupt check (XXX Don't know if I like
// this or not)
// XXX above is not entirely correct (anymore).

//#define SCPE_OK    0
#define STOP_STOP   1
#define STOP_BKPT   2

// not really STOP codes, but get returned from instruction loops
#define CONT_TRA    -1  // encountered a transfer instruction; don't bump PPR.IC
#define CONT_DIS    -2  // instruction was a DIS
#define CONT_XEC    -3  // instruction was a XEC or XED
#define CONT_RET    -5  // encountered a return instruction; don't bump PPR.IC,
                        // do instruction fetch

//
// mask entry flags
//  MTAB_XTD extended entry
//  MTAB_VDV valid for devices
//  MTAB_VUN valid for units
//  MTAB_VALO takes a value (optional)
//  MTAB_VALR takes a value (required)
//  MTAB_NMO valid only in named SHOW
//  MTAB_NC do not convert option value to upper case
//  MTAB_SHP SHOW parameter takes optional value

// Requires a value, DEV and DEVn both work, not in "show"
#define MTAB_unit_value      MTAB_XTD | MTAB_VUN | MTAB_VDV | MTAB_NMO | MTAB_VALR
// Requires a value, DEVn, not in "show"
#define MTAB_unitonly_value      MTAB_XTD | MTAB_VUN | MTAB_NMO | MTAB_VALR
// Requires a value, DEV and DEVn both work, in "show"
#define MTAB_unit_value_show MTAB_XTD | MTAB_VUN | MTAB_VDV | MTAB_VALR
// Requires a value, DEV only, not in "show", uppercase value
#define MTAB_dev_valr_noshow MTAB_XTD | MTAB_VDV | MTAB_NMO | MTAB_VALR
// Requires a value, DEV only, not in "show", don't uppercase value
#define MTAB_dev_valr_nouc MTAB_XTD | MTAB_VDV | MTAB_NMO | MTAB_VALR | MTAB_NC
// Requires a value, DEV only, not in "show"
#define MTAB_dev_value     MTAB_XTD | MTAB_VDV | MTAB_NMO | MTAB_VALR
// No value, DEV only, in "show"
#define MTAB_dev_novalue     MTAB_XTD | MTAB_VDV
// Requires a value, DEVn only, in "show", don't uppercase value
#define MTAB_unit_valr_nouc MTAB_XTD | MTAB_VUN | MTAB_VALR | MTAB_NC
// Value optional, DEVn only, do not uppercase value
#define MTAB_unit_nouc MTAB_XTD | MTAB_VUN | MTAB_NC
// Value optional, DEVn only,  uppercase value
#define MTAB_unit_uc MTAB_XTD | MTAB_VUN
// Value required, DEV only
#define MTAB_dev_valr MTAB_XTD | MTAB_VDV | MTAB_VALR
// End of list marker
#define MTAB_eol { 0, 0, NULL, NULL, 0, 0, NULL, NULL }

extern uint32_t sim_brk_summ, sim_brk_types, sim_brk_dflt;
extern FILE *sim_deb;
void sim_printf( const char * format, ... );    // not really simh, by my impl

#if defined(LOCKLESS)
void dps8m_sim_debug (uint32_t dbits, DEVICE* dptr, uint64_t cnt, const char* fmt, ...);

#endif
#define sim_fatal(format, ...) { _sim_err (format, ##__VA_ARGS__); exit (1); }
#define sim_msg sim_printf
#define sim_warn sim_printf
#define sim_print sim_printf
