#include "dn355.h"
#include "cpu.h"
#include "iom.h"
#include "caf.h"
#include "utils.h"
#include "coupler_tcp.h"
#include "coupler.h"
#include "core.h"
#include "hsla.h"
#include "hdbg.h"

#define MBXTRACE

//  6100  dia_pcw
//  6101  mailbox_requests
//  6102  term_inpt_mpx_wd
//  6103  last_mbx_req_count
//  6104  num_in_use
//  6105  mbx_used_flags
//  6106  crash_data0
//  6107  crash_data1

//   mbox 0
//  6110  word1; // dn355_no; is_hsla; la_no; slot_no
//  6111  word2; // cmd_data_len; op_code; io_cmd
//  6112  command_data 0
//  6113  command_data 1
//  6114  command_data 2
//  6115  word6; // data_addr, word_cnt
//  6116  pad3
//  6117  pad3

//   mbox 1
//  6120  word1; // dn355_no; is_hsla; la_no; slot_no
//  6121  word2; // cmd_data_len; op_code; io_cmd
//  6122  command_data 0
//  6123  command_data 1
//  6124  command_data 2
//  6125  word6; // data_addr, word_cnt
//  6126  pad3
//  6127  pad3

//   mbox 2
//  6130  word1; // dn355_no; is_hsla; la_no; slot_no
//  6131  word2; // cmd_data_len; op_code; io_cmd
//  6132  command_data 0
//  6133  command_data 1
//  6134  command_data 2
//  6135  word6; // data_addr, word_cnt
//  6136  pad3
//  6137  pad3

//   mbox 3
//  6140  word1; // dn355_no; is_hsla; la_no; slot_no
//  6141  word2; // cmd_data_len; op_code; io_cmd
//  6142  command_data 0
//  6143  command_data 1
//  6144  command_data 2
//  6145  word6; // data_addr, word_cnt
//  6146  pad3
//  6147  pad3

//   mbox 4
//  6150  word1; // dn355_no; is_hsla; la_no; slot_no
//  6151  word2; // cmd_data_len; op_code; io_cmd
//  6152  command_data 0
//  6153  command_data 1
//  6154  command_data 2
//  6155  word6; // data_addr, word_cnt
//  6156  pad3
//  6157  pad3

//   mbox 5
//  6160  word1; // dn355_no; is_hsla; la_no; slot_no
//  6161  word2; // cmd_data_len; op_code; io_cmd
//  6162  command_data 0
//  6163  command_data 1
//  6164  command_data 2
//  6165  word6; // data_addr, word_cnt
//  6166  pad3
//  6167  pad3

//   mbox 6
//  6170  word1; // dn355_no; is_hsla; la_no; slot_no
//  6171  word2; // cmd_data_len; op_code; io_cmd
//  6172  command_data 0
//  6173  command_data 1
//  6174  command_data 2
//  6175  word6; // data_addr, word_cnt
//  6176  pad3
//  6177  pad3

//   mbox 7
//  6200  word1; // dn355_no; is_hsla; la_no; slot_no
//  6201  word2; // cmd_data_len; op_code; io_cmd
//  6202  command_data 0
//  6203  command_data 1
//  6204  command_data 2
//  6205  word6; // data_addr, word_cnt
//  6206  pad3
//  6207  pad3

//  mbox 8  fnp 0
//  6210 word1; // dn355_no; is_hsla; la_no; slot_no
//  6211 word2; // cmd_data_len; op_code; io_cmd
//  6212
//  6213
//  6214
//  6215
//  6216
//  6217
//  6220
//  6221
//  6222
//  6223
//  6224
//  6225
//  6226
//  6227
//  6230
//  6231
//  6232
//  6233
//  6234
//  6235
//  6236
//  6237
//  6240
//  6241
//  6242
//  6243

//  mbox 9  fnp 1
//  6244 word1; // dn355_no; is_hsla; la_no; slot_no
//  6245 word2; // cmd_data_len; op_code; io_cmd
//  6246
//  6247
//  6250
//  6251
//  6252
//  6253
//  6254
//  6255
//  6256
//  6257
//  6260
//  6261
//  6262
//  6263
//  6264
//  6265
//  6266
//  6267
//  6270
//  6271
//  6272
//  6273
//  6274
//  6275
//  6276
//  6277


//  mbox 10 fnp 2
//  6300 word1; // dn355_no; is_hsla; la_no; slot_no
//  6301 word2; // cmd_data_len; op_code; io_cmd
//  6302
//  6303
//  6304
//  6305
//  6306
//  6307
//  6310
//  6311
//  6312
//  6313
//  6314
//  6315
//  6316
//  6317
//  6320
//  6321
//  6322
//  6323
//  6324
//  6325
//  6326
//  6327
//  6330
//  6331
//  6332
//  6333

//  mbox 11 fnp 3
//  6334 word1; // dn355_no; is_hsla; la_no; slot_no
//  6335 word2; // cmd_data_len; op_code; io_cmd
//  6336
//  6337
//  6340
//  6341
//  6342
//  6343
//  6344
//  6345
//  6346
//  6347
//  6350
//  6351
//  6352
//  6353
//  6354
//  6355
//  6356
//  6357
//  6360
//  6361
//  6362
//  6363
//  6364
//  6365
//  6366
//  6367

#define MBX_BASE 06100
#define MBX_LAST 06370
#define MBX_LEN (MBX_LAST - MBX_BASE)
//static word36 mbxShadow[MBX_LEN];
//static unsigned int mbxBase[12] = {06110, 06120, 06130, 06140, 06150, 06160, 06170, 06200,
//                                   06210, 06244, 00630, 06334};

#ifdef MBXTRACE
#define mbase 06100
#define mhdrl 8
#define msubmbxl 8
#define msubmbxc 8
#define fsubmbxl 28
#define mbxcmdo 1
#define mcmda(n)  (mbase + mhdrl + msubmbxl * (n) + mbxcmdo)
#define mfcmda(n)  (mbase + mhdrl + msubmbxl * msubmbxc + fsubmbxl * (n) + mbxcmdo)

#define FTRC(a,d) ftrace (a, d)

static void ftracertx (uint n, word9 op_code, word36 w) {
  switch (op_code) {
    case 0005:
      sim_printf ("FNP RTX Input Accepted\n");
      break;
    default:
      sim_printf ("unk rtx op_code %o mbx %o w %012lo\n", op_code, n, w);
      break;
  }
}


static void ftracercd (uint n, word9 op_code, word36 w) {
  switch (op_code) {
    case 0100:
      sim_printf ("FNP RCD Accept New Terminal\n");
      break;
    case 0101:
      sim_printf ("FNP RCD Line Disconnected\n");
      break;
    case 0102:
      sim_printf ("FNP RCD Input to MBX\n");
      break;
    case 0105:
      sim_printf ("FNP RCD Send Output\n");
      break;
    case 0112:
      sim_printf ("FNP RCD Accept Input\n");
      break;
    case 0113:
      sim_printf ("FNP RCD Line Break\n");
      break;
    case 0114:
      sim_printf ("FNP RCD WRU Timeout\n");
      break;
    case 0115:
      sim_printf ("FNP RCD Error Message\n");
      break;
    case 0124:
      sim_printf ("FNP RCD Line Status\n");
      break;
    default:
      sim_printf ("unk rcd op_code %o mbx %o w %012lo\n", op_code, n, w);
      break;
  }
}

static void ftracem (uint n, word36 w) {
  word9 io_cmd = (word9) getbits36 (w, 27, 9);
  word9 op_code = (word9) getbits36 (w, 18, 9);
  switch (io_cmd) {
    case 1:
      ftracercd (n, op_code, w);
      break;
    case 2:
      ftracertx (n, op_code, w);
      break;
    default:
      sim_printf ("ftracem unk io_cmd %o mbx %o w %012lo\n", io_cmd, n, w);
      break;
  }
}

static void ftrace (word24 addr, word36 w) {
  for (uint n = 0; n < 4; n ++) {
    if (addr == mfcmda (n))
      ftracem (n, w);
  }
}

#define MTRC(a,d) mtrace (a, d)

static void mtracewtx (uint n, word9 op_code, word36 w) {
  switch (op_code) {
    case 0012:
      sim_printf ("CS WTX  Accept Output\n");
      break;
    case 0013:
      sim_printf ("CS WTX  Accept Last Output\n");
      break;
    default:
      sim_printf ("unk wtx op_code %o mbx %o w %012lo\n", op_code, n, w);
      break;
  }
}

static void mtracewcd (uint n, word9 op_code, word36 w) {
  switch (op_code) {
    case 0000:
      sim_printf ("CS WCD  Terminal Accecpted\n");
      break;
    case 0001:
      sim_printf ("CS WCD  Disconnect Line\n");
      break;
    case 0002:
      sim_printf ("CS WCD  Disconnect All Lines\n");
      break;
    case 0004:
      sim_printf ("CS WCD  Accept Calls\n");
      break;
    case 0006:
      sim_printf ("CS WCD  Set Line Type\n");
      break;
    case 0007:
      sim_printf ("CS WCD  Enter Receive Mode\n");
      break;
    case 0010:
      sim_printf ("CS WCD  Set Framing Characters\n");
      break;
    case 0023:
      sim_printf ("CS WCD  Dump Memory\n");
      break;
    case 0024:
      sim_printf ("CS WCD  Patch Memory\n");
      break;
    case 0026:
      sim_printf ("CS WCD  Line Control\n");
      break;
    case 0042:
      sim_printf ("CS WCD  Alter Parameters\n");
      break;
    case 0045:
      sim_printf ("CS WCD  Set Delay Table\n");
      break;
    default:
      sim_printf ("unk wcd op_code %o mbx %o w %012lo\n", op_code, n, w);
      break;
  }
}

static void mtracem (uint n, word36 w) {
  word9 io_cmd = (word9) getbits36 (w, 27, 9);
  word9 op_code = (word9) getbits36 (w, 18, 9);
  switch (io_cmd) {
    case 3:
      mtracewcd (n, op_code, w);
      break;
    case 4:
      mtracewtx (n, op_code, w);
      break;
    default:
      sim_printf ("mtracem unk io_cmd %o mbx %o w %012lo\n", io_cmd, n, w);
      break;
  }
}

static void mtrace (word24 addr, word36 w) {
  for (uint n = 0; n < 8; n ++) {
    if (addr == mcmda (n)) {
      mtracem (n, w);
      return;
     }
  }
  for (uint n = 0; n < 4; n ++) {
    if (addr == mfcmda (n)) {
      mtracem (n, w);
      return;
    }
  }
}
#else
#define FTRC(a,d)
#define MTRC(a,d)
#endif

struct couplerData_s cd;

static t_stat couplerReset (DEVICE *dptr) {
  cd.task_register.BT_INH = 0;
  cd.task_register.STORED_BOOT = 0;
  return SCPE_OK;
}
static DEBTAB couplerDT [] = {
  { "DEBUG", DBG_DEBUG },
  { "DMA",   DBG_DMA },
  { NULL, 0 }
};

static UNIT couplerUnit = {
  UDATA (NULL, UNIT_FIX|UNIT_BINK, MEM_SIZE), 0, 0, 0, 0, 0, NULL, NULL
};

static MTAB couplerMod [] = {
  { 0, 0, NULL, NULL, NULL, NULL, NULL, NULL }
};

DEVICE couplerDev = {
  "COUPLER",        /* name */
  & couplerUnit,    /* units */
  NULL,             /* registers */
  couplerMod,       /* modifiers */
  1,                /* #units */
  8,                /* address radix */
  15,               /* address width */
  1,                /* address increment */
  8,                /* data radix */
  18,               /* data width */
  NULL,             /* examine routine */
  NULL,             /* deposit routine */
  couplerReset,     /* reset routine */
  NULL,             /* boot routine */
  couplerAttach,    /* attach routine */
  couplerDetach,    /* detach routine */
  NULL,             /* context */
  DEV_DEBUG,        /* flags */
  0,                /* debug control flags */
  couplerDT,        /* debug flag names */
  NULL,             /* memory size change */
  NULL,             /* logical name */
  NULL,             // attach help
  NULL,             // help
  NULL,             // help context
  NULL,             // device description
};

static void diaStatus (word18 status0, word18 status1) {
  word18 statusICW = coreRead (ADDR_IOM_MBX_DIA_STATUS_ICW + 0, __func__);
  word18 tallyWord = coreRead (ADDR_IOM_MBX_DIA_STATUS_ICW + 1, __func__);
  unsigned int tally = tallyWord & BITS12;
  bool exhaust = !! (tallyWord & 010000);
  word15 addr = statusICW & BITS15;
  word3 charPtr = (statusICW >> 15) & 3;
  if (charPtr == 1 && tally == 1 && exhaust) {
    coreWrite (addr + 0, status0, __func__);
    coreWrite (addr + 1, status1, __func__);
  } else if (charPtr == 1 && tally == 2 && exhaust) {
    // Not right, but I don't know what is expected.
    coreWrite (addr + 0, status0, __func__);
    coreWrite (addr + 1, status1, __func__);
  } else {
    HDBGNote (__func__, "Got charPtr %o tally %o exhaust %o; skipping status write", charPtr, tally, exhaust);
    sim_printf ("WARN: %s: Got charPtr %o tally %o exhaust %o; skipping status write\n", __func__, charPtr, tally, exhaust);
  }
}

void couplerConnect (word36 PCW) {
  cd.l6.PCW = PCW;
  cd.l6.connect = true;
  return;
}

void couplerInit (void) {
  memset(& cd, 0, sizeof (cd));
  cd.state = cstateIdle;
}

// Data reply to data read command

void dataCmdReceived (uv_stream_t * req, dnPkt * pkt) {
  word24 addr;
  word36 data;
  int n = sscanf (pkt->cmdData.data, "%08o:%012lo", & addr, & data);
  if (n != 2) {
    sim_printf ("ERR: %s unable to extract address\n", __func__);
    HDBGNote0 (__func__, "ERR: unable to extract address");
    return;
  }
  HDBGNote (__func__, "%08o:%012lo", addr, data);
  if (cd.DMADataRecvd) {
    sim_printf ("ERR: Incoming DMA data buffer overrun\n");
    return;
  }
  MTRC (addr, data);
  cd.DMAData = data;
  cd.DMADataAddr = addr;
  cd.DMADataRecvd = true;
}

void cmdReceived (uv_stream_t * req, dnPkt * pkt, ssize_t nread) {

  while (nread > 0) { // break up messages combined by TCP

    if (nread < sizeof (dnPkt)) {
      sim_printf ("WARN: %s incoming packet size %ld, expected %ld\n", __func__, nread, sizeof (dnPkt));
      HDBGNote (__func__, "WARN: incoming packet size %ld, expected %ld", nread, sizeof (dnPkt));
      break;
    }

    if (pkt->cmd == DN_CMD_CONNECT) {
      cd.l66.connect = true;
    } else if (pkt->cmd == DN_CMD_DATA) {
      dataCmdReceived (req, pkt);
    } else if (pkt->cmd == DN_CMD_OFFHOOK) {
      offhookCmdReceived (req, pkt);
    } else {
       sim_printf ("ERR: %s Ignoring cmd %c %o\n", __func__, isprint (pkt->cmd) ? pkt->cmd : '.', pkt->cmd);
       HDBGNote (__func__, "WARN: Ignoring cmd %c %o", isprint (pkt->cmd) ? pkt->cmd : '.', pkt->cmd);
    }

    nread -= sizeof (dnPkt);
    pkt ++;
  }
}

static void configurationStatus (word3 w, word15 dcwY) {
  switch (w) {
    case 00:
      // dd01 seems to say that only the port configurations are set, but
      // gicb seems to expect:
      //          even
      //   config null
      //   cspab  oct                     port a and port b
      //   cspcd  oct                     port c and port d
      //   csmbx  oct                     cs mailbox address
      //   csics  oct                     cs interrupt cell switch
      //   cslwa  oct                     lower address bounds switches
      //   csupc  oct                     upper address bounds switches
      //          bss     2
      //
      //    gicb looks at csmbx and csics
    
      // System controller port configurations
      //  0- 8 Port A
      //    0-2 ICA configuration panel locical port number assignmnet
      //    3   Interlaced
      //    4   Enabled
      //    5   Initialize enabled
      //    6-8 Memory size in each port
      //  9-17 Port B
      // 18-26 Port B
      // 27-35 Port B
    
      // No idea, so:
      //    0-2 000
      //      3 0
      //      4 1
      //      5 1
      //    6-8 111
      //  037
      coreWrite (dcwY + 0, (0037 << 9) | 0137, __func__);
      coreWrite (dcwY + 1, (0137 << 9) | 0237, __func__);

      // Mailbox address
      coreWrite (dcwY + 2, cpu.mbxAddr, __func__);

      // Interrupt level switches
      coreWrite (dcwY + 3, (word15) (((cpu.intrCellSwitches & 07) << 3) | (cpu.emerCellSwitches & 07)), __func__);

      // Lower address bounds switches
      coreWrite (dcwY + 4, cpu.lowerBound, __func__);

      // Upper address bounds switches
      coreWrite (dcwY + 5, cpu.upperBound, __func__);
 
      break;

    default:
      break;
  }
} // configurationStatus


#define GOTO(s) cd.state = s; return;

void couplerRun (void) {
  switch (cd.state) {

    case cstateIdle: {

      //  L6 Connect?

      if (cd.l6.connect) {
        cd.l6.connect = false;
        GOTO (cstateL6Connect);
      }

      // L66 Connect?

      if (cd.l66.connect) {
        cd.l66.connect = false;
        GOTO (cstateL66Connect);
      }

      return;

    } // case cstateIdle

    //
    // L6
    //

    case cstateL6Connect: {
      cd.l6.connect = false;
      cd.l6.pcwCmd = (word6) getbits36 (cd.l6.PCW, 30, 6);
      DBG_DIA (sim_printf ("L6 connect PCW %012lo cmd %02o %05o\n", cd.l6.PCW, cd.l6.pcwCmd, cpu.rIC);)


      switch (cd.l6.pcwCmd) {


        case 070:
        case 072: { // Start xfer
          // Get the LICW address
          word15 pcwY = (word15) getbits36 (cd.l6.PCW, 3, 15);
          // Get the LICW
          cd.l6.LICW = coreReadDouble (pcwY, __func__);
          word15 Y = (word15) getbits36 (cd.l6.LICW, 3, 15);
          word18 Z = (word18) getbits36 (cd.l6.LICW, 24, 12);
          cd.l6.licwY = Y;
          cd.l6.licwZ = Z;
//sim_printf ("licwz starts at %d\n", Z);
          GOTO (cstateL6GetDCW);
        } // 70, 72 start xfer.

        case 073: { // SXC
          //GOTO (cstateL6SXC);
          word3 intrLvl = cd.l6.PCW & 07; // Low 3 bits of X
          CS_SEND_SXC (intrLvl);
//sim_printf ("SEND_SXC 2 %d\n", intrLvl);
          GOTO (cstateIdle);
        } // case SXC

        default: { 
sim_printf (">>>>>>>>>>>>>>>>>>>>> pcw cmd ignored %o IC %05o\n", cd.l6.pcwCmd, cpu.rIC);
          GOTO (cstateIdle);
        } // case start xfer
      } // switch pcwCmd
    } // cstateL6Connect

    case cstateL6GetDCW: {
      if (cd.l6.licwZ <= 0) { // DCW list exhausted
//printf ("cstateL6GetDCW DCW list exhausted\n");
        diaStatus (000001, 0000000);
        doIntr (2, COUPLER_CHANNEL_NO, "coupler cstateL6GetDCW");
        GOTO (cstateIdle);
      }
      // Get the next DCW first dword
      cd.l6.DCW1 = coreReadDouble (cd.l6.licwY, "cstateL6GetDCW");
      cd.l6.licwY += 2;
      GOTO (cstateL6GetDCW2);
    } // cstateL6GetDCW
  
    case cstateL6GetDCW2: {
      // Get the next DCW second dword
      cd.l6.DCW2 = coreReadDouble ((cd.l6.licwY) & BITS15, "cstateL6GetDCW2");
      cd.l6.licwY += 2;
//sim_printf ("licwz at %d DCW %012lo %012lo\n", cd.l6.licwZ, cd.l6.DCW1, cd.l6.DCW2);

      // Update LICW
      putbits36 (& cd.l6.LICW, 3, 15, cd.l6.licwY); 
      if (cd.l6.licwZ >= 2)
        cd.l6.licwZ -= 2;
      else
        cd.l6.licwZ = 0;
      if (cd.l6.licwZ)
        putbits36 (& cd.l6.LICW, 23, 13, cd.l6.licwZ); // Set E=0, Z;
      else
        putbits36 (& cd.l6.LICW, 23, 13, 010000); // Set E=1, Z=0;
  
      word6  dcwC = (word6) getbits36 (cd.l6.DCW1, 30, 6);
      word18 dcwW = (word18) getbits36 (cd.l6.DCW1, 0, 18);
      word6  dcwX = (word6) getbits36 (cd.l6.DCW1, 24, 6);
      word15 dcwY = (word15) getbits36 (cd.l6.DCW2, 3, 15);
      word12 dcwZ = (word12) getbits36 (cd.l6.DCW2, 24, 12);

      DBG_DIA (printf ("L6 dcw %02o\n", dcwC);)

// dia_man.map355; lines 251-259
//
// diatrg  bool    65      transfer gate from cs to fnp
// diadis  bool    70      disconnect
// diainf  bool    71      interrupt fnp
// diajmp  bool    72      jump
// diainc  bool    73      interrupt cs
// diardc  bool    74      read configuration switches
// diaftc  bool    75      data transfer from fnp to cs
// diactf  bool    76       "      "      "   cs to fnp
// diawrp  bool    77      wraparound

      switch (dcwC) {
        case 065: { // Xfer from CS, destructive read
          //xferFromCS (dcwW, dcwY, dcwZ);
          //waitDMAComplete ();
          //break;
          cd.l6.dmaL66Addr = dcwW;
          cd.l6.dmaL6Addr = dcwY;
          cd.l6.dmaTally = dcwZ == 0 ? 4096 : dcwZ;
          cd.l6.readClear = true;
//printf ("destructive read from %o\n", dcwW);
          GOTO (cstateL6StartXferFromCS);
        } // 076 Xfer from CS

        case 070:   // DISCONNECT
          diaStatus (000001, 0000000);
// Verified; trace of gicb shows that this vectors to 
// l.trm1 ind     .trm1           terminate vector for configuration read
// And
// dia090 ind     **              gets "tsy"d to on dia interrupt
//        tsy     a.d006-*,*      (rstclk) dia terminate interrupt occurred
          doIntr (2, 4, "coupler dcwC DISCONNECT");
          //GOTO (cstateL6GetDCW); -- I think this is correct, but it crashes MCS hard. Maybe an error in the LICW tally logic? XXX
          GOTO (cstateIdle);

        case 073: { // Set execute cell
          word3 intrLvl = dcwX & 07; // Low 3 bits of X
          CS_SEND_SXC (intrLvl);
//sim_printf ("SEND_SXC 1 %d\n", intrLvl);
          GOTO (cstateL6GetDCW);
        }

        // dd01 pg 182
        case 074: { // CONFIGURATION STATUS
          word3 w = dcwW & BITS3;
          configurationStatus (w, dcwY);
          GOTO (cstateL6GetDCW); // get next dcw 
        } // configuration status

        case 075: { // Xfer to CS
          cd.l6.dmaL66Addr = dcwW;
          cd.l6.dmaL6Addr = dcwY;
          cd.l6.dmaTally = dcwZ == 0 ? 4096 : dcwZ;
          GOTO (cstateL6StartXferToCS);
        }

        case 076: { // Xfer from CS
          //xferFromCS (dcwW, dcwY, dcwZ);
          //waitDMAComplete ();
          //break;
          cd.l6.dmaL66Addr = dcwW;
          cd.l6.dmaL6Addr = dcwY;
          cd.l6.dmaTally = dcwZ == 0 ? 4096 : dcwZ;
          cd.l6.readClear = false;
          GOTO (cstateL6StartXferFromCS);
        } // 076 Xfer from CS

        default:
          sim_printf ("ERR: %s dcwC cmd ??? %o\n", __func__, dcwC);
          //GOTO (cstateL6GetDCW); // continue the CIOC
          GOTO (cstateIdle); // abort the CIOC
          return;
        
      } // switch (dcwC)
      return;

//sim_printf ("get dcw dcw %012lo %012lo\n", cd.l6.DCW1, cd.l6.DCW2);
//bail ();
    } // cstateL6GetDCW2

    case cstateL6StartXferFromCS: {
      if  (cd.l6.dmaTally <= 0) {
        GOTO (cstateL6GetDCW);
      } // tally <= 0

      // DMA read word from L6
      cd.DMADataRecvd = false;
      if (cd.l6.readClear) {
        CS_SEND_READ_CLEAR (cd.l6.dmaL66Addr);
//sim_printf ("l6 read and clear %o\n", cd.l6.dmaL66Addr);
      } else {
        CS_SEND_READ (cd.l6.dmaL66Addr);
      }
      GOTO (cstateL6WaitXferFromCS);
    } // cstateL6StartXferFromCS

    case cstateL6WaitXferFromCS: {
      if (! cd.DMADataRecvd)
        return;
      if (cd.DMADataAddr != cd.l6.dmaL66Addr) {
        sim_printf ("DMA addr expected %o, got %o\n", cpu.mbxAddr, cd.l6.dmaL6Addr);
      }
      word15 l6Addr = cd.l6.dmaL6Addr & BITS15;
//sim_printf ("from L66 %08o:%012lo L6 %05o\n", cd.l6.dmaL66Addr, cd.DMAData, cd.l6.dmaL6Addr);
      if (cd.l6.readClear)
        coreOrWriteDouble (l6Addr, cd.DMAData, __func__);
      else
        coreWriteDouble (l6Addr, cd.DMAData, __func__);

      // Update dma registers
      cd.l6.dmaL66Addr ++;
      cd.l6.dmaL6Addr += 2;
      cd.l6.dmaTally --;
      
      GOTO (cstateL6StartXferFromCS);
    } // cstateL6WaitXferFromCS

    case cstateL6StartXferToCS: {
      if  (cd.l6.dmaTally <= 0) {
        GOTO (cstateL6GetDCW);
      } // tally <= 0

      // DMA write word to L66
      word36 w = coreReadDouble (cd.l6.dmaL6Addr, __func__);
//sim_printf ("write to cs l6 %o l66 %o data %012lo\n", cd.l6.dmaL6Addr, cd.l6.dmaL66Addr, w);
      FTRC (cd.l6.dmaL66Addr, w);
      CS_SEND_WRITE (cd.l6.dmaL66Addr, w);
      cd.l6.dmaTally --;
      cd.l6.dmaL6Addr += 2;
      cd.l6.dmaL66Addr ++;
      return;
      
    } // cstateL6StartXferToCS

    //
    // L66
    //

    // L66 connect; send for PCW
    case cstateL66Connect: {
      cd.DMADataRecvd = false;
      CS_SEND_READ_CLEAR (cpu.mbxAddr);
//sim_printf ("l66 read and clear %o\n", cpu.mbxAddr);
      GOTO (cstateL66WaitPCW);
    } // cstateL66Connect

    // L66 connect; wait for PCW
    case cstateL66WaitPCW: {
      if (! cd.DMADataRecvd)
        return;
      if (cd.DMADataAddr != cpu.mbxAddr) {
        sim_printf ("PCW addr expected %o, got %o\n", cpu.mbxAddr, cd.DMADataAddr);
      }
      cd.l66.PCW = cd.DMAData;
      cd.DMADataRecvd = false;
      word6 cmd = (word6) getbits36 (cd.l66.PCW, 30, 6);
      word24 l66Addr = (word24) getbits36 (cd.l66.PCW, 0, 18);
//sim_printf ("L66 connect PCW %012lo cmd %02o l66Addr %08o\n", cd.l66.PCW, cmd, l66Addr);
      switch (cmd) {

        case 071: { // 71 interrupt L6
          //DD01, pg 5.18;  71 "INTERRUPT DATANET FNP (on level specified by SPECIAL INT. switch)"
          diaStatus (000001, 0000000);
          word6 sublevel = (word6) getbits36 (cd.l66.PCW, 24, 6);
//#ifdef TRACE1
//sim_printf ("71 interrupt L6 %012lo sublevel %02o\n", cd.l66.PCW, sublevel);
//#endif
// Verified correct; dn355 trace seen to go thorugh mailbox jumptable to ivp.
          doIntr (cpu.intrCellSwitches, sublevel, "coupler 71 interrupt L6");
          CS_SEND_DISC ();
          GOTO (cstateIdle);
        }

        case 072: //  72 boot
#ifdef TRACE1
sim_printf ("72 boot\n");
#endif
          cd.booting = true;
          cd.l66.dmaL66Addr = l66Addr;

          // Get the words with the L6 address of the bootload code (LICW)
          cd.DMADataRecvd = false;
          CS_SEND_READ (cd.l66.dmaL66Addr);
          GOTO (cstateL66WaitLICW);

#if 0
        case 073:
          sim_printf ("73 interrupt L66\n");
          HDBGNote0 (__func__, "73 interrupt L66");
          GOTO (cstateIdle);
#endif

        case 075:
          cd.l66.dmaL66Addr = l66Addr;
#ifdef TRACE1
sim_printf ("75 TEST DATA xfer from L6 to L66 TICW @ %08o\n", cd.l66.dmaL66Addr);
#endif
          HDBGNote0 (__func__, "75 TEST DATA xfer from L6 to L66");
          // Get the word with the L6 address and tallu (TEST ICW)
          cd.DMADataRecvd = false;
          CS_SEND_READ (cd.l66.dmaL66Addr);
          GOTO (cstateL66WaitTICW);

#if 0
        case 076:
          sim_printf ("76 xfer from L66 to L6\n");
          HDBGNote0 (__func__, "76 xfer from L66 to L6");
          xferFromL66ToL6 (l66Addr);
          // Completion routine will set complete...
          return;
#endif
        default:
          sim_printf ("unknown %o\n", cmd);
          HDBGNote (__func__, "unknown cmd %o %012lo", cmd, cd.l66.PCW);
          GOTO (cstateIdle);
      } // switch (cmd)
    } // cstateL66WaitPCW

    // Waiting for the L66 connect PCW boot cmd LICW.
    case cstateL66WaitLICW: {
      if (! cd.DMADataRecvd)
        return;
      if (cd.DMADataAddr != cd.l66.dmaL66Addr) {
        sim_printf ("LICW addr expected %o, got %o\n", cpu.mbxAddr, cd.l66.dmaL66Addr);
      }
      cd.l66.LICW = cd.DMAData;
      cd.DMADataRecvd = false;

      // GICB is located after the LICW
      cd.l66.dmaL66Addr ++;
      word18 l6Addr = (word18) getbits36 (cd.l66.LICW, 0, 18);
      word12 tally = (word12) getbits36 (cd.l66.LICW, 24, 12);
      cd.l66.dmaL6Addr = l6Addr;
      cd.l66.dmaTally = tally == 0 ? 4096 : tally;
      GOTO (cstateL66StartXferFromCS);
    } // cstateL66WaitLICW

    // Waiting for the L66 connect PCW Test Data Xfer to L66 TEST ICW.
    case cstateL66WaitTICW: {
      if (! cd.DMADataRecvd)
        return;
      if (cd.DMADataAddr != cd.l66.dmaL66Addr) {
        sim_printf ("TICW addr expected %o, got %o\n", cpu.mbxAddr, cd.l66.dmaL66Addr);
      }
      cd.l66.LICW = cd.DMAData;
      cd.DMADataRecvd = false;

      // Test data is written after the TICW (maybe)
      cd.l66.dmaL66Addr ++;
      word18 l6Addr = (word18) getbits36 (cd.l66.LICW, 0, 18);
      word12 tally = (word12) getbits36 (cd.l66.LICW, 24, 12);
      cd.l66.dmaL6Addr = l6Addr;
      cd.l66.dmaTally = tally == 0 ? 4096 : tally;
//sim_printf ("TICW %012lo L66 %08o L6 %06o Tally %d\n", cd.l66.LICW, cd.l66.dmaL66Addr, cd.l66.dmaL6Addr, cd.l66.dmaTally);
      GOTO (cstateL66StartXferToCS);
    } // cstateL66WaitTICW

    case cstateL66StartXferFromCS: {
      if  (cd.l66.dmaTally <= 0) {
        if (cd.booting) {

          //word36 bootloadStatus = 0;
          //putbits36_1 (& bootloadStatus, 0, 1); // real_status = 1
          //putbits36_3 (& bootloadStatus, 3, 0); // major_status = BOOTLOAD_OK;
          //putbits36_8 (& bootloadStatus, 9, 0); // substatus = BOOTLOAD_OK;
          //putbits36_17 (& bootloadStatus, 17, 0); // channel_no = 0;
          //word36 bootloadStatus = 0400000000000l;
// Causes premature unwiring
#if 0
          CS_SEND_WRITE (cpu.mbxAddr + 6, bootloadStatus);

          CS_SEND_DISC ();
#endif
          // dd01 pg 177
          cd.booting = false;
          cpu.booted = true;
          // dd01 opg 5-78 102 ICA terminate
          doIntr (2, COUPLER_CHANNEL_NO, "coupler cstateL66StartXferFromCS done");
//          doIntr (2, 4, "coupler cstateL66StartXferFromCS done");
        } else {
          CS_SEND_DISC ();
        }
        GOTO (cstateIdle);
      } // tally <= 0

      // DMA read word from L66
      cd.DMADataRecvd = false;
      CS_SEND_READ (cd.l66.dmaL66Addr);
      GOTO (cstateL66WaitXferFromCS);
    } // cstateL66StartXferFromCS

    case cstateL66WaitXferFromCS: {
      if (! cd.DMADataRecvd)
        return;
      if (cd.DMADataAddr != cd.l66.dmaL66Addr) {
        sim_printf ("PCW addr expected %o, got %o\n", cpu.mbxAddr, cd.l66.dmaL66Addr);
      }
if (0) {
  if (cd.l66.dmaL6Addr % 010 == 0)
    sim_debug (DBG_DMA, & couplerDev, "DMA* %05o\n", cd.l66.dmaL6Addr);
  word18 w0 = (cd.DMAData >> 18) & BITS18;
  word18 w1 = (cd.DMAData >>  0) & BITS18;
  sim_printf ("DMA\t%05o\toctal\t%06o\t\t\" %s\n", cd.l66.dmaL6Addr, w0, disassemble ((word15) cd.l66.dmaL6Addr, w0));
  sim_printf ("DMA\t%05o\toctal\t%06o\t\t\" %s\n", cd.l66.dmaL6Addr, w1, disassemble ((word15) cd.l66.dmaL6Addr + 1, w1));
  //sim_debug (DBG_DMA, & couplerDev, "DMA\toctal\t%06o\t\t\" %s\n", w0, disassemble (cd.l66.dmaL6Addr, w0));
  //sim_debug (DBG_DMA, & couplerDev, "DMA\toctal\t%06o\t\t\" %s\n", w1, disassemble (cd.l66.dmaL6Addr + 1, w1));
  //sim_debug (DBG_DMA, & couplerDev, "DMA\toctal\t%06o\t%06o\n", w0, w1);
}
//sim_printf ("from L66 %08o:%012lo L6 %05o\n", cd.l66.dmaL66Addr, cd.DMAData, cd.l66.dmaL6Addr);
      word15 l6Addr = cd.l66.dmaL6Addr & BITS15;
      coreWriteDouble (l6Addr, cd.DMAData, __func__);

      // Update dma registers
      cd.l66.dmaL66Addr ++;
      cd.l66.dmaL6Addr += 2;
      cd.l66.dmaTally --;
      
      GOTO (cstateL66StartXferFromCS);
    } // cstateL66WaitXferFromCS

    case cstateL66StartXferToCS: {
      if  (cd.l66.dmaTally <= 0) {
        CS_SEND_DISC ();
        GOTO (cstateIdle);
      } // tally <= 0

      // DMA write word to L66
      word36 w = coreReadDouble (cd.l66.dmaL6Addr, "cstateL66StartXferToCS");
//sim_printf ("write to cs l6 %o l66 %o data %012lo\n", cd.l66.dmaL6Addr, cd.l66.dmaL66Addr, w);
      FTRC (cd.l66.dmaL66Addr, w);
      CS_SEND_WRITE (cd.l66.dmaL66Addr, w);
      cd.l66.dmaTally --;
      cd.l66.dmaL6Addr += 2;
      cd.l66.dmaL66Addr ++;
      return;
      
    } // cstateL66StartXferToCS

  } // switch (cd.state)
}

