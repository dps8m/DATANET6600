include Makefile.mk

#CFLAGS += -I./simh_7ad57d7
CFLAGS += -O0 -g
#CFLAGS +=  -Wall -Wconversion -Wsign-conversion -Wno-unknown-warning-option
CFLAGS +=  -Wall
CFLAGS += -DHDBG
#CFLAGS += -DPKT_TRACE
#CFLAGS += -DPKT_TRACE2
LDFLAGS += -ldl -luv -lm

C_SRCS = dn355.c cpu.c coupler.c caf.c mem.c utils.c iom.c console.c timer.c core.c pager.c hsla.c prt.c uvutil.c libtelnet.c hdbg.c main.c sim_timer.c sim_console.c sim_tmxr.c sim_sock.c sim_fio.c linehistory.c sim_disk.c sim_tape.c coupler_tcp.c dialin.c
H_SRCS = coupler.h  dn355.h  cpu.h ipc.h caf.h mem.h utils.h iom.h console.h timer.h core.h pager.h hsla.h prt.h uvutil.h libtelnet.h hdbg.h simh.h sim_disk.h sim_tape.h sim_sock.h linehistory.h sysdefs.h sim_tmxr.h sim_defs.h coupler_tcp.h dialin.h

OBJS  := $(patsubst %.c,%.o,$(C_SRCS))

all : dn355 boot annotate dvftest

dn355 : tags $(OBJS) 
	$(LD) $(LDFLAGS) $(OBJS) -o dn355

test : test.o 
	$(LD) $(LDFLAGS) test.o -o test

boot : boot.o
	$(LD) $(LDFLAGS) boot.o -o boot 
	
annotate : annotate.o
	$(LD) $(LDFLAGS) annotate.o -o annotate 
	
dvftest : dvftest.o
	$(LD) $(LDFLAGS) dvftest.o -o dvftest 

tags : $(C_SRCS) $(H_SRCS)
	-ctags $(C_SRCS) $(H_SRCS)


clean:
	-rm dn355 $(OBJS) tags $(C_SRCS:.c=.d) $(wildcard $(C_SRCS:.c=.d.[0-9]*)) test.o test

%.d: %.c
	@set -e; rm -f $@; \
	$(CC) -MM $(CFLAGS) $< > $@.$$$$; \
	grep -v dps8.sha1.txt $@.$$$$ | sed 's,\($*\)\.o[ :]*,\1.o $@ : ,g' > $@; \
	rm -f $@.$$$$

-include $(C_SRCS:.c=.d)

