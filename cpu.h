extern cpu_t cpu;
extern DEVICE cpuDev;
extern jmp_buf jmpMain;

void cpuInit (void);

void brkbrk (void);
void doFault (word15 faultAddr, const char * msg) NO_RETURN;
void doIntr (uint level, uint sublevel, const char * ctx);
char * disassemble (word15 addr, word18 ins);

