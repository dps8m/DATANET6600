#include <stdint.h>
#include <stdbool.h>
#include <setjmp.h>
#include <inttypes.h>
#include <unistd.h>
#include <ctype.h>
#include <uv.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>


#include "sim_defs.h"

//#define WALL
//#define REMOTE_CONSOLE

// Debug

#define DBG_HSLA(x) 
//#define DBG_HSLA(x) x

#define DBG_DIA(x) 
//#define DBG_DIA(x) x

#define DBG_INTR(x) 
//#define DBG_INTR(x) x

#define DBG_PRTR(x) 
//#define DBG_PRTR(x) x

//#define FAKE_TRACE

#ifdef __GNUC__
#define NO_RETURN   __attribute__ ((noreturn))
#define UNUSED      __attribute__ ((unused))
#else
#define NO_RETURN
#define UNUSED
#endif

#define FREE(p) do  \
  {                 \
    free((p));      \
    (p) = NULL;     \
  } while(0)

typedef uint32_t uint;

typedef uint8_t word1;
typedef uint8_t word2;
typedef uint8_t word3;
typedef uint8_t word4;
typedef uint8_t word5;
typedef uint8_t word6;
typedef uint8_t word8;
typedef uint16_t word9;
typedef uint16_t word12;
typedef uint16_t word14;
typedef uint16_t word15;
typedef uint16_t word16;
typedef uint32_t word17;
typedef uint32_t word18;
typedef uint32_t word20;
typedef uint32_t word24;
typedef uint64_t word36;
typedef uint64_t word37;
typedef uint64_t word38;

#define BITS0    ((word18)0400000)
#define BITS2   ((word2) 03)
#define BITS3   ((word3) 07)
#define BITS6   ((word6) 077)
#define BITS9   ((word9) 0777)
#define BITS8   ((word9) 0377)
#define BITS12  ((word12)07777)
#define BITS15  ((word15)077777)
#define BITS16  ((word16)0177777)
#define BITS17  ((word17)0377777)
#define BITS18  ((word18)0777777)
#define BITS34  ((word36)0177777777777l)
#define BITS36  ((word36)0777777777777l)

#define MASK6 ((word9) 077)
#define MASK9 ((word9) 0777)
#define MASK15 ((word15) 077777)
#define MASK18 ((word18) 0777777)

#define SIGN6            040        // represents sign bit of a 9-bit 2-comp number
#define SGNX6_15   077740        // sign extend a 6-bit number to 18-bits
#define SIGNEXT6_15(x)    (((x) & SIGN6) ? ((x) | SGNX9_15) : (x))

#define SIGN9           0400        // represents sign bit of a 9-bit 2-comp number
#define SGNX9_18   0777400        // sign extend a 9-bit number to 18-bits
#define SIGNEXT9_18(x)    (((x) & SIGN9) ? ((x) | SGNX9_18) : (x))
#define SGNX9_15   077400        // sign extend a 9-bit number to 18-bits
#define SIGNEXT9_15(x)    (((x) & SIGN9) ? ((x) | SGNX9_15) : (x))

#define SIGN12         04000        // represents sign bit of a 12-bit 2-comp number
#define SGNX12  0774000        // sign extend a 12-bit number to 18-bits
#define SIGNEXT12(x)    (((x) & SIGN12) ? ((x) | SGNX12) : (x))

//#define SIGN14        020000        // represents sign bit of a 14-bit 2-comp number
//#define SGNX14  0760000        // sign extend a 14-bit number to 18-bits
//#define SIGNEXT14(x)    (((x) & SIGN14) ? ((x) | SGNX14) : (x))

#define SIGN15        040000        // represents sign bit of a 15-bit 2-comp number
#define SGNX15  0740000        // sign extend a 15-bit number to 18-bits
#define SIGNEXT15(x)    (((x) & SIGN15) ? ((x) | SGNX15) : (x))

#define SIGN18       0400000        // represents sign bit of a 18-bit 2-comp number
#define SGNX18  037777400000        // sign extend a 18-bit number to 32-bits
#define SIGNEXT18(x)    ((int32_t) (((x) & SIGN18) ? ((x) | SGNX18) : (x)))

#define _I(x)    (((x) & BITS0) ? true : false)   // extract indirect bit
#define _T(x)    (((x) >> 15) & BITS2)           // extract T field
#define _C(x)    (((x) >> 15) & BITS3)           // extract C (char address) from 18-bit word
#define _W(x)     ((x) & BITS15)                 // extract W (word address) from 18-bit word

// Assemble 3 bit character address and 15 bit word address into 18 bit address
#define PACKCW(c, w) ((word18) ((((w) & BITS15) | (((c) & BITS3) << 15))))
#define UNPACKC(cw) ((word3)(((cw) >> 15) & BITS3))
#define UNPACKW(cw) ((word15)((cw) & BITS15))
#define SETF(flags, x)         flags = ((flags) |  (x))
#define CLRF(flags, x)         flags = ((flags) & ~(x))
#define TSTF(flags, x)         ((flags) & (x))
#define SCF(cond, flags, x)    { if (cond) SETF((flags), x); else CLRF((flags), x); }

#define I_ZERO  (1u << 17)  // 400000
#define I_NEG   (1u << 16)  // 200000
#define I_CARRY (1u << 15)  // 100000
#define I_OVF   (1u << 14)  // 040000
#define I_II    (1u << 13)  // 020000
#define I_PFI   (1u << 12)  // 010000
#define I_OFI   (1u << 11)  // 004000
#define I_PE    (1u << 10)  // 002000

// Word size
enum { WSZ = 18 };

// Address size
enum { ASZ = 15 };

// Memory size
//enum { MEM_SIZE = 1 << 15 }; // 32K
//enum { MEM_SIZE = 1 << 16 }; // 64K
enum { MEM_SIZE = 1 << 17 }; // 128K
//enum { MEM_SIZE = 1 << 18 }; // 256K

typedef struct
{
    word18 M[MEM_SIZE];      // Our memory store. 32K x 18. Oooh, that's at least 589,806 cores (not incl parity/ecc)
    
    word18  rIR;            // indicator register
    
    word18  rA;             // accumulator A
    word18  rQ;             // accumulator Q
    word15  rIC;            // 15-bit instruction counter
    word18  rX1;            // index register X1
    word18  rX2;            // index register X2
    word18  rX3;            // index register X3
    word1   rII;            // interrupt inhibit
    word16  rIE;            // interrupt level enable (yes, 16. not a typo)

#define N_IOM_CHANNELS 64

#define N_HSLAS 3
#define N_HSLAS_CONFIGURED 1
#define N_HSLA_LINES 32

#define HSLA1_CHANNEL 6
#define HSLA2_CHANNEL 7
#define HSLA3_CHANNEL 8

// Interrupt cells

#define N_INTR_CELLS 16
#define N_INTR_SUBLEVELS 16
    bool cells [N_INTR_CELLS];
    bool intrPresent;

    
// Delayed faults

    bool dlyFault;
    word15 dlyFaultAddr;
    const char * dlyCtx;

// Instruction decoder workspace

    word6  OPCODE;
    word1  I;
    word2  T;
    word9  D;
    word18 Dchaddr;  // grp1 D expanded into character address
    word3  S1;
    word3  S2;
    word6  K;
    word15 W;
    word3  C;
    word18 Y;
    word36 YY;
    word15 NEXT_IC;


// CAF results

// Interupt processing

    bool inhIntrChk;

// panel switches

    word3 portALogicalPort;
    word1 portAInterlace;
    word1 portAEnable;
    word1 portAInitEnable;
    word3 portAMemSize;
    word3 portBLogicalPort;
    word1 portBInterlace;
    word1 portBEnable;
    word1 portBInitEnable;
    word3 portBMemSize;
    word3 portCLogicalPort;
    word1 portCInterlace;
    word1 portCEnable;
    word1 portCInitEnable;
    word3 portCMemSize;
    word3 portDLogicalPort;
    word1 portDInterlace;
    word1 portDEnable;
    word1 portDInitEnable;
    word3 portDMemSize;
    word24 mbxAddr;
    word5 intrCellSwitches;
    word5 emerCellSwitches;
    word9 lowerBound;
    word9 upperBound;
    word1 enableMemoryTimer;
    word1 enableAddressBounds;
    word1 CSWriteInhibit;
    word1 fast;
    word1 ovfEnable;

//
    bool disState;

// Used to keep the CPU in DIS state dispite timer interrupts.
    bool booted;

    uint64_t cycleCnt;

    uint diagh;

    bool diag;

} cpu_t;

extern cpu_t cpu;

// Memory locations
//
//    0 -  377   IOM interrupt vectors
//  400 -  417   Program interrupt cells
//  420 -  437   IOM fault status
//  440 -  447   Processor fault vectors
//  450 -  777   IOM MBX communication region
// 1000 - 1777   HSLA1 MBX, if enabled
// 2000 - 2777   HSLA1 MBX, if enabled
// 3000 - 3777   HSLA1 MBX, if enabled


// IOM interrupt vectors 0 - 

#define ADDR_IOM_INT_VECTOR_BASE            000000

// 0000 - 0017 Channel  0, 20, 40, 60
// 0020 - 0037 Channel  1 
// 0040 - 0057 Channel  2 
// 0060 - 0077 Channel  3 

// 0100 - 0117 Channel  4 
// 0120 - 0137 Channel  5 
// 0140 - 0157 Channel  6 
// 0160 - 0177 Channel  7 

// 0200 - 0217 Channel 10 
// 0220 - 0237 Channel 11 
// 0240 - 0257 Channel 12 
// 0260 - 0277 Channel 13 

// 0300 - 0317 Channel 14 
// 0320 - 0337 Channel 15 
// 0340 - 0357 Channel 16 
// 0360 - 0377 Channel 17 


// 0000x

#define ADDR_IOM_CONSOLE_FAULT              000000
#define ADDR_IOM_CONSOLE_REQ                000001
#define ADDR_IOM_CONSOLE_TERMINATE          000002
#define ADDR_IOM_DIA_SPECIAL00              000003
#define ADDR_IOM_HSLA1S00_ACTIVE_TERMINATE  000004
#define ADDR_IOM_HSLA1S16_ACTIVE_TERMINATE  000005
#define ADDR_IOM_HSLA1S00_CONFIG_TERMINATE  000006
#define ADDR_IOM_HSLA1S16_CONFIG_TERMINATE  000007

// 0001x

// 0002x

#define ADDR_IOM_DIA_SPECIAL01              000023
#define ADDR_IOM_HSLA1S01_ACTIVE_TERMINATE  000024
#define ADDR_IOM_HSLA1S17_ACTIVE_TERMINATE  000025
#define ADDR_IOM_HSLA1S01_CONFIG_TERMINATE  000026
#define ADDR_IOM_HSLA1S17_CONFIG_TERMINATE  000027

// 0003x

// 0004x

#define ADDR_IOM_PRINTER_FAULT              000040
#define ADDR_IOM_PRINTER_ATTENTION          000041
#define ADDR_IOM_PRINTER_TERMINATE          000042
#define ADDR_IOM_DIA_SPECIAL02              000043
#define ADDR_IOM_HSLA1S02_ACTIVE_TERMINATE  000044
#define ADDR_IOM_HSLA1S18_ACTIVE_TERMINATE  000045
#define ADDR_IOM_HSLA1S02_CONFIG_TERMINATE  000046
#define ADDR_IOM_HSLA1S18_CONFIG_TERMINATE  000047

// 0005x

// 0006x

#define ADDR_IOM_DIA_SPECIAL03              000063
#define ADDR_IOM_HSLA1S03_ACTIVE_TERMINATE  000064
#define ADDR_IOM_HSLA1S19_ACTIVE_TERMINATE  000065
#define ADDR_IOM_HSLA1S03_CONFIG_TERMINATE  000066
#define ADDR_IOM_HSLA1S19_CONFIG_TERMINATE  000067

// 0007x

// 0010x

// 100   100 0000 Sublevel 4 Level 0

#define ADDR_IOM_DIA_SPECIAL04              000103
#define ADDR_IOM_HSLA1S04_ACTIVE_TERMINATE  000104
#define ADDR_IOM_HSLA1S20_ACTIVE_TERMINATE  000105
#define ADDR_IOM_HSLA1S04_CONFIG_TERMINATE  000106
#define ADDR_IOM_HSLA1S20_CONFIG_TERMINATE  000107

// 0011x

// 0012x

#define ADDR_IOM_DIA_SPECIAL05              000123
#define ADDR_IOM_HSLA1S05_ACTIVE_TERMINATE  000124
#define ADDR_IOM_HSLA1S21_ACTIVE_TERMINATE  000125
#define ADDR_IOM_HSLA1S05_CONFIG_TERMINATE  000126
#define ADDR_IOM_HSLA1S21_CONFIG_TERMINATE  000127

// 0013x

// 0014x

#define ADDR_IOM_HSLA1_FAULT                000140
#define ADDR_IOM_DIA_SPECIAL06              000143
#define ADDR_IOM_HSLA1S06_ACTIVE_TERMINATE  000144
#define ADDR_IOM_HSLA1S22_ACTIVE_TERMINATE  000145
#define ADDR_IOM_HSLA1S06_CONFIG_TERMINATE  000146
#define ADDR_IOM_HSLA1S22_CONFIG_TERMINATE  000147

// 0015x

// 0016x

#define ADDR_IOM_DIA_SPECIAL07              000163
#define ADDR_IOM_HSLA1S07_ACTIVE_TERMINATE  000164
#define ADDR_IOM_HSLA1S23_ACTIVE_TERMINATE  000165
#define ADDR_IOM_HSLA1S07_CONFIG_TERMINATE  000166
#define ADDR_IOM_HSLA1S23_CONFIG_TERMINATE  000167

// 0017x

// 0020x

#define ADDR_IOM_DIA_SPECIAL08              000203
#define ADDR_IOM_HSLA1S08_ACTIVE_TERMINATE  000204
#define ADDR_IOM_HSLA1S24_ACTIVE_TERMINATE  000205
#define ADDR_IOM_HSLA1S08_CONFIG_TERMINATE  000206
#define ADDR_IOM_HSLA1S24_CONFIG_TERMINATE  000207

// 0021x

// 0022x

#define ADDR_LSLA1_FAULT                    000220
#define ADDR_LSLA1_ACTIVE_TERMINATE         000221
#define ADDR_LSLA1_CONFIG_TERMINATE         000222
#define ADDR_IOM_DIA_SPECIAL09              000223
#define ADDR_IOM_HSLA1S09_ACTIVE_TERMINATE  000224
#define ADDR_IOM_HSLA1S25_ACTIVE_TERMINATE  000225
#define ADDR_IOM_HSLA1S09_CONFIG_TERMINATE  000226
#define ADDR_IOM_HSLA1S25_CONFIG_TERMINATE  000227

// 0023x

// 0024x

#define ADDR_LSLA2_FAULT                    000240
#define ADDR_LSLA2_ACTIVE_TERMINATE         000241
#define ADDR_LSLA2_CONFIG_TERMINATE         000242
#define ADDR_IOM_DIA_SPECIAL10              000243
#define ADDR_IOM_HSLA1S10_ACTIVE_TERMINATE  000244
#define ADDR_IOM_HSLA1S26_ACTIVE_TERMINATE  000245
#define ADDR_IOM_HSLA1S10_CONFIG_TERMINATE  000246
#define ADDR_IOM_HSLA1S26_CONFIG_TERMINATE  000247

// 0025x

// 0026x

#define ADDR_LSLA3_FAULT                    000260
#define ADDR_LSLA3_ACTIVE_TERMINATE         000261
#define ADDR_LSLA3_CONFIG_TERMINATE         000262
#define ADDR_IOM_DIA_SPECIAL11              000263
#define ADDR_IOM_HSLA1S11_ACTIVE_TERMINATE  000264
#define ADDR_IOM_HSLA1S27_ACTIVE_TERMINATE  000265
#define ADDR_IOM_HSLA1S11_CONFIG_TERMINATE  000266
#define ADDR_IOM_HSLA1S27_CONFIG_TERMINATE  000267

// 0027x

// 0030x

#define ADDR_LSLA4_FAULT                    000300
#define ADDR_LSLA4_ACTIVE_TERMINATE         000301
#define ADDR_LSLA4_CONFIG_TERMINATE         000302
#define ADDR_IOM_DIA_SPECIAL12              000303
#define ADDR_IOM_HSLA1S12_ACTIVE_TERMINATE  000304
#define ADDR_IOM_HSLA1S28_ACTIVE_TERMINATE  000305
#define ADDR_IOM_HSLA1S12_CONFIG_TERMINATE  000306
#define ADDR_IOM_HSLA1S28_CONFIG_TERMINATE  000307

// 0031x

// 0032x

#define ADDR_LSLA5_FAULT                    000320
#define ADDR_LSLA5_ACTIVE_TERMINATE         000321
#define ADDR_LSLA5_CONFIG_TERMINATE         000322
#define ADDR_IOM_DIA_SPECIAL13              000323
#define ADDR_IOM_HSLA1S13_ACTIVE_TERMINATE  000324
#define ADDR_IOM_HSLA1S29_ACTIVE_TERMINATE  000325
#define ADDR_IOM_HSLA1S13_CONFIG_TERMINATE  000326
#define ADDR_IOM_HSLA1S29_CONFIG_TERMINATE  000327

// 0033x

// 0034x

#define ADDR_LSLA6_FAULT                    000340
#define ADDR_LSLA6_ACTIVE_TERMINATE         000341
#define ADDR_LSLA6_CONFIG_TERMINATE         000342
#define ADDR_IOM_DIA_SPECIAL14              000343
#define ADDR_IOM_HSLA1S14_ACTIVE_TERMINATE  000344
#define ADDR_IOM_HSLA1S30_ACTIVE_TERMINATE  000345
#define ADDR_IOM_HSLA1S14_CONFIG_TERMINATE  000346
#define ADDR_IOM_HSLA1S30_CONFIG_TERMINATE  000347

// 0035x

// 0036x

#define ADDR_TIMER_FAULT                    000360
#define ADDR_TIMER_RUNOUT                   000361
#define ADDR_TIMER_ROLLOVER                 000362
#define ADDR_IOM_DIA_SPECIAL15              000363
#define ADDR_IOM_HSLA1S15_ACTIVE_TERMINATE  000364
#define ADDR_IOM_HSLA1S31_ACTIVE_TERMINATE  000365
#define ADDR_IOM_HSLA1S15_CONFIG_TERMINATE  000366
#define ADDR_IOM_HSLA1S31_CONFIG_TERMINATE  000367

// 0037x

// Program interrupt cells 400-417

#define ADDR_PROG_INT_CELL_BASE             000400
#define ADDR_PROG_INT_LEVEL_00              000400
#define ADDR_PROG_COMMON_PERIPH_FAULT       000400
#define ADDR_PROG_INT_LEVEL_01              000401
#define ADDR_PROG_COMMON_PERIPH_ATTN        000401
#define ADDR_PROG_INT_LEVEL_02              000402
#define ADDR_PROG_COMMON_PERIPH_TERMINATE   000402
#define ADDR_PROG_INT_LEVEL_03              000403
#define ADDR_PROG_DIA_SPECIAL               000403
#define ADDR_PROG_INT_LEVEL_04              000404
#define ADDR_PROG_HSLA1_00_15_ACTIVE        000404
#define ADDR_PROG_INT_LEVEL_05              000405
#define ADDR_PROG_HSLA1_16_31_ACTIVE        000405
#define ADDR_PROG_INT_LEVEL_06              000406
#define ADDR_PROG_HSLA1_00_15_CONFIG        000407
#define ADDR_PROG_INT_LEVEL_07              000407
#define ADDR_PROG_HSLA1_16_31_CONFIG        000407
#define ADDR_PROG_INT_LEVEL_08              000410
#define ADDR_PROG_HSLA2_00_15_ACTIVE        000410
#define ADDR_PROG_INT_LEVEL_09              000411
#define ADDR_PROG_HSLA2_16_31_ACTIVE        000411
#define ADDR_PROG_INT_LEVEL_10              000412
#define ADDR_PROG_HSLA2_00_15_CONFIG        000412
#define ADDR_PROG_INT_LEVEL_11              000413
#define ADDR_PROG_HSLA2_16_31_CONFIG        000413
#define ADDR_PROG_INT_LEVEL_12              000414
#define ADDR_PROG_HSLA3_00_15_ACTIVE        000414
#define ADDR_PROG_INT_LEVEL_13              000415
#define ADDR_PROG_HSLA3_16_31_ACTIVE        000415
#define ADDR_PROG_INT_LEVEL_14              000416
#define ADDR_PROG_HSLA3_00_15_CONFIG        000416
#define ADDR_PROG_INT_LEVEL_15              000417
#define ADDR_PROG_HSLA3_16_31_CONFIG        000417

// IOM fault status 420-437

#define ADDR_IOM_FAULT_STATUS_BASE          000420
#define ADDR_IOM_FAULT_STATUS_CONSOLE       000420
#define ADDR_IOM_FAULT_STATUS_PRINTER       000422
#define ADDR_IOM_FAULT_STATUS_DIA           000424
#define ADDR_IOM_FAULT_STATUS_HSLA1         000426
#define ADDR_IOM_FAULT_STATUS_HSLA2         000427
#define ADDR_IOM_FAULT_STATUS_HSLA3         000430
#define ADDR_IOM_FAULT_STATUS_LSLA1         000431
#define ADDR_IOM_FAULT_STATUS_LSLA2         000432
#define ADDR_IOM_FAULT_STATUS_LSLA3         000433
#define ADDR_IOM_FAULT_STATUS_LSLA4         000434
#define ADDR_IOM_FAULT_STATUS_LSLA5         000435

// Processor fault vectors 440-447

#define ADDR_PROC_FAULT_VECTOR_BASE         000440
#define ADDR_FAULT_POWER_SHUTDOWN_BEGINNING 000440
#define ADDR_FAULT_RESTART                  000441
#define ADDR_FAULT_PARITY                   000442
#define ADDR_FAULT_ILLEGAL_OPCODE           000443
#define ADDR_FAULT_OVERFLOW                 000444
#define ADDR_FAULT_ILLEGAL_STORE            000445
#define ADDR_FAULT_DIVIDE_CHECK             000446
#define ADDR_FAULT_ILLEGAL_PROGRAM_INT      000447

// IOM MBX communication region 450-1777

#define ADDR_IOM_MBX_BASE                   000450
#define ADDR_IOM_MBX_INTERVAL_TIMER         000450
#define ADDR_IOM_MBX_ELAPSED_TIMER          000451

#define ADDR_IOM_MBX_DIA_PCW1               000454
#define ADDR_IOM_MBX_DIA_PCW2               000455

#define ADDR_IOM_MBX_DIA_STATUS_ICW         000456

#define ADDR_IOM_MBX_CONSOLE_STATUS_ICW     000460

#define ADDR_IOM_MBX_CONSOLE_DATA_ICW       000462

#define ADDR_IOM_MBX_PRINTER_STATUS_ICW     000470

#define ADDR_IOM_MBX_PRINTER_DATA_ICW       000472

#define ADDR_IOM_MBX_PAGE_TABLE_PTR         000475

#define ADDR_IOM_MBX_MEMORY_YELLOW_CTR      000476

#define ADDR_IOM_MBX_LSLA1_BASE             000500

#define ADDR_IOM_MBX_LSLA2_BASE             000520

#define ADDR_IOM_MBX_LSLA3_BASE             000540

#define ADDR_IOM_MBX_LSLA4_BASE             000560

#define ADDR_IOM_MBX_LSLA5_BASE             000600

#define ADDR_IOM_MBX_LSLA6_BASE             000620

// MCS system communitcation region 640-760

#define LEN_IOM_MBX_HSLA_SUBCHNL            020
#define N_IOM_MBX_HSLA_SUBCHNLS             32
#define LEN_IOM_MBX_HSLA                    (LEN_IOM_MBX_HSLA_SUBCHNL * N_IOM_MBX_HSLA_SUBCHNLS)

#define ADDR_IOM_MBX_HSLA1_BASE             001000

#define ADDR_IOM_MBX_HSLA2_BASE             002000

#define ADDR_IOM_MBX_HSLA3_BASE             003000


// FAULTS

// DC88, pg 32:
//  "Two faults, storaage parity error and overflow, can be inhibited by
//   the program be setting the appropriate bit in the indicator register
//   (using the LDI instruction). If one of these two faults occurs, the indi-
//   cator associated iwth the fault is turned on and no transfer to a fault
//   processing routine is accomplished.

// Fault vector memory locations
enum 
  {
    faultPowerShutdownBeginning = ADDR_FAULT_POWER_SHUTDOWN_BEGINNING,
    faultRestart =                ADDR_FAULT_RESTART,
    faultParity =                 ADDR_FAULT_PARITY,
    faultIllegalOpcode =          ADDR_FAULT_ILLEGAL_OPCODE,
    faultOverflow =               ADDR_FAULT_OVERFLOW,
    faultIllegalStore =           ADDR_FAULT_ILLEGAL_STORE,
    faultDivideCheck =            ADDR_FAULT_DIVIDE_CHECK,
    faultIllegalProgramInt =      ADDR_FAULT_ILLEGAL_PROGRAM_INT
  };

#define DBG_DEBUG       (1U << 0)
#define DBG_TRACE       (1U << 1)
#define DBG_REG         (1U << 2)
#define DBG_FINAL       (1U << 3)
#define DBG_CAF         (1U << 4)
#define DBG_SRC         (1U << 5)
#define DBG_DMA         (1U << 6)
#define DBG_IND         (1U << 7)

#define STOP_STOP   1

// JMP_ENTRY must be 0, which is the return value of the setjmp initial
// entry
#define JMP_ENTRY       0
#define JMP_REENTRY     1
#define JMP_STOP        2

// DATANET FNP GENERAL MEMORY MAP
//
//  00000 00377 Interrupt vectors
//  00400 00417 Interrupt cells
//  00420 00436 IOM Fault status
//  00440 00447 Processor fault vectors
//  00450 00777 I/O Comm. Region
//  01000 01777 HLSA #1 I/O Comm. Region
//  02000 02777 #2
//  03000 03777 #3
//  04000 77777 Program area



static inline word18 getbits18 (word18 x, uint i, uint n)
  {
    // bit 17 is right end, bit zero is 18th from the right
    int shift = 17 - (int) i - (int) n + 1;
    if (shift < 0 || shift > 17)
      {
        sim_printf ("getbits18: bad args (%06o,i=%d,n=%d)\n", x, i, n);
        return 0;
      }
     else
      return (x >> (unsigned) shift) & ~ (~0U << n);
  }

#define MASKBITS(x) (~(~((uint)0) << x))   // lower (x) bits all ones

static inline word18 setbits18 (word18 x, uint p, uint n, word18 val)
  {
    int shift = 18 - (int) p - (int) n;
    if (shift < 0 || shift > 17)
      {
        sim_printf ("setbits18: bad args (%06o,pos=%d,n=%d)\n", x, p, n);
        return 0;
      }
    word18 mask = ~ (~0U << n);  // n low bits on
    mask <<= (unsigned) shift;  // shift 1s to proper position; result 0*1{n}0*
    // caller may provide val that is too big, e.g., a word with all bits
    // set to one, so we mask val
    word18 result = (x & ~ mask) | ((val & MASKBITS (n)) << (18 - p - n));
    return result;
  }

static inline void putbits18 (word18 * x, uint p, uint n, word18 val)
  {
    int shift = 18 - (int) p - (int) n;
    if (shift < 0 || shift > 17)
      {
        sim_printf ("putbits18: bad args (%06o,pos=%d,n=%d)\n", * x, p, n);
        return;
      }
    word18 mask = ~ (~0U << n);  // n low bits on
    mask <<= (unsigned) shift;  // shift 1s to proper position; result 0*1{n}0*
    // caller may provide val that is too big, e.g., a word with all bits
    // set to one, so we mask val
    * x = (* x & ~mask) | ((val & MASKBITS (n)) << (18 - p - n));
    return;
  }

static inline word36 getbits36(word36 x, uint i, uint n) {
    // bit 35 is right end, bit zero is 36th from the right
    int shift = 35-(int)i-(int)n+1;
    if (shift < 0 || shift > 35) {
        sim_printf ("getbits36: bad args (%012lo,i=%d,n=%d)\n", x, i, n);
        return 0;
    } else
        return (x >> (unsigned) shift) & ~ (~0U << n);
}

static inline word36 setbits36(word36 x, uint p, uint n, word36 val)
{
    int shift = 36 - (int) p - (int) n;
    if (shift < 0 || shift > 35) {
        sim_printf ("setbits36: bad args (%012lo,pos=%d,n=%d)\n", x, p, n);
        return 0;
    }
    word36 mask = ~ (~0U<<n);  // n low bits on
    mask <<= (unsigned) shift;  // shift 1s to proper position; result 0*1{n}0*
    // caller may provide val that is too big, e.g., a word with all bits
    // set to one, so we mask val
    word36 result = (x & ~ mask) | ((val&MASKBITS(n)) << (36 - p - n));
    return result;
}

static inline void putbits36 (word36 * x, uint p, uint n, word36 val)
  {
    int shift = 36 - (int) p - (int) n;
    if (shift < 0 || shift > 35)
      {
        sim_printf ("putbits36: bad args (%012lo,pos=%d,n=%d)\n", * x, p, n);
        return;
      }
    word36 mask = ~ (~0U << n);  // n low bits on
    mask <<= (unsigned) shift;  // shift 1s to proper position; result 0*1{n}0*
    // caller may provide val that is too big, e.g., a word with all bits
    // set to one, so we mask val
    * x = (* x & ~mask) | ((val & MASKBITS (n)) << (36 - p - n));
    return;
  }

// Requires a value, DEVn only, in "show", don't uppercase value
#define MTAB_unit_valr_nouc MTAB_XTD | MTAB_VUN | MTAB_VALR | MTAB_NC
// Requires a value, DEV only, not in "show", don't uppercase value
#define MTAB_dev_valr_nouc MTAB_XTD | MTAB_VDV | MTAB_NMO | MTAB_VALR | MTAB_NC

void bail (void);
