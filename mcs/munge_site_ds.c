// Convert
//  000000 000000024204 000000000000 000000000000 000000000000 
//  000004 123456789123 000000000000 000000000000 000000000000 
// to
//  000000 000000
//  000001 024204
//  000002 000000
//  000003 000000
//  000004 000000
//  000005 000000
//  000006 000000
//  000007 000000
//  000010 123456
//  000011 789123
//  ....

//  000004 123456789123 000000000000 000000000000 000000000000 
//  0      7           20           33           46

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static unsigned int M[65536];

static unsigned ext6 (char * p) {
  char buf[7];
  for (int i = 0; i < 6; i ++)
    buf[i] = p[i];
  buf[6] = 0;
  return (unsigned int) strtoul (buf, NULL, 8);
}

int main (int argc, char * argv []) {
  memset (M, 0, sizeof (M));
  char buffer[256];
  while (fgets (buffer, sizeof (buffer), stdin)) {
    unsigned int addr36 = ext6 (buffer + 0);
    unsigned int addr18 = addr36 * 2 - 2;
    if (addr36) {
      M[addr18 + 0] = ext6 (buffer +  7);
      M[addr18 + 1] = ext6 (buffer +  7 + 6);
    }
    M[addr18 + 2] = ext6 (buffer + 20);
    M[addr18 + 3] = ext6 (buffer + 20 + 6);
    M[addr18 + 4] = ext6 (buffer + 33);
    M[addr18 + 5] = ext6 (buffer + 33 + 6);
    M[addr18 + 6] = ext6 (buffer + 46);
    M[addr18 + 7] = ext6 (buffer + 46 + 6);
  }


  printf ("WIDTH=18;\n");
  printf ("DPETH=32768;\n");
  printf ("ADDRESS_RADIX=OCT;\n");
  printf ("DATA_RADIX=OCT;\n");
  printf ("CONTENT BEGIN\n");
  for (unsigned int addr18 = 0; addr18 < 65536; addr18 ++) {
    if (M[addr18])
      printf ("   %06o : %06o;\n", addr18, M[addr18]);
  }
  printf ("END;\n");
}
