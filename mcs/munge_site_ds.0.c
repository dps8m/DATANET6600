// Convert
//  000000 000000024204 000000000000 000000000000 000000000000 
//  000004 123456789123 000000000000 000000000000 000000000000 
// to
//  000000 000000
//  000001 024204
//  000002 000000
//  000003 000000
//  000004 000000
//  000005 000000
//  000006 000000
//  000007 000000
//  000010 123456
//  000011 789123
//  ....

//  000004 123456789123 000000000000 000000000000 000000000000 
//  0      7           20           33           46

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static void ext6 (char * p, char * q) {
  for (int i = 0; i < 6; i ++)
    *p++ = *q++;
  *p = 0;
}

int main (int argc, char * argv []) {
  printf ("WIDTH=18;\n");
  printf ("DPETH=32768;\n");
  printf ("ADDRESS_RADIX=OCT;\n");
  printf ("DATA_RADIX=OCT;\n");
  printf ("CONTENT BEGIN\n");
  char buffer[256];
  while (fgets (buffer, sizeof (buffer), stdin)) {
    char addr[7], w0[7], w1[7], w2[7], w3[7], w4[7], w5[7], w6[7], w7[7];
    ext6 (addr, buffer + 0);
    ext6 (w0, buffer +  7);
    ext6 (w1, buffer +  7 + 6);
    ext6 (w2, buffer + 20);
    ext6 (w3, buffer + 20 + 6);
    ext6 (w4, buffer + 33);
    ext6 (w5, buffer + 33 + 6);
    ext6 (w6, buffer + 46);
    ext6 (w7, buffer + 46+ 6);
    unsigned int addr36 = (unsigned int) strtoul (addr, NULL, 8);
    unsigned int addr18 = addr36 * 2 - 2;
    if (addr36) {
      if (strcmp (w0, "000000")) printf ("   %06o : %s;\n", addr18 + 0, w0);
      if (strcmp (w0, "000000")) printf ("   %06o : %s;\n", addr18 + 1, w1);
    }
    if (strcmp (w0, "000000")) printf ("   %06o : %s;\n", addr18 + 2, w2);
    if (strcmp (w0, "000000")) printf ("   %06o : %s;\n", addr18 + 3, w3);
    if (strcmp (w0, "000000")) printf ("   %06o : %s;\n", addr18 + 4, w4);
    if (strcmp (w0, "000000")) printf ("   %06o : %s;\n", addr18 + 5, w5);
    if (strcmp (w0, "000000")) printf ("   %06o : %s;\n", addr18 + 6, w6);
    if (strcmp (w0, "000000")) printf ("   %06o : %s;\n", addr18 + 7, w7);
  }
  printf ("END;\n");
}
