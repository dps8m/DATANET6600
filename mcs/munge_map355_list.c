// Convert
//
// ...
// ^M                   00000              2        org     0
// ^M       00000  0 71 005        5       3        tra     l5-*
// ^M       00001  000000                  4        oct     0
// ^M       00002  000001                  5        oct     1
// ^M       00003  000777                  6        oct     777
// ^M       00004  777777                  7        oct     777777
// ^M       00005  673  001                8 l5     ila     1
// ^M       00006  0 64 002       10       9        tnz     l10-*
// ^M       00007  000001                 10        oct     1     // diag 1
// ^M       00010  673  000               11 l10    ila     0
// ^M       00011  0 74 002       13      12        tze     l13-*
// ^M       00012  000002                 13        oct     2
// ^M       00013  673  007               14 l13    ila     7
// ^M  end of binary card 00000001
// ^M                   00000             15        end
// ^M  end of binary card 00000002
// ^M    14 is the next available location.
//
// to .mif file

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>

static unsigned int M[65536];

int main (int argc, char * argv []) {
  memset (M, 0, sizeof (M));
  char buffer[256];
  char * p;
  unsigned int expect = 0;
  unsigned int last = 0;
  int n;
  char address[6];
  unsigned long addr;
  unsigned int orgAddr, lineNumber;

  while (fgets (buffer, sizeof (buffer), stdin)) {
    //if (strcmp (buffer + 8, "is the next available location.") == 0) {
      //printf ("break\n");
      //break;
    //}
    // Is the line "ooo is the next available locaton."?
    if (p = strstr (buffer, "is the next available location.")) {
      // Yes; get the location
      n = sscanf (buffer + 1 /* skip leading ^M */, "%o is the next", & last);
      if (n != 1) {
        fprintf (stderr, "Failed to parse 'last'\n");
      }
      break;
    }

    // Is it an org?
    char orgstr[5];
    if (sscanf (buffer + 1, "%o %u %4s ", & orgAddr, & lineNumber, & orgstr) == 3 && strcmp (orgstr, "org") == 0) {
      expect = orgAddr;
      continue;
    }

    // Does the line have an address field?
    strncpy (address, buffer + 8, 5);
    address[5] = 0;
    if (strspn (address, "01234567") != 5)
      continue;
    addr = strtoul (address, NULL, 8);

    // Does the line have a data field?
    char datastr [9];
    strncpy (datastr, buffer + 15, 8);
    datastr[8] = 0;
    if (strspn (datastr, " 01234567") != 8)
      continue;
    char * s = datastr;
    char * d = datastr;
    do while (isspace (*s)) s++; while(*d++ = *s++);
    unsigned long data = strtoul (datastr, NULL, 8);

    // Error checking....
    if (addr != expect) {
      fprintf (stderr, "Address is %05o; expected %05o\n", addr, expect);
    }
    expect = addr + 1;

    // Save the data
    M[addr] = data;
  } // while (fgets ())

  if (expect != last) {
    fprintf (stderr, "Last is %05o; expected %05o\n", last, expect);
  }

  // Generate mif file.
  printf ("WIDTH=18;\n");
  printf ("DPETH=32768;\n");
  printf ("ADDRESS_RADIX=OCT;\n");
  printf ("DATA_RADIX=BIN;\n");
  printf ("CONTENT BEGIN\n");
  for (unsigned int addr18 = 0; addr18 < 65536; addr18 ++) {
    unsigned int w = M[addr18];
    if (w) {
      printf ("   %05o : ", addr18);
#define bit(x) printf ("%d", (w & (x)) ? 1 : 0);
      bit (0400000);
      bit (0200000);
      bit (0100000);
      bit (0040000);
      bit (0020000);
      bit (0010000);
      bit (0004000);
      bit (0002000);
      bit (0001000);
      bit (0000400);
      bit (0000200);
      bit (0000100);
      bit (0000040);
      bit (0000020);
      bit (0000010);
      bit (0000004);
      bit (0000002);
      bit (0000001);
#undef bit
      printf (";\n");
    }
  }
  printf ("END;\n");
}
