//
//  caf.c
//  dn355
//
//  Created by Harry Reed on 12/31/15.
//
// Calculates word & character addresses for use bt dn355
//

#include "dn355.h"
#include "cpu.h"
#include "caf.h"
#include "pager.h"
#include "hdbg.h"

//
// Notation
//
//  DD01 3-14:
//   Memory address:  CY  (0-2)  WY (3-17)
//   Indirect address word:  I (0) T (1-2) Y (3-17)
//   Instruction word: S (9) D (10-17)  [We will elide the S bit and use D (9-17)] 
//   Instruction word w/character address:  CD (9-11)  S (12) WD (13-17)
//   Index register:  CX (0-2)  WX (3-17)


// D -  9 bit D field from the instruction, sign extended to 15 bits.
//      It may be a 9 bit displacement or a 3 bit character address/6 bit
//      displacement.
// indxA - 15 bit address from the base register
// indxCA - 3 bit character address from the base register

// "The effectice address in the cahacter addressing mode is formed by
// treating the D field of the instruction as having a 3-bit character (fractional)
// part and a 6-bit word part.  The 6-bit word part is expanded into a 15-bit
// signed number by extending bit 12 of the instruction word nine places to the
// left."

// some char position defines
#define W_W     0
#define W_DW    1   // double word
#define B_0     2
#define B_1     3
#define C_0     4
#define C_1     5
#define C_2     6
#define U_7     7   // canonical illegal access




#define CA_W    0
#define CA_DW   1
#define CA_02   2
#define CA_12   3
#define CA_03   4
#define CA_13   5
#define CA_23   6
#define CA_ILL  7

// Add CD, WD to CX, WX

word18 cafAddrAdd (word3 CD, word15 WD, word3 CX, word15 WX){

  HDBGNote (__func__, "entry CD %o WD %05o CX %o WX %05o", CD, WD, CX, WX);

  word3 CY; // operand character address
  word15 WY; // operand word address

  // DD01, Pg 3-20, Figure 3-3
  //
  //                      CD
  //                   -----------------------------------------------------------------------------
  //  CX               0  word   1  d.w.   2  0/2    3  1/2    4  0/3    5  1/3    6  2/3    7  ill.
  //  -----------
  //  0 word           0  word   7  ill.   7  ill.   7  ill.   7  ill.   7  ill.   7  ill.   7  ill.
  //  1 double word    7  ill.   7  ill.   7  ill.   7  ill.   7  ill.   7  ill.   7  ill.   7  ill.
  //  2 0/2            7  ill.   7  ill.   2  0/2    3  1/2    7  ill.   7  ill.   7  ill.   7  ill.
  //  3 1/2            7  ill.   7  ill.   3  1/2    2+ 0/2    7  ill.   7  ill.   7  ill.   7  ill.
  //  4 0/3            7  ill.   7  ill.   7  ill.   7  ill.   4  0/3    5  1/3    6  2/3    7  ill.
  //  5 1/3            7  ill.   7  ill.   7  ill.   7  ill.   5  1/3    6  2/3    4+ 0/3    7  ill.
  //  6 2/3            7  ill.   7  ill.   7  ill.   7  ill.   6  2/3    7+ 0/3    5+ 1/3    7  ill.
  //  7 Illegal        7  ill.   7  ill.   7  ill.   7  ill.   7  ill.   7  ill.   7  ill.   7  ill.

  WY = (WD + WX) & BITS15;

  switch (CX) {

    case CA_W: { // word
      if (CD == CA_W) { // word + word
        CY = CA_W; // word + word ==> word
      } else {
        CY = CA_ILL; // Illegal
      }
      break;
    }         

    case CA_DW: { // double word
      CY = CA_ILL; // Illegal
      break;
    }

    case CA_02: { // 0/2
      if (CD == CA_02) { // 0/2 + 0/2
        CY = CA_02; // 0/2
        break;
      }
      if (CD == CA_12) { // 0/2 + 1/2
        CY = CA_12; // 1/2
        break;
      }
      CY = CA_ILL;
      break;
    }

    case CA_12: { // 1/2
      if (CD == CA_02) { // 1/2 + 0/2
        CY = CA_12; // 1/2
        break;
      }
      if (CD == CA_12) { // 1/2 + 1/2
        CY = CA_02; // 0/2 +
        WY ++;
        break;
      }
      CY = CA_ILL;
      break;
    }

    case CA_03: { // 0/3
      if (CD == CA_03) { // 0/3 + 0/3
        CY = CA_03; // 0/3
        break;
      }
      if (CD == CA_13) { // 0/3 + 1/3
        CY = CA_13; // 1/3
        break;
      }
      if (CD == CA_23) { // 0/3 + 2/3
        CY = CA_23; // 2/3
        break;
      }
      CY = CA_ILL;
      break;
    }

    case CA_13: { // 1/3
      if (CD == CA_03) { // 1/3 + 0/3
        CY = CA_13; // 1/3
        break;
      }
      if (CD == CA_13) { // 1/3 + 1/3
        CY = CA_23; // 2/3
        break;
      }
      if (CD == CA_23) { // 1/3 + 2/3
        CY = CA_03; // 0/3 +
        WY ++;
        break;
      }
      CY = CA_ILL;
      break;
    }

    case CA_23: { // 2/3
      if (CD == CA_03) { // 2/3 + 0/3
        CY = CA_23; // 2/3
        break;
      }
      if (CD == CA_13) { // 2/3 + 1/3
        CY = CA_03; // 0/3 +
        WY ++;
        break;
      }
      if (CD == CA_23) { // 2/3 + 2/3
        CY = CA_13; // 1/3 +
        WY ++;
        break;
      }
      CY = CA_ILL;
      break;
    }
    case CA_ILL: {
      CY = CA_ILL;
      break;
    }
  } // switch CX
  HDBGNote (__func__, "CX %o WY %05o", CX, WY);
  word18 Y;
  Y = PACKCW (CY, WY);
  HDBGNote (__func__, "character address Y %06o", Y);
  return Y;
}


// The T and D fields from the instruction
static word18 cafDisplace (word2 T, word9 D) {

  word3 CX;  // The character address from the base register
  word15 WX; // The word address from the base register


  switch (T) {

    case 0: { // IC is base register
      CX = 0; // IC is always word address
      WX = cpu.rIC;
      break;
    }

    case 1: { // X1 is base register
      CX = UNPACKC (cpu.rX1);
      WX = UNPACKW (cpu.rX1);
      break;
    }

    case 2: { // X2 is base register
      CX = UNPACKC (cpu.rX2);
      WX = UNPACKW (cpu.rX2);
      break;
    }

    case 3: { // X3 is base register
      CX = UNPACKC (cpu.rX3);
      WX = UNPACKW (cpu.rX3);
      break;
    }
  } // switch T

  word3 CY; // Operand character address
  word15 WY; // Operand word address
  word18 Y; // Operand address

  // Are we doing word addressing?
  if (CX == 0) {
    // Yes; word addressing
    WY = (WX + SIGNEXT9_15 (D)) & BITS15;
    CY = W_W;
    Y = PACKCW (CY, WY);
    return Y;
  }

  // Character addressing

  word3 CD;  // Operand character address
  word15 WD;  // Operand word address

  // The displacement field is a 3 bit character address and
  // a 6 bit displacement
  CD = (D >> 6) & BITS3;
  WD = SIGNEXT6_15 (D & BITS6);

  // Add CD, WD to CX, WX
  Y = cafAddrAdd (CD, WD, CX, WX);
  return Y;
}



static word18 cafIndirect (word2 IT, word15 ID) {

  word3 CX;  // The character address from the base register
  word15 WX; // The word address from the base register

  HDBGNote (__func__, "entry IT %o ID %05o", IT, ID);

  switch (IT) {

    case 0: { // IC is base register
      CX = W_W; // IC is always word address
      WX = 0;
      break;
    }

    case 1: { // X1 is base register
      CX = UNPACKC (cpu.rX1);
      WX = UNPACKW (cpu.rX1);
      break;
    }

    case 2: { // X2 is base register
      CX = UNPACKC (cpu.rX2);
      WX = UNPACKW (cpu.rX2);
      break;
    }

    case 3: { // X3 is base register
      CX = UNPACKC (cpu.rX3);
      WX = UNPACKW (cpu.rX3);
      break;
    }
  } // switch IT

  HDBGNote (__func__, "entry CX %o WX %05o", CX, WX);

  word3 CY; // Operand character address
  word15 WY; // Operand word address
  word18 Y; // Operand address

  // Are we doing word addressing?
  if (CX == 0) {
    // Yes; word addressing
    WY = (WX + ID) & BITS15;
    CY = W_W;
    Y = PACKCW (CY, WY);
    HDBGNote (__func__, "word addressing returns Y %06o", Y);
    return Y;
  }

  // Character addressing

  word3 CD;  // Operand character address
  word15 WD;  // Operand word address

  // No displacement field in indirect 
  CD = W_W;
  WD = ID;

  // Add CD, WD to CX, WX
  Y = cafAddrAdd (CD, WD, CX, WX);
  return Y;
}

word18 doCAF (void) {
  // The basic method of forming an effective address (¥**) consists of adding
  // the 9-bit instruction displacement (D) field to a selected base register (X1,
  // X2, X3, or IC). The displacement field is treated as a 9-bit twos complement
  // number to allow the effective address to be greater than, or less than, the base
  // address.

  // [Believed to be the case:]
  // IF the base register is an index register, AND the index register has a non-zero
  // character address, then the displacement is a 3 bit character address and a 6 bit
  // signed displacement.

  // When the instruction specifies indirect addressing (I bit=1), the effective
  // address is calculated as above to form the address for an indirect word with the 
  // format:
  //
  //     0   1   2   3                    17
  //   +-------------------------------------+
  //   | I |   T   |      Y                  |
  //   +-------------------------------------+
  //

    
  // "The displacement field is treated as a 9-bit twos complement
  // number to allow the effective address to be greater than, or less than, the base
  // address."

  HDBGNote (__func__, "start cpu.D %05o cpu.T %o", cpu.D, cpu.T);

  // The basic method of forming an effective address (Y**) consists of adding
  // the 9-bit instruction displacement (D) field to a selected base register (X1,
  // X2, X3, or IC).

  word18 Y = cafDisplace (cpu.T, cpu.D);
  HDBGNote (__func__, "cafDisplace Y %06o", Y);

  // When the instruction specifies indirect addressing (I bit=1), the effective
  // address is calculated as above to form the address for an indirect word with the 
  // format:
  //
  //     0   1   2   3                    17
  //   +-------------------------------------+
  //   | I |   T   |      Y                  |
  //   +-------------------------------------+
  //

  bool indirect = !!cpu.I;

  HDBGNote (__func__, "indirect %o", indirect);

  // There is no limitiation on the number of indirect levels at modification.
  while (indirect) {

    // The Y field of the indirect woed is an address field, and no displacemt
    // has to be added fo form the effective address.  The indirect bit is this word
    // specifies whether or not continued indirect addressing is to be performed. The
    // T field specifies which of the three index registers is to be used for further
    // indirect modification. If the T field in the indirect word is 00, no
    // modication is performed and the Y field becomes the effective address.

    word18 ind = fromMemory (Y, "doCaf ind fetch");
    HDBGNote (__func__, "ind %06o", ind);
    indirect = !! (ind & 0400000);
    HDBGNote (__func__, "indirect now %o", indirect);

    // "The character addressing mechanism may also be used during indirect
    // addressing. If the T field of the  indirect word references an index register
    // the C field of which is non-zero, the 15-bit address field of the indirect word
    // is broken into a 3-bit C field and  a 12-bit signed word field, as shown in
    // Figure 3-5. The effective address    is then calculated using the same fractional
    // address rules as in basic level addressing.

    word2 IT = (word2) getbits18 (ind, 1, 2);
    word15 IY = (word15) getbits18 (ind, 3, 15);
    HDBGNote (__func__, "ind IT %o IY %05o", IT, IY);
    Y = cafIndirect (IT, IY);
  } // while indirect

  HDBGNote (__func__, "returning Y %06o", Y);
  return Y;
}


// Get the character pointed to by an ICW at 'addr'; advance the ICW to the next
// character

word9 getICWChar (word15 addr) {
  word18 dataICWEven, dataICWOdd;
  dataICWEven = fromMemory (addr, __func__);
  dataICWOdd = fromMemory ((addr + 1) & BITS15, __func__);

  word3 IC = getbits18 (dataICWEven, 0, 3);
  word15 IW = getbits18 (dataICWEven, 3, 15);
  word1 E = getbits18 (dataICWOdd, 5, 1);
  word12 TALLY = getbits18 (dataICWOdd, 6, 12);

  word18 ch;
  uint extraTally = 0;

  switch (IC) {

    case CA_02: { // 0/2
      ch = fromMemory (dataICWEven, __func__);
      // Bunp charaddr
      putbits18 (& dataICWEven, 0, 3, CA_12);
      break;
    }

    case CA_12: { // 1/2
      ch = fromMemory (dataICWEven, __func__);
      // Reset charaddr
      putbits18 (& dataICWEven, 0, 3, CA_02);
      // Bump addr
      putbits18 (& dataICWEven, 3, 15, (IW + 1) & BITS15);
      break;
    }

    // The printer data ICW is in 6 bit even though the data is 9 bit.
    // For 0/3, return 0/2 and set IC to 1/3, decrement tally by 1
    // For 1/3, return 1/2 and set IC to 0/3, increment addr, decrement tally by 2
    case CA_03: { // 0/3
      word18 IW02 = (((word18) CA_02) << 15) | IW;
      ch = fromMemory (IW02, __func__);
      // Bunp charaddr
      putbits18 (& dataICWEven, 0, 3, CA_13);
      break;
    }

    case CA_13: { // 1/3
      word18 IW12 = (((word18) CA_12) << 15) | IW;
      ch = fromMemory (IW12, __func__);
      // Bunp charaddr
      putbits18 (& dataICWEven, 0, 3, CA_03);
      // Bump addr
      putbits18 (& dataICWEven, 3, 15, (IW + 1) & BITS15);
      extraTally = 1;
      break;
    }

    default: {
      sim_printf ("%s can't handle IC %o\n", __func__, IC);
      ch = 0;
      break;
    }
  } // switch IC

  
  if (! E) {
    if (TALLY <= 0 + extraTally) {
      putbits18 (& dataICWOdd, 5, 1, 1); // set exhaust
      putbits18 (& dataICWOdd, 6, 12, 0); // decrement tally
    } else {
      putbits18 (& dataICWOdd, 6, 12, TALLY - 1 - extraTally); // decrement tally
    }
  }

  toMemory (dataICWEven, addr, 0, 0, __func__);
  toMemory (dataICWOdd, (addr + 1) & BITS15, 0, 0, __func__);

  return ch;
}
