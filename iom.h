// IOM Faults
//    0 -  7  MBZ
//    8 - 10  Data Command
//   11 - 13  Interrupt Command
//   14 - 17  Fault Type
//
//    Data Command
//      000 None
//      001 Load
//      010 Store
//      011 ADD
//      100 Subtract
//      101 AND
//      110 OR
//      111 Fault
//
//   Interrupt command
//      000 None
//      001 Uncondtional
//      010 Conditional or TRO (Tally Run Out)
//      011 Conditional or PTRO (Pre-Tally Run Out)
//      100 Conditional or Data Negative
//      101 Conditional or Zero
//      110 Conditional or Overflow
//      111 Fault
//
//   Fault Type
//     0000 None
//     0100 Program Fault
//     1000 Memory Parity Error
//     1100 Illegal Command to IOM
//     1010 Adder/Bus Parity
//     1001 Indirect Channel Detected Parity
//     1101 Direct Channel Detected Parity
//     1111 IOM Bus Priority Break

// The following code combinations cause an Illegal Channel Request Fault
//
//   Data  Interrupt
//    7     X
//    X     7
//    0     0
//    0     2
//    0     3
//    0     4
//    0     5
//    0     6


// Default h/w channel assignments

//  ? D0Ainterface (0-17)
//  ? HSLA (6-10)
//  ? LSLA (11-16)
//  ? PSA (disk) (3,5)
//  ? CMA (FNP<->FNP) (0-77)
//  0 CCA Control Console
//  1 Card reader
//  2 Line printer


#define CONSOLE_CHANNEL_NO     000

#define PRINTER_CHANNEL_NO     002

#define COUPLER_CHANNEL_NO     004
// test_fnp seems to think it is channel 3?
//#define COUPLER_CHANNEL_NO     003

#define HSLA1_CHANNEL_NO       006
#define HSLA2_CHANNEL_NO       007
#define HSLA3_CHANNEL_NO       010
#define LSLA1_CHANNEL_NO       011
#define LSLA2_CHANNEL_NO       012
#define LSLA3_CHANNEL_NO       013
#define LSLA4_CHANNEL_NO       014
#define LSLA5_CHANNEL_NO       015
#define LSLA6_CHANNEL_NO       016

#define TIMER_CHANNEL_NO       077
#define DATA_SWITCH_CHANNEL_NO 077

#define MASK_BIT  23

// 77 timer and switch channel


typedef struct {
  enum iomState_e { iomIdle, iomConnect } iomState;
  word14 ciocW;
  word6 chan;
  //word18 status;
  word18 channelStatus[N_IOM_CHANNELS];
  bool masked[N_IOM_CHANNELS];
  // debug
  word15 IC;
} iom_t;
extern iom_t iom;

typedef enum { iomProceed, iomTerminate, iomPending } iomAction;
void iomCIOC (void);
//void setIOMStatus (void);
void setStatusChar (word15 mbx, word9 status);
void setStatus (word15 mbox, word18 statusEven, word18 statusOdd);
void iomInit (void);
void iomRun (void);
